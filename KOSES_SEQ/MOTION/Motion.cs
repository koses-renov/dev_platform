﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
//using DoCSP.MOTION.Attach;
using System;
using System.Collections.Generic;
using System.Threading;
using KOSES_SEQ.OP;
using System.Diagnostics;

namespace KOSES_SEQ.MOTION
{
    ///// <summary>
    ///// 장비 구동부 기본 동작 함수 모음
    ///// 아래 기능을 2개 이상 모아 복합적인 동작을 구현해야 하는 경우 Manual 항목에 추가한다.
    ///// </summary>
    //public class Motion : MotionController
    //{
    //    public Motion()
    //    {
    //        SetMotionController();

    //        motionOpen = MotionControllerOpen();

    //        //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
    //        //{
    //        //    Motion_Attach a = new Motion_Attach(this, ai);
    //        //    attach.Add(a);
    //        //}
    //        //foreach (Def.DispenserIdx ai in Enum.GetValues(typeof(Def.DispenserIdx)))
    //        //{
    //        //    Motion_Dispensing d = new Motion_Dispensing(this, ai);
    //        //    dispensing.Add(d);
    //        //}
    //        //lineLaser = new Motion_Dispensing_Laser(this);
    //        //frame = new Motion_Frame(this);
    //        //foreach (Def.InspectionIdx ii in Enum.GetValues(typeof(Def.InspectionIdx)))
    //        //{
    //        //    Motion_Inspection i = new Motion_Inspection(this, ii);
    //        //    inspection.Add(i);
    //        //}
    //        //lens = new Motion_Lens(this);
    //        //magLoader = new Motion_MagLoader(this);
    //        //magUnloader = new Motion_MagUnloader(this);
    //        //foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
    //        //{
    //        //    Motion_Shuttle s = new Motion_Shuttle(this, si);
    //        //    shuttle.Add(s);
    //        //}
    //        //foreach (Def.ShuttleIdx mi in Enum.GetValues(typeof(Def.ShuttleIdx)))
    //        //{
    //        //    Motion_Mask m = new Motion_Mask(this, mi);
    //        //    mask.Add(m);
    //        //}
    //        //ppLoader = new Motion_PPLoader(this);
    //        //ppUnloader = new Motion_PPUnloader(this);
    //        //rail = new Motion_Rail(this);
    //        //autoCal = new Motion_AutoCal(this);
    //        //reject = new Motion_Reject(this);
    //    }


    //    //public List<Motion_Attach> attach = new List<Motion_Attach>();
    //    //#region Attach
    //    //public Motion_Attach Attach_1
    //    //{
    //    //    get
    //    //    {
    //    //        return attach[(int)Def.AttachIdx.A1];
    //    //    }
    //    //}
    //    //public Motion_Attach Attach_2
    //    //{
    //    //    get
    //    //    {
    //    //        return attach[(int)Def.AttachIdx.A2];
    //    //    }
    //    //}
    //    //public Motion_Attach Attach_3
    //    //{
    //    //    get
    //    //    {
    //    //        return attach[(int)Def.AttachIdx.A3];
    //    //    }
    //    //}
    //    //#endregion
    //    //public List<Motion_Dispensing> dispensing = new List<Motion_Dispensing>();
    //    //#region Dispensing
    //    //public Motion_Dispensing LeftDispensing
    //    //{
    //    //    get
    //    //    {
    //    //        return dispensing[(int)Def.DispenserIdx.Left];
    //    //    }
    //    //}
    //    //public Motion_Dispensing RightDispensing
    //    //{
    //    //    get
    //    //    {
    //    //        return dispensing[(int)Def.DispenserIdx.Right];
    //    //    }
    //    //}
    //    //#endregion
    //    //public Motion_Dispensing_Laser lineLaser = null;
    //    //public Motion_Frame frame = null;
    //    //public List<Motion_Inspection> inspection = new List<Motion_Inspection>();
    //    //public Motion_Lens lens = null;
    //    //public Motion_MagLoader magLoader = null;
    //    //public Motion_MagUnloader magUnloader = null;
    //    //public List<Motion_Shuttle> shuttle = new List<Motion_Shuttle>();
    //    //#region Shuttle
    //    //public Motion_Shuttle Shuttle_1
    //    //{
    //    //    get
    //    //    {
    //    //        return shuttle[(int)Def.ShuttleIdx.S1];
    //    //    }
    //    //}
    //    //public Motion_Shuttle Shuttle_2
    //    //{
    //    //    get
    //    //    {
    //    //        return shuttle[(int)Def.ShuttleIdx.S2];
    //    //    }
    //    //}
    //    //public Motion_Shuttle Shuttle_3
    //    //{
    //    //    get
    //    //    {
    //    //        return shuttle[(int)Def.ShuttleIdx.S3];
    //    //    }
    //    //}
    //    //public Motion_Shuttle Shuttle_4
    //    //{
    //    //    get
    //    //    {
    //    //        return shuttle[(int)Def.ShuttleIdx.S4];
    //    //    }
    //    //}
    //    //public Motion_Shuttle Shuttle_5
    //    //{
    //    //    get
    //    //    {
    //    //        return shuttle[(int)Def.ShuttleIdx.S5];
    //    //    }
    //    //}
    //    //public Motion_Shuttle Shuttle_6
    //    //{
    //    //    get
    //    //    {
    //    //        return shuttle[(int)Def.ShuttleIdx.S6];
    //    //    }
    //    //}
    //    //#endregion
    //    //public List<Motion_Mask> mask = new List<Motion_Mask>();
    //    //#region Mask
    //    //public Motion_Mask Mask_1
    //    //{
    //    //    get
    //    //    {
    //    //        return mask[(int)Def.ShuttleIdx.S1];
    //    //    }
    //    //}
    //    //public Motion_Mask Mask_2
    //    //{
    //    //    get
    //    //    {
    //    //        return mask[(int)Def.ShuttleIdx.S2];
    //    //    }
    //    //}
    //    //public Motion_Mask Mask_3
    //    //{
    //    //    get
    //    //    {
    //    //        return mask[(int)Def.ShuttleIdx.S3];
    //    //    }
    //    //}
    //    //public Motion_Mask Mask_4
    //    //{
    //    //    get
    //    //    {
    //    //        return mask[(int)Def.ShuttleIdx.S4];
    //    //    }
    //    //}
    //    //public Motion_Mask Mask_5
    //    //{
    //    //    get
    //    //    {
    //    //        return mask[(int)Def.ShuttleIdx.S5];
    //    //    }
    //    //}
    //    //public Motion_Mask Mask_6
    //    //{
    //    //    get
    //    //    {
    //    //        return mask[(int)Def.ShuttleIdx.S6];
    //    //    }
    //    //}
    //    //#endregion
    //    //public Motion_PPLoader ppLoader = null;
    //    //public Motion_PPUnloader ppUnloader = null;
    //    //public Motion_Rail rail = null;        
    //    //public Motion_AutoCal autoCal = null;
    //    //public Motion_Reject reject = null;

    //    bool motionOpen = false;      

    //    public bool IsOpen()
    //    {
    //        //  AJIN 계열은 컨트롤러가 연결되지 않은 환경에서 실행하는 경우 예외상황 발생
    //        //  함수를 try ~ catch 로 예외처리를 하면 프로그램 동작중 아래의 함수에서 CPU 점유율이 높아짐
    //        //   - UpdateInputStatus
    //        //   - UpdateOutputStatus
    //        //   - UpdateMotorStatus
    //        //  상기 함수에서 모션 컨트롤러 연결이 실패한 경우 바로 리턴 시켜 디버그시 CPU 점유율을 낮춘다.
    //        return motionOpen;
    //    }


    //    public bool IsMotionControllerOpen()
    //    {
    //        return motionOpen;
    //    }
    //}

    public class Motion_Wait
    {
        //WaitTimer_Tick waitTickTimer = new WaitTimer_Tick();
        Stopwatch sw = new Stopwatch();
        Def.Code code = Def.Code.OK;
        int motionWaitTimerOver = 0;

        public bool IsBreak
        {
            get
            {
                return this.isBreak();
            }
        }

        public Def.Code BreakCode
        {
            get
            {
                return this.code;
            }
        }

        public Motion_Wait(int waitTime, Def.Code code)
        {
            this.code = code;
            //waitTickTimer.Start(waitTime);
            motionWaitTimerOver = waitTime;
            sw.Start();
        }
        bool isBreak()
        {
            //  예외 상황이 발생하면 Break() 함수 밖의 
            //  while 을 빠져나가도록 true 를 반환한다.
            if (sw.ElapsedMilliseconds > motionWaitTimerOver)
                //if (waitTickTimer.IsTimeover())
            {
                return true;
            }
            if (SysOperator.GetUserStop())
            {
                this.code = Def.Code.USER_STOP;
                return true;
            }

            return false;
        }

        public void Delay(int ms = 1)
        {
            Thread.Sleep(ms);
        }

    }

}
