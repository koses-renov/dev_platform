﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.OP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class ucJog : UserControl
    {
        Def.Motor SelectedAxis = 0;

        public ucJog()
        {
            InitializeComponent();
            ChangeAxis(0);
        }

        public ucJog(Def.Motor axis)
        {
            InitializeComponent();
            ChangeAxis(SelectedAxis);
        }

        private void tlpJog_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
                tmrMotorStatus.Start();
            else
                tmrMotorStatus.Stop();
        }

        private void tmrMotorStatus_Tick(object sender, EventArgs e)
        {
            UpdateMotorStatus();
        }

        private void UpdateMotorStatus()
        {
            if (SysOperator.Instance.motion.IsOpen() == false)
            {
                return;
            }

            bool b;

            b = SysOperator.Instance.motion.IsServoOn(SelectedAxis);
            led_TeachingServo.On = b;

            b = SysOperator.Instance.motion.IsAlarm(SelectedAxis);
            led_TeachingAlarm.On = b;

            b = SysOperator.Instance.motion.GetInPositionStatus(SelectedAxis);
            led_TeachingInpos.On = b;

            b = SysOperator.Instance.motion.IsPlusSensor(SelectedAxis);
            led_TeachingCwLimit.On = b;

            b = SysOperator.Instance.motion.IsHomeSensor(SelectedAxis);
            led_TeachingHome.On = b;

            b = SysOperator.Instance.motion.IsMinusSensor(SelectedAxis);
            led_TeachingCcwLimit.On = b;

            string cmdPos = string.Format("{0:0.00000}", SysOperator.Instance.motion.GetCmdPos(SelectedAxis));
            string encPos = string.Format("{0:0.00000}", SysOperator.Instance.motion.GetEncPos(SelectedAxis));
            led_TeachingCommand.Text = cmdPos;
            led_TeachingEncoder.Text = encPos;

            cmdPos = null;
            encPos = null;
        }

        public void ChangeAxis(Def.Motor axis)
        {
            SelectedAxis = axis;
            lbl_TeachingAxisName.Text = SelectedAxis.ToString();
        }

        private void btn_TeachingZeroSet_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.SetPosZero(SelectedAxis);
        }

        private void btn_TeachingServoOn_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.ServoOn(SelectedAxis);
        }

        private void btn_TeachingServoOff_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.ServoOff(SelectedAxis);
        }

        private void btn_TeachingAlarmClear_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.ResetAlarm(SelectedAxis);
        }

        private void btn_TeachingHome_Click(object sender, EventArgs e)
        {
            //Door Open도 Check하고~~
            //Motor Inter Lock Check 해야 함...

            /////////////////////////////////////////////////////////////////////
            // 알람시 개별 홈 안되도록..
            ////////////////////////////////////////////////////////////////////
            if (SysOperator.Instance.motion.IsAlarm(SelectedAxis))
                return;

            SysOperator.Instance.motion.Home(SelectedAxis);
        }

        private void btn_TeachingJogCcw_MouseDown(object sender, MouseEventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(SelectedAxis, ref errMsg))
            {
                //Program.MessageBox($"Jog Minus", $"Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //Program.log.Action($"{logHeader} Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}");
                return;
            }

            SysOperator.Instance.motion.MoveJog(SelectedAxis, Def.MotorDir.MINUS);
        }

        private void btn_TeachingJogCcw_MouseUp(object sender, MouseEventArgs e)
        {
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }

        private void btn_TeachingJogCw_MouseDown(object sender, MouseEventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(SelectedAxis, ref errMsg))
            {
                //Program.MessageBox($"Jog Minus", $"Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //Program.log.Action($"{logHeader} Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}");
                return;
            }

            SysOperator.Instance.motion.MoveJog(SelectedAxis, Def.MotorDir.PLUS);
            //Program.log.Action($"{logHeader} Jog Minus");
        }

        private void btn_TeachingJogCw_MouseUp(object sender, MouseEventArgs e)
        {
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }

        private void btn_TeachingJogRelDot03_Click(object sender, EventArgs e)
        {
            Button obtn = sender as Button;

            string szTemp = "";
            txt_TeachingRelValue.Clear();

            szTemp = obtn.Text;
            txt_TeachingRelValue.AppendText(szTemp);
        }

        private void btn_TeachingJogRelCcw_Click(object sender, EventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            if (double.TryParse($"{txt_TeachingRelValue.Text}", out double getdata))
            {
                SysOperator.Instance.motion.MoveRel(SelectedAxis, Def.MotorDir.MINUS, getdata);
            }
        }

        private void btn_TeachingJogRelCw_Click(object sender, EventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            if (double.TryParse($"{txt_TeachingRelValue.Text}", out double getdata))
            {
                SysOperator.Instance.motion.MoveRel(SelectedAxis, Def.MotorDir.PLUS, getdata);
            }
        }

        private void btn_TeachingAbsGetValue_Click(object sender, EventArgs e)
        {
        }

        private void btn_TeachingJogAbs_Click(object sender, EventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(SelectedAxis, ref errMsg))
            {
                //SysOperator.MessageBox($"Abs Move", $"Fail To Move Abs (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //SysOperator.log.Action($"{logHeader} Fail To Move Abs (CheckCrashMotor) : {errMsg}");
                return;
            }

            if (double.TryParse($"{txt_TeachingAbsValue.Text}", out double getdata))
            {
                SysOperator.Instance.motion.MoveAbs(SelectedAxis, getdata);
            }
        }

        private void btn_TeachingMotorStop_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }
    }
}
