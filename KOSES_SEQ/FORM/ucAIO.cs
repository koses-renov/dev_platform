﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using KosesControl;

namespace KOSES_SEQ.OP
{
    public partial class ucAIO : UserControl
    {
        public enum IOType { INPUT, OUTPUT };
        /// <summary>
        /// 현재 I/O 채널
        /// </summary>
        public int CurChNo { get; set; }
        /// <summary>
        /// I/O 채널 최대 개수
        /// </summary>
        public int MaxChCnt { get; private set; } = 4;
        /// <summary>
        /// 채널당 IO 최대 개수
        /// </summary>
        public int MaxIOinCh { get; private set; } = 4;

        #region ----- Form 내부 변수 -----
        /// <summary>
        /// I/O 타입
        /// </summary>
        IOType IO_Type;

        bool IsSearch = false;
        object Interlock = new object();
        List<int> SearchIONoList = new List<int>();
        #endregion

        #region ----- Form Initialize -----
        public ucAIO(IOType io_type, int chNo, bool check = false)
        {
            InitializeComponent();
            this.IO_Type = io_type;

            this.Check.Visible = check;

            if (chNo < 0)
                this.CurChNo = 0;
            else
                this.CurChNo = chNo;

            ShowIO_List();
        }

        //페이지 로딩 느려보일때 쓰면 좋음
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion

        #region ----- Form Show Event -----
        private void ShowIO_List()
        {
            dispIoTitle();

            tmrIo.Enabled = false;

            if (this.IO_Type == IOType.INPUT)
            {
                this.dispIoInputGrid();
            }
            else
            {
                this.dispIoOutputGrid();
            }

            tmrIo.Enabled = true;
        }

        private void dispIoTitle()
        {
            labIoTitle.BackColor = Color.Transparent;
            labIoTitle.ForeColor = Color.White;

            if (this.IO_Type == IOType.INPUT)
                panIoTitle.BackColor = Color.SteelBlue;
            else if (this.IO_Type == IOType.OUTPUT)
                panIoTitle.BackColor = Color.DarkSalmon;

            labIoTitle.Text = string.Format("{0} (CH {1:00})", this.IO_Type, CurChNo);
        }

        private void dispIoInputGrid()
        {
            this.dgvInOut.RowCount = MaxIOinCh;
            foreach (DataGridViewRow row in this.dgvInOut.Rows)
            {
                int portNo = row.Index + (CurChNo * MaxIOinCh);
                row.Cells[0].Value = $"X{row.Index + (CurChNo * 100)} ({portNo})";
                row.Cells[1].Value = "Analog Input Name";
                row.Cells[2].Value = "OFF";
            }
        }

        private void dispIoOutputGrid()
        {
            this.dgvInOut.RowCount = MaxIOinCh;
            foreach (DataGridViewRow row in this.dgvInOut.Rows)
            {
                int portNo = row.Index + (CurChNo * MaxIOinCh);
                row.Cells[0].Value = $"Y{row.Index + (CurChNo * 100)} ({portNo})";
                row.Cells[1].Value = "Analog Output Name";
                row.Cells[2].Value = "OFF";
                dgvInOut.Columns[2].ReadOnly = false;
            }
        }
        #endregion

        #region ----- Form Update Timer -----
        private void tmrIo_Tick(object sender, EventArgs e)
        {
            if (this.Visible && this.Created)
            {
                if (this.IO_Type == IOType.OUTPUT)
                    UpdateDOutput();
                else
                    UpdateDInput();
            }
            else
                tmrIo.Stop();
        }
        private void IOView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
                tmrIo.Start();
        }

        private void UpdateDInput()
        {
            int chIndex = (CurChNo * MaxIOinCh);
            for (int i = 0; i < MaxChCnt; i++)
            {
                double value = SysOperator.Instance.motion.GetInputAnalog(chIndex + i);
                this.dgvInOut.Rows[i].Cells[2].Value = value.ToString("F2");
            }
        }

        private void UpdateDOutput()
        {
            int chIndex = (CurChNo * MaxIOinCh);
            for (int i = 0; i < MaxChCnt; i++)
            {
                double value = SysOperator.Instance.motion.GetInputAnalog(chIndex + i);
                this.dgvInOut.Rows[i].Cells[2].Value = value.ToString("F2");
            }
        }
        #endregion

        #region ----- Form Button & Grid Event -----
        private void btnIoChDown_Click(object sender, EventArgs e)
        {
            if (CurChNo > 0)
            {
                IsSearch = false;
                //this.btnSearch.Icon = Properties.Resources.outline_search_white_18dp;
                ChangeDownChannel();
            }
        }
        private void btnIoChUp_Click(object sender, EventArgs e)
        {
            if (CurChNo < MaxChCnt - 1)
            {
                IsSearch = false;
                //this.btnSearch.Icon = Properties.Resources.outline_search_white_18dp;
                ChangeUpChannel();
            }
        }
        public void ChangeUpChannel()
        {
            this.CurChNo++;
            ShowIO_List();
        }
        public void ChangeDownChannel()
        {
            this.CurChNo--;
            ShowIO_List();
        }

        /// <summary>
        /// I/O 검색 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if(!IsSearch)
            //    {
            //        if (this.tbSearchNo.Text == this.tbSearchNo.HintText) this.tbSearchNo.Text = "";
            //        if (this.tbSearchName.Text == this.tbSearchName.HintText) this.tbSearchName.Text = "";

            //        List<(int, string)> ioList;
            //        if (this.IO_Type == IOEnum.OUTPUT)
            //            Program.HandlerSvc.DOutList(-1, out ioList);
            //        else
            //            Program.HandlerSvc.DInList(-1, out ioList);

            //        ioList = ioList.FindAll(list => list.Item1.ToString().IndexOf(tbSearchNo.Text, StringComparison.OrdinalIgnoreCase) >= 0).
            //                        FindAll(list => list.Item2.IndexOf(tbSearchName.Text, StringComparison.OrdinalIgnoreCase) >= 0);


            //        this.dgvInOut.Rows.Clear();
            //        this.SearchIONoList.Clear();
            //        this.dgvInOut.RowCount = ioList.Count;
            //        foreach (DataGridViewRow row in this.dgvInOut.Rows)
            //        {
            //            row.Cells[0].Value = ioList[row.Index].Item1;
            //            row.Cells[1].Value = ioList[row.Index].Item2; //"Y000 " + 
            //            row.Cells[2].Value = "OFF";     // dOut[i].Item2;
            //            SearchIONoList.Add(ioList[row.Index].Item1);
            //        }
            //        this.IsSearch = true;

            //        this.btnSearch.Icon = Properties.Resources.outline_undo_white_18dp;
            //    }
            //    else
            //    {
            //        if (this.IO_Type == IOEnum.OUTPUT)
            //            dispIoOutputGrid();
            //        else
            //            dispIoInputGrid();

            //        this.IsSearch = false;
            //        this.btnSearch.Icon = Properties.Resources.outline_search_white_18dp;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageView.ShowMessage($"I can't find it.{ex.Message}", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        /// <summary>
        /// dgvInOut Cell값 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInOut_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (this.IO_Type == IOType.OUTPUT && e.ColumnIndex == 2)
            {
                int ioNo = CurChNo * MaxIOinCh + e.RowIndex;
                SysOperator.Instance.motion.SetOutputAnalog(ioNo, Convert.ToDouble(dgvInOut[e.ColumnIndex, e.RowIndex].Value));
            }
        }

        private void dgvInOut_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.ColumnIndex == Check.Index && e.RowIndex != -1)
            //{
            //    if (this.IO_Type == IOEnum.INPUT)
            //        HandlerSystem.InputIONameList[CurChNo].Checks[e.RowIndex] = !(bool)dgvInOut.Rows[e.RowIndex].Cells[3].Value;
            //    else if (this.IO_Type == IOEnum.OUTPUT)
            //        HandlerSystem.OutputIONameList[CurChNo].Checks[e.RowIndex] = !(bool)dgvInOut.Rows[e.RowIndex].Cells[3].Value;
            //}
        }

        private void dgvInOut_SelectionChanged(object sender, EventArgs e)
        {
            this.dgvInOut.ClearSelection();
        }
        #endregion

        #region ----- 단축키 Event -----
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Keys key = keyData & ~(Keys.Shift | Keys.Control);
            switch (key)
            {
                case Keys.Enter:
                    this.IsSearch = false;
                    this.btnSearch.PerformClick();
                    break;
                case Keys.Escape:
                    this.IsSearch = true;
                    this.btnSearch.PerformClick();
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion
    }
}
