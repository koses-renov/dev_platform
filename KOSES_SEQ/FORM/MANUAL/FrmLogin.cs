﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.MESSAGE;
using KOSES_SEQ.OP;

namespace KOSES_SEQ.POPUP
{

    public delegate void deleLoginChanged(enumLogInLevel level, string userID);
    public partial class FrmLogin : Form
    {
        public event deleLoginChanged OnLoginChanged;
        enumLogInLevel selectedLevel = enumLogInLevel.Level_OP;
        public FrmLogin()
        {
            InitializeComponent();

            this.VisibleChanged += FrmLogin_VisibleChanged;
            this.FormClosing += FrmLogin_FormClosing; ;
        }

        private void FrmLogin_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                tbUserID.Text = string.Empty;
                tbPass.Text = string.Empty;
            }
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            enumLogInLevel level = UserManager.Instance.ValidLogIn(selectedLevel, tbUserID.Text, tbPass.Text, out string fail);
            
            if (!string.IsNullOrEmpty(fail))
            {
                FRM.Msg.ShowMsg(fail);
                return;
            }

            OnLoginChanged?.Invoke(level, tbUserID.Text);
            this.Hide();
        }

        private void btnOp_Click(object sender, EventArgs e)
        {
            Button pBtn = (Button)sender;
            int nTag = 0;
            if (!int.TryParse(pBtn.Tag.ToString(), out nTag)) return;

            btnOp.BackColor = SystemColors.Control;
            btnEng.BackColor = SystemColors.Control;
            btnAdmin.BackColor = SystemColors.Control;

            //enumLogInLevel selectedLevel = enumLogInLevel.Unknown;

            if (nTag == 0)
            {
                selectedLevel = enumLogInLevel.Level_OP;
                btnOp.BackColor = Color.MediumAquamarine;
                tbUserID.Focus();
            }
            else if (nTag == 1)
            {
                selectedLevel = enumLogInLevel.Level_ENG;
                btnEng.BackColor = Color.MediumAquamarine;
                tbUserID.Focus();
            }
            else
            {
                selectedLevel = enumLogInLevel.Level_ADMIN;
                btnAdmin.BackColor = Color.MediumAquamarine;
                tbUserID.Focus();
            }

            var userList = new AutoCompleteStringCollection();

            var users = UserManager.Instance.GetUserInfosByLevel(selectedLevel).Select(u => u.UserID).ToArray();
            userList.AddRange(users);

            tbUserID.AutoCompleteCustomSource = userList;
            tbUserID.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            tbUserID.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEditUser_Click(object sender, EventArgs e)
        {
            FrmEditUser frm = new FrmEditUser();
            frm.TopMost = true;
            frm.Show();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            OnLoginChanged?.Invoke(enumLogInLevel.Unknown, string.Empty);
            this.Hide();
        }
    }
}
