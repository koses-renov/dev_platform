﻿using KOSES_SEQ.MANAGER;
using KOSES_SEQ.USERCONTROL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmManual : Form
    {
        public frmManual()
        {
            InitializeComponent();

            this.Load += FrmManual_Load;
            this.VisibleChanged += FrmManual_VisibleChanged;
            SysOperator.Instance.Tenkey.OnDisplayChanged += Tenkey_OnDisplayChanged;
        }

        // 깜빡임 제거
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public static void SetDoubleBuffered(Control control)
        {
            if (SystemInformation.TerminalServerSession)
                return;
            typeof(Control).InvokeMember("DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, control, new object[] { true });
        }

        private void FrmManual_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == false)
                    return;

                ChangeTenkeyTab();
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        private void FrmManual_Load(object sender, EventArgs e)
        {
            // TabControl 내 FlowLayoutPanel double bufferd 설정
            foreach (Control ctl in tcTenkey.Controls)
            {
                foreach (Control ctlSub in ctl.Controls)
                {
                    if (ctlSub is FlowLayoutPanel fp)
                    {
                        SetDoubleBuffered(fp);
                    }
                }
            }
        }

        private void Tenkey_OnDisplayChanged(int num)
        {
            ChangeTenkeyDisplay(num);
        }

        /// <summary>
        /// Tenkey Display UI 숫자 변경
        /// </summary>
        /// <param name="num"></param>
        private void ChangeTenkeyDisplay(int num)
        {
            if (InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    if (this.Visible)
                        ChangeTenkeyTab();
                    lblTenkeyDisplay.Text = num.ToString("D2");
                });
            }
            else
            {
                if (this.Visible)
                    ChangeTenkeyTab();
                lblTenkeyDisplay.Text = num.ToString("D2");
            }
        }

        /// <summary>
        /// Tenkey 에 해당하는 시퀀스 아이템이 있는 탭페이지로 전환
        /// </summary>
        private void ChangeTenkeyTab()
        {
            int num = SysOperator.Instance.Tenkey.CurrentDisplayValue;

            foreach (TabPage tp in tcTenkey.TabPages)
            {
                foreach (Control ctlSub in tp.Controls)
                {
                    foreach (Control ctlItem in ctlSub.Controls)
                    {
                        if (ctlItem is KosesTenkeyItem item)
                        {
                            if (item.TenkeyNo == num)
                            {
                                tcTenkey.SelectedTab = tp;
                                return;
                            }
                        }
                    }
                }
            }
        }

        private void btnTenkeyLoader_Click(object sender, EventArgs e)
        {
            tcTenkey.SelectedIndex = 0;
        }

        private void btnTenkeyFlux_Click(object sender, EventArgs e)
        {
            tcTenkey.SelectedIndex = 1;
        }
    }
}
