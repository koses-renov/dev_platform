﻿namespace KOSES_SEQ.OP
{
    partial class frmAlarmInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAlarmInfo));
            this.panManual = new System.Windows.Forms.Panel();
            this.tcAlarm = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvAlarmLogView = new System.Windows.Forms.DataGridView();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnAlarmLogView = new KosesControl.Controls.KosesButton();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateFrom = new System.Windows.Forms.Label();
            this.dtToTime = new System.Windows.Forms.DateTimePicker();
            this.dtFromTime = new System.Windows.Forms.DateTimePicker();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.spcAlarmEdit = new System.Windows.Forms.SplitContainer();
            this.dgvAlarmList = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbAlarmImageView = new System.Windows.Forms.PictureBox();
            this.cbImageList = new System.Windows.Forms.ComboBox();
            this.btnAlarmListLoad = new KosesControl.Controls.KosesButton();
            this.btnAlarmListSend = new KosesControl.Controls.KosesButton();
            this.btnAlarmInfoUpdate = new KosesControl.Controls.KosesButton();
            this.cbUseAlarmCount = new System.Windows.Forms.ComboBox();
            this.cbAlarmLevel = new System.Windows.Forms.ComboBox();
            this.rtbAction = new System.Windows.Forms.RichTextBox();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.tbAlarmCode = new System.Windows.Forms.TextBox();
            this.tbAlarmMessage = new System.Windows.Forms.TextBox();
            this.panManualRight = new System.Windows.Forms.Panel();
            this.btnAlarmLog = new System.Windows.Forms.Button();
            this.btnEditAlarm = new System.Windows.Forms.Button();
            this.lblAction = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblPartImage = new System.Windows.Forms.Label();
            this.lblUseAlarmCount = new System.Windows.Forms.Label();
            this.lblAlarmLevel = new System.Windows.Forms.Label();
            this.lblAlarmCode = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.panManual.SuspendLayout();
            this.tcAlarm.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmLogView)).BeginInit();
            this.pnlBottom.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcAlarmEdit)).BeginInit();
            this.spcAlarmEdit.Panel1.SuspendLayout();
            this.spcAlarmEdit.Panel2.SuspendLayout();
            this.spcAlarmEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmList)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlarmImageView)).BeginInit();
            this.panManualRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.tcAlarm);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(1153, 959);
            this.panManual.TabIndex = 953;
            // 
            // tcAlarm
            // 
            this.tcAlarm.Controls.Add(this.tabPage1);
            this.tcAlarm.Controls.Add(this.tabPage2);
            this.tcAlarm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcAlarm.Location = new System.Drawing.Point(1, 1);
            this.tcAlarm.Multiline = true;
            this.tcAlarm.Name = "tcAlarm";
            this.tcAlarm.SelectedIndex = 0;
            this.tcAlarm.Size = new System.Drawing.Size(1151, 957);
            this.tcAlarm.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1143, 928);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ALARM LOG";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvAlarmLogView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlBottom);
            this.splitContainer1.Size = new System.Drawing.Size(1137, 922);
            this.splitContainer1.SplitterDistance = 715;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvAlarmLogView
            // 
            this.dgvAlarmLogView.AllowUserToAddRows = false;
            this.dgvAlarmLogView.AllowUserToDeleteRows = false;
            this.dgvAlarmLogView.AllowUserToResizeRows = false;
            this.dgvAlarmLogView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAlarmLogView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlarmLogView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAlarmLogView.Location = new System.Drawing.Point(0, 0);
            this.dgvAlarmLogView.Name = "dgvAlarmLogView";
            this.dgvAlarmLogView.RowHeadersVisible = false;
            this.dgvAlarmLogView.Size = new System.Drawing.Size(1137, 715);
            this.dgvAlarmLogView.TabIndex = 0;
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.btnAlarmLogView);
            this.pnlBottom.Controls.Add(this.lblDateTo);
            this.pnlBottom.Controls.Add(this.lblDateFrom);
            this.pnlBottom.Controls.Add(this.dtToTime);
            this.pnlBottom.Controls.Add(this.dtFromTime);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottom.Location = new System.Drawing.Point(0, 0);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(1137, 203);
            this.pnlBottom.TabIndex = 0;
            // 
            // btnAlarmLogView
            // 
            this.btnAlarmLogView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlarmLogView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmLogView.Bold = true;
            this.btnAlarmLogView.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmLogView.Depth = 0;
            this.btnAlarmLogView.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmLogView.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmLogView.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmLogView.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnAlarmLogView.DrawShadows = true;
            this.btnAlarmLogView.FontSize = 14F;
            this.btnAlarmLogView.ForeColor = System.Drawing.Color.White;
            this.btnAlarmLogView.HighEmphasis = true;
            this.btnAlarmLogView.Icon = null;
            this.btnAlarmLogView.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnAlarmLogView.Location = new System.Drawing.Point(1026, 107);
            this.btnAlarmLogView.MouseOnOpacity = 50;
            this.btnAlarmLogView.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnAlarmLogView.Name = "btnAlarmLogView";
            this.btnAlarmLogView.Selected = false;
            this.btnAlarmLogView.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnAlarmLogView.Size = new System.Drawing.Size(100, 51);
            this.btnAlarmLogView.TabIndex = 13;
            this.btnAlarmLogView.Text = "VIEW";
            this.btnAlarmLogView.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnAlarmLogView.UseVisualStyleBackColor = false;
            this.btnAlarmLogView.Click += new System.EventHandler(this.btnAlarmLogView_Click);
            // 
            // lblDateTo
            // 
            this.lblDateTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblDateTo.Location = new System.Drawing.Point(786, 140);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(28, 17);
            this.lblDateTo.TabIndex = 12;
            this.lblDateTo.Text = "TO";
            // 
            // lblDateFrom
            // 
            this.lblDateFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDateFrom.AutoSize = true;
            this.lblDateFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblDateFrom.Location = new System.Drawing.Point(766, 111);
            this.lblDateFrom.Name = "lblDateFrom";
            this.lblDateFrom.Size = new System.Drawing.Size(48, 17);
            this.lblDateFrom.TabIndex = 11;
            this.lblDateFrom.Text = "FROM";
            // 
            // dtToTime
            // 
            this.dtToTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dtToTime.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtToTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToTime.Location = new System.Drawing.Point(820, 136);
            this.dtToTime.Name = "dtToTime";
            this.dtToTime.Size = new System.Drawing.Size(200, 23);
            this.dtToTime.TabIndex = 10;
            // 
            // dtFromTime
            // 
            this.dtFromTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dtFromTime.CustomFormat = "yyyy-MM-dd HH:mm";
            this.dtFromTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromTime.Location = new System.Drawing.Point(820, 107);
            this.dtFromTime.Name = "dtFromTime";
            this.dtFromTime.Size = new System.Drawing.Size(200, 23);
            this.dtFromTime.TabIndex = 9;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.spcAlarmEdit);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1143, 928);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ALARM EDIT";
            // 
            // spcAlarmEdit
            // 
            this.spcAlarmEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcAlarmEdit.Location = new System.Drawing.Point(3, 3);
            this.spcAlarmEdit.Name = "spcAlarmEdit";
            // 
            // spcAlarmEdit.Panel1
            // 
            this.spcAlarmEdit.Panel1.Controls.Add(this.dgvAlarmList);
            // 
            // spcAlarmEdit.Panel2
            // 
            this.spcAlarmEdit.Panel2.Controls.Add(this.panel1);
            this.spcAlarmEdit.Size = new System.Drawing.Size(1137, 922);
            this.spcAlarmEdit.SplitterDistance = 665;
            this.spcAlarmEdit.TabIndex = 0;
            // 
            // dgvAlarmList
            // 
            this.dgvAlarmList.AllowUserToAddRows = false;
            this.dgvAlarmList.AllowUserToDeleteRows = false;
            this.dgvAlarmList.AllowUserToResizeRows = false;
            this.dgvAlarmList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAlarmList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlarmList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAlarmList.EnableHeadersVisualStyles = false;
            this.dgvAlarmList.Location = new System.Drawing.Point(0, 0);
            this.dgvAlarmList.MultiSelect = false;
            this.dgvAlarmList.Name = "dgvAlarmList";
            this.dgvAlarmList.ReadOnly = true;
            this.dgvAlarmList.RowHeadersVisible = false;
            this.dgvAlarmList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAlarmList.Size = new System.Drawing.Size(665, 922);
            this.dgvAlarmList.TabIndex = 1;
            this.dgvAlarmList.SelectionChanged += new System.EventHandler(this.dgvAlarmList_SelectionChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbAlarmImageView);
            this.panel1.Controls.Add(this.lblPartImage);
            this.panel1.Controls.Add(this.cbImageList);
            this.panel1.Controls.Add(this.btnAlarmListLoad);
            this.panel1.Controls.Add(this.btnAlarmListSend);
            this.panel1.Controls.Add(this.btnAlarmInfoUpdate);
            this.panel1.Controls.Add(this.lblAction);
            this.panel1.Controls.Add(this.lblDescription);
            this.panel1.Controls.Add(this.lblUseAlarmCount);
            this.panel1.Controls.Add(this.cbUseAlarmCount);
            this.panel1.Controls.Add(this.lblAlarmLevel);
            this.panel1.Controls.Add(this.cbAlarmLevel);
            this.panel1.Controls.Add(this.lblAlarmCode);
            this.panel1.Controls.Add(this.lblMessage);
            this.panel1.Controls.Add(this.rtbAction);
            this.panel1.Controls.Add(this.rtbDescription);
            this.panel1.Controls.Add(this.tbAlarmCode);
            this.panel1.Controls.Add(this.tbAlarmMessage);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(468, 922);
            this.panel1.TabIndex = 0;
            // 
            // pbAlarmImageView
            // 
            this.pbAlarmImageView.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbAlarmImageView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAlarmImageView.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbAlarmImageView.ErrorImage")));
            this.pbAlarmImageView.Image = ((System.Drawing.Image)(resources.GetObject("pbAlarmImageView.Image")));
            this.pbAlarmImageView.Location = new System.Drawing.Point(33, 207);
            this.pbAlarmImageView.Name = "pbAlarmImageView";
            this.pbAlarmImageView.Size = new System.Drawing.Size(400, 300);
            this.pbAlarmImageView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAlarmImageView.TabIndex = 20;
            this.pbAlarmImageView.TabStop = false;
            this.pbAlarmImageView.Paint += new System.Windows.Forms.PaintEventHandler(this.pbAlarmImageView_Paint);
            this.pbAlarmImageView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbAlarmImageView_MouseClick);
            // 
            // cbImageList
            // 
            this.cbImageList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbImageList.FormattingEnabled = true;
            this.cbImageList.Location = new System.Drawing.Point(2, 177);
            this.cbImageList.Name = "cbImageList";
            this.cbImageList.Size = new System.Drawing.Size(463, 24);
            this.cbImageList.TabIndex = 18;
            this.cbImageList.SelectedIndexChanged += new System.EventHandler(this.cbImageList_SelectedIndexChanged);
            // 
            // btnAlarmListLoad
            // 
            this.btnAlarmListLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAlarmListLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmListLoad.Bold = true;
            this.btnAlarmListLoad.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmListLoad.Depth = 0;
            this.btnAlarmListLoad.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmListLoad.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmListLoad.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmListLoad.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnAlarmListLoad.DrawShadows = true;
            this.btnAlarmListLoad.FontSize = 14F;
            this.btnAlarmListLoad.ForeColor = System.Drawing.Color.White;
            this.btnAlarmListLoad.HighEmphasis = true;
            this.btnAlarmListLoad.Icon = null;
            this.btnAlarmListLoad.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnAlarmListLoad.Location = new System.Drawing.Point(10, 840);
            this.btnAlarmListLoad.MouseOnOpacity = 50;
            this.btnAlarmListLoad.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnAlarmListLoad.Name = "btnAlarmListLoad";
            this.btnAlarmListLoad.Selected = false;
            this.btnAlarmListLoad.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnAlarmListLoad.Size = new System.Drawing.Size(117, 36);
            this.btnAlarmListLoad.TabIndex = 17;
            this.btnAlarmListLoad.Text = "LIST LOAD";
            this.btnAlarmListLoad.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnAlarmListLoad.UseVisualStyleBackColor = false;
            this.btnAlarmListLoad.Click += new System.EventHandler(this.btnAlarmListLoad_Click);
            // 
            // btnAlarmListSend
            // 
            this.btnAlarmListSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAlarmListSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmListSend.Bold = true;
            this.btnAlarmListSend.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmListSend.Depth = 0;
            this.btnAlarmListSend.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmListSend.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmListSend.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmListSend.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnAlarmListSend.DrawShadows = true;
            this.btnAlarmListSend.FontSize = 14F;
            this.btnAlarmListSend.ForeColor = System.Drawing.Color.White;
            this.btnAlarmListSend.HighEmphasis = true;
            this.btnAlarmListSend.Icon = null;
            this.btnAlarmListSend.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnAlarmListSend.Location = new System.Drawing.Point(10, 882);
            this.btnAlarmListSend.MouseOnOpacity = 50;
            this.btnAlarmListSend.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnAlarmListSend.Name = "btnAlarmListSend";
            this.btnAlarmListSend.Selected = false;
            this.btnAlarmListSend.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnAlarmListSend.Size = new System.Drawing.Size(117, 36);
            this.btnAlarmListSend.TabIndex = 16;
            this.btnAlarmListSend.Text = "LIST SEND";
            this.btnAlarmListSend.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnAlarmListSend.UseVisualStyleBackColor = false;
            this.btnAlarmListSend.Click += new System.EventHandler(this.btnAlarmListSend_Click);
            // 
            // btnAlarmInfoUpdate
            // 
            this.btnAlarmInfoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAlarmInfoUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmInfoUpdate.Bold = true;
            this.btnAlarmInfoUpdate.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmInfoUpdate.Depth = 0;
            this.btnAlarmInfoUpdate.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnAlarmInfoUpdate.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmInfoUpdate.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAlarmInfoUpdate.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnAlarmInfoUpdate.DrawShadows = true;
            this.btnAlarmInfoUpdate.FontSize = 14F;
            this.btnAlarmInfoUpdate.ForeColor = System.Drawing.Color.White;
            this.btnAlarmInfoUpdate.HighEmphasis = true;
            this.btnAlarmInfoUpdate.Icon = null;
            this.btnAlarmInfoUpdate.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnAlarmInfoUpdate.Location = new System.Drawing.Point(344, 840);
            this.btnAlarmInfoUpdate.MouseOnOpacity = 50;
            this.btnAlarmInfoUpdate.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnAlarmInfoUpdate.Name = "btnAlarmInfoUpdate";
            this.btnAlarmInfoUpdate.Selected = false;
            this.btnAlarmInfoUpdate.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnAlarmInfoUpdate.Size = new System.Drawing.Size(117, 78);
            this.btnAlarmInfoUpdate.TabIndex = 15;
            this.btnAlarmInfoUpdate.Text = "SAVE";
            this.btnAlarmInfoUpdate.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnAlarmInfoUpdate.UseVisualStyleBackColor = false;
            this.btnAlarmInfoUpdate.Click += new System.EventHandler(this.btnAlarmInfoUpdate_Click);
            // 
            // cbUseAlarmCount
            // 
            this.cbUseAlarmCount.FormattingEnabled = true;
            this.cbUseAlarmCount.Location = new System.Drawing.Point(2, 124);
            this.cbUseAlarmCount.Name = "cbUseAlarmCount";
            this.cbUseAlarmCount.Size = new System.Drawing.Size(135, 24);
            this.cbUseAlarmCount.TabIndex = 11;
            // 
            // cbAlarmLevel
            // 
            this.cbAlarmLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAlarmLevel.FormattingEnabled = true;
            this.cbAlarmLevel.Location = new System.Drawing.Point(344, 74);
            this.cbAlarmLevel.Name = "cbAlarmLevel";
            this.cbAlarmLevel.Size = new System.Drawing.Size(121, 24);
            this.cbAlarmLevel.TabIndex = 7;
            // 
            // rtbAction
            // 
            this.rtbAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbAction.Location = new System.Drawing.Point(3, 741);
            this.rtbAction.Name = "rtbAction";
            this.rtbAction.Size = new System.Drawing.Size(462, 96);
            this.rtbAction.TabIndex = 3;
            this.rtbAction.Text = "";
            // 
            // rtbDescription
            // 
            this.rtbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbDescription.Location = new System.Drawing.Point(3, 617);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(462, 96);
            this.rtbDescription.TabIndex = 2;
            this.rtbDescription.Text = "";
            // 
            // tbAlarmCode
            // 
            this.tbAlarmCode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbAlarmCode.Location = new System.Drawing.Point(3, 74);
            this.tbAlarmCode.Name = "tbAlarmCode";
            this.tbAlarmCode.ReadOnly = true;
            this.tbAlarmCode.Size = new System.Drawing.Size(134, 23);
            this.tbAlarmCode.TabIndex = 1;
            // 
            // tbAlarmMessage
            // 
            this.tbAlarmMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAlarmMessage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbAlarmMessage.Location = new System.Drawing.Point(3, 24);
            this.tbAlarmMessage.Name = "tbAlarmMessage";
            this.tbAlarmMessage.Size = new System.Drawing.Size(462, 23);
            this.tbAlarmMessage.TabIndex = 0;
            // 
            // panManualRight
            // 
            this.panManualRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panManualRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panManualRight.Controls.Add(this.btnAlarmLog);
            this.panManualRight.Controls.Add(this.btnEditAlarm);
            this.panManualRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panManualRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManualRight.Location = new System.Drawing.Point(1153, 0);
            this.panManualRight.Margin = new System.Windows.Forms.Padding(0);
            this.panManualRight.Name = "panManualRight";
            this.panManualRight.Size = new System.Drawing.Size(151, 959);
            this.panManualRight.TabIndex = 952;
            // 
            // btnAlarmLog
            // 
            this.btnAlarmLog.BackColor = System.Drawing.Color.Transparent;
            this.btnAlarmLog.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAlarmLog.Location = new System.Drawing.Point(9, 19);
            this.btnAlarmLog.Name = "btnAlarmLog";
            this.btnAlarmLog.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btnAlarmLog.Size = new System.Drawing.Size(130, 40);
            this.btnAlarmLog.TabIndex = 950;
            this.btnAlarmLog.Text = "ALARM LOG";
            this.btnAlarmLog.UseVisualStyleBackColor = false;
            this.btnAlarmLog.Click += new System.EventHandler(this.btnAlarmLog_Click);
            // 
            // btnEditAlarm
            // 
            this.btnEditAlarm.BackColor = System.Drawing.Color.Transparent;
            this.btnEditAlarm.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnEditAlarm.Location = new System.Drawing.Point(9, 65);
            this.btnEditAlarm.Name = "btnEditAlarm";
            this.btnEditAlarm.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btnEditAlarm.Size = new System.Drawing.Size(130, 40);
            this.btnEditAlarm.TabIndex = 948;
            this.btnEditAlarm.Text = "EDIT ALARM";
            this.btnEditAlarm.UseVisualStyleBackColor = false;
            this.btnEditAlarm.Click += new System.EventHandler(this.btnEditAlarm_Click);
            // 
            // lblAction
            // 
            this.lblAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAction.AutoSize = true;
            this.lblAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAction.Location = new System.Drawing.Point(3, 720);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(65, 17);
            this.lblAction.TabIndex = 14;
            this.lblAction.Text = "ACTION";
            // 
            // lblDescription
            // 
            this.lblDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblDescription.Location = new System.Drawing.Point(3, 596);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(111, 17);
            this.lblDescription.TabIndex = 13;
            this.lblDescription.Text = "DESCRIPTION";
            // 
            // lblPartImage
            // 
            this.lblPartImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPartImage.AutoSize = true;
            this.lblPartImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblPartImage.Location = new System.Drawing.Point(3, 157);
            this.lblPartImage.Name = "lblPartImage";
            this.lblPartImage.Size = new System.Drawing.Size(102, 17);
            this.lblPartImage.TabIndex = 19;
            this.lblPartImage.Text = "PART IMAGE";
            // 
            // lblUseAlarmCount
            // 
            this.lblUseAlarmCount.AutoSize = true;
            this.lblUseAlarmCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblUseAlarmCount.Location = new System.Drawing.Point(3, 104);
            this.lblUseAlarmCount.Name = "lblUseAlarmCount";
            this.lblUseAlarmCount.Size = new System.Drawing.Size(155, 17);
            this.lblUseAlarmCount.TabIndex = 12;
            this.lblUseAlarmCount.Text = "USE ALARM COUNT";
            // 
            // lblAlarmLevel
            // 
            this.lblAlarmLevel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAlarmLevel.AutoSize = true;
            this.lblAlarmLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAlarmLevel.Location = new System.Drawing.Point(341, 54);
            this.lblAlarmLevel.Name = "lblAlarmLevel";
            this.lblAlarmLevel.Size = new System.Drawing.Size(113, 17);
            this.lblAlarmLevel.TabIndex = 8;
            this.lblAlarmLevel.Text = "ALARM LEVEL";
            // 
            // lblAlarmCode
            // 
            this.lblAlarmCode.AutoSize = true;
            this.lblAlarmCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAlarmCode.Location = new System.Drawing.Point(3, 54);
            this.lblAlarmCode.Name = "lblAlarmCode";
            this.lblAlarmCode.Size = new System.Drawing.Size(108, 17);
            this.lblAlarmCode.TabIndex = 6;
            this.lblAlarmCode.Text = "ALARM CODE";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblMessage.Location = new System.Drawing.Point(3, 3);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(82, 17);
            this.lblMessage.TabIndex = 5;
            this.lblMessage.Text = "MESSAGE";
            // 
            // frmAlarmInfo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1304, 959);
            this.Controls.Add(this.panManual);
            this.Controls.Add(this.panManualRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAlarmInfo";
            this.Text = "Manual";
            this.panManual.ResumeLayout(false);
            this.tcAlarm.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmLogView)).EndInit();
            this.pnlBottom.ResumeLayout(false);
            this.pnlBottom.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.spcAlarmEdit.Panel1.ResumeLayout(false);
            this.spcAlarmEdit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcAlarmEdit)).EndInit();
            this.spcAlarmEdit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlarmImageView)).EndInit();
            this.panManualRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        internal System.Windows.Forms.Panel panManualRight;
        private System.Windows.Forms.Button btnAlarmLog;
        private System.Windows.Forms.Button btnEditAlarm;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer spcAlarmEdit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbAlarmMessage;
        private System.Windows.Forms.RichTextBox rtbAction;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.TextBox tbAlarmCode;
        private System.Windows.Forms.ComboBox cbAlarmLevel;
        private System.Windows.Forms.ComboBox cbUseAlarmCount;
        private KosesControl.Controls.KosesButton btnAlarmInfoUpdate;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvAlarmLogView;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateFrom;
        private System.Windows.Forms.DateTimePicker dtToTime;
        private System.Windows.Forms.DateTimePicker dtFromTime;
        private System.Windows.Forms.DataGridView dgvAlarmList;
        private KosesControl.Controls.KosesButton btnAlarmListSend;
        private KosesControl.Controls.KosesButton btnAlarmListLoad;
        private System.Windows.Forms.TabControl tcAlarm;
        private System.Windows.Forms.ComboBox cbImageList;
        private System.Windows.Forms.PictureBox pbAlarmImageView;
        private KosesControl.Controls.KosesButton btnAlarmLogView;
        private System.Windows.Forms.Label lblPartImage;
        private System.Windows.Forms.Label lblAction;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblUseAlarmCount;
        private System.Windows.Forms.Label lblAlarmLevel;
        private System.Windows.Forms.Label lblAlarmCode;
        private System.Windows.Forms.Label lblMessage;
    }
}