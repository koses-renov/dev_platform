﻿namespace KOSES_SEQ.OP
{
    partial class frmSystem_Motor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panManual = new System.Windows.Forms.Panel();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.dgvParameter = new KosesControl.Controls.KosesDataGridView();
            this.ColParamNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColParamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColParamValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMotor = new KosesControl.Controls.KosesDataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnJog = new System.Windows.Forms.Panel();
            this.gb_Jog = new KosesControl.Controls.KosesGroupBox();
            this.timer_MotorStatus = new System.Windows.Forms.Timer(this.components);
            this.panManual.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotor)).BeginInit();
            this.pnJog.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.kosesGroupBox2);
            this.panManual.Controls.Add(this.pnJog);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(1305, 875);
            this.panManual.TabIndex = 953;
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.kosesGroupBox2.Controls.Add(this.dgvParameter);
            this.kosesGroupBox2.Controls.Add(this.dgvMotor);
            this.kosesGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kosesGroupBox2.Location = new System.Drawing.Point(1, 1);
            this.kosesGroupBox2.Margin = new System.Windows.Forms.Padding(5);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Radius = 0;
            this.kosesGroupBox2.Size = new System.Drawing.Size(996, 873);
            this.kosesGroupBox2.TabIndex = 957;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "MOTOR";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // dgvParameter
            // 
            this.dgvParameter.AllowUserToAddRows = false;
            this.dgvParameter.AllowUserToDeleteRows = false;
            this.dgvParameter.AllowUserToResizeColumns = false;
            this.dgvParameter.AllowUserToResizeRows = false;
            this.dgvParameter.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvParameter.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvParameter.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvParameter.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvParameter.ColumnHeadersHeight = 30;
            this.dgvParameter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvParameter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColParamNo,
            this.ColParamName,
            this.ColParamValue});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvParameter.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvParameter.DisabledBackColor = System.Drawing.SystemColors.Control;
            this.dgvParameter.DisabledTextColor = System.Drawing.SystemColors.GrayText;
            this.dgvParameter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvParameter.EnableHeadersVisualStyles = false;
            this.dgvParameter.Location = new System.Drawing.Point(272, 26);
            this.dgvParameter.MultiSelect = false;
            this.dgvParameter.Name = "dgvParameter";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvParameter.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvParameter.RowHeadersVisible = false;
            this.dgvParameter.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 10F);
            this.dgvParameter.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvParameter.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvParameter.RowTemplate.Height = 25;
            this.dgvParameter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvParameter.Size = new System.Drawing.Size(721, 844);
            this.dgvParameter.TabIndex = 956;
            // 
            // ColParamNo
            // 
            this.ColParamNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColParamNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColParamNo.FillWeight = 30F;
            this.ColParamNo.HeaderText = "No";
            this.ColParamNo.Name = "ColParamNo";
            this.ColParamNo.ReadOnly = true;
            this.ColParamNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColParamNo.Width = 42;
            // 
            // ColParamName
            // 
            this.ColParamName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ColParamName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColParamName.HeaderText = "ITEM";
            this.ColParamName.Name = "ColParamName";
            this.ColParamName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColParamValue
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColParamValue.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColParamValue.HeaderText = "Value";
            this.ColParamValue.Name = "ColParamValue";
            this.ColParamValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvMotor
            // 
            this.dgvMotor.AllowUserToAddRows = false;
            this.dgvMotor.AllowUserToDeleteRows = false;
            this.dgvMotor.AllowUserToResizeColumns = false;
            this.dgvMotor.AllowUserToResizeRows = false;
            this.dgvMotor.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvMotor.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvMotor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvMotor.ColumnHeadersHeight = 30;
            this.dgvMotor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMotor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMotor.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvMotor.DisabledBackColor = System.Drawing.SystemColors.Control;
            this.dgvMotor.DisabledTextColor = System.Drawing.SystemColors.GrayText;
            this.dgvMotor.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvMotor.EnableHeadersVisualStyles = false;
            this.dgvMotor.Location = new System.Drawing.Point(3, 26);
            this.dgvMotor.MultiSelect = false;
            this.dgvMotor.Name = "dgvMotor";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotor.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvMotor.RowHeadersVisible = false;
            this.dgvMotor.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 10F);
            this.dgvMotor.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvMotor.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvMotor.RowTemplate.Height = 25;
            this.dgvMotor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvMotor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMotor.Size = new System.Drawing.Size(269, 844);
            this.dgvMotor.TabIndex = 957;
            this.dgvMotor.SelectionChanged += new System.EventHandler(this.dgvMotor_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn1.FillWeight = 30F;
            this.dataGridViewTextBoxColumn1.HeaderText = "No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 42;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn2.HeaderText = "Motor Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // pnJog
            // 
            this.pnJog.Controls.Add(this.gb_Jog);
            this.pnJog.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnJog.Location = new System.Drawing.Point(997, 1);
            this.pnJog.Name = "pnJog";
            this.pnJog.Size = new System.Drawing.Size(307, 873);
            this.pnJog.TabIndex = 956;
            // 
            // gb_Jog
            // 
            this.gb_Jog.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gb_Jog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_Jog.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_Jog.Location = new System.Drawing.Point(0, 0);
            this.gb_Jog.Margin = new System.Windows.Forms.Padding(5);
            this.gb_Jog.Name = "gb_Jog";
            this.gb_Jog.Radius = 0;
            this.gb_Jog.Size = new System.Drawing.Size(307, 873);
            this.gb_Jog.TabIndex = 953;
            this.gb_Jog.TabStop = false;
            this.gb_Jog.Text = "JOG";
            this.gb_Jog.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.gb_Jog.TitleForeColor = System.Drawing.Color.White;
            this.gb_Jog.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.gb_Jog.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.gb_Jog.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // timer_MotorStatus
            // 
            this.timer_MotorStatus.Interval = 500;
            // 
            // frmSystem_Motor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1305, 875);
            this.Controls.Add(this.panManual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSystem_Motor";
            this.Text = "Recipe";
            this.Load += new System.EventHandler(this.frmMotor_Load);
            this.panManual.ResumeLayout(false);
            this.kosesGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvParameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotor)).EndInit();
            this.pnJog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        private System.Windows.Forms.Timer timer_MotorStatus;
        private System.Windows.Forms.Panel pnJog;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private KosesControl.Controls.KosesDataGridView dgvParameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColParamNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColParamName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColParamValue;
        private KosesControl.Controls.KosesDataGridView dgvMotor;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private KosesControl.Controls.KosesGroupBox gb_Jog;
    }
}