﻿namespace KOSES_SEQ.OP
{
    partial class frmMotorStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMotorStatus));
            this.panManual = new System.Windows.Forms.Panel();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvMotorStatus3 = new KosesControl.Controls.KosesDataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMotorStatus2 = new KosesControl.Controls.KosesDataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvMotorStatus1 = new KosesControl.Controls.KosesDataGridView();
            this.ColParamNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMotorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColServo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColAlarm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColInposition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColHome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDecrease = new System.Windows.Forms.Button();
            this.btnIncrease = new System.Windows.Forms.Button();
            this.lbSpeedRate = new KosesControl.Controls.LedLabel();
            this.btnMinus = new KosesControl.Controls.KosesButton();
            this.btnPlus = new KosesControl.Controls.KosesButton();
            this.btn_AllServoOn = new System.Windows.Forms.Button();
            this.btn_AllAlarmClear = new System.Windows.Forms.Button();
            this.btn_AllServoOff = new System.Windows.Forms.Button();
            this.btn_AllStop = new System.Windows.Forms.Button();
            this.timer_MotorStatus = new System.Windows.Forms.Timer(this.components);
            this.tmrMotorStatus = new System.Windows.Forms.Timer(this.components);
            this.btnSet = new System.Windows.Forms.Button();
            this.panManual.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorStatus3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorStatus2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorStatus1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.kosesGroupBox2);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(994, 649);
            this.panManual.TabIndex = 953;
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.kosesGroupBox2.Controls.Add(this.tableLayoutPanel2);
            this.kosesGroupBox2.Controls.Add(this.tableLayoutPanel1);
            this.kosesGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kosesGroupBox2.Location = new System.Drawing.Point(1, 1);
            this.kosesGroupBox2.Margin = new System.Windows.Forms.Padding(5);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Padding = new System.Windows.Forms.Padding(0);
            this.kosesGroupBox2.Radius = 0;
            this.kosesGroupBox2.Size = new System.Drawing.Size(992, 647);
            this.kosesGroupBox2.TabIndex = 957;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "MOTOR STATUS";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.dgvMotorStatus3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvMotorStatus2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvMotorStatus1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 23);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(992, 440);
            this.tableLayoutPanel2.TabIndex = 958;
            // 
            // dgvMotorStatus3
            // 
            this.dgvMotorStatus3.AllowUserToAddRows = false;
            this.dgvMotorStatus3.AllowUserToDeleteRows = false;
            this.dgvMotorStatus3.AllowUserToResizeColumns = false;
            this.dgvMotorStatus3.AllowUserToResizeRows = false;
            this.dgvMotorStatus3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvMotorStatus3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvMotorStatus3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotorStatus3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgvMotorStatus3.ColumnHeadersHeight = 25;
            this.dgvMotorStatus3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMotorStatus3.ColumnHeadersVisible = false;
            this.dgvMotorStatus3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14});
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMotorStatus3.DefaultCellStyle = dataGridViewCellStyle42;
            this.dgvMotorStatus3.DisabledBackColor = System.Drawing.SystemColors.Control;
            this.dgvMotorStatus3.DisabledTextColor = System.Drawing.SystemColors.GrayText;
            this.dgvMotorStatus3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMotorStatus3.EnableHeadersVisualStyles = false;
            this.dgvMotorStatus3.Location = new System.Drawing.Point(660, 0);
            this.dgvMotorStatus3.Margin = new System.Windows.Forms.Padding(0);
            this.dgvMotorStatus3.MultiSelect = false;
            this.dgvMotorStatus3.Name = "dgvMotorStatus3";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotorStatus3.RowHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this.dgvMotorStatus3.RowHeadersVisible = false;
            this.dgvMotorStatus3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dgvMotorStatus3.RowsDefaultCellStyle = dataGridViewCellStyle44;
            this.dgvMotorStatus3.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvMotorStatus3.RowTemplate.Height = 25;
            this.dgvMotorStatus3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMotorStatus3.Size = new System.Drawing.Size(332, 440);
            this.dgvMotorStatus3.TabIndex = 958;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewTextBoxColumn8.FillWeight = 30F;
            this.dataGridViewTextBoxColumn8.HeaderText = "No";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 5;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewTextBoxColumn9.HeaderText = "Motor Name";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Lime;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle37;
            this.dataGridViewTextBoxColumn10.HeaderText = "Position";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridViewTextBoxColumn11.HeaderText = "";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 25;
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn12.HeaderText = "";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 25;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridViewTextBoxColumn13.HeaderText = "";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 25;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewTextBoxColumn14.HeaderText = "";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 25;
            // 
            // dgvMotorStatus2
            // 
            this.dgvMotorStatus2.AllowUserToAddRows = false;
            this.dgvMotorStatus2.AllowUserToDeleteRows = false;
            this.dgvMotorStatus2.AllowUserToResizeColumns = false;
            this.dgvMotorStatus2.AllowUserToResizeRows = false;
            this.dgvMotorStatus2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvMotorStatus2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvMotorStatus2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotorStatus2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this.dgvMotorStatus2.ColumnHeadersHeight = 25;
            this.dgvMotorStatus2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMotorStatus2.ColumnHeadersVisible = false;
            this.dgvMotorStatus2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle53.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMotorStatus2.DefaultCellStyle = dataGridViewCellStyle53;
            this.dgvMotorStatus2.DisabledBackColor = System.Drawing.SystemColors.Control;
            this.dgvMotorStatus2.DisabledTextColor = System.Drawing.SystemColors.GrayText;
            this.dgvMotorStatus2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMotorStatus2.EnableHeadersVisualStyles = false;
            this.dgvMotorStatus2.Location = new System.Drawing.Point(330, 0);
            this.dgvMotorStatus2.Margin = new System.Windows.Forms.Padding(0);
            this.dgvMotorStatus2.MultiSelect = false;
            this.dgvMotorStatus2.Name = "dgvMotorStatus2";
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotorStatus2.RowHeadersDefaultCellStyle = dataGridViewCellStyle54;
            this.dgvMotorStatus2.RowHeadersVisible = false;
            this.dgvMotorStatus2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dgvMotorStatus2.RowsDefaultCellStyle = dataGridViewCellStyle55;
            this.dgvMotorStatus2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvMotorStatus2.RowTemplate.Height = 25;
            this.dgvMotorStatus2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMotorStatus2.Size = new System.Drawing.Size(330, 440);
            this.dgvMotorStatus2.TabIndex = 957;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn1.FillWeight = 30F;
            this.dataGridViewTextBoxColumn1.HeaderText = "No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 5;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewTextBoxColumn2.HeaderText = "Motor Name";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle48.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Lime;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewTextBoxColumn3.HeaderText = "Position";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewTextBoxColumn4.HeaderText = "";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 25;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewTextBoxColumn5.HeaderText = "";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 25;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle51.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn6.HeaderText = "";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 25;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle52.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle52.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewTextBoxColumn7.HeaderText = "";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 25;
            // 
            // dgvMotorStatus1
            // 
            this.dgvMotorStatus1.AllowUserToAddRows = false;
            this.dgvMotorStatus1.AllowUserToDeleteRows = false;
            this.dgvMotorStatus1.AllowUserToResizeColumns = false;
            this.dgvMotorStatus1.AllowUserToResizeRows = false;
            this.dgvMotorStatus1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvMotorStatus1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvMotorStatus1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotorStatus1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle56;
            this.dgvMotorStatus1.ColumnHeadersHeight = 25;
            this.dgvMotorStatus1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMotorStatus1.ColumnHeadersVisible = false;
            this.dgvMotorStatus1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColParamNo,
            this.ColMotorName,
            this.ColPosition,
            this.ColServo,
            this.ColAlarm,
            this.ColInposition,
            this.ColHome});
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle64.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle64.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMotorStatus1.DefaultCellStyle = dataGridViewCellStyle64;
            this.dgvMotorStatus1.DisabledBackColor = System.Drawing.SystemColors.Control;
            this.dgvMotorStatus1.DisabledTextColor = System.Drawing.SystemColors.GrayText;
            this.dgvMotorStatus1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMotorStatus1.EnableHeadersVisualStyles = false;
            this.dgvMotorStatus1.Location = new System.Drawing.Point(0, 0);
            this.dgvMotorStatus1.Margin = new System.Windows.Forms.Padding(0);
            this.dgvMotorStatus1.MultiSelect = false;
            this.dgvMotorStatus1.Name = "dgvMotorStatus1";
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle65.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle65.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle65.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle65.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle65.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle65.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMotorStatus1.RowHeadersDefaultCellStyle = dataGridViewCellStyle65;
            this.dgvMotorStatus1.RowHeadersVisible = false;
            this.dgvMotorStatus1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle66.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.dgvMotorStatus1.RowsDefaultCellStyle = dataGridViewCellStyle66;
            this.dgvMotorStatus1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvMotorStatus1.RowTemplate.Height = 25;
            this.dgvMotorStatus1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMotorStatus1.Size = new System.Drawing.Size(330, 440);
            this.dgvMotorStatus1.TabIndex = 956;
            this.dgvMotorStatus1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMotorStatus_CellContentClick);
            // 
            // ColParamNo
            // 
            this.ColParamNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle57.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ColParamNo.DefaultCellStyle = dataGridViewCellStyle57;
            this.ColParamNo.FillWeight = 30F;
            this.ColParamNo.HeaderText = "No";
            this.ColParamNo.Name = "ColParamNo";
            this.ColParamNo.ReadOnly = true;
            this.ColParamNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColParamNo.Width = 5;
            // 
            // ColMotorName
            // 
            this.ColMotorName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.ColMotorName.DefaultCellStyle = dataGridViewCellStyle58;
            this.ColMotorName.HeaderText = "Motor Name";
            this.ColMotorName.Name = "ColMotorName";
            this.ColMotorName.ReadOnly = true;
            this.ColMotorName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColPosition
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle59.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle59.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle59.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle59.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle59.SelectionForeColor = System.Drawing.Color.Lime;
            this.ColPosition.DefaultCellStyle = dataGridViewCellStyle59;
            this.ColPosition.HeaderText = "Position";
            this.ColPosition.Name = "ColPosition";
            this.ColPosition.ReadOnly = true;
            this.ColPosition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColServo
            // 
            dataGridViewCellStyle60.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle60.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.Color.Black;
            this.ColServo.DefaultCellStyle = dataGridViewCellStyle60;
            this.ColServo.HeaderText = "";
            this.ColServo.Name = "ColServo";
            this.ColServo.ReadOnly = true;
            this.ColServo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColServo.Width = 25;
            // 
            // ColAlarm
            // 
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle61.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.Color.Black;
            this.ColAlarm.DefaultCellStyle = dataGridViewCellStyle61;
            this.ColAlarm.HeaderText = "";
            this.ColAlarm.Name = "ColAlarm";
            this.ColAlarm.ReadOnly = true;
            this.ColAlarm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColAlarm.Width = 25;
            // 
            // ColInposition
            // 
            dataGridViewCellStyle62.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle62.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            this.ColInposition.DefaultCellStyle = dataGridViewCellStyle62;
            this.ColInposition.HeaderText = "";
            this.ColInposition.Name = "ColInposition";
            this.ColInposition.ReadOnly = true;
            this.ColInposition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColInposition.Width = 25;
            // 
            // ColHome
            // 
            dataGridViewCellStyle63.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle63.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.Color.Black;
            this.ColHome.DefaultCellStyle = dataGridViewCellStyle63;
            this.ColHome.HeaderText = "";
            this.ColHome.Name = "ColHome";
            this.ColHome.ReadOnly = true;
            this.ColHome.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColHome.Width = 25;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 577F));
            this.tableLayoutPanel1.Controls.Add(this.kosesGroupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 463);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(992, 184);
            this.tableLayoutPanel1.TabIndex = 957;
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.kosesGroupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.kosesGroupBox1.Radius = 0;
            this.kosesGroupBox1.Size = new System.Drawing.Size(415, 184);
            this.kosesGroupBox1.TabIndex = 0;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "LOG";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSet);
            this.panel1.Controls.Add(this.btnDecrease);
            this.panel1.Controls.Add(this.btnIncrease);
            this.panel1.Controls.Add(this.lbSpeedRate);
            this.panel1.Controls.Add(this.btnMinus);
            this.panel1.Controls.Add(this.btnPlus);
            this.panel1.Controls.Add(this.btn_AllServoOn);
            this.panel1.Controls.Add(this.btn_AllAlarmClear);
            this.panel1.Controls.Add(this.btn_AllServoOff);
            this.panel1.Controls.Add(this.btn_AllStop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(418, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(571, 178);
            this.panel1.TabIndex = 1;
            // 
            // btnDecrease
            // 
            this.btnDecrease.BackColor = System.Drawing.Color.White;
            this.btnDecrease.FlatAppearance.BorderSize = 0;
            this.btnDecrease.Image = ((System.Drawing.Image)(resources.GetObject("btnDecrease.Image")));
            this.btnDecrease.Location = new System.Drawing.Point(25, 12);
            this.btnDecrease.Margin = new System.Windows.Forms.Padding(0);
            this.btnDecrease.Name = "btnDecrease";
            this.btnDecrease.Size = new System.Drawing.Size(67, 25);
            this.btnDecrease.TabIndex = 995;
            this.btnDecrease.UseVisualStyleBackColor = false;
            this.btnDecrease.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnDecrease_MouseDown);
            this.btnDecrease.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnDecrease_MouseUp);
            // 
            // btnIncrease
            // 
            this.btnIncrease.BackColor = System.Drawing.Color.White;
            this.btnIncrease.FlatAppearance.BorderSize = 0;
            this.btnIncrease.Image = ((System.Drawing.Image)(resources.GetObject("btnIncrease.Image")));
            this.btnIncrease.Location = new System.Drawing.Point(103, 12);
            this.btnIncrease.Margin = new System.Windows.Forms.Padding(0);
            this.btnIncrease.Name = "btnIncrease";
            this.btnIncrease.Size = new System.Drawing.Size(67, 25);
            this.btnIncrease.TabIndex = 994;
            this.btnIncrease.UseVisualStyleBackColor = false;
            this.btnIncrease.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnIncrease_MouseDown);
            this.btnIncrease.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnIncrease_MouseUp);
            // 
            // lbVelocity
            // 
            this.lbSpeedRate.BackColor = System.Drawing.Color.Black;
            this.lbSpeedRate.Bold = false;
            this.lbSpeedRate.DecimalPoint = 0;
            this.lbSpeedRate.Font = new System.Drawing.Font("LED Dot-Matrix", 35F);
            this.lbSpeedRate.FontSize = 35F;
            this.lbSpeedRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lbSpeedRate.LedOffColor = System.Drawing.Color.Gray;
            this.lbSpeedRate.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lbSpeedRate.Location = new System.Drawing.Point(25, 45);
            this.lbSpeedRate.Maximum = 100D;
            this.lbSpeedRate.Minimum = 0D;
            this.lbSpeedRate.Name = "lbVelocity";
            this.lbSpeedRate.On = true;
            this.lbSpeedRate.Size = new System.Drawing.Size(145, 48);
            this.lbSpeedRate.TabIndex = 993;
            this.lbSpeedRate.Text = "100";
            this.lbSpeedRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbSpeedRate.TextType = KosesControl.TextTypes.NUMBER;
            // 
            // btnMinus
            // 
            this.btnMinus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnMinus.Bold = true;
            this.btnMinus.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnMinus.Depth = 0;
            this.btnMinus.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnMinus.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnMinus.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnMinus.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnMinus.DrawShadows = true;
            this.btnMinus.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinus.FontSize = 30F;
            this.btnMinus.ForeColor = System.Drawing.Color.White;
            this.btnMinus.HighEmphasis = true;
            this.btnMinus.Icon = null;
            this.btnMinus.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnMinus.Location = new System.Drawing.Point(25, 102);
            this.btnMinus.MouseOnOpacity = 50;
            this.btnMinus.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Selected = false;
            this.btnMinus.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnMinus.Size = new System.Drawing.Size(67, 64);
            this.btnMinus.TabIndex = 992;
            this.btnMinus.Text = "－";
            this.btnMinus.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnMinus.UseVisualStyleBackColor = false;
            this.btnMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMinus_MouseDown);
            this.btnMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMinus_MouseUp);
            // 
            // btnPlus
            // 
            this.btnPlus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnPlus.Bold = true;
            this.btnPlus.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPlus.Depth = 0;
            this.btnPlus.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(102)))), ((int)(((byte)(179)))));
            this.btnPlus.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPlus.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnPlus.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnPlus.DrawShadows = true;
            this.btnPlus.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.FontSize = 30F;
            this.btnPlus.ForeColor = System.Drawing.Color.White;
            this.btnPlus.HighEmphasis = true;
            this.btnPlus.Icon = null;
            this.btnPlus.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnPlus.Location = new System.Drawing.Point(103, 102);
            this.btnPlus.MouseOnOpacity = 50;
            this.btnPlus.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Selected = false;
            this.btnPlus.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(102)))), ((int)(((byte)(179)))));
            this.btnPlus.Size = new System.Drawing.Size(67, 64);
            this.btnPlus.TabIndex = 991;
            this.btnPlus.Text = "＋";
            this.btnPlus.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnPlus.UseVisualStyleBackColor = false;
            this.btnPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPlus_MouseDown);
            this.btnPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPlus_MouseUp);
            // 
            // btn_AllServoOn
            // 
            this.btn_AllServoOn.BackColor = System.Drawing.Color.Transparent;
            this.btn_AllServoOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AllServoOn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllServoOn.Image = ((System.Drawing.Image)(resources.GetObject("btn_AllServoOn.Image")));
            this.btn_AllServoOn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_AllServoOn.Location = new System.Drawing.Point(254, 12);
            this.btn_AllServoOn.Margin = new System.Windows.Forms.Padding(1);
            this.btn_AllServoOn.Name = "btn_AllServoOn";
            this.btn_AllServoOn.Padding = new System.Windows.Forms.Padding(5);
            this.btn_AllServoOn.Size = new System.Drawing.Size(151, 75);
            this.btn_AllServoOn.TabIndex = 989;
            this.btn_AllServoOn.Text = "ALL\r\nSERVO\r\nON";
            this.btn_AllServoOn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_AllServoOn.UseVisualStyleBackColor = false;
            this.btn_AllServoOn.Click += new System.EventHandler(this.btn_AllServoOn_Click);
            // 
            // btn_AllAlarmClear
            // 
            this.btn_AllAlarmClear.BackColor = System.Drawing.Color.Transparent;
            this.btn_AllAlarmClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AllAlarmClear.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllAlarmClear.Image = ((System.Drawing.Image)(resources.GetObject("btn_AllAlarmClear.Image")));
            this.btn_AllAlarmClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_AllAlarmClear.Location = new System.Drawing.Point(254, 91);
            this.btn_AllAlarmClear.Margin = new System.Windows.Forms.Padding(1);
            this.btn_AllAlarmClear.Name = "btn_AllAlarmClear";
            this.btn_AllAlarmClear.Padding = new System.Windows.Forms.Padding(5);
            this.btn_AllAlarmClear.Size = new System.Drawing.Size(151, 75);
            this.btn_AllAlarmClear.TabIndex = 987;
            this.btn_AllAlarmClear.Text = "ALL\r\nALARM\r\nCLEAR";
            this.btn_AllAlarmClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_AllAlarmClear.UseVisualStyleBackColor = false;
            this.btn_AllAlarmClear.Click += new System.EventHandler(this.btn_AllAlarmClear_Click);
            // 
            // btn_AllServoOff
            // 
            this.btn_AllServoOff.BackColor = System.Drawing.Color.Transparent;
            this.btn_AllServoOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AllServoOff.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllServoOff.Image = ((System.Drawing.Image)(resources.GetObject("btn_AllServoOff.Image")));
            this.btn_AllServoOff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_AllServoOff.Location = new System.Drawing.Point(407, 12);
            this.btn_AllServoOff.Margin = new System.Windows.Forms.Padding(1);
            this.btn_AllServoOff.Name = "btn_AllServoOff";
            this.btn_AllServoOff.Padding = new System.Windows.Forms.Padding(5);
            this.btn_AllServoOff.Size = new System.Drawing.Size(151, 75);
            this.btn_AllServoOff.TabIndex = 988;
            this.btn_AllServoOff.Text = "ALL\r\nSERVO\r\nOFF";
            this.btn_AllServoOff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_AllServoOff.UseVisualStyleBackColor = false;
            this.btn_AllServoOff.Click += new System.EventHandler(this.btn_AllServoOff_Click);
            // 
            // btn_AllStop
            // 
            this.btn_AllStop.BackColor = System.Drawing.Color.Transparent;
            this.btn_AllStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AllStop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllStop.Image = ((System.Drawing.Image)(resources.GetObject("btn_AllStop.Image")));
            this.btn_AllStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_AllStop.Location = new System.Drawing.Point(407, 91);
            this.btn_AllStop.Margin = new System.Windows.Forms.Padding(1);
            this.btn_AllStop.Name = "btn_AllStop";
            this.btn_AllStop.Padding = new System.Windows.Forms.Padding(5);
            this.btn_AllStop.Size = new System.Drawing.Size(151, 75);
            this.btn_AllStop.TabIndex = 986;
            this.btn_AllStop.Text = "ALL\r\nSTOP";
            this.btn_AllStop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_AllStop.UseVisualStyleBackColor = false;
            this.btn_AllStop.Click += new System.EventHandler(this.btn_AllStop_Click);
            // 
            // timer_MotorStatus
            // 
            this.timer_MotorStatus.Interval = 500;
            // 
            // tmrMotorStatus
            // 
            this.tmrMotorStatus.Interval = 500;
            this.tmrMotorStatus.Tick += new System.EventHandler(this.tmrMotorStatus_Tick);
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(176, 12);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(73, 154);
            this.btnSet.TabIndex = 996;
            this.btnSet.Text = "SET";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // frmMotorStatus
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(994, 649);
            this.Controls.Add(this.panManual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMotorStatus";
            this.Text = "Recipe";
            this.Load += new System.EventHandler(this.frmMotor_Load);
            this.VisibleChanged += new System.EventHandler(this.frmMotor_VisibleChanged);
            this.panManual.ResumeLayout(false);
            this.kosesGroupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorStatus3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorStatus2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMotorStatus1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        private System.Windows.Forms.Timer timer_MotorStatus;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_AllStop;
        private KosesControl.Controls.KosesButton btnPlus;
        private KosesControl.Controls.KosesButton btnMinus;
        private System.Windows.Forms.Timer tmrMotorStatus;
        private KosesControl.Controls.KosesDataGridView dgvMotorStatus1;
        private System.Windows.Forms.Button btn_AllServoOn;
        private System.Windows.Forms.Button btn_AllAlarmClear;
        private System.Windows.Forms.Button btn_AllServoOff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColParamNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMotorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPosition;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColServo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColAlarm;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColInposition;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColHome;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private KosesControl.Controls.LedLabel lbSpeedRate;
        private System.Windows.Forms.Button btnDecrease;
        private System.Windows.Forms.Button btnIncrease;
        private KosesControl.Controls.KosesDataGridView dgvMotorStatus3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private KosesControl.Controls.KosesDataGridView dgvMotorStatus2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.Button btnSet;
    }
}