﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using KOSES_SEQ.ALARM;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.OP;

namespace KOSES_SEQ.POPUP
{

    /// <summary>
    /// 알람 발생 시 팝업 창
    /// </summary>
    public partial class FrmErrorPopup : Form
    {
        CAlarm SelectedAlarm { get; set; }

        public FrmErrorPopup()
        {
            InitializeComponent();

            this.FormClosing += FrmLogin_FormClosing;
            this.Load += FrmErrorPopup_Load;
        }

        private void FrmErrorPopup_Load(object sender, EventArgs e)
        {
            this.TopMost = true;

            AlarmManager.Instance.OnAlarmOccured -= AlarmManager_OnAlarmOccured;
            AlarmManager.Instance.OnAlarmOccured += AlarmManager_OnAlarmOccured;
        }

        /// <summary>
        /// 알람 매니저에서 알람 발생 이벤트 발생
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="isReset"></param>
        private void AlarmManager_OnAlarmOccured(CAlarm alarm, bool isReset = false)
        {
            if (isReset == false)
            {
                SelectedAlarm = alarm;
                if (InvokeRequired)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        LoadCurrentAlarms();
                        this.Show();
                    });
                }
                else
                {
                    LoadCurrentAlarms();
                    this.Show();
                }
                
            }
            else
            {
                lock (AlarmManager.Instance.CurrentAlarmList)
                {
                    if (AlarmManager.Instance.CurrentAlarmList.Count == 0)
                    {
                        SelectedAlarm = null;
                        if (InvokeRequired)
                        {
                            this.Invoke((MethodInvoker)delegate ()
                            {
                                this.Hide();
                            });
                        }
                        else
                        {
                            this.Hide();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 폼 클로즈 시 숨김처리 (실제 닫지 않음)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        /// <summary>
        /// 리셋 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, EventArgs e)
        {
            SelectedAlarm = null;

            SysOperator.Instance.Reset();

            this.Hide();
        }

        /// <summary>
        /// 알람 상세 내용 보여 줌
        /// </summary>
        /// <param name="alarm"></param>
        private void ShowAlarm(CAlarm alarm)
        {
            if (alarm != null)
            {
                tbAlarmMessage.Text = alarm.Message;
                rtbDescription.Text = alarm.Description;
                rtbAction.Text = alarm.Action;

                if (!string.IsNullOrEmpty(alarm.ImageFileName) && File.Exists(Path.Combine(DefPath.AlarmImage, alarm.ImageFileName)))
                {
                    pbAlarmImageView.Image = Image.FromFile(Path.Combine(DefPath.AlarmImage, alarm.ImageFileName));
                }
                else
                {
                    pbAlarmImageView.Image = pbAlarmImageView.ErrorImage;
                }

                pbAlarmImageView.Invalidate();
            }
        }

        /// <summary>
        /// 현재 발생된 알람 보여줌
        /// </summary>
        private void LoadCurrentAlarms()
        {
            try
            {
                DataTable dt = DBManager.Instance.AlarmInfoDB.GetAlarms(AlarmManager.Instance.CurrentAlarmList.Keys.ToArray());

                dgvAlarmView.DataSource = dt;

                dgvAlarmView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 알람 발생된 파트의 이미지
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbAlarmImageView_Paint(object sender, PaintEventArgs e)
        {
            if (SelectedAlarm != null)
            {
                try
                {
                    if (SelectedAlarm != null)
                    {
                        Point pt1 = new Point((int)(SelectedAlarm.ImagePivotX), (int)(SelectedAlarm.ImagePivotY) - 10);
                        Point pt2 = new Point((int)(SelectedAlarm.ImagePivotX), (int)(SelectedAlarm.ImagePivotY) + 10);

                        Point pt3 = new Point((int)(SelectedAlarm.ImagePivotX) - 10, (int)(SelectedAlarm.ImagePivotY));
                        Point pt4 = new Point((int)(SelectedAlarm.ImagePivotX) + 10, (int)(SelectedAlarm.ImagePivotY));

                        Graphics g = e.Graphics;
                        
                        using (Pen redPen = new Pen(Color.Red, 3))
                        {
                            g.DrawLine(redPen, pt1, pt2);
                            g.DrawLine(redPen, pt3, pt4);
                        }
                        
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// 현재 발생되어 있는 알람 중 하나 선택 시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAlarmView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvAlarmView.SelectedRows.Count == 1)
            {
                if (int.TryParse(dgvAlarmView.SelectedRows[0].Cells[0].Value.ToString(), out int code))
                {
                    if (AlarmManager.Instance.CurrentAlarmList.TryGetValue(code, out CAlarm alarm))
                    {
                        SelectedAlarm = alarm;
                    }
                    else
                    {
                        SelectedAlarm = null;
                    }
                }
            }
            else
            {
                SelectedAlarm = null;
            }

            ShowAlarm(SelectedAlarm);
        }
    }
}
