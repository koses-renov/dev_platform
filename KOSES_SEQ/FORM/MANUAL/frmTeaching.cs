﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmTeaching : Form
    {
        Teaching TeachingData;

        Def.Motor SelectedAxis = Def.Motor.Max;
        public frmTeaching()
        {
            InitializeComponent();

            //this.Load += FrmTeaching_Load;
            this.VisibleChanged += FrmTeaching_VisibleChanged;
            SysOperator.Instance.Tenkey.OnPushedTenkeyHomeButton += Tenkey_OnPushedTenkeyHomeButton;
            SysOperator.Instance.Tenkey.OnPushedTenkeyStepButton += Tenkey_OnPushedTenkeyStepButton;
        }

        private void Tenkey_OnPushedTenkeyStepButton(int num, bool isPush)
        {
            if (this.Visible)
            {
                if (isPush)
                    JogCW();
                else
                    JogStop();
            }
        }

        private void Tenkey_OnPushedTenkeyHomeButton(int num, bool isPush)
        {
            if (this.Visible)
            {
                if (isPush)
                    JogCCW();
                else
                    JogStop();
            }
        }

        private void FrmTeaching_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == true)
                {
                    TeachingData = TeachingManager.Instance.TeachingData.Clone() as Teaching;

                    TeachingData.SetTeachingNodeName();

                    pgTeaching.SelectedObject = null;

                    tvTeaching.Nodes.Clear();

                    TreeNode tnRoot = new TreeNode(SysOperator.Instance.defaultInfo.RecipeName);

                    AddSubTreeNode(tnRoot, TeachingData);

                    tvTeaching.Nodes.Add(tnRoot);

                    tvTeaching.NodeMouseClick -= TvTeaching_NodeMouseClick;
                    tvTeaching.NodeMouseClick += TvTeaching_NodeMouseClick;

                    tvTeaching.ExpandAll();

                    pgTeaching.ExpandAllGridItems();
                }

                // Jog 사용 시 숫자 제어 Disable/Enable
                SysOperator.Instance.Tenkey.ChangeDisableNumMode(this.Visible);
            }
            catch
            {

            }
        }                                                                                                               

        private void TvTeaching_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag is TeachingBase teach)
            {
                pgTeaching.SelectedObject = teach;

                pgTeaching.Refresh();

                double position = TeachingManager.Instance.TeachingData.Test.TestPosition.Position;

                pgTeaching.ExpandAllGridItems();
            }
        }

        /// <summary>
        /// 티칭 정보를 재귀적으로 트리노드 생성 함
        /// </summary>
        /// <param name="nodeParent"></param>
        /// <param name="teach"></param>
        private void AddSubTreeNode(TreeNode nodeParent, TeachingBase teach)
        {
            if (teach != null)
            {
                try
                {
                    TreeNode tnSub = new TreeNode(teach.GetNodeName());
                    tnSub.Tag = teach;

                    foreach (MemberInfo info in teach.GetType().GetMembers())
                    {
                        if (info is FieldInfo fInfo && fInfo.GetType().IsClass)
                        {
                            var field = fInfo.GetValue(teach);
                            if (field.GetType().IsArray)
                            {
                                foreach (object subField in field as Array)
                                {
                                    if (subField is TeachingBase)
                                    {
                                        AddSubTreeNode(tnSub, subField as TeachingBase);
                                    }
                                }
                            }
                            else
                            {
                                if (field is TeachingBase)
                                {
                                    if (field != null)
                                    {
                                        AddSubTreeNode(tnSub, field as TeachingBase);
                                    }
                                }
                            }

                        }
                    }
                    nodeParent.Nodes.Add(tnSub);
                }
                catch
                {
                }

            }
        }

        private void btnManualClose_Click(object sender, EventArgs e)
        {
            //Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnRecipeSave_Click(object sender, EventArgs e)
        {
            TeachingManager.Instance.TeachingData = this.TeachingData.Clone() as Teaching;
            TeachingManager.Instance.Save();
        }

        private void pgTeaching_SelectedObjectsChanged(object sender, EventArgs e)
        {

        }

        private void pgTeaching_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            // 모터 포지션인 경우 해당 축을 Attribute에서 정보를 가져옴 (조그 이동 시 사용 예정)
            if (sender is PropertyGrid pg && pg.SelectedObject is TeachingBase teach)
            {
                GridItem item = pg.SelectedGridItem;
                if (item != null && item.PropertyDescriptor != null && teach.GetType().GetProperty(item.PropertyDescriptor.Name) != null)
                {
                    Attribute att = teach.GetType().GetProperty(item.PropertyDescriptor.Name).GetCustomAttribute(typeof(MotorPositionAttribute));
                    if (att is MotorPositionAttribute motorAtt)
                    {
                        SelectedAxis = motorAtt.Motor;

                        lbl_TeachingAxisName.Text = SelectedAxis.ToString();
                    }
                    else
                    {
                        SelectedAxis = Def.Motor.Max;
                        lbl_TeachingAxisName.Text = $"{SelectedAxis.ToString()} (MOTOR NONE SELECTED)";
                    }
                }
                if (item != null && item.Parent is GridItem parent && parent.Value is TeachingPosition positionInfo)
                {
                    Attribute att = teach.GetType().GetProperty(parent.PropertyDescriptor.Name).GetCustomAttribute(typeof(MotorPositionAttribute));
                    if (att is MotorPositionAttribute motorAtt)
                    {
                        SelectedAxis = motorAtt.Motor;

                        lbl_TeachingAxisName.Text = SelectedAxis.ToString();
                    }
                    else
                    {
                        SelectedAxis = Def.Motor.Max;
                        lbl_TeachingAxisName.Text = $"{SelectedAxis.ToString()} (MOTOR NONE SELECTED)";
                    }
                }
            }
        }


        private void btn_TeachingZeroSet_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.SetPosZero(SelectedAxis);
        }

        private void UpdateMotorStatus_Start()
        {
            timer_MotorStatus.Start();
        }
        private void UpdateMotorStatus_Stop()
        {
            timer_MotorStatus.Stop();
        }

        private void timer_MotorStatus_Tick(object sender, EventArgs e)
        {
            UpdateMotorStatus();
        }

        private void UpdateMotorStatus()
        {
            if (SysOperator.Instance.motion.IsOpen() == false)
            {
                return;
            }

            bool b;

            b = SysOperator.Instance.motion.IsServoOn(SelectedAxis);
            led_TeachingServo.On = b;
            

            b = SysOperator.Instance.motion.IsAlarm(SelectedAxis);
            led_TeachingAlarm.On = b;
            

            b = SysOperator.Instance.motion.GetInPositionStatus(SelectedAxis);         
            led_TeachingInpos.On = b;

            b = SysOperator.Instance.motion.IsPlusSensor(SelectedAxis);
            led_TeachingCwLimit.On = b;
            
            b = SysOperator.Instance.motion.IsHomeSensor(SelectedAxis);
            led_TeachingHome.On = b;

            b = SysOperator.Instance.motion.IsMinusSensor(SelectedAxis);
            led_TeachingCcwLimit.On = b;

            string cmdPos = string.Format("{0:0.00000}", SysOperator.Instance.motion.GetCmdPos(SelectedAxis));
            string encPos = string.Format("{0:0.00000}", SysOperator.Instance.motion.GetEncPos(SelectedAxis));
            led_TeachingCommand.Text = cmdPos;
            led_TeachingEncoder.Text = encPos;

            cmdPos = null;
            encPos = null;
        }

        private void frmTeaching_Load(object sender, EventArgs e)
        {
            UpdateMotorStatus_Start();
        }

        private void frmTeaching_FormClosed(object sender, FormClosedEventArgs e)
        {
            UpdateMotorStatus_Stop();
        }

        private bool SelectedAxisCheck()
        {
            bool b = false;
            if ((SelectedAxis < Def.Motor.Max) && (SelectedAxis >= (Def.Motor.Max - Def.Motor.Max)))
            {
                b = true;
            }
            return b;
        }

        private void btn_TeachingServoOn_Click(object sender, EventArgs e)
        {
            if (!SelectedAxisCheck()) return;  
            SysOperator.Instance.motion.ServoOn(SelectedAxis);
        }

        private void btn_TeachingServoOff_Click(object sender, EventArgs e)
        {
            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.ServoOff(SelectedAxis);
        }

        private void btn_TeachingAlarmClear_Click(object sender, EventArgs e)
        {
            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.ResetAlarm(SelectedAxis);
        }

        private void btn_TeachingHome_Click(object sender, EventArgs e)
        {
            //Door Open도 Check하고~~
            //Motor Inter Lock Check 해야 함...

            /////////////////////////////////////////////////////////////////////
            // 알람시 개별 홈 안되도록..
            ////////////////////////////////////////////////////////////////////
            if (SysOperator.Instance.motion.IsAlarm(SelectedAxis))
                return;

            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.Home(SelectedAxis);
        }

        private void btn_TeachingJogCcw_MouseDown(object sender, MouseEventArgs e)
        {
            JogCCW();
        }

        

        private void btn_TeachingJogCcw_MouseUp(object sender, MouseEventArgs e)
        {
            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }

        private void btn_TeachingJogCw_MouseDown(object sender, MouseEventArgs e)
        {
            JogCW();
        }

        private void btn_TeachingJogCw_MouseUp(object sender, MouseEventArgs e)
        {
            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }

        private void btn_TeachingJogRelDot03_Click(object sender, EventArgs e)
        {
            Button obtn = sender as Button;

            string szTemp = "";
            txt_TeachingRelValue.Clear();

            szTemp = obtn.Text;            
            txt_TeachingRelValue.AppendText(szTemp);
        }

        private void btn_TeachingJogRelCcw_Click(object sender, EventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            if (!SelectedAxisCheck()) return;

            if (double.TryParse($"{txt_TeachingRelValue.Text}", out double getdata))
            {
                SysOperator.Instance.motion.MoveRel(SelectedAxis, Def.MotorDir.MINUS, getdata);
            }
                            
            //SysOperator.log.Action($"{logHeader} Move Rel Minus : {dis.ToString()}");
        }

        private void btn_TeachingJogRelCw_Click(object sender, EventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }
            if (!SelectedAxisCheck()) return;

            if (double.TryParse($"{txt_TeachingRelValue.Text}", out double getdata))
            {
                SysOperator.Instance.motion.MoveRel(SelectedAxis, Def.MotorDir.PLUS, getdata);
            }
            //SysOperator.log.Action($"{logHeader} Move Rel Minus : {dis.ToString()}");
        }

        private void btn_TeachingJogAbs_Click(object sender, EventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }
           
            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(SelectedAxis, ref errMsg))
            {
                //SysOperator.MessageBox($"Abs Move", $"Fail To Move Abs (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //SysOperator.log.Action($"{logHeader} Fail To Move Abs (CheckCrashMotor) : {errMsg}");
                return;
            }

            if (!SelectedAxisCheck()) return;
            if (double.TryParse($"{txt_TeachingAbsValue.Text}", out double getdata))
            {
                SysOperator.Instance.motion.MoveAbs(SelectedAxis, getdata);
            }
            //SysOperator.log.Action($"{logHeader} Move Abs : {pos.ToString()}");
        }

        private void btn_TeachingMotorStop_Click(object sender, EventArgs e)
        {
            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }

        private void btn_TeachingJogCw_Click(object sender, EventArgs e)
        {

        }

        private void btn_TeachingAbsGetValue_Click(object sender, EventArgs e)
        {
            txt_TeachingAbsValue.Clear();
            GridItem item = pgTeaching.SelectedGridItem;
            if (item != null && item.Value != null)
            {
                if (double.TryParse($"{item.Value}", out double getdata))
                {
                    txt_TeachingAbsValue.Text = $"{getdata}";
                }
            }
        }

        private void btn_TeachingSetEncValue_Click(object sender, EventArgs e)
        {
            if (!SelectedAxisCheck()) return;
            if (pgTeaching.SelectedObject is TeachingBase teach && double.TryParse(led_TeachingEncoder.Text, out double enc))
            {
                Type type = teach.GetType();
                PropertyInfo[] infos = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                GridItem item = pgTeaching.SelectedGridItem;

                if (item != null && item.PropertyDescriptor != null)
                {
                    if (item.Value is TeachingPosition position)
                    {
                        foreach (var info in infos)
                        {
                            if (info.Name == item.PropertyDescriptor.Name)
                            {
                                position.Position = enc;
                                info.SetValue(teach, position);
                                pgTeaching.Refresh();
                            }
                        }
                    }
                    else if (item.Parent != null && item.Parent.PropertyDescriptor != null && item.Parent.Value is TeachingPosition position2)
                    {
                        foreach (var info in infos)
                        {
                            if (info.Name == item.Parent.PropertyDescriptor.Name)
                            {
                                position2.Position = enc;
                                info.SetValue(teach, position2);
                                pgTeaching.Refresh();
                            }
                        }
                    }
                    else
                    {
                        foreach (var info in infos)
                        {
                            if (info.Name == item.PropertyDescriptor.Name)
                            {
                                info.SetValue(teach, enc);
                                pgTeaching.Refresh();
                            }
                        }
                    }
                }
            }          
        }




        private void JogCCW()
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(SelectedAxis, ref errMsg))
            {
                //Program.MessageBox($"Jog Minus", $"Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //Program.log.Action($"{logHeader} Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}");
                return;
            }

            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.MoveJog(SelectedAxis, Def.MotorDir.MINUS);
            //Program.log.Action($"{logHeader} Jog Minus");
        }

        private void JogCW()
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(SelectedAxis, ref errMsg))
            {
                //Program.MessageBox($"Jog Minus", $"Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //Program.log.Action($"{logHeader} Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}");
                return;
            }

            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.MoveJog(SelectedAxis, Def.MotorDir.PLUS);
            //Program.log.Action($"{logHeader} Jog Minus");
        }

        private void JogStop()
        {
            if (!SelectedAxisCheck()) return;
            SysOperator.Instance.motion.Stop(SelectedAxis);
        }
    }
}
