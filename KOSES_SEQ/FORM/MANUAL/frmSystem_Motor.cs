﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmSystem_Motor : Form
    {
        Def.Motor SelectedAxis = Def.Motor.Max;

        private ucJog Jog = new ucJog();
        public frmSystem_Motor()
        {
            InitializeComponent();
            this.VisibleChanged += FrmMotor_VisibleChanged;
        }
        private void frmMotor_Load(object sender, EventArgs e)
        {
            //MotorParameter
            SetdgvParameterRows();

            //Motor
            SetdgvMotorRows();

            Jog.Dock = DockStyle.Fill;
            gb_Jog.Controls.Add(Jog);
            //pnJog.Controls.Add(Jog);
        }

        public override void Refresh()
        {
            base.Refresh();
            GetMotorParameter(0);
        }

        private void FrmMotor_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == true)
                {
                    Refresh();
                }
            }
            catch
            {

            }
        }

        private void dgvMotor_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMotor.SelectedRows[0].Index < 0) return;
            SelectedAxis = (Def.Motor)dgvMotor.SelectedRows[0].Index;
            GetMotorParameter(dgvMotor.SelectedRows[0].Index);
            Jog.ChangeAxis(SelectedAxis);
        }

        private void SetdgvMotorRows()
        {
            for (Def.Motor motor = 0; motor < Def.Motor.Max; motor++)
            {
                int index = Convert.ToInt32(motor);
                dgvMotor.Rows.Add();
                dgvMotor.Rows[index].Cells[0].Value = index;
                dgvMotor.Rows[index].Cells[1].Value = motor;
            }
        }

        private void SetdgvParameterRows()
        {
            dgvParameter.Rows.Clear();
            dgvParameter.Rows.Add("", "======= MOVE =======");
            dgvParameter.Rows.Add(0, DefString.Acc);
            dgvParameter.Rows.Add(1, DefString.Dec);
            dgvParameter.Rows.Add(2, DefString.Vel);
            dgvParameter.Rows.Add(3, DefString.MotorProfile);
            dgvParameter.Rows.Add(4, DefString.MotorType);

            dgvParameter.Rows.Add("", "======= HOME =======");
            dgvParameter.Rows.Add(0, DefString.HomeMethod);
            dgvParameter.Rows.Add(1, DefString.Z_Phase);
            dgvParameter.Rows.Add(2, DefString.HomeAcc_0);
            dgvParameter.Rows.Add(3, DefString.HomeAcc_1);
            dgvParameter.Rows.Add(4, DefString.HomeAcc_2);
            dgvParameter.Rows.Add(5, DefString.HomeAcc_3);
            dgvParameter.Rows.Add(6, DefString.HomeAcc_4);
            dgvParameter.Rows.Add(7, DefString.HomeVel_0);
            dgvParameter.Rows.Add(8, DefString.HomeVel_1);
            dgvParameter.Rows.Add(9, DefString.HomeVel_2);
            dgvParameter.Rows.Add(10, DefString.HomeVel_3);
            dgvParameter.Rows.Add(11, DefString.HomeVel_4);
            dgvParameter.Rows.Add(12, DefString.HomeDec);
            dgvParameter.Rows.Add(13, DefString.HomeOffset);
            dgvParameter.Rows.Add(14, DefString.HomeClearTime);
            dgvParameter.Rows.Add(15, DefString.HomeDir);

            dgvParameter.Rows.Add("", "======= SETUP =======");
            dgvParameter.Rows.Add(0, DefString.UnitPerPulse);
            dgvParameter.Rows.Add(1, DefString.PulseMode);
            dgvParameter.Rows.Add(2, DefString.EncodeDir);
            dgvParameter.Rows.Add(3, DefString.PlusSignalLevel);
            dgvParameter.Rows.Add(4, DefString.MinusSignalLevel);
            dgvParameter.Rows.Add(5, DefString.HomeSignalLevel);
            dgvParameter.Rows.Add(6, DefString.InposSignalLevel);
            dgvParameter.Rows.Add(7, DefString.AlarmSignalLevel);
            dgvParameter.Rows.Add(8, DefString.ServoSignalLevel);
            dgvParameter.Rows.Add(9, DefString.EmergencySignalLevel);
            dgvParameter.Rows.Add(10, DefString.EncoderCount);
            dgvParameter.Rows.Add(11, DefString.AccUnit);
        }

        private void GetMotorParameter(int axisNo)
        {
            //Section [MOVE]
            dgvParameter.Rows[1].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].acc;
            dgvParameter.Rows[2].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].dec;
            dgvParameter.Rows[3].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].vel;
            dgvParameter.Rows[4].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].motorProfile;
            dgvParameter.Rows[5].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].motorType;

            //Section [HOME]
            dgvParameter.Rows[7].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].homeMethod;
            dgvParameter.Rows[8].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].zPhase;
            dgvParameter.Rows[9].Cells[2].Value  = SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[0];
            dgvParameter.Rows[10].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[1];
            dgvParameter.Rows[11].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[2];
            dgvParameter.Rows[12].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[3];
            dgvParameter.Rows[13].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[4];
            dgvParameter.Rows[14].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[0];
            dgvParameter.Rows[15].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[1];
            dgvParameter.Rows[16].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[2];
            dgvParameter.Rows[17].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[3];
            dgvParameter.Rows[18].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[4];
            dgvParameter.Rows[19].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].homeDec;
            dgvParameter.Rows[20].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].homeOffset;
            dgvParameter.Rows[21].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].homeClearTime;
            dgvParameter.Rows[22].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].homeDir;

            //Section [SETUP]
            dgvParameter.Rows[24].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].unitPerPulse;
            dgvParameter.Rows[25].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].pulseMode;
            dgvParameter.Rows[26].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].encodeDir;
            dgvParameter.Rows[27].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].plusSignalLevel;
            dgvParameter.Rows[28].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].minusSignalLevel;
            dgvParameter.Rows[29].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].homeSignalLevel;
            dgvParameter.Rows[30].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].inposSignalLevel;
            dgvParameter.Rows[31].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].alarmSignalLevel;
            dgvParameter.Rows[32].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].servoSignalLevel;
            dgvParameter.Rows[33].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].emergencySignalLevel;
            dgvParameter.Rows[34].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].encoderCount;
            dgvParameter.Rows[35].Cells[2].Value = SysOperator.Instance.motorPara.para[axisNo].accUnit;
        }

        private void SetMotorParameter(int axisNo)
        {
            //Section [MOVE]
            SysOperator.Instance.motorPara.para[axisNo].acc             = Convert.ToDouble(dgvParameter.Rows[1].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].dec             = Convert.ToDouble(dgvParameter.Rows[2].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].vel             = double.Parse(dgvParameter.Rows[3].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].motorProfile    = Convert.ToInt32(dgvParameter.Rows[4].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].motorType       = int.Parse(dgvParameter.Rows[5].Cells[2].Value.ToString());

            //Section [HOME]
            SysOperator.Instance.motorPara.para[axisNo].homeMethod      = Convert.ToInt32(dgvParameter.Rows[7].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].zPhase          = Convert.ToInt32(dgvParameter.Rows[8].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[0] = Convert.ToDouble(dgvParameter.Rows[9].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[1] = Convert.ToDouble(dgvParameter.Rows[10].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[2] = Convert.ToDouble(dgvParameter.Rows[11].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[3] = Convert.ToDouble(dgvParameter.Rows[12].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeAcc[4] = Convert.ToDouble(dgvParameter.Rows[13].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[0] = Convert.ToDouble(dgvParameter.Rows[14].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[1] = Convert.ToDouble(dgvParameter.Rows[15].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[2] = Convert.ToDouble(dgvParameter.Rows[16].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[3] = Convert.ToDouble(dgvParameter.Rows[17].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].multiHomeVel[4] = Convert.ToDouble(dgvParameter.Rows[18].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].homeDec         = Convert.ToDouble(dgvParameter.Rows[19].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].homeOffset      = Convert.ToDouble(dgvParameter.Rows[20].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].homeClearTime   = Convert.ToDouble(dgvParameter.Rows[21].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].homeDir         = Convert.ToInt32(dgvParameter.Rows[22].Cells[2].Value.ToString());

            //Section [SETUP]
            SysOperator.Instance.motorPara.para[axisNo].unitPerPulse    = Convert.ToDouble(dgvParameter.Rows[24].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].pulseMode       = Convert.ToInt32(dgvParameter.Rows[25].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].encodeDir       = Convert.ToInt32(dgvParameter.Rows[26].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].plusSignalLevel = Convert.ToInt32(dgvParameter.Rows[27].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].minusSignalLevel= Convert.ToInt32(dgvParameter.Rows[28].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].homeSignalLevel = Convert.ToInt32(dgvParameter.Rows[29].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].inposSignalLevel= Convert.ToInt32(dgvParameter.Rows[30].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].alarmSignalLevel= Convert.ToInt32(dgvParameter.Rows[31].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].servoSignalLevel= Convert.ToInt32(dgvParameter.Rows[32].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].emergencySignalLevel= Convert.ToInt32(dgvParameter.Rows[33].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].encoderCount        = Convert.ToInt32(dgvParameter.Rows[34].Cells[2].Value.ToString());
            SysOperator.Instance.motorPara.para[axisNo].accUnit             = Convert.ToInt32(dgvParameter.Rows[35].Cells[2].Value.ToString());
        }


        public void DataSave()
        {
            if (FRM.Msg.Confirm($"MOTOR PARA DATA를 저장 하시겠습니까?"))
            {
                SetMotorParameter(dgvMotor.SelectedRows[0].Index);
                SysOperator.Instance.motorPara.para[dgvMotor.SelectedRows[0].Index].Save();
                SysOperator.Instance.motion.SetParameter(SelectedAxis);

                //Program.LogWrite("MOTOR PARA SAVED", this.motor.ToString(), DefColor.LightGreen, LibDef.LogType.Data);
               //Program.NoticeStart($"MOTOR DATA SAVED", Def.NoticeMediumFontSize, DefColor.MsgNormal, 5000);
            }
        }

        public void DataLoad()
        {
            if (FRM.Msg.Confirm($"MOTOR PARA DATA를 LOAD 하시겠습니까?"))
            {
                //Program.motorPara.para[(int)this.motor].Load();
                //MotorPara_MemoryToTable(this.motor);

                //Program.LogWrite("MOTOR PARA LOADED", this.motor.ToString(), DefColor.LightGreen, LibDef.LogType.Data);
                //Program.NoticeStart($"MOTOR DATA LOADED", Def.NoticeMediumFontSize, DefColor.MsgNormal, 5000);
            }
        }
    }
}
