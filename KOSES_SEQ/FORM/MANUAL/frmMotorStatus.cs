﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmMotorStatus : Form
    {
        bool isIncrease;
        int speedRate = 0;
        Def.Motor selectedAxis;
        Timer tmrVel = new Timer();

        public frmMotorStatus()
        {
            InitializeComponent();
            tmrVel.Interval = 100;
            tmrVel.Tick += TmrVel_Tick;
        }

        private void TmrVel_Tick(object sender, EventArgs e)
        {
            if (tmrVel.Interval < 10) tmrVel.Interval = 10;
            tmrVel.Interval -= 5;
            if (isIncrease)
                lbSpeedRate.Text = (Convert.ToInt32(lbSpeedRate.Text) + 1).ToString();
            else
                lbSpeedRate.Text = (Convert.ToInt32(lbSpeedRate.Text) - 1).ToString();
        }

        private void frmMotor_Load(object sender, EventArgs e)
        {
            SetdgvMotorStatus();
        }

        private void SetdgvMotorStatus()
        {
            dgvMotorStatus1.Rows.Clear();

            for(Def.Motor motor = 0; motor < Def.Motor.Max; motor++)
            {
                dgvMotorStatus1.Rows.Add(Convert.ToInt32(motor), motor.ToString(), "0.000", "S", "A", "I", "H");
            }
        }

        private void frmMotor_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == true)
                {
                    tmrMotorStatus.Start();
                }
                else
                {
                    tmrMotorStatus.Stop();
                }
            }
            catch
            {

            }
        }

        private void tmrMotorStatus_Tick(object sender, EventArgs e)
        {
            for (Def.Motor motor = 0; motor < Def.Motor.Max; motor++)
            {
                int motorIndex = Convert.ToInt32(motor);

                UpdatePosition(motorIndex, SysOperator.Instance.motion.GetCmdPos(motor).ToString("F3"));
                UpdateServo(motorIndex, SysOperator.Instance.motion.IsServoOn(motor));
                UpdateAlarm(motorIndex, SysOperator.Instance.motion.IsAlarm(motor));
                UpdateInPosition(motorIndex, SysOperator.Instance.motion.GetInPositionStatus(motor));
                UpdateHome(motorIndex, SysOperator.Instance.motion.IsHomeCompleted(motor));
            }
        }
        private void UpdatePosition(int motorIndex, string position)
        {
            if (dgvMotorStatus1.Rows[motorIndex].Cells[2].Value?.ToString() == position) return;

            dgvMotorStatus1.Rows[motorIndex].Cells[2].Value = position;
        }

        private void UpdateServo(int motorIndex, bool isServoOn)
        {
            Color color = isServoOn ? Color.Lime : Color.Black;
            if (dgvMotorStatus1.Rows[motorIndex].Cells[3].Style?.BackColor == color) return;
            dgvMotorStatus1.Rows[motorIndex].Cells[3].Style.SelectionBackColor = dgvMotorStatus1.Rows[motorIndex].Cells[3].Style.BackColor = color;
            dgvMotorStatus1.Rows[motorIndex].Cells[3].Style.SelectionForeColor = dgvMotorStatus1.Rows[motorIndex].Cells[3].Style.ForeColor = isServoOn ? Color.Black : Color.Lime;
            //string onOff = isServoOn ? "ON" : "OFF";
            //if (dgvMotorStatus.Rows[motorIndex].Cells[3].Value?.ToString() == onOff) return;

            //dgvMotorStatus.Rows[motorIndex].Cells[3].Value = onOff;
            //dgvMotorStatus.Rows[motorIndex].Cells[3].Style.SelectionBackColor = dgvMotorStatus.Rows[motorIndex].Cells[3].Style.BackColor = isServoOn ? Color.Lime : Color.DarkGreen;
        }

        private void UpdateAlarm(int motorIndex, bool isAlarm)
        {
            Color color = isAlarm ? Color.Red : Color.Black;
            if (dgvMotorStatus1.Rows[motorIndex].Cells[4].Style?.BackColor == color) return;
            dgvMotorStatus1.Rows[motorIndex].Cells[4].Style.SelectionBackColor = dgvMotorStatus1.Rows[motorIndex].Cells[4].Style.BackColor = color;
            dgvMotorStatus1.Rows[motorIndex].Cells[4].Style.SelectionForeColor = dgvMotorStatus1.Rows[motorIndex].Cells[4].Style.ForeColor = isAlarm ? Color.Black : Color.Red;
            //string OnOff = isAlarm ? "ON" : "OFF";
            //if (dgvMotorStatus.Rows[motorIndex].Cells[4].Value?.ToString() == OnOff) return;

            //dgvMotorStatus.Rows[motorIndex].Cells[4].Value = OnOff;
            //dgvMotorStatus.Rows[motorIndex].Cells[4].Style.SelectionBackColor = dgvMotorStatus.Rows[motorIndex].Cells[4].Style.BackColor = isAlarm ? Color.Red : Color.DarkRed;
        }

        private void UpdateInPosition(int motorIndex, bool isInPos)
        {
            Color color = isInPos ? Color.FromArgb(30, 144, 255) : Color.Black;
            if (dgvMotorStatus1.Rows[motorIndex].Cells[5].Style?.BackColor == color) return;
            dgvMotorStatus1.Rows[motorIndex].Cells[5].Style.SelectionBackColor = dgvMotorStatus1.Rows[motorIndex].Cells[5].Style.BackColor = color;
            dgvMotorStatus1.Rows[motorIndex].Cells[5].Style.SelectionForeColor = dgvMotorStatus1.Rows[motorIndex].Cells[5].Style.ForeColor = isInPos ? Color.Black : Color.FromArgb(30, 144, 255);
            //string onOff = isInPos ? "ON" : "OFF";
            //if (dgvMotorStatus.Rows[motorIndex].Cells[5].Value?.ToString() == onOff) return;

            //dgvMotorStatus.Rows[motorIndex].Cells[5].Value = onOff;
            //dgvMotorStatus.Rows[motorIndex].Cells[5].Style.SelectionBackColor = dgvMotorStatus.Rows[motorIndex].Cells[5].Style.BackColor = isInPos ? Color.Blue : Color.DarkBlue;
        }
        private void UpdateHome(int motorIndex, bool isHome)
        {
            Color color = isHome ? Color.Yellow : Color.Black;
            if (dgvMotorStatus1.Rows[motorIndex].Cells[6].Style?.BackColor == color) return;
            dgvMotorStatus1.Rows[motorIndex].Cells[6].Style.SelectionBackColor = dgvMotorStatus1.Rows[motorIndex].Cells[6].Style.BackColor = color;
            dgvMotorStatus1.Rows[motorIndex].Cells[6].Style.SelectionForeColor = dgvMotorStatus1.Rows[motorIndex].Cells[6].Style.ForeColor = isHome ? Color.Black : Color.Yellow;
            //string onOff = isHome ? "ON" : "OFF";
            //if (dgvMotorStatus.Rows[motorIndex].Cells[6].Value?.ToString() == onOff) return;

            //dgvMotorStatus.Rows[motorIndex].Cells[6].Value = onOff;
            //dgvMotorStatus.Rows[motorIndex].Cells[6].Style.SelectionBackColor = dgvMotorStatus.Rows[motorIndex].Cells[6].Style.BackColor = isHome ? Color.Yellow : Color.Goldenrod;
        }

        private void dgvMotorStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedAxis = (Def.Motor)e.RowIndex;
            switch (e.ColumnIndex)
            {
                //Servo On Off
                case 3:
                    if (!SysOperator.Instance.motion.IsServoOn(selectedAxis))
                        SysOperator.Instance.motion.ServoOn(selectedAxis);
                    else
                        SysOperator.Instance.motion.ServoOff(selectedAxis);
                    break;
                //Alarm Reset
                case 4:
                    if (SysOperator.Instance.motion.IsAlarm(selectedAxis))
                        SysOperator.Instance.motion.ResetAlarm(selectedAxis);
                    break;
                //Homing
                case 6:
                    if (!SysOperator.Instance.motion.IsHomeCompleted(selectedAxis))
                        SysOperator.Instance.motion.Home(selectedAxis);
                    break;
            }
        }

        private void btn_AllServoOn_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.ServoOnAll();
        }

        private void btn_AllServoOff_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.ServoOffAll();
        }

        private void btn_AllAlarmClear_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.AlarmClearAll();
        }

        private void btn_AllStop_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.motion.StopAll();
        }

        private void btnDecrease_MouseDown(object sender, MouseEventArgs e)
        {
            isIncrease = false;
            tmrVel.Start();
        }

        private void btnIncrease_MouseDown(object sender, MouseEventArgs e)
        {
            isIncrease = true;
            tmrVel.Start();
        }

        private void btnDecrease_MouseUp(object sender, MouseEventArgs e)
        {
            tmrVel.Interval = 100;
            tmrVel.Stop();
        }

        private void btnIncrease_MouseUp(object sender, MouseEventArgs e)
        {
            tmrVel.Interval = 100;
            tmrVel.Stop();
        }

        private void btnMinus_MouseDown(object sender, MouseEventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(selectedAxis, ref errMsg))
            {
                //Program.MessageBox($"Jog Minus", $"Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //Program.log.Action($"{logHeader} Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}");
                return;
            }

            SysOperator.Instance.motion.MoveJog(selectedAxis, Def.MotorDir.MINUS, speedRate);
        }

        private void btnPlus_MouseDown(object sender, MouseEventArgs e)
        {
            Def.Code code;
            code = SysOperator.CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return;
            }

            // 모터 인터락
            string errMsg = "";
            if (!SysOperator.Instance.motion.CheckCrashMotor(selectedAxis, ref errMsg))
            {
                //Program.MessageBox($"Jog Minus", $"Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}", DefColor.LightRed);
                //Program.log.Action($"{logHeader} Fail To Move Jog (-) (CheckCrashMotor) : {errMsg}");
                return;
            }

            SysOperator.Instance.motion.MoveJog(selectedAxis, Def.MotorDir.PLUS, speedRate);
            //Program.log.Action($"{logHeader} Jog Minus");
        }

        private void btnMinus_MouseUp(object sender, MouseEventArgs e)
        {
            SysOperator.Instance.motion.Stop(selectedAxis);
        }

        private void btnPlus_MouseUp(object sender, MouseEventArgs e)
        {
            SysOperator.Instance.motion.Stop(selectedAxis);
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            speedRate = Convert.ToInt32(lbSpeedRate.Text);
        }
    }
}
