﻿namespace KOSES_SEQ.OP
{
    partial class frmSystem_TowerLamp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panSystemTowerLamp = new System.Windows.Forms.Panel();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.kosesGroupBox6 = new KosesControl.Controls.KosesGroupBox();
            this.kosesGroupBox5 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TowerLampDoorOpenGreenBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampDoorOpenGreen = new KosesControl.Controls.LedButton();
            this.btn_TowerLampDoorOpenYellowBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampDoorOpenYellow = new KosesControl.Controls.LedButton();
            this.btn_TowerLampDoorOpenRedBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampDoorOpenRed = new KosesControl.Controls.LedButton();
            this.kosesGroupBox4 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TowerLampErrorBuzzer = new KosesControl.Controls.LedButton();
            this.btn_TowerLampErrorGreenBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampErrorGreen = new KosesControl.Controls.LedButton();
            this.btn_TowerLampErrorYellowBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampErrorYellow = new KosesControl.Controls.LedButton();
            this.btn_TowerLampErrorRedBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampErrorRed = new KosesControl.Controls.LedButton();
            this.kosesGroupBox3 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TowerLampStopGreenBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampStopGreen = new KosesControl.Controls.LedButton();
            this.btn_TowerLampStopYellowBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampStopYellow = new KosesControl.Controls.LedButton();
            this.btn_TowerLampStopRedBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampStopRed = new KosesControl.Controls.LedButton();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TowerLampRunGreenBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampRunGreen = new KosesControl.Controls.LedButton();
            this.btn_TowerLampRunYellowBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampRunYellow = new KosesControl.Controls.LedButton();
            this.btn_TowerLampRunRedBlink = new KosesControl.Controls.LedButton();
            this.btn_TowerLampRunRed = new KosesControl.Controls.LedButton();
            this.panSystemTowerLamp.SuspendLayout();
            this.kosesGroupBox1.SuspendLayout();
            this.kosesGroupBox5.SuspendLayout();
            this.kosesGroupBox4.SuspendLayout();
            this.kosesGroupBox3.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panSystemTowerLamp
            // 
            this.panSystemTowerLamp.BackColor = System.Drawing.Color.Transparent;
            this.panSystemTowerLamp.Controls.Add(this.kosesGroupBox1);
            this.panSystemTowerLamp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panSystemTowerLamp.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panSystemTowerLamp.Location = new System.Drawing.Point(0, 0);
            this.panSystemTowerLamp.Name = "panSystemTowerLamp";
            this.panSystemTowerLamp.Padding = new System.Windows.Forms.Padding(1);
            this.panSystemTowerLamp.Size = new System.Drawing.Size(1125, 875);
            this.panSystemTowerLamp.TabIndex = 953;
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox1.Controls.Add(this.kosesGroupBox6);
            this.kosesGroupBox1.Controls.Add(this.kosesGroupBox5);
            this.kosesGroupBox1.Controls.Add(this.kosesGroupBox4);
            this.kosesGroupBox1.Controls.Add(this.kosesGroupBox3);
            this.kosesGroupBox1.Controls.Add(this.kosesGroupBox2);
            this.kosesGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kosesGroupBox1.Location = new System.Drawing.Point(1, 1);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Radius = 0;
            this.kosesGroupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.kosesGroupBox1.Size = new System.Drawing.Size(1123, 873);
            this.kosesGroupBox1.TabIndex = 0;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "TOWER LAMP";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // kosesGroupBox6
            // 
            this.kosesGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox6.Location = new System.Drawing.Point(904, 45);
            this.kosesGroupBox6.Name = "kosesGroupBox6";
            this.kosesGroupBox6.Radius = 0;
            this.kosesGroupBox6.Size = new System.Drawing.Size(200, 655);
            this.kosesGroupBox6.TabIndex = 4;
            this.kosesGroupBox6.TabStop = false;
            this.kosesGroupBox6.Text = "ALL OFF";
            this.kosesGroupBox6.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox6.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox6.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox6.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox6.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // kosesGroupBox5
            // 
            this.kosesGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox5.Controls.Add(this.btn_TowerLampDoorOpenGreenBlink);
            this.kosesGroupBox5.Controls.Add(this.btn_TowerLampDoorOpenGreen);
            this.kosesGroupBox5.Controls.Add(this.btn_TowerLampDoorOpenYellowBlink);
            this.kosesGroupBox5.Controls.Add(this.btn_TowerLampDoorOpenYellow);
            this.kosesGroupBox5.Controls.Add(this.btn_TowerLampDoorOpenRedBlink);
            this.kosesGroupBox5.Controls.Add(this.btn_TowerLampDoorOpenRed);
            this.kosesGroupBox5.Location = new System.Drawing.Point(684, 45);
            this.kosesGroupBox5.Name = "kosesGroupBox5";
            this.kosesGroupBox5.Radius = 0;
            this.kosesGroupBox5.Size = new System.Drawing.Size(200, 655);
            this.kosesGroupBox5.TabIndex = 3;
            this.kosesGroupBox5.TabStop = false;
            this.kosesGroupBox5.Text = "DOOR OPEN";
            this.kosesGroupBox5.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox5.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox5.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox5.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox5.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TowerLampDoorOpenGreenBlink
            // 
            this.btn_TowerLampDoorOpenGreenBlink.Bold = false;
            this.btn_TowerLampDoorOpenGreenBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampDoorOpenGreenBlink.FontSize = 0F;
            this.btn_TowerLampDoorOpenGreenBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampDoorOpenGreenBlink.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampDoorOpenGreenBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampDoorOpenGreenBlink.LedWidth = 23;
            this.btn_TowerLampDoorOpenGreenBlink.Location = new System.Drawing.Point(23, 410);
            this.btn_TowerLampDoorOpenGreenBlink.Name = "btn_TowerLampDoorOpenGreenBlink";
            this.btn_TowerLampDoorOpenGreenBlink.On = false;
            this.btn_TowerLampDoorOpenGreenBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampDoorOpenGreenBlink.TabIndex = 24;
            this.btn_TowerLampDoorOpenGreenBlink.Text = "BLINK(G)";
            this.btn_TowerLampDoorOpenGreenBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampDoorOpenGreen
            // 
            this.btn_TowerLampDoorOpenGreen.Bold = false;
            this.btn_TowerLampDoorOpenGreen.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampDoorOpenGreen.FontSize = 0F;
            this.btn_TowerLampDoorOpenGreen.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampDoorOpenGreen.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampDoorOpenGreen.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampDoorOpenGreen.LedWidth = 23;
            this.btn_TowerLampDoorOpenGreen.Location = new System.Drawing.Point(23, 360);
            this.btn_TowerLampDoorOpenGreen.Name = "btn_TowerLampDoorOpenGreen";
            this.btn_TowerLampDoorOpenGreen.On = true;
            this.btn_TowerLampDoorOpenGreen.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampDoorOpenGreen.TabIndex = 23;
            this.btn_TowerLampDoorOpenGreen.Text = "GREEN";
            this.btn_TowerLampDoorOpenGreen.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampDoorOpenYellowBlink
            // 
            this.btn_TowerLampDoorOpenYellowBlink.Bold = false;
            this.btn_TowerLampDoorOpenYellowBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampDoorOpenYellowBlink.FontSize = 0F;
            this.btn_TowerLampDoorOpenYellowBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampDoorOpenYellowBlink.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampDoorOpenYellowBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampDoorOpenYellowBlink.LedWidth = 23;
            this.btn_TowerLampDoorOpenYellowBlink.Location = new System.Drawing.Point(23, 246);
            this.btn_TowerLampDoorOpenYellowBlink.Name = "btn_TowerLampDoorOpenYellowBlink";
            this.btn_TowerLampDoorOpenYellowBlink.On = false;
            this.btn_TowerLampDoorOpenYellowBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampDoorOpenYellowBlink.TabIndex = 22;
            this.btn_TowerLampDoorOpenYellowBlink.Text = "BLINK(Y)";
            this.btn_TowerLampDoorOpenYellowBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampDoorOpenYellow
            // 
            this.btn_TowerLampDoorOpenYellow.Bold = false;
            this.btn_TowerLampDoorOpenYellow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampDoorOpenYellow.FontSize = 0F;
            this.btn_TowerLampDoorOpenYellow.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampDoorOpenYellow.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampDoorOpenYellow.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampDoorOpenYellow.LedWidth = 23;
            this.btn_TowerLampDoorOpenYellow.Location = new System.Drawing.Point(23, 196);
            this.btn_TowerLampDoorOpenYellow.Name = "btn_TowerLampDoorOpenYellow";
            this.btn_TowerLampDoorOpenYellow.On = true;
            this.btn_TowerLampDoorOpenYellow.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampDoorOpenYellow.TabIndex = 21;
            this.btn_TowerLampDoorOpenYellow.Text = "YELLOW";
            this.btn_TowerLampDoorOpenYellow.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampDoorOpenRedBlink
            // 
            this.btn_TowerLampDoorOpenRedBlink.Bold = false;
            this.btn_TowerLampDoorOpenRedBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampDoorOpenRedBlink.FontSize = 0F;
            this.btn_TowerLampDoorOpenRedBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampDoorOpenRedBlink.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampDoorOpenRedBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampDoorOpenRedBlink.LedWidth = 23;
            this.btn_TowerLampDoorOpenRedBlink.Location = new System.Drawing.Point(23, 94);
            this.btn_TowerLampDoorOpenRedBlink.Name = "btn_TowerLampDoorOpenRedBlink";
            this.btn_TowerLampDoorOpenRedBlink.On = false;
            this.btn_TowerLampDoorOpenRedBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampDoorOpenRedBlink.TabIndex = 20;
            this.btn_TowerLampDoorOpenRedBlink.Text = "BLINK(R)";
            this.btn_TowerLampDoorOpenRedBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampDoorOpenRed
            // 
            this.btn_TowerLampDoorOpenRed.Bold = false;
            this.btn_TowerLampDoorOpenRed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampDoorOpenRed.FontSize = 0F;
            this.btn_TowerLampDoorOpenRed.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampDoorOpenRed.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampDoorOpenRed.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampDoorOpenRed.LedWidth = 23;
            this.btn_TowerLampDoorOpenRed.Location = new System.Drawing.Point(23, 44);
            this.btn_TowerLampDoorOpenRed.Name = "btn_TowerLampDoorOpenRed";
            this.btn_TowerLampDoorOpenRed.On = true;
            this.btn_TowerLampDoorOpenRed.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampDoorOpenRed.TabIndex = 19;
            this.btn_TowerLampDoorOpenRed.Text = "RED";
            this.btn_TowerLampDoorOpenRed.UseVisualStyleBackColor = true;
            // 
            // kosesGroupBox4
            // 
            this.kosesGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorBuzzer);
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorGreenBlink);
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorGreen);
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorYellowBlink);
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorYellow);
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorRedBlink);
            this.kosesGroupBox4.Controls.Add(this.btn_TowerLampErrorRed);
            this.kosesGroupBox4.Location = new System.Drawing.Point(464, 45);
            this.kosesGroupBox4.Name = "kosesGroupBox4";
            this.kosesGroupBox4.Radius = 0;
            this.kosesGroupBox4.Size = new System.Drawing.Size(200, 655);
            this.kosesGroupBox4.TabIndex = 2;
            this.kosesGroupBox4.TabStop = false;
            this.kosesGroupBox4.Text = "ERROR";
            this.kosesGroupBox4.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox4.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox4.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox4.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox4.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TowerLampErrorBuzzer
            // 
            this.btn_TowerLampErrorBuzzer.Bold = false;
            this.btn_TowerLampErrorBuzzer.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorBuzzer.FontSize = 0F;
            this.btn_TowerLampErrorBuzzer.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorBuzzer.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampErrorBuzzer.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorBuzzer.LedWidth = 23;
            this.btn_TowerLampErrorBuzzer.Location = new System.Drawing.Point(23, 499);
            this.btn_TowerLampErrorBuzzer.Name = "btn_TowerLampErrorBuzzer";
            this.btn_TowerLampErrorBuzzer.On = true;
            this.btn_TowerLampErrorBuzzer.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorBuzzer.TabIndex = 19;
            this.btn_TowerLampErrorBuzzer.Text = "BUZZER";
            this.btn_TowerLampErrorBuzzer.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampErrorGreenBlink
            // 
            this.btn_TowerLampErrorGreenBlink.Bold = false;
            this.btn_TowerLampErrorGreenBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorGreenBlink.FontSize = 0F;
            this.btn_TowerLampErrorGreenBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorGreenBlink.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampErrorGreenBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorGreenBlink.LedWidth = 23;
            this.btn_TowerLampErrorGreenBlink.Location = new System.Drawing.Point(23, 410);
            this.btn_TowerLampErrorGreenBlink.Name = "btn_TowerLampErrorGreenBlink";
            this.btn_TowerLampErrorGreenBlink.On = false;
            this.btn_TowerLampErrorGreenBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorGreenBlink.TabIndex = 18;
            this.btn_TowerLampErrorGreenBlink.Text = "BLINK(G)";
            this.btn_TowerLampErrorGreenBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampErrorGreen
            // 
            this.btn_TowerLampErrorGreen.Bold = false;
            this.btn_TowerLampErrorGreen.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorGreen.FontSize = 0F;
            this.btn_TowerLampErrorGreen.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorGreen.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampErrorGreen.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorGreen.LedWidth = 23;
            this.btn_TowerLampErrorGreen.Location = new System.Drawing.Point(23, 360);
            this.btn_TowerLampErrorGreen.Name = "btn_TowerLampErrorGreen";
            this.btn_TowerLampErrorGreen.On = true;
            this.btn_TowerLampErrorGreen.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorGreen.TabIndex = 17;
            this.btn_TowerLampErrorGreen.Text = "GREEN";
            this.btn_TowerLampErrorGreen.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampErrorYellowBlink
            // 
            this.btn_TowerLampErrorYellowBlink.Bold = false;
            this.btn_TowerLampErrorYellowBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorYellowBlink.FontSize = 0F;
            this.btn_TowerLampErrorYellowBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorYellowBlink.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampErrorYellowBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorYellowBlink.LedWidth = 23;
            this.btn_TowerLampErrorYellowBlink.Location = new System.Drawing.Point(23, 246);
            this.btn_TowerLampErrorYellowBlink.Name = "btn_TowerLampErrorYellowBlink";
            this.btn_TowerLampErrorYellowBlink.On = false;
            this.btn_TowerLampErrorYellowBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorYellowBlink.TabIndex = 16;
            this.btn_TowerLampErrorYellowBlink.Text = "BLINK(Y)";
            this.btn_TowerLampErrorYellowBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampErrorYellow
            // 
            this.btn_TowerLampErrorYellow.Bold = false;
            this.btn_TowerLampErrorYellow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorYellow.FontSize = 0F;
            this.btn_TowerLampErrorYellow.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorYellow.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampErrorYellow.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorYellow.LedWidth = 23;
            this.btn_TowerLampErrorYellow.Location = new System.Drawing.Point(23, 196);
            this.btn_TowerLampErrorYellow.Name = "btn_TowerLampErrorYellow";
            this.btn_TowerLampErrorYellow.On = true;
            this.btn_TowerLampErrorYellow.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorYellow.TabIndex = 15;
            this.btn_TowerLampErrorYellow.Text = "YELLOW";
            this.btn_TowerLampErrorYellow.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampErrorRedBlink
            // 
            this.btn_TowerLampErrorRedBlink.Bold = false;
            this.btn_TowerLampErrorRedBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorRedBlink.FontSize = 0F;
            this.btn_TowerLampErrorRedBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorRedBlink.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampErrorRedBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorRedBlink.LedWidth = 23;
            this.btn_TowerLampErrorRedBlink.Location = new System.Drawing.Point(23, 94);
            this.btn_TowerLampErrorRedBlink.Name = "btn_TowerLampErrorRedBlink";
            this.btn_TowerLampErrorRedBlink.On = false;
            this.btn_TowerLampErrorRedBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorRedBlink.TabIndex = 14;
            this.btn_TowerLampErrorRedBlink.Text = "BLINK(R)";
            this.btn_TowerLampErrorRedBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampErrorRed
            // 
            this.btn_TowerLampErrorRed.Bold = false;
            this.btn_TowerLampErrorRed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampErrorRed.FontSize = 0F;
            this.btn_TowerLampErrorRed.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampErrorRed.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampErrorRed.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampErrorRed.LedWidth = 23;
            this.btn_TowerLampErrorRed.Location = new System.Drawing.Point(23, 44);
            this.btn_TowerLampErrorRed.Name = "btn_TowerLampErrorRed";
            this.btn_TowerLampErrorRed.On = true;
            this.btn_TowerLampErrorRed.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampErrorRed.TabIndex = 13;
            this.btn_TowerLampErrorRed.Text = "RED";
            this.btn_TowerLampErrorRed.UseVisualStyleBackColor = true;
            // 
            // kosesGroupBox3
            // 
            this.kosesGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox3.Controls.Add(this.btn_TowerLampStopGreenBlink);
            this.kosesGroupBox3.Controls.Add(this.btn_TowerLampStopGreen);
            this.kosesGroupBox3.Controls.Add(this.btn_TowerLampStopYellowBlink);
            this.kosesGroupBox3.Controls.Add(this.btn_TowerLampStopYellow);
            this.kosesGroupBox3.Controls.Add(this.btn_TowerLampStopRedBlink);
            this.kosesGroupBox3.Controls.Add(this.btn_TowerLampStopRed);
            this.kosesGroupBox3.Location = new System.Drawing.Point(244, 45);
            this.kosesGroupBox3.Name = "kosesGroupBox3";
            this.kosesGroupBox3.Radius = 0;
            this.kosesGroupBox3.Size = new System.Drawing.Size(200, 655);
            this.kosesGroupBox3.TabIndex = 1;
            this.kosesGroupBox3.TabStop = false;
            this.kosesGroupBox3.Text = "STOP";
            this.kosesGroupBox3.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox3.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox3.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox3.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox3.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TowerLampStopGreenBlink
            // 
            this.btn_TowerLampStopGreenBlink.Bold = false;
            this.btn_TowerLampStopGreenBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampStopGreenBlink.FontSize = 0F;
            this.btn_TowerLampStopGreenBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampStopGreenBlink.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampStopGreenBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampStopGreenBlink.LedWidth = 23;
            this.btn_TowerLampStopGreenBlink.Location = new System.Drawing.Point(22, 410);
            this.btn_TowerLampStopGreenBlink.Name = "btn_TowerLampStopGreenBlink";
            this.btn_TowerLampStopGreenBlink.On = false;
            this.btn_TowerLampStopGreenBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampStopGreenBlink.TabIndex = 12;
            this.btn_TowerLampStopGreenBlink.Text = "BLINK(G)";
            this.btn_TowerLampStopGreenBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampStopGreen
            // 
            this.btn_TowerLampStopGreen.Bold = false;
            this.btn_TowerLampStopGreen.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampStopGreen.FontSize = 0F;
            this.btn_TowerLampStopGreen.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampStopGreen.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampStopGreen.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampStopGreen.LedWidth = 23;
            this.btn_TowerLampStopGreen.Location = new System.Drawing.Point(22, 360);
            this.btn_TowerLampStopGreen.Name = "btn_TowerLampStopGreen";
            this.btn_TowerLampStopGreen.On = true;
            this.btn_TowerLampStopGreen.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampStopGreen.TabIndex = 11;
            this.btn_TowerLampStopGreen.Text = "GREEN";
            this.btn_TowerLampStopGreen.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampStopYellowBlink
            // 
            this.btn_TowerLampStopYellowBlink.Bold = false;
            this.btn_TowerLampStopYellowBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampStopYellowBlink.FontSize = 0F;
            this.btn_TowerLampStopYellowBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampStopYellowBlink.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampStopYellowBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampStopYellowBlink.LedWidth = 23;
            this.btn_TowerLampStopYellowBlink.Location = new System.Drawing.Point(22, 246);
            this.btn_TowerLampStopYellowBlink.Name = "btn_TowerLampStopYellowBlink";
            this.btn_TowerLampStopYellowBlink.On = false;
            this.btn_TowerLampStopYellowBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampStopYellowBlink.TabIndex = 10;
            this.btn_TowerLampStopYellowBlink.Text = "BLINK(Y)";
            this.btn_TowerLampStopYellowBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampStopYellow
            // 
            this.btn_TowerLampStopYellow.Bold = false;
            this.btn_TowerLampStopYellow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampStopYellow.FontSize = 0F;
            this.btn_TowerLampStopYellow.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampStopYellow.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampStopYellow.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampStopYellow.LedWidth = 23;
            this.btn_TowerLampStopYellow.Location = new System.Drawing.Point(22, 196);
            this.btn_TowerLampStopYellow.Name = "btn_TowerLampStopYellow";
            this.btn_TowerLampStopYellow.On = true;
            this.btn_TowerLampStopYellow.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampStopYellow.TabIndex = 9;
            this.btn_TowerLampStopYellow.Text = "YELLOW";
            this.btn_TowerLampStopYellow.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampStopRedBlink
            // 
            this.btn_TowerLampStopRedBlink.Bold = false;
            this.btn_TowerLampStopRedBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampStopRedBlink.FontSize = 0F;
            this.btn_TowerLampStopRedBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampStopRedBlink.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampStopRedBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampStopRedBlink.LedWidth = 23;
            this.btn_TowerLampStopRedBlink.Location = new System.Drawing.Point(22, 94);
            this.btn_TowerLampStopRedBlink.Name = "btn_TowerLampStopRedBlink";
            this.btn_TowerLampStopRedBlink.On = false;
            this.btn_TowerLampStopRedBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampStopRedBlink.TabIndex = 8;
            this.btn_TowerLampStopRedBlink.Text = "BLINK(R)";
            this.btn_TowerLampStopRedBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampStopRed
            // 
            this.btn_TowerLampStopRed.Bold = false;
            this.btn_TowerLampStopRed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampStopRed.FontSize = 0F;
            this.btn_TowerLampStopRed.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampStopRed.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampStopRed.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampStopRed.LedWidth = 23;
            this.btn_TowerLampStopRed.Location = new System.Drawing.Point(22, 44);
            this.btn_TowerLampStopRed.Name = "btn_TowerLampStopRed";
            this.btn_TowerLampStopRed.On = true;
            this.btn_TowerLampStopRed.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampStopRed.TabIndex = 7;
            this.btn_TowerLampStopRed.Text = "RED";
            this.btn_TowerLampStopRed.UseVisualStyleBackColor = true;
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox2.Controls.Add(this.btn_TowerLampRunGreenBlink);
            this.kosesGroupBox2.Controls.Add(this.btn_TowerLampRunGreen);
            this.kosesGroupBox2.Controls.Add(this.btn_TowerLampRunYellowBlink);
            this.kosesGroupBox2.Controls.Add(this.btn_TowerLampRunYellow);
            this.kosesGroupBox2.Controls.Add(this.btn_TowerLampRunRedBlink);
            this.kosesGroupBox2.Controls.Add(this.btn_TowerLampRunRed);
            this.kosesGroupBox2.Location = new System.Drawing.Point(24, 45);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Radius = 0;
            this.kosesGroupBox2.Size = new System.Drawing.Size(200, 655);
            this.kosesGroupBox2.TabIndex = 0;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "RUN";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TowerLampRunGreenBlink
            // 
            this.btn_TowerLampRunGreenBlink.Bold = false;
            this.btn_TowerLampRunGreenBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampRunGreenBlink.FontSize = 0F;
            this.btn_TowerLampRunGreenBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampRunGreenBlink.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampRunGreenBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampRunGreenBlink.LedWidth = 23;
            this.btn_TowerLampRunGreenBlink.Location = new System.Drawing.Point(18, 410);
            this.btn_TowerLampRunGreenBlink.Name = "btn_TowerLampRunGreenBlink";
            this.btn_TowerLampRunGreenBlink.On = false;
            this.btn_TowerLampRunGreenBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampRunGreenBlink.TabIndex = 6;
            this.btn_TowerLampRunGreenBlink.Text = "BLINK(G)";
            this.btn_TowerLampRunGreenBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampRunGreen
            // 
            this.btn_TowerLampRunGreen.Bold = false;
            this.btn_TowerLampRunGreen.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampRunGreen.FontSize = 0F;
            this.btn_TowerLampRunGreen.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampRunGreen.LedColor = System.Drawing.Color.Lime;
            this.btn_TowerLampRunGreen.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampRunGreen.LedWidth = 23;
            this.btn_TowerLampRunGreen.Location = new System.Drawing.Point(18, 360);
            this.btn_TowerLampRunGreen.Name = "btn_TowerLampRunGreen";
            this.btn_TowerLampRunGreen.On = true;
            this.btn_TowerLampRunGreen.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampRunGreen.TabIndex = 5;
            this.btn_TowerLampRunGreen.Text = "GREEN";
            this.btn_TowerLampRunGreen.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampRunYellowBlink
            // 
            this.btn_TowerLampRunYellowBlink.Bold = false;
            this.btn_TowerLampRunYellowBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampRunYellowBlink.FontSize = 0F;
            this.btn_TowerLampRunYellowBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampRunYellowBlink.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampRunYellowBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampRunYellowBlink.LedWidth = 23;
            this.btn_TowerLampRunYellowBlink.Location = new System.Drawing.Point(18, 246);
            this.btn_TowerLampRunYellowBlink.Name = "btn_TowerLampRunYellowBlink";
            this.btn_TowerLampRunYellowBlink.On = false;
            this.btn_TowerLampRunYellowBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampRunYellowBlink.TabIndex = 4;
            this.btn_TowerLampRunYellowBlink.Text = "BLINK(Y)";
            this.btn_TowerLampRunYellowBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampRunYellow
            // 
            this.btn_TowerLampRunYellow.Bold = false;
            this.btn_TowerLampRunYellow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampRunYellow.FontSize = 0F;
            this.btn_TowerLampRunYellow.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampRunYellow.LedColor = System.Drawing.Color.Yellow;
            this.btn_TowerLampRunYellow.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampRunYellow.LedWidth = 23;
            this.btn_TowerLampRunYellow.Location = new System.Drawing.Point(18, 196);
            this.btn_TowerLampRunYellow.Name = "btn_TowerLampRunYellow";
            this.btn_TowerLampRunYellow.On = true;
            this.btn_TowerLampRunYellow.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampRunYellow.TabIndex = 3;
            this.btn_TowerLampRunYellow.Text = "YELLOW";
            this.btn_TowerLampRunYellow.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampRunRedBlink
            // 
            this.btn_TowerLampRunRedBlink.Bold = false;
            this.btn_TowerLampRunRedBlink.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampRunRedBlink.FontSize = 0F;
            this.btn_TowerLampRunRedBlink.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampRunRedBlink.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampRunRedBlink.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampRunRedBlink.LedWidth = 23;
            this.btn_TowerLampRunRedBlink.Location = new System.Drawing.Point(18, 94);
            this.btn_TowerLampRunRedBlink.Name = "btn_TowerLampRunRedBlink";
            this.btn_TowerLampRunRedBlink.On = false;
            this.btn_TowerLampRunRedBlink.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampRunRedBlink.TabIndex = 2;
            this.btn_TowerLampRunRedBlink.Text = "BLINK(R)";
            this.btn_TowerLampRunRedBlink.UseVisualStyleBackColor = true;
            // 
            // btn_TowerLampRunRed
            // 
            this.btn_TowerLampRunRed.Bold = false;
            this.btn_TowerLampRunRed.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TowerLampRunRed.FontSize = 0F;
            this.btn_TowerLampRunRed.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.btn_TowerLampRunRed.LedColor = System.Drawing.Color.Red;
            this.btn_TowerLampRunRed.LedPadding = new System.Windows.Forms.Padding(5);
            this.btn_TowerLampRunRed.LedWidth = 23;
            this.btn_TowerLampRunRed.Location = new System.Drawing.Point(18, 44);
            this.btn_TowerLampRunRed.Name = "btn_TowerLampRunRed";
            this.btn_TowerLampRunRed.On = true;
            this.btn_TowerLampRunRed.Size = new System.Drawing.Size(162, 35);
            this.btn_TowerLampRunRed.TabIndex = 1;
            this.btn_TowerLampRunRed.Text = "RED";
            this.btn_TowerLampRunRed.UseVisualStyleBackColor = true;
            // 
            // frmSystem_TowerLamp
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1125, 875);
            this.Controls.Add(this.panSystemTowerLamp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSystem_TowerLamp";
            this.Text = "Tower Lamp";
            this.Load += new System.EventHandler(this.frmSystem_TowerLamp_Load);
            this.panSystemTowerLamp.ResumeLayout(false);
            this.kosesGroupBox1.ResumeLayout(false);
            this.kosesGroupBox5.ResumeLayout(false);
            this.kosesGroupBox4.ResumeLayout(false);
            this.kosesGroupBox3.ResumeLayout(false);
            this.kosesGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panSystemTowerLamp;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox6;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox5;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox4;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox3;
        private KosesControl.Controls.LedButton btn_TowerLampRunRed;
        private KosesControl.Controls.LedButton btn_TowerLampRunRedBlink;
        private KosesControl.Controls.LedButton btn_TowerLampErrorGreenBlink;
        private KosesControl.Controls.LedButton btn_TowerLampErrorGreen;
        private KosesControl.Controls.LedButton btn_TowerLampErrorYellowBlink;
        private KosesControl.Controls.LedButton btn_TowerLampErrorYellow;
        private KosesControl.Controls.LedButton btn_TowerLampErrorRedBlink;
        private KosesControl.Controls.LedButton btn_TowerLampErrorRed;
        private KosesControl.Controls.LedButton btn_TowerLampStopGreenBlink;
        private KosesControl.Controls.LedButton btn_TowerLampStopGreen;
        private KosesControl.Controls.LedButton btn_TowerLampStopYellowBlink;
        private KosesControl.Controls.LedButton btn_TowerLampStopYellow;
        private KosesControl.Controls.LedButton btn_TowerLampStopRedBlink;
        private KosesControl.Controls.LedButton btn_TowerLampStopRed;
        private KosesControl.Controls.LedButton btn_TowerLampRunGreenBlink;
        private KosesControl.Controls.LedButton btn_TowerLampRunGreen;
        private KosesControl.Controls.LedButton btn_TowerLampRunYellowBlink;
        private KosesControl.Controls.LedButton btn_TowerLampRunYellow;
        private KosesControl.Controls.LedButton btn_TowerLampDoorOpenGreenBlink;
        private KosesControl.Controls.LedButton btn_TowerLampDoorOpenGreen;
        private KosesControl.Controls.LedButton btn_TowerLampDoorOpenYellowBlink;
        private KosesControl.Controls.LedButton btn_TowerLampDoorOpenYellow;
        private KosesControl.Controls.LedButton btn_TowerLampDoorOpenRedBlink;
        private KosesControl.Controls.LedButton btn_TowerLampDoorOpenRed;
        private KosesControl.Controls.LedButton btn_TowerLampErrorBuzzer;
    }
}