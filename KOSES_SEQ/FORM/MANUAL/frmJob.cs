﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmJob : Form
    {
        public frmJob()
        {
            InitializeComponent();

            this.VisibleChanged += FrmJob_VisibleChanged;
        }

        private void FrmJob_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == true)
                {
                    UpdateGroupList();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Group 항목 선택 시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvGroup_SelectionChanged(object sender, EventArgs e)
        {
            // 폴더 여러개 선택 시에도 대응 하도록 했으나 레시피 복사/이동/삭제 시 어려움이 있어 data grid view MultiSelect 속성 false로 함
            if (dgvGroup.SelectedRows.Count > 0)
            {
                string[] folders = new string[dgvGroup.SelectedRows.Count];

                for (int i = 0; i < dgvGroup.SelectedRows.Count; i++)
                {
                    folders[i] = dgvGroup.SelectedRows[i].Cells[1].Value.ToString();
                }

                // 선택 된 그룹 표시
                tbGroupName.Text = dgvGroup.SelectedRows[0].Cells[1].Value.ToString();

                UpdateRecipeList(folders);
            }
        }

        /// <summary>
        /// Recipe 항목 선택 시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvRecipe_SelectionChanged(object sender, EventArgs e)
        {
            // data grid view MultiSelect 속성 false로 함
            if (dgvRecipe.SelectedRows.Count > 0)
            {
                // 선택 된 레시피 표시
                tbRecipeName.Text = dgvRecipe.SelectedRows[0].Cells[1].Value.ToString();
            }
        }

        /// <summary>
        /// Update Group List
        /// </summary>
        private void UpdateGroupList()
        {
            dgvGroup.Rows.Clear();
            int index = 1;
            foreach (string strSubPath in Directory.GetDirectories(DefPath.Recipe))
            {
                dgvGroup.Rows.Add(index, strSubPath.Replace(DefPath.Recipe + "\\", ""));
                index++;
            }

            foreach (DataGridViewRow row in dgvGroup.Rows)
            {
                row.ReadOnly = true;
            }
        }

        /// <summary>
        /// Update Recipe List
        /// </summary>
        /// <param name="folderList"></param>
        private void UpdateRecipeList(string[] folderList)
        {
            dgvRecipe.Rows.Clear();
            foreach (string folder in folderList)
            {
                string[] paths = { DefPath.Recipe, folder };
                DirectoryInfo di = new DirectoryInfo(Path.Combine(paths));
                FileInfo[] files = di.GetFiles();

                int index = 1;
                foreach (FileInfo file in files)
                {
                    dgvRecipe.Rows.Add(index, file);
                    index++;
                }
            }

            foreach (DataGridViewRow row in dgvRecipe.Rows)
            {
                row.ReadOnly = true;
            }
        }

        /// <summary>
        /// Group 생성 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateGroup_Click(object sender, EventArgs e)
        {
            // 생성할 그룹 입력 하지 않은 경우
            if (string.IsNullOrEmpty(tbGroupName.Text))
                return;

            if (!Directory.Exists(DefPath.Recipe + @"\" + tbGroupName.Text))
            {
                Directory.CreateDirectory(DefPath.Recipe + @"\" + tbGroupName.Text);
            }

            UpdateGroupList();
        }

        /// <summary>
        /// 그룹 삭제 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbGroupName.Text))
                return;

            // 현재 레시피 그룹이면 삭제 불가
            if (SysOperator.Instance.defaultInfo.RecipeGroupName == tbGroupName.Text)
            {
                FRM.Msg.ShowMsg($"Can't delete current recipe group.({SysOperator.Instance.defaultInfo.RecipeGroupName})");
                return;
            }

            if (Directory.Exists(DefPath.Recipe + @"\" + tbGroupName.Text))
            {
                if (FRM.Msg.Confirm($"Are you sure you want to delete {tbGroupName.Text} group?"))
                {
                    Directory.Delete(DefPath.Recipe + @"\" + tbGroupName.Text, true);
                }
            }

            UpdateGroupList();
        }

        /// <summary>
        /// 레시피 삭제 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteRecipe_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbGroupName.Text))
                return;
            if (string.IsNullOrEmpty(tbRecipeName.Text))
                return;

            // 현재 레시피는 삭제 불가
            if (SysOperator.Instance.defaultInfo.RecipeGroupName == tbGroupName.Text
                && SysOperator.Instance.defaultInfo.RecipeName == Path.GetFileNameWithoutExtension(tbRecipeName.Text))
            {
                FRM.Msg.ShowMsg($"Can't delete current recipe.({SysOperator.Instance.defaultInfo.RecipeName})");
                return;
            }

            string[] paths = { DefPath.Recipe, tbGroupName.Text, tbRecipeName.Text };
            string path = Path.Combine(paths);

            if (File.Exists(path))
            {
                if (FRM.Msg.Confirm($"Are you sure you want to delete {tbRecipeName.Text} file?"))
                {
                    File.Delete(path);
                }
            }

            UpdateGroupList();
        }

        /// <summary>
        /// 레시피 복사 버튼 클릭
        /// 다른 이름으로 복사 함
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRecipeCopy_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbGroupName.Text))
                return;
            if (string.IsNullOrEmpty(tbRecipeName.Text))
                return;

            if (dgvRecipe.SelectedRows.Count > 0 
                && dgvRecipe.SelectedRows[0].Cells[1].Value is FileInfo fi 
                && File.Exists(fi.FullName))
            {
                string[] destPaths = { DefPath.Recipe, tbGroupName.Text, tbRecipeName.Text };

                string path = GenerateCopyFileName(Path.Combine(destPaths));

                if (!string.IsNullOrEmpty(path))
                {
                    if (!fi.FullName.Equals(path))
                        File.Copy(fi.FullName, path, false);
                }
            }

            UpdateGroupList();
        }

        /// <summary>
        /// Rename 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRenameRecipe_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbGroupName.Text))
                return;
            if (string.IsNullOrEmpty(tbRecipeName.Text))
                return;
            if (dgvRecipe.SelectedRows.Count == 0)
                return;

            if (dgvRecipe.SelectedRows.Count > 0 && dgvRecipe.SelectedRows[0].Cells[1].Value is FileInfo fi)
            {
                // 현재 레시피 이름 변경 불가 하도록
                if (SysOperator.Instance.defaultInfo.RecipeGroupName == tbGroupName.Text
                    && SysOperator.Instance.defaultInfo.RecipeName == Path.GetFileNameWithoutExtension(fi.FullName))
                {
                    FRM.Msg.ShowMsg($"Can't rename current recipe.({SysOperator.Instance.defaultInfo.RecipeName})");
                    return;
                }

                string[] destPaths = { DefPath.Recipe, tbGroupName.Text, tbRecipeName.Text };

                if (File.Exists(Path.Combine(destPaths)))
                {
                    FRM.Msg.ShowMsg($"Already exist recipe name.({SysOperator.Instance.defaultInfo.RecipeName})");
                    return;
                }

                File.Move(fi.FullName, Path.Combine(destPaths));
            }

            UpdateGroupList();
        }


        /// <summary>
        /// Recipe 변경 버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeRecipe_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbGroupName.Text))
                return;
            if (string.IsNullOrEmpty(tbRecipeName.Text))
                return;
            if (dgvRecipe.SelectedRows.Count == 0)
                return;

            if (dgvRecipe.SelectedRows[0].Cells[1].Value is FileInfo fi && File.Exists(fi.FullName))
            {
                // Recipe 그룹,이름 정보 변경
                SysOperator.Instance.SetRecipeName(tbGroupName.Text, Path.GetFileNameWithoutExtension(fi.FullName));

                // Recipe 불러오기
                TeachingManager.Instance.Load();
            }
        }

        /// <summary>
        /// 파일 복사 시 이름 생성 메서드
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string GenerateCopyFileName(string path)
        {
            string ext = Path.GetExtension(path);
            string baseFileName = path.Replace(ext, "");

            if (!File.Exists(path))
                return path;
            
            int i = 0;
            string copyFileName;
            do
            {
                copyFileName = $"{baseFileName}-Copy{i + 1}{ext}";
                if (i > 255)
                    return null;
                i++;
            } while (File.Exists(copyFileName));

            return copyFileName;
        }
    }
}
