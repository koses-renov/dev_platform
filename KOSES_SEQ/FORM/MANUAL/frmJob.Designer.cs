﻿namespace KOSES_SEQ.OP
{
    partial class frmJob
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJob));
            this.panManual = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvGroup = new System.Windows.Forms.DataGridView();
            this.colGroupNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGroupFolder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvRecipe = new System.Windows.Forms.DataGridView();
            this.colRecipeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRecipeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbGroupName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbRecipeName = new System.Windows.Forms.TextBox();
            this.panManualRight = new System.Windows.Forms.Panel();
            this.btnChangeRecipe = new System.Windows.Forms.Button();
            this.btnRecipeCopy = new KosesControl.Controls.KosesButton();
            this.btnRenameRecipe = new KosesControl.Controls.KosesButton();
            this.btnDeleteRecipe = new KosesControl.Controls.KosesButton();
            this.btnCreateGroup = new KosesControl.Controls.KosesButton();
            this.btnDeleteGroup = new KosesControl.Controls.KosesButton();
            this.panManual.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipe)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panManualRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.tableLayoutPanel1);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(968, 743);
            this.panManual.TabIndex = 953;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.32494F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.67506F));
            this.tableLayoutPanel1.Controls.Add(this.dgvGroup, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvRecipe, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.36735F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.63265F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(966, 741);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dgvGroup
            // 
            this.dgvGroup.AllowUserToAddRows = false;
            this.dgvGroup.AllowUserToDeleteRows = false;
            this.dgvGroup.AllowUserToResizeColumns = false;
            this.dgvGroup.AllowUserToResizeRows = false;
            this.dgvGroup.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGroup.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGroupNo,
            this.colGroupFolder});
            this.dgvGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGroup.EnableHeadersVisualStyles = false;
            this.dgvGroup.Location = new System.Drawing.Point(3, 3);
            this.dgvGroup.MultiSelect = false;
            this.dgvGroup.Name = "dgvGroup";
            this.dgvGroup.RowHeadersVisible = false;
            this.dgvGroup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGroup.Size = new System.Drawing.Size(325, 574);
            this.dgvGroup.TabIndex = 0;
            this.dgvGroup.SelectionChanged += new System.EventHandler(this.dgvGroup_SelectionChanged);
            // 
            // colGroupNo
            // 
            this.colGroupNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colGroupNo.FillWeight = 50.76142F;
            this.colGroupNo.HeaderText = "GROUP NO.";
            this.colGroupNo.Name = "colGroupNo";
            this.colGroupNo.ReadOnly = true;
            this.colGroupNo.Width = 60;
            // 
            // colGroupFolder
            // 
            this.colGroupFolder.FillWeight = 149.2386F;
            this.colGroupFolder.HeaderText = "GROUP";
            this.colGroupFolder.Name = "colGroupFolder";
            this.colGroupFolder.ReadOnly = true;
            // 
            // dgvRecipe
            // 
            this.dgvRecipe.AllowUserToAddRows = false;
            this.dgvRecipe.AllowUserToDeleteRows = false;
            this.dgvRecipe.AllowUserToResizeColumns = false;
            this.dgvRecipe.AllowUserToResizeRows = false;
            this.dgvRecipe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRecipe.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvRecipe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecipe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colRecipeNo,
            this.colRecipeName});
            this.dgvRecipe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRecipe.EnableHeadersVisualStyles = false;
            this.dgvRecipe.Location = new System.Drawing.Point(334, 3);
            this.dgvRecipe.MultiSelect = false;
            this.dgvRecipe.Name = "dgvRecipe";
            this.dgvRecipe.RowHeadersVisible = false;
            this.dgvRecipe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecipe.Size = new System.Drawing.Size(629, 574);
            this.dgvRecipe.TabIndex = 1;
            this.dgvRecipe.SelectionChanged += new System.EventHandler(this.dgvRecipe_SelectionChanged);
            // 
            // colRecipeNo
            // 
            this.colRecipeNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colRecipeNo.FillWeight = 81.21828F;
            this.colRecipeNo.HeaderText = "RECIPE NO.";
            this.colRecipeNo.Name = "colRecipeNo";
            this.colRecipeNo.Width = 60;
            // 
            // colRecipeName
            // 
            this.colRecipeName.FillWeight = 118.7817F;
            this.colRecipeName.HeaderText = "RECIPE NAME";
            this.colRecipeName.Name = "colRecipeName";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDeleteGroup);
            this.panel1.Controls.Add(this.btnCreateGroup);
            this.panel1.Controls.Add(this.tbGroupName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 583);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(325, 155);
            this.panel1.TabIndex = 2;
            // 
            // tbGroupName
            // 
            this.tbGroupName.Location = new System.Drawing.Point(3, 3);
            this.tbGroupName.Name = "tbGroupName";
            this.tbGroupName.Size = new System.Drawing.Size(287, 23);
            this.tbGroupName.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnDeleteRecipe);
            this.panel2.Controls.Add(this.btnRenameRecipe);
            this.panel2.Controls.Add(this.btnRecipeCopy);
            this.panel2.Controls.Add(this.tbRecipeName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(334, 583);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(629, 155);
            this.panel2.TabIndex = 3;
            // 
            // tbRecipeName
            // 
            this.tbRecipeName.Location = new System.Drawing.Point(3, 3);
            this.tbRecipeName.Name = "tbRecipeName";
            this.tbRecipeName.Size = new System.Drawing.Size(312, 23);
            this.tbRecipeName.TabIndex = 1;
            // 
            // panManualRight
            // 
            this.panManualRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panManualRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panManualRight.Controls.Add(this.btnChangeRecipe);
            this.panManualRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panManualRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManualRight.Location = new System.Drawing.Point(968, 0);
            this.panManualRight.Margin = new System.Windows.Forms.Padding(0);
            this.panManualRight.Name = "panManualRight";
            this.panManualRight.Size = new System.Drawing.Size(136, 743);
            this.panManualRight.TabIndex = 952;
            // 
            // btnChangeRecipe
            // 
            this.btnChangeRecipe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangeRecipe.BackColor = System.Drawing.Color.Transparent;
            this.btnChangeRecipe.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeRecipe.Image")));
            this.btnChangeRecipe.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnChangeRecipe.Location = new System.Drawing.Point(25, 652);
            this.btnChangeRecipe.Name = "btnChangeRecipe";
            this.btnChangeRecipe.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btnChangeRecipe.Size = new System.Drawing.Size(98, 86);
            this.btnChangeRecipe.TabIndex = 954;
            this.btnChangeRecipe.Text = "Change Recipe";
            this.btnChangeRecipe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnChangeRecipe.UseVisualStyleBackColor = false;
            this.btnChangeRecipe.Click += new System.EventHandler(this.btnChangeRecipe_Click);
            // 
            // btnRecipeCopy
            // 
            this.btnRecipeCopy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnRecipeCopy.Bold = true;
            this.btnRecipeCopy.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRecipeCopy.Depth = 0;
            this.btnRecipeCopy.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnRecipeCopy.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRecipeCopy.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRecipeCopy.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnRecipeCopy.DrawShadows = true;
            this.btnRecipeCopy.FontSize = 14F;
            this.btnRecipeCopy.ForeColor = System.Drawing.Color.White;
            this.btnRecipeCopy.HighEmphasis = true;
            this.btnRecipeCopy.Icon = null;
            this.btnRecipeCopy.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnRecipeCopy.Location = new System.Drawing.Point(3, 32);
            this.btnRecipeCopy.MouseOnOpacity = 50;
            this.btnRecipeCopy.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnRecipeCopy.Name = "btnRecipeCopy";
            this.btnRecipeCopy.Selected = false;
            this.btnRecipeCopy.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnRecipeCopy.Size = new System.Drawing.Size(100, 51);
            this.btnRecipeCopy.TabIndex = 14;
            this.btnRecipeCopy.Text = "COPY";
            this.btnRecipeCopy.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnRecipeCopy.UseVisualStyleBackColor = false;
            this.btnRecipeCopy.Click += new System.EventHandler(this.btnRecipeCopy_Click);
            // 
            // btnRenameRecipe
            // 
            this.btnRenameRecipe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnRenameRecipe.Bold = true;
            this.btnRenameRecipe.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRenameRecipe.Depth = 0;
            this.btnRenameRecipe.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnRenameRecipe.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRenameRecipe.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRenameRecipe.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnRenameRecipe.DrawShadows = true;
            this.btnRenameRecipe.FontSize = 14F;
            this.btnRenameRecipe.ForeColor = System.Drawing.Color.White;
            this.btnRenameRecipe.HighEmphasis = true;
            this.btnRenameRecipe.Icon = null;
            this.btnRenameRecipe.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnRenameRecipe.Location = new System.Drawing.Point(109, 32);
            this.btnRenameRecipe.MouseOnOpacity = 50;
            this.btnRenameRecipe.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnRenameRecipe.Name = "btnRenameRecipe";
            this.btnRenameRecipe.Selected = false;
            this.btnRenameRecipe.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnRenameRecipe.Size = new System.Drawing.Size(100, 51);
            this.btnRenameRecipe.TabIndex = 15;
            this.btnRenameRecipe.Text = "RENAME";
            this.btnRenameRecipe.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnRenameRecipe.UseVisualStyleBackColor = false;
            this.btnRenameRecipe.Click += new System.EventHandler(this.btnRenameRecipe_Click);
            // 
            // btnDeleteRecipe
            // 
            this.btnDeleteRecipe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnDeleteRecipe.Bold = true;
            this.btnDeleteRecipe.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteRecipe.Depth = 0;
            this.btnDeleteRecipe.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnDeleteRecipe.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteRecipe.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteRecipe.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnDeleteRecipe.DrawShadows = true;
            this.btnDeleteRecipe.FontSize = 14F;
            this.btnDeleteRecipe.ForeColor = System.Drawing.Color.White;
            this.btnDeleteRecipe.HighEmphasis = true;
            this.btnDeleteRecipe.Icon = null;
            this.btnDeleteRecipe.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnDeleteRecipe.Location = new System.Drawing.Point(215, 32);
            this.btnDeleteRecipe.MouseOnOpacity = 50;
            this.btnDeleteRecipe.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnDeleteRecipe.Name = "btnDeleteRecipe";
            this.btnDeleteRecipe.Selected = false;
            this.btnDeleteRecipe.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnDeleteRecipe.Size = new System.Drawing.Size(100, 51);
            this.btnDeleteRecipe.TabIndex = 16;
            this.btnDeleteRecipe.Text = "DELETE";
            this.btnDeleteRecipe.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnDeleteRecipe.UseVisualStyleBackColor = false;
            this.btnDeleteRecipe.Click += new System.EventHandler(this.btnDeleteRecipe_Click);
            // 
            // btnCreateGroup
            // 
            this.btnCreateGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnCreateGroup.Bold = true;
            this.btnCreateGroup.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreateGroup.Depth = 0;
            this.btnCreateGroup.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnCreateGroup.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreateGroup.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCreateGroup.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnCreateGroup.DrawShadows = true;
            this.btnCreateGroup.FontSize = 14F;
            this.btnCreateGroup.ForeColor = System.Drawing.Color.White;
            this.btnCreateGroup.HighEmphasis = true;
            this.btnCreateGroup.Icon = null;
            this.btnCreateGroup.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnCreateGroup.Location = new System.Drawing.Point(3, 32);
            this.btnCreateGroup.MouseOnOpacity = 50;
            this.btnCreateGroup.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnCreateGroup.Name = "btnCreateGroup";
            this.btnCreateGroup.Selected = false;
            this.btnCreateGroup.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnCreateGroup.Size = new System.Drawing.Size(100, 51);
            this.btnCreateGroup.TabIndex = 15;
            this.btnCreateGroup.Text = "CREATE";
            this.btnCreateGroup.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnCreateGroup.UseVisualStyleBackColor = false;
            this.btnCreateGroup.Click += new System.EventHandler(this.btnCreateGroup_Click);
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnDeleteGroup.Bold = true;
            this.btnDeleteGroup.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteGroup.Depth = 0;
            this.btnDeleteGroup.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnDeleteGroup.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteGroup.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDeleteGroup.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnDeleteGroup.DrawShadows = true;
            this.btnDeleteGroup.FontSize = 14F;
            this.btnDeleteGroup.ForeColor = System.Drawing.Color.White;
            this.btnDeleteGroup.HighEmphasis = true;
            this.btnDeleteGroup.Icon = null;
            this.btnDeleteGroup.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnDeleteGroup.Location = new System.Drawing.Point(109, 32);
            this.btnDeleteGroup.MouseOnOpacity = 50;
            this.btnDeleteGroup.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Selected = false;
            this.btnDeleteGroup.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnDeleteGroup.Size = new System.Drawing.Size(100, 51);
            this.btnDeleteGroup.TabIndex = 16;
            this.btnDeleteGroup.Text = "DELETE";
            this.btnDeleteGroup.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnDeleteGroup.UseVisualStyleBackColor = false;
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // frmJob
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1104, 743);
            this.Controls.Add(this.panManual);
            this.Controls.Add(this.panManualRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmJob";
            this.Text = "Recipe";
            this.panManual.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipe)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panManualRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        internal System.Windows.Forms.Panel panManualRight;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView dgvGroup;
        private System.Windows.Forms.DataGridView dgvRecipe;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGroupNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGroupFolder;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRecipeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRecipeName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbGroupName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbRecipeName;
        private System.Windows.Forms.Button btnChangeRecipe;
        private KosesControl.Controls.KosesButton btnRecipeCopy;
        private KosesControl.Controls.KosesButton btnRenameRecipe;
        private KosesControl.Controls.KosesButton btnDeleteRecipe;
        private KosesControl.Controls.KosesButton btnCreateGroup;
        private KosesControl.Controls.KosesButton btnDeleteGroup;
    }
}