﻿namespace KOSES_SEQ.POPUP
{
    partial class FrmEditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditUser));
            this.label1 = new System.Windows.Forms.Label();
            this.tbCurPass = new System.Windows.Forms.TextBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.btnEng = new System.Windows.Forms.Button();
            this.btnOp = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNewPass1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNewPass2 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tbUserID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lstUser = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(341, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 23);
            this.label1.TabIndex = 15;
            this.label1.Text = "Current Password";
            // 
            // tbCurPass
            // 
            this.tbCurPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbCurPass.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbCurPass.Location = new System.Drawing.Point(539, 204);
            this.tbCurPass.Name = "tbCurPass";
            this.tbCurPass.PasswordChar = '*';
            this.tbCurPass.Size = new System.Drawing.Size(253, 30);
            this.tbCurPass.TabIndex = 14;
            this.tbCurPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbCurPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurPass_KeyPress);
            // 
            // btnMaker
            // 
            this.btnMaker.BackColor = System.Drawing.SystemColors.Control;
            this.btnMaker.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMaker.Image = ((System.Drawing.Image)(resources.GetObject("btnMaker.Image")));
            this.btnMaker.Location = new System.Drawing.Point(641, 24);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(121, 124);
            this.btnMaker.TabIndex = 13;
            this.btnMaker.Tag = "2";
            this.btnMaker.Text = "MAKER";
            this.btnMaker.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMaker.UseVisualStyleBackColor = false;
            this.btnMaker.Click += new System.EventHandler(this.btnOp_Click);
            // 
            // btnEng
            // 
            this.btnEng.BackColor = System.Drawing.SystemColors.Control;
            this.btnEng.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEng.Image = ((System.Drawing.Image)(resources.GetObject("btnEng.Image")));
            this.btnEng.Location = new System.Drawing.Point(504, 24);
            this.btnEng.Name = "btnEng";
            this.btnEng.Size = new System.Drawing.Size(121, 124);
            this.btnEng.TabIndex = 12;
            this.btnEng.Tag = "1";
            this.btnEng.Text = "ENGINEER";
            this.btnEng.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnEng.UseVisualStyleBackColor = false;
            this.btnEng.Click += new System.EventHandler(this.btnOp_Click);
            // 
            // btnOp
            // 
            this.btnOp.BackColor = System.Drawing.SystemColors.Control;
            this.btnOp.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnOp.Image = ((System.Drawing.Image)(resources.GetObject("btnOp.Image")));
            this.btnOp.Location = new System.Drawing.Point(367, 24);
            this.btnOp.Name = "btnOp";
            this.btnOp.Size = new System.Drawing.Size(121, 124);
            this.btnOp.TabIndex = 11;
            this.btnOp.Tag = "0";
            this.btnOp.Text = "OP";
            this.btnOp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnOp.UseVisualStyleBackColor = false;
            this.btnOp.Click += new System.EventHandler(this.btnOp_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(370, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "New Password";
            // 
            // tbNewPass1
            // 
            this.tbNewPass1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNewPass1.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbNewPass1.Location = new System.Drawing.Point(539, 240);
            this.tbNewPass1.Name = "tbNewPass1";
            this.tbNewPass1.PasswordChar = '*';
            this.tbNewPass1.Size = new System.Drawing.Size(253, 30);
            this.tbNewPass1.TabIndex = 18;
            this.tbNewPass1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbNewPass1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewPass1_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(337, 278);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 23);
            this.label3.TabIndex = 21;
            this.label3.Text = "Confirm Password";
            // 
            // tbNewPass2
            // 
            this.tbNewPass2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNewPass2.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbNewPass2.Location = new System.Drawing.Point(539, 276);
            this.tbNewPass2.Name = "tbNewPass2";
            this.tbNewPass2.PasswordChar = '*';
            this.tbNewPass2.Size = new System.Drawing.Size(253, 30);
            this.tbNewPass2.TabIndex = 20;
            this.tbNewPass2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbNewPass2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewPass2_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Location = new System.Drawing.Point(2, 327);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(820, 59);
            this.panel1.TabIndex = 24;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(389, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(99, 45);
            this.btnCancel.TabIndex = 25;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
            this.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOk.Location = new System.Drawing.Point(284, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(99, 45);
            this.btnOk.TabIndex = 24;
            this.btnOk.Text = "OK";
            this.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tbUserID
            // 
            this.tbUserID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbUserID.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbUserID.Location = new System.Drawing.Point(539, 168);
            this.tbUserID.Name = "tbUserID";
            this.tbUserID.Size = new System.Drawing.Size(254, 30);
            this.tbUserID.TabIndex = 26;
            this.tbUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(430, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 23);
            this.label4.TabIndex = 25;
            this.label4.Text = "USER ID";
            // 
            // lstUser
            // 
            this.lstUser.FormattingEnabled = true;
            this.lstUser.Location = new System.Drawing.Point(2, 3);
            this.lstUser.Name = "lstUser";
            this.lstUser.Size = new System.Drawing.Size(310, 316);
            this.lstUser.TabIndex = 27;
            this.lstUser.SelectedValueChanged += new System.EventHandler(this.lstUser_SelectedValueChanged);
            // 
            // FrmEditUser
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(823, 386);
            this.ControlBox = false;
            this.Controls.Add(this.lstUser);
            this.Controls.Add(this.tbUserID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbNewPass2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNewPass1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCurPass);
            this.Controls.Add(this.btnMaker);
            this.Controls.Add(this.btnEng);
            this.Controls.Add(this.btnOp);
            this.Name = "FrmEditUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Password change window";
            this.Load += new System.EventHandler(this.frmChangePass_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCurPass;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.Button btnEng;
        private System.Windows.Forms.Button btnOp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNewPass1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNewPass2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox tbUserID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstUser;
    }
}