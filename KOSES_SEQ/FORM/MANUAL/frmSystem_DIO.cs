﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmSystem_DIO : Form
    {
        private ucDIO[] DIO_Input = new ucDIO[1];
        private ucDIO[] DIO_Output = new ucDIO[1];

        public frmSystem_DIO()
        {
            InitializeComponent();

            this.VisibleChanged += FrmMotor_VisibleChanged;
        }

        private void frmMotor_Load(object sender, EventArgs e)
        {
            SetDisplayDIO(3);
        }

        private void FrmMotor_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == true)
                {
                    Refresh();
                }
            }
            catch
            {

            }
        }

        private void SetDisplayDIO(int displayIOCount)
        {
            tlpDIO.ColumnCount = displayIOCount + 2;

            DIO_Input = new ucDIO[displayIOCount];
            DIO_Output = new ucDIO[displayIOCount];

            for (int i = 0; i < displayIOCount; i++)
            {
                CreateInputTable(i);
                CreateOutputTable(i);
            }
        }

        private void CreateInputTable(int chNo)
        {
            DIO_Input[chNo] = new ucDIO(ucDIO.IOType.INPUT, chNo);
            DIO_Input[chNo].Dock = DockStyle.Fill;
            tlpDIO.Controls.Add(this.DIO_Input[chNo], chNo + 1, 0);
        }

        private void CreateOutputTable(int chNo)
        {
            DIO_Output[chNo] = new ucDIO(ucDIO.IOType.OUTPUT, chNo);
            DIO_Output[chNo].Dock = DockStyle.Fill;
            tlpDIO.Controls.Add(this.DIO_Output[chNo], chNo + 1, 1);
        }


        public override void Refresh()
        {
            base.Refresh();
        }
        private void btnLeft_Input_Click(object sender, EventArgs e)
        {
            if (this.DIO_Input[0].CurChNo == 0)
                return;

            for (int i = 0; i < this.DIO_Input.Length; i++)
                this.DIO_Input[i].ChangeDownChannel();
        }

        private void btnLeft_Output_Click(object sender, EventArgs e)
        {
            if (this.DIO_Output[0].CurChNo == 0)
                return;

            for (int i = 0; i < this.DIO_Output.Length; i++)
                this.DIO_Output[i].ChangeDownChannel();
        }

        private void btnRight_Input_Click(object sender, EventArgs e)
        {
            if (this.DIO_Input[this.DIO_Input.Length - 1].CurChNo == this.DIO_Input[this.DIO_Input.Length - 1].MaxChCnt)
                return;

            for (int i = 0; i < this.DIO_Input.Length; i++)
                this.DIO_Input[i].ChangeUpChannel();
        }

        private void btnRight_Output_Click(object sender, EventArgs e)
        {
            if (this.DIO_Output[this.DIO_Output.Length - 1].CurChNo == this.DIO_Output[this.DIO_Output.Length - 1].MaxChCnt)
                return;

            for (int i = 0; i < this.DIO_Output.Length; i++)
                this.DIO_Output[i].ChangeUpChannel();
        }
    }
}
