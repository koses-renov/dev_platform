﻿namespace KOSES_SEQ.POPUP
{
    partial class FrmErrorPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmErrorPopup));
            this.btnReset = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pbAlarmImageView = new System.Windows.Forms.PictureBox();
            this.rtbAction = new System.Windows.Forms.RichTextBox();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.dgvAlarmView = new System.Windows.Forms.DataGridView();
            this.lblAction = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.tbAlarmMessage = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlarmImageView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(738, 665);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(149, 68);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "RESET";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(8, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(879, 51);
            this.panel1.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(390, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 39);
            this.label2.TabIndex = 0;
            this.label2.Text = "ALARM";
            // 
            // pbAlarmImageView
            // 
            this.pbAlarmImageView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAlarmImageView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbAlarmImageView.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbAlarmImageView.ErrorImage")));
            this.pbAlarmImageView.Image = ((System.Drawing.Image)(resources.GetObject("pbAlarmImageView.Image")));
            this.pbAlarmImageView.Location = new System.Drawing.Point(487, 150);
            this.pbAlarmImageView.Name = "pbAlarmImageView";
            this.pbAlarmImageView.Size = new System.Drawing.Size(400, 300);
            this.pbAlarmImageView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAlarmImageView.TabIndex = 27;
            this.pbAlarmImageView.TabStop = false;
            this.pbAlarmImageView.Paint += new System.Windows.Forms.PaintEventHandler(this.pbAlarmImageView_Paint);
            // 
            // rtbAction
            // 
            this.rtbAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbAction.Location = new System.Drawing.Point(8, 312);
            this.rtbAction.Name = "rtbAction";
            this.rtbAction.Size = new System.Drawing.Size(471, 135);
            this.rtbAction.TabIndex = 23;
            this.rtbAction.Text = "";
            // 
            // rtbDescription
            // 
            this.rtbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbDescription.Location = new System.Drawing.Point(8, 149);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(471, 135);
            this.rtbDescription.TabIndex = 22;
            this.rtbDescription.Text = "";
            // 
            // dgvAlarmView
            // 
            this.dgvAlarmView.AllowUserToAddRows = false;
            this.dgvAlarmView.AllowUserToDeleteRows = false;
            this.dgvAlarmView.AllowUserToResizeRows = false;
            this.dgvAlarmView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAlarmView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAlarmView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlarmView.Location = new System.Drawing.Point(8, 460);
            this.dgvAlarmView.MultiSelect = false;
            this.dgvAlarmView.Name = "dgvAlarmView";
            this.dgvAlarmView.ReadOnly = true;
            this.dgvAlarmView.RowHeadersVisible = false;
            this.dgvAlarmView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAlarmView.Size = new System.Drawing.Size(879, 199);
            this.dgvAlarmView.TabIndex = 28;
            this.dgvAlarmView.SelectionChanged += new System.EventHandler(this.dgvAlarmView_SelectionChanged);
            // 
            // lblAction
            // 
            this.lblAction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAction.AutoSize = true;
            this.lblAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAction.Location = new System.Drawing.Point(8, 289);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(59, 17);
            this.lblAction.TabIndex = 26;
            this.lblAction.Text = "ACTION";
            // 
            // lblDescription
            // 
            this.lblDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblDescription.Location = new System.Drawing.Point(8, 126);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(100, 17);
            this.lblDescription.TabIndex = 25;
            this.lblDescription.Text = "DESCRIPTION";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblMessage.Location = new System.Drawing.Point(8, 72);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(75, 17);
            this.lblMessage.TabIndex = 24;
            this.lblMessage.Text = "MESSAGE";
            // 
            // tbAlarmMessage
            // 
            this.tbAlarmMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAlarmMessage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbAlarmMessage.Location = new System.Drawing.Point(8, 95);
            this.tbAlarmMessage.Name = "tbAlarmMessage";
            this.tbAlarmMessage.Size = new System.Drawing.Size(879, 20);
            this.tbAlarmMessage.TabIndex = 21;
            // 
            // FrmErrorPopup
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(895, 741);
            this.ControlBox = false;
            this.Controls.Add(this.dgvAlarmView);
            this.Controls.Add(this.pbAlarmImageView);
            this.Controls.Add(this.lblAction);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.rtbAction);
            this.Controls.Add(this.rtbDescription);
            this.Controls.Add(this.tbAlarmMessage);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnReset);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FrmErrorPopup";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ERROR";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAlarmImageView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlarmView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbAlarmImageView;
        private System.Windows.Forms.Label lblAction;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.RichTextBox rtbAction;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.TextBox tbAlarmMessage;
        private System.Windows.Forms.DataGridView dgvAlarmView;
    }
}