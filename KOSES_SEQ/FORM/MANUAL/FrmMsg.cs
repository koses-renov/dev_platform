﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KOSES_SEQ.OP;

namespace KOSES_SEQ.MESSAGE
{
    public delegate void dShowMsg(string s, int timeout = 3000);

    public partial class frmMsg : Form
    {
        public frmMsg()
        {
            InitializeComponent();
        }

         public static void FinishMsg(string s)
        {
            frmMsg msg = new frmMsg();

            msg.btnCancel.Visible = false;
            msg.btnOk.Visible = false;
            msg.Text = "Message";
            msg.lblText.Text = s;
            msg.ShowDialog();
            //SYS.StopAll();
        }

        public void ShowMsg(string s, int timeout = 3000)
        {
            try
            {
                if (IsDisposed || Disposing)
                    return;
                dShowMsg showmsg = new dShowMsg(ShowMsg);
                if (InvokeRequired)
                    Invoke(showmsg,new object[]{s,timeout });
                else
                {
                    btnClose.Visible = false;
                    btnOk.Visible = false;
                    this.Text = "Message";
                    this.lblText.Text = s;
                    this.ShowDialog(FRM.Main);

                    if (timeout > 0)
                    {
                        this.tmrHide.Enabled = true;
                        this.tmrHide.Interval= timeout;
                    }
                }
            }
            catch(Exception ex)
            {
                //APP.Logger.Fatal(Name + ".FrmMsg.ShowMsg",ex);
            }
        }

        public bool Confirm(string s)
        {
            btnClose.Visible = false;
            this.btnOk.Visible = true;
            this.Text = "Confirm";
            this.lblText.Text = s;

            switch (this.ShowDialog(FRM.Main))
            {
            case DialogResult.OK: return true;
            default: return false;
            }
        }

        public void Warning(string s)
        {
            btnClose.Visible = false;
            btnOk.Visible = false;
            this.Text = "Warning";
            this.lblText.Text = s;

            this.ShowDialog(FRM.Main);
        }

        private void tmrHide_Tick(object sender, EventArgs e)
        {
            tmrHide.Enabled = false;
            Hide();
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            tmrHide.Enabled = false;
            Hide();
        }

        private void btnHide_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible) tmrHide.Enabled = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
