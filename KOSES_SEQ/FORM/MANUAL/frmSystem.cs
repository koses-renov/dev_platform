﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmSystem : Form
    {
        
        public string currentFormName;

        public enum FormList
        {
            motor,
            dio,
            aio,
            tower
        }

        public frmSystem_Motor motorForm = null;
        public frmSystem_DIO dioForm = null;
        public frmSystem_AIO aioForm = null;
        public frmSystem_TowerLamp towerForm = null;

        public frmSystem()
        {
            InitializeComponent();
            currentFormName = this.Name;

            CreateSubForm();
        }

        private void CreateSubForm()
        {
            motorForm = new frmSystem_Motor();
            dioForm = new frmSystem_DIO();
            aioForm = new frmSystem_AIO();
            towerForm = new frmSystem_TowerLamp();

            InitSubForm(motorForm);
            InitSubForm(dioForm);
            InitSubForm(aioForm);
            InitSubForm(towerForm);
        }

        void InitSubForm(Form form)
        {
            form.TopLevel = false;
            panSystem.Controls.Add(form);
            form.Dock = DockStyle.Fill;
            form.Show();
            //form.Left = panSystem.Left;
            //form.Top = panSystem.Top;
        }

        FormList curFormList;
        public void ChangeSubForm(FormList fl)
        {
            curFormList = fl;

            btn_SystemMotor.Visible = true;
            btn_SystemDIO.Visible = true;
            btn_SystemAIO.Visible = true;
            btn_SystemTowerLamp.Visible = true;

            //btnMotor.BackColor = DefColor.Unselect;
            //btnIO.BackColor = DefColor.Unselect;

            //motorForm.DoHide();
            //dioForm.DoHide();
            //aioForm.DoHide();

            btn_SystemSave.Hide();

            switch (fl)
            {
                case FormList.motor:
                    //motorForm.Show();
                    motorForm.BringToFront();
                    currentFormName = motorForm.Name;
                    btn_SystemSave.Show();
                    break;
                case FormList.dio:
                    //dioForm.Show();
                    dioForm.BringToFront();
                    currentFormName = dioForm.Name;
                    btn_SystemSave.Show();
                    break;
                case FormList.aio:
                    aioForm.BringToFront();
                    currentFormName = aioForm.Name;
                    btn_SystemSave.Show();
                    break;

                case FormList.tower:
                    //btnIO.BackColor = DefColor.Select;
                    //towerForm.Show();
                    towerForm.BringToFront();
                    currentFormName = towerForm.Name;
                    btn_SystemSave.Show();
                    break;
            }
        }

        private void frmSystem_Load(object sender, EventArgs e)
        {
            ChangeSubForm(FormList.motor);
        }

        private void btn_SystemMotor_Click(object sender, EventArgs e)
        {
            ChangeSubForm(FormList.motor);
        }

        private void btn_SystemDIO_Click(object sender, EventArgs e)
        {
            ChangeSubForm(FormList.dio);
        }

        private void btn_SystemAIO_Click(object sender, EventArgs e)
        {
            ChangeSubForm(FormList.aio);
        }

        private void btn_SystemTowerLamp_Click(object sender, EventArgs e)
        {
            ChangeSubForm(FormList.tower);
        }

        public void DataSave()
        {
            if (currentFormName == motorForm.Name)
            {
                motorForm.DataSave();
            }
            //else if (currentFormName == ioForm.Name)
            //{

            //}

        }

        public void DataLoad()
        {
            //if (currentFormName == motorForm.Name)
            //{
            //    motorForm.DataLoad();
            //}
            //else if (currentFormName == ioForm.Name)
            //{

            //}
        }

        private void btn_SystemSave_Click(object sender, EventArgs e)
        {
            DataSave();
        }

        private void btn_SystemLoad_Click(object sender, EventArgs e)
        {
            DataLoad();
        }

        public void PrepareForClose()
        {
            //motorForm.PrepareForClose();
            //ioForm.PrepareForClose();
        }
    }
}
