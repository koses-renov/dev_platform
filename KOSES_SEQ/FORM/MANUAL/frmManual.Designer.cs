﻿namespace KOSES_SEQ.OP
{
    partial class frmManual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManual));
            this.panManual = new System.Windows.Forms.Panel();
            this.spcManual = new System.Windows.Forms.SplitContainer();
            this.spcTenkey = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTenkeyDisplay = new KosesControl.Controls.LedLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbUseTenkey = new System.Windows.Forms.CheckBox();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.tcTenkey = new System.Windows.Forms.TabControl();
            this.tpLoader = new System.Windows.Forms.TabPage();
            this.flpLoaderItems = new System.Windows.Forms.FlowLayoutPanel();
            this.kosesTenkeyItem1 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem2 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem3 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem7 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem8 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem9 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem10 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem11 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem12 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem13 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem14 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem15 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.tbFlux = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.kosesTenkeyItem4 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem5 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.kosesTenkeyItem6 = new KOSES_SEQ.USERCONTROL.KosesTenkeyItem();
            this.panManualRight = new System.Windows.Forms.Panel();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.btnTenkeyLoader = new KosesControl.Controls.KosesButton();
            this.btnManualClose = new System.Windows.Forms.Button();
            this.btnTenkeyFlux = new KosesControl.Controls.KosesButton();
            this.panManual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcManual)).BeginInit();
            this.spcManual.Panel1.SuspendLayout();
            this.spcManual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcTenkey)).BeginInit();
            this.spcTenkey.Panel1.SuspendLayout();
            this.spcTenkey.Panel2.SuspendLayout();
            this.spcTenkey.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            this.tcTenkey.SuspendLayout();
            this.tpLoader.SuspendLayout();
            this.flpLoaderItems.SuspendLayout();
            this.tbFlux.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panManualRight.SuspendLayout();
            this.kosesGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.spcManual);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(1153, 743);
            this.panManual.TabIndex = 953;
            // 
            // spcManual
            // 
            this.spcManual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spcManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcManual.IsSplitterFixed = true;
            this.spcManual.Location = new System.Drawing.Point(1, 1);
            this.spcManual.Name = "spcManual";
            // 
            // spcManual.Panel1
            // 
            this.spcManual.Panel1.Controls.Add(this.spcTenkey);
            this.spcManual.Size = new System.Drawing.Size(1151, 741);
            this.spcManual.SplitterDistance = 595;
            this.spcManual.TabIndex = 0;
            // 
            // spcTenkey
            // 
            this.spcTenkey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spcTenkey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcTenkey.IsSplitterFixed = true;
            this.spcTenkey.Location = new System.Drawing.Point(0, 0);
            this.spcTenkey.Name = "spcTenkey";
            this.spcTenkey.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcTenkey.Panel1
            // 
            this.spcTenkey.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // spcTenkey.Panel2
            // 
            this.spcTenkey.Panel2.Controls.Add(this.kosesGroupBox2);
            this.spcTenkey.Size = new System.Drawing.Size(595, 741);
            this.spcTenkey.SplitterDistance = 94;
            this.spcTenkey.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.43276F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.56724F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191F));
            this.tableLayoutPanel1.Controls.Add(this.lblTenkeyDisplay, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbUseTenkey, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(593, 92);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTenkeyDisplay
            // 
            this.lblTenkeyDisplay.AutoSize = true;
            this.lblTenkeyDisplay.BackColor = System.Drawing.Color.Black;
            this.lblTenkeyDisplay.Bold = false;
            this.lblTenkeyDisplay.DecimalPoint = 0;
            this.lblTenkeyDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTenkeyDisplay.Font = new System.Drawing.Font("LED Dot-Matrix", 60F);
            this.lblTenkeyDisplay.FontSize = 60F;
            this.lblTenkeyDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lblTenkeyDisplay.LedOffColor = System.Drawing.Color.Gray;
            this.lblTenkeyDisplay.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lblTenkeyDisplay.Location = new System.Drawing.Point(193, 0);
            this.lblTenkeyDisplay.Maximum = 99D;
            this.lblTenkeyDisplay.Minimum = 0D;
            this.lblTenkeyDisplay.Name = "lblTenkeyDisplay";
            this.lblTenkeyDisplay.On = true;
            this.lblTenkeyDisplay.Size = new System.Drawing.Size(205, 92);
            this.lblTenkeyDisplay.TabIndex = 0;
            this.lblTenkeyDisplay.Text = "00";
            this.lblTenkeyDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTenkeyDisplay.TextType = KosesControl.TextTypes.NUMBER;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Tahoma", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 92);
            this.label1.TabIndex = 1;
            this.label1.Text = "TENKEY NO.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbUseTenkey
            // 
            this.cbUseTenkey.AutoSize = true;
            this.cbUseTenkey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbUseTenkey.Font = new System.Drawing.Font("Tahoma", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUseTenkey.Location = new System.Drawing.Point(404, 3);
            this.cbUseTenkey.Name = "cbUseTenkey";
            this.cbUseTenkey.Size = new System.Drawing.Size(186, 86);
            this.cbUseTenkey.TabIndex = 2;
            this.cbUseTenkey.Text = "USE TENKEY";
            this.cbUseTenkey.UseVisualStyleBackColor = true;
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesGroupBox2.Controls.Add(this.tcTenkey);
            this.kosesGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Radius = 15;
            this.kosesGroupBox2.Size = new System.Drawing.Size(593, 641);
            this.kosesGroupBox2.TabIndex = 0;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "TENKEY";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // tcTenkey
            // 
            this.tcTenkey.Controls.Add(this.tpLoader);
            this.tcTenkey.Controls.Add(this.tbFlux);
            this.tcTenkey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTenkey.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcTenkey.Location = new System.Drawing.Point(3, 19);
            this.tcTenkey.Name = "tcTenkey";
            this.tcTenkey.SelectedIndex = 0;
            this.tcTenkey.Size = new System.Drawing.Size(587, 619);
            this.tcTenkey.TabIndex = 0;
            // 
            // tpLoader
            // 
            this.tpLoader.Controls.Add(this.flpLoaderItems);
            this.tpLoader.Location = new System.Drawing.Point(4, 28);
            this.tpLoader.Name = "tpLoader";
            this.tpLoader.Padding = new System.Windows.Forms.Padding(3);
            this.tpLoader.Size = new System.Drawing.Size(579, 587);
            this.tpLoader.TabIndex = 0;
            this.tpLoader.Text = "LOADER";
            this.tpLoader.UseVisualStyleBackColor = true;
            // 
            // flpLoaderItems
            // 
            this.flpLoaderItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpLoaderItems.AutoScroll = true;
            this.flpLoaderItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem1);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem2);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem3);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem7);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem8);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem9);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem10);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem11);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem12);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem13);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem14);
            this.flpLoaderItems.Controls.Add(this.kosesTenkeyItem15);
            this.flpLoaderItems.Location = new System.Drawing.Point(3, 3);
            this.flpLoaderItems.Name = "flpLoaderItems";
            this.flpLoaderItems.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flpLoaderItems.Size = new System.Drawing.Size(960, 1100);
            this.flpLoaderItems.TabIndex = 0;
            // 
            // kosesTenkeyItem1
            // 
            this.kosesTenkeyItem1.HomeText = "HOME #";
            this.kosesTenkeyItem1.Location = new System.Drawing.Point(5, 1);
            this.kosesTenkeyItem1.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem1.Name = "kosesTenkeyItem1";
            this.kosesTenkeyItem1.SequenceName = "ELEVATOR";
            this.kosesTenkeyItem1.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem1.StepText = "STEP *";
            this.kosesTenkeyItem1.TabIndex = 0;
            this.kosesTenkeyItem1.TenkeyNo = 1;
            this.kosesTenkeyItem1.UseHome = true;
            this.kosesTenkeyItem1.UseStep = true;
            // 
            // kosesTenkeyItem2
            // 
            this.kosesTenkeyItem2.HomeText = "HOME #";
            this.kosesTenkeyItem2.Location = new System.Drawing.Point(5, 48);
            this.kosesTenkeyItem2.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem2.Name = "kosesTenkeyItem2";
            this.kosesTenkeyItem2.SequenceName = "ELEVATOR PUSHER";
            this.kosesTenkeyItem2.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem2.StepText = "STEP *";
            this.kosesTenkeyItem2.TabIndex = 1;
            this.kosesTenkeyItem2.TenkeyNo = 2;
            this.kosesTenkeyItem2.UseHome = true;
            this.kosesTenkeyItem2.UseStep = false;
            // 
            // kosesTenkeyItem3
            // 
            this.kosesTenkeyItem3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem3.HomeText = "DW #";
            this.kosesTenkeyItem3.Location = new System.Drawing.Point(5, 95);
            this.kosesTenkeyItem3.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem3.Name = "kosesTenkeyItem3";
            this.kosesTenkeyItem3.SequenceName = "ELEVATOR MGZ CLAMP";
            this.kosesTenkeyItem3.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem3.StepText = "UP *";
            this.kosesTenkeyItem3.TabIndex = 2;
            this.kosesTenkeyItem3.TenkeyNo = 3;
            this.kosesTenkeyItem3.UseHome = true;
            this.kosesTenkeyItem3.UseStep = true;
            // 
            // kosesTenkeyItem7
            // 
            this.kosesTenkeyItem7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem7.HomeText = "HOME #";
            this.kosesTenkeyItem7.Location = new System.Drawing.Point(5, 142);
            this.kosesTenkeyItem7.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem7.Name = "kosesTenkeyItem7";
            this.kosesTenkeyItem7.SequenceName = "MGZ GUIDE";
            this.kosesTenkeyItem7.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem7.StepText = "UP *";
            this.kosesTenkeyItem7.TabIndex = 3;
            this.kosesTenkeyItem7.TenkeyNo = 4;
            this.kosesTenkeyItem7.UseHome = true;
            this.kosesTenkeyItem7.UseStep = false;
            // 
            // kosesTenkeyItem8
            // 
            this.kosesTenkeyItem8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem8.HomeText = "BW #";
            this.kosesTenkeyItem8.Location = new System.Drawing.Point(5, 189);
            this.kosesTenkeyItem8.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem8.Name = "kosesTenkeyItem8";
            this.kosesTenkeyItem8.SequenceName = "ELEVATOR MGZ ALIGNER";
            this.kosesTenkeyItem8.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem8.StepText = "FW *";
            this.kosesTenkeyItem8.TabIndex = 4;
            this.kosesTenkeyItem8.TenkeyNo = 5;
            this.kosesTenkeyItem8.UseHome = true;
            this.kosesTenkeyItem8.UseStep = true;
            // 
            // kosesTenkeyItem9
            // 
            this.kosesTenkeyItem9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem9.HomeText = "DW #";
            this.kosesTenkeyItem9.Location = new System.Drawing.Point(5, 236);
            this.kosesTenkeyItem9.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem9.Name = "kosesTenkeyItem9";
            this.kosesTenkeyItem9.SequenceName = "ALIGNER";
            this.kosesTenkeyItem9.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem9.StepText = "UP *";
            this.kosesTenkeyItem9.TabIndex = 5;
            this.kosesTenkeyItem9.TenkeyNo = 6;
            this.kosesTenkeyItem9.UseHome = true;
            this.kosesTenkeyItem9.UseStep = true;
            // 
            // kosesTenkeyItem10
            // 
            this.kosesTenkeyItem10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem10.HomeText = "HOME #";
            this.kosesTenkeyItem10.Location = new System.Drawing.Point(5, 283);
            this.kosesTenkeyItem10.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem10.Name = "kosesTenkeyItem10";
            this.kosesTenkeyItem10.SequenceName = "IN-INDEX";
            this.kosesTenkeyItem10.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem10.StepText = "STEP *";
            this.kosesTenkeyItem10.TabIndex = 6;
            this.kosesTenkeyItem10.TenkeyNo = 7;
            this.kosesTenkeyItem10.UseHome = true;
            this.kosesTenkeyItem10.UseStep = true;
            // 
            // kosesTenkeyItem11
            // 
            this.kosesTenkeyItem11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem11.HomeText = "HOME #";
            this.kosesTenkeyItem11.Location = new System.Drawing.Point(5, 330);
            this.kosesTenkeyItem11.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem11.Name = "kosesTenkeyItem11";
            this.kosesTenkeyItem11.SequenceName = "OUT-INDEX";
            this.kosesTenkeyItem11.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem11.StepText = "STEP *";
            this.kosesTenkeyItem11.TabIndex = 7;
            this.kosesTenkeyItem11.TenkeyNo = 8;
            this.kosesTenkeyItem11.UseHome = true;
            this.kosesTenkeyItem11.UseStep = true;
            // 
            // kosesTenkeyItem12
            // 
            this.kosesTenkeyItem12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem12.HomeText = "HOME #";
            this.kosesTenkeyItem12.Location = new System.Drawing.Point(5, 377);
            this.kosesTenkeyItem12.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem12.Name = "kosesTenkeyItem12";
            this.kosesTenkeyItem12.SequenceName = "PNP";
            this.kosesTenkeyItem12.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem12.StepText = "STEP *";
            this.kosesTenkeyItem12.TabIndex = 8;
            this.kosesTenkeyItem12.TenkeyNo = 9;
            this.kosesTenkeyItem12.UseHome = true;
            this.kosesTenkeyItem12.UseStep = true;
            // 
            // kosesTenkeyItem13
            // 
            this.kosesTenkeyItem13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem13.HomeText = "OFF #";
            this.kosesTenkeyItem13.Location = new System.Drawing.Point(5, 424);
            this.kosesTenkeyItem13.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem13.Name = "kosesTenkeyItem13";
            this.kosesTenkeyItem13.SequenceName = "IN-INDEX VAC";
            this.kosesTenkeyItem13.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem13.StepText = "ON *";
            this.kosesTenkeyItem13.TabIndex = 9;
            this.kosesTenkeyItem13.TenkeyNo = 71;
            this.kosesTenkeyItem13.UseHome = true;
            this.kosesTenkeyItem13.UseStep = true;
            // 
            // kosesTenkeyItem14
            // 
            this.kosesTenkeyItem14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem14.HomeText = "OFF #";
            this.kosesTenkeyItem14.Location = new System.Drawing.Point(5, 471);
            this.kosesTenkeyItem14.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem14.Name = "kosesTenkeyItem14";
            this.kosesTenkeyItem14.SequenceName = "OUT-INDEX VAC";
            this.kosesTenkeyItem14.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem14.StepText = "ON *";
            this.kosesTenkeyItem14.TabIndex = 10;
            this.kosesTenkeyItem14.TenkeyNo = 81;
            this.kosesTenkeyItem14.UseHome = true;
            this.kosesTenkeyItem14.UseStep = true;
            // 
            // kosesTenkeyItem15
            // 
            this.kosesTenkeyItem15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem15.HomeText = "DW #";
            this.kosesTenkeyItem15.Location = new System.Drawing.Point(5, 518);
            this.kosesTenkeyItem15.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem15.Name = "kosesTenkeyItem15";
            this.kosesTenkeyItem15.SequenceName = "ALIGNER KIT CHANGE";
            this.kosesTenkeyItem15.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem15.StepText = "HOME *";
            this.kosesTenkeyItem15.TabIndex = 11;
            this.kosesTenkeyItem15.TenkeyNo = 90;
            this.kosesTenkeyItem15.UseHome = false;
            this.kosesTenkeyItem15.UseStep = true;
            // 
            // tbFlux
            // 
            this.tbFlux.Controls.Add(this.flowLayoutPanel1);
            this.tbFlux.Location = new System.Drawing.Point(4, 28);
            this.tbFlux.Name = "tbFlux";
            this.tbFlux.Padding = new System.Windows.Forms.Padding(3);
            this.tbFlux.Size = new System.Drawing.Size(192, 68);
            this.tbFlux.TabIndex = 1;
            this.tbFlux.Text = "FLUX";
            this.tbFlux.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.kosesTenkeyItem4);
            this.flowLayoutPanel1.Controls.Add(this.kosesTenkeyItem5);
            this.flowLayoutPanel1.Controls.Add(this.kosesTenkeyItem6);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(186, 62);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // kosesTenkeyItem4
            // 
            this.kosesTenkeyItem4.HomeText = "HOME #";
            this.kosesTenkeyItem4.Location = new System.Drawing.Point(5, 1);
            this.kosesTenkeyItem4.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem4.Name = "kosesTenkeyItem4";
            this.kosesTenkeyItem4.SequenceName = "FLUX";
            this.kosesTenkeyItem4.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem4.StepText = "STEP *";
            this.kosesTenkeyItem4.TabIndex = 0;
            this.kosesTenkeyItem4.TenkeyNo = 11;
            this.kosesTenkeyItem4.UseHome = true;
            this.kosesTenkeyItem4.UseStep = true;
            // 
            // kosesTenkeyItem5
            // 
            this.kosesTenkeyItem5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem5.HomeText = "HOME #";
            this.kosesTenkeyItem5.Location = new System.Drawing.Point(5, 48);
            this.kosesTenkeyItem5.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem5.Name = "kosesTenkeyItem5";
            this.kosesTenkeyItem5.SequenceName = "FLUX CUP SQUEEGE";
            this.kosesTenkeyItem5.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem5.StepText = "STEP *";
            this.kosesTenkeyItem5.TabIndex = 1;
            this.kosesTenkeyItem5.TenkeyNo = 12;
            this.kosesTenkeyItem5.UseHome = true;
            this.kosesTenkeyItem5.UseStep = true;
            // 
            // kosesTenkeyItem6
            // 
            this.kosesTenkeyItem6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kosesTenkeyItem6.HomeText = "DW #";
            this.kosesTenkeyItem6.Location = new System.Drawing.Point(5, 95);
            this.kosesTenkeyItem6.Margin = new System.Windows.Forms.Padding(5, 1, 5, 1);
            this.kosesTenkeyItem6.Name = "kosesTenkeyItem6";
            this.kosesTenkeyItem6.SequenceName = "FLUX Y CLEANER MOVE";
            this.kosesTenkeyItem6.Size = new System.Drawing.Size(544, 45);
            this.kosesTenkeyItem6.StepText = "MOVE *";
            this.kosesTenkeyItem6.TabIndex = 2;
            this.kosesTenkeyItem6.TenkeyNo = 14;
            this.kosesTenkeyItem6.UseHome = false;
            this.kosesTenkeyItem6.UseStep = true;
            // 
            // panManualRight
            // 
            this.panManualRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panManualRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panManualRight.Controls.Add(this.kosesGroupBox1);
            this.panManualRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panManualRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManualRight.Location = new System.Drawing.Point(1153, 0);
            this.panManualRight.Margin = new System.Windows.Forms.Padding(0);
            this.panManualRight.Name = "panManualRight";
            this.panManualRight.Size = new System.Drawing.Size(151, 743);
            this.panManualRight.TabIndex = 952;
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.kosesGroupBox1.Controls.Add(this.btnTenkeyFlux);
            this.kosesGroupBox1.Controls.Add(this.btnTenkeyLoader);
            this.kosesGroupBox1.Controls.Add(this.btnManualClose);
            this.kosesGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Radius = 15;
            this.kosesGroupBox1.Size = new System.Drawing.Size(149, 741);
            this.kosesGroupBox1.TabIndex = 952;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "SELECT";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btnTenkeyLoader
            // 
            this.btnTenkeyLoader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnTenkeyLoader.Bold = true;
            this.btnTenkeyLoader.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTenkeyLoader.Depth = 0;
            this.btnTenkeyLoader.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnTenkeyLoader.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTenkeyLoader.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTenkeyLoader.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnTenkeyLoader.DrawShadows = true;
            this.btnTenkeyLoader.FontSize = 14F;
            this.btnTenkeyLoader.ForeColor = System.Drawing.Color.White;
            this.btnTenkeyLoader.HighEmphasis = true;
            this.btnTenkeyLoader.Icon = null;
            this.btnTenkeyLoader.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnTenkeyLoader.Location = new System.Drawing.Point(11, 22);
            this.btnTenkeyLoader.MouseOnOpacity = 50;
            this.btnTenkeyLoader.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnTenkeyLoader.Name = "btnTenkeyLoader";
            this.btnTenkeyLoader.Selected = false;
            this.btnTenkeyLoader.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnTenkeyLoader.Size = new System.Drawing.Size(127, 49);
            this.btnTenkeyLoader.TabIndex = 956;
            this.btnTenkeyLoader.Text = "LOADER";
            this.btnTenkeyLoader.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnTenkeyLoader.UseVisualStyleBackColor = false;
            this.btnTenkeyLoader.Click += new System.EventHandler(this.btnTenkeyLoader_Click);
            // 
            // btnManualClose
            // 
            this.btnManualClose.BackColor = System.Drawing.Color.Transparent;
            this.btnManualClose.Image = ((System.Drawing.Image)(resources.GetObject("btnManualClose.Image")));
            this.btnManualClose.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnManualClose.Location = new System.Drawing.Point(16, 690);
            this.btnManualClose.Name = "btnManualClose";
            this.btnManualClose.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btnManualClose.Size = new System.Drawing.Size(130, 40);
            this.btnManualClose.TabIndex = 955;
            this.btnManualClose.Text = "닫기";
            this.btnManualClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnManualClose.UseVisualStyleBackColor = false;
            this.btnManualClose.Visible = false;
            // 
            // btnTenkeyFlux
            // 
            this.btnTenkeyFlux.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnTenkeyFlux.Bold = true;
            this.btnTenkeyFlux.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTenkeyFlux.Depth = 0;
            this.btnTenkeyFlux.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnTenkeyFlux.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTenkeyFlux.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTenkeyFlux.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnTenkeyFlux.DrawShadows = true;
            this.btnTenkeyFlux.FontSize = 14F;
            this.btnTenkeyFlux.ForeColor = System.Drawing.Color.White;
            this.btnTenkeyFlux.HighEmphasis = true;
            this.btnTenkeyFlux.Icon = null;
            this.btnTenkeyFlux.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnTenkeyFlux.Location = new System.Drawing.Point(11, 77);
            this.btnTenkeyFlux.MouseOnOpacity = 50;
            this.btnTenkeyFlux.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnTenkeyFlux.Name = "btnTenkeyFlux";
            this.btnTenkeyFlux.Selected = false;
            this.btnTenkeyFlux.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnTenkeyFlux.Size = new System.Drawing.Size(127, 49);
            this.btnTenkeyFlux.TabIndex = 957;
            this.btnTenkeyFlux.Text = "FLUX";
            this.btnTenkeyFlux.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnTenkeyFlux.UseVisualStyleBackColor = false;
            this.btnTenkeyFlux.Click += new System.EventHandler(this.btnTenkeyFlux_Click);
            // 
            // frmManual
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1304, 743);
            this.Controls.Add(this.panManual);
            this.Controls.Add(this.panManualRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmManual";
            this.Text = "Manual";
            this.panManual.ResumeLayout(false);
            this.spcManual.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcManual)).EndInit();
            this.spcManual.ResumeLayout(false);
            this.spcTenkey.Panel1.ResumeLayout(false);
            this.spcTenkey.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcTenkey)).EndInit();
            this.spcTenkey.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.kosesGroupBox2.ResumeLayout(false);
            this.tcTenkey.ResumeLayout(false);
            this.tpLoader.ResumeLayout(false);
            this.flpLoaderItems.ResumeLayout(false);
            this.tbFlux.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panManualRight.ResumeLayout(false);
            this.kosesGroupBox1.ResumeLayout(false);
            this.kosesGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        internal System.Windows.Forms.Panel panManualRight;
        private System.Windows.Forms.Button btnManualClose;
        private KosesControl.Controls.KosesButton btnTenkeyLoader;
        private System.Windows.Forms.SplitContainer spcManual;
        private System.Windows.Forms.SplitContainer spcTenkey;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private KosesControl.Controls.LedLabel lblTenkeyDisplay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbUseTenkey;
        private System.Windows.Forms.FlowLayoutPanel flpLoaderItems;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem1;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private System.Windows.Forms.TabControl tcTenkey;
        private System.Windows.Forms.TabPage tpLoader;
        private System.Windows.Forms.TabPage tbFlux;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem2;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem4;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem5;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem6;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem7;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem8;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem9;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem10;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem11;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem12;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem13;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem14;
        private USERCONTROL.KosesTenkeyItem kosesTenkeyItem15;
        private KosesControl.Controls.KosesButton btnTenkeyFlux;
    }
}