﻿namespace KOSES_SEQ.OP
{
    partial class frmSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSystem));
            this.panSystem = new System.Windows.Forms.Panel();
            this.panSystemRight = new System.Windows.Forms.Panel();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.btn_SystemLoad = new System.Windows.Forms.Button();
            this.btn_SystemSave = new System.Windows.Forms.Button();
            this.btn_SystemTowerLamp = new KosesControl.Controls.KosesButton();
            this.btn_SystemAIO = new KosesControl.Controls.KosesButton();
            this.btn_SystemDIO = new KosesControl.Controls.KosesButton();
            this.btn_SystemMotor = new KosesControl.Controls.KosesButton();
            this.panSystemRight.SuspendLayout();
            this.kosesGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panSystem
            // 
            this.panSystem.BackColor = System.Drawing.Color.White;
            this.panSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panSystem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panSystem.Location = new System.Drawing.Point(0, 0);
            this.panSystem.Name = "panSystem";
            this.panSystem.Padding = new System.Windows.Forms.Padding(1);
            this.panSystem.Size = new System.Drawing.Size(1154, 875);
            this.panSystem.TabIndex = 953;
            // 
            // panSystemRight
            // 
            this.panSystemRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panSystemRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panSystemRight.Controls.Add(this.kosesGroupBox1);
            this.panSystemRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panSystemRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panSystemRight.Location = new System.Drawing.Point(1154, 0);
            this.panSystemRight.Margin = new System.Windows.Forms.Padding(0);
            this.panSystemRight.Name = "panSystemRight";
            this.panSystemRight.Size = new System.Drawing.Size(151, 875);
            this.panSystemRight.TabIndex = 952;
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.kosesGroupBox1.Controls.Add(this.btn_SystemLoad);
            this.kosesGroupBox1.Controls.Add(this.btn_SystemSave);
            this.kosesGroupBox1.Controls.Add(this.btn_SystemTowerLamp);
            this.kosesGroupBox1.Controls.Add(this.btn_SystemAIO);
            this.kosesGroupBox1.Controls.Add(this.btn_SystemDIO);
            this.kosesGroupBox1.Controls.Add(this.btn_SystemMotor);
            this.kosesGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kosesGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.kosesGroupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Radius = 0;
            this.kosesGroupBox1.Size = new System.Drawing.Size(149, 873);
            this.kosesGroupBox1.TabIndex = 952;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "SELECT";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_SystemLoad
            // 
            this.btn_SystemLoad.BackColor = System.Drawing.Color.Transparent;
            this.btn_SystemLoad.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SystemLoad.Image = ((System.Drawing.Image)(resources.GetObject("btn_SystemLoad.Image")));
            this.btn_SystemLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SystemLoad.Location = new System.Drawing.Point(11, 821);
            this.btn_SystemLoad.Name = "btn_SystemLoad";
            this.btn_SystemLoad.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btn_SystemLoad.Size = new System.Drawing.Size(127, 49);
            this.btn_SystemLoad.TabIndex = 961;
            this.btn_SystemLoad.Text = "   LOAD";
            this.btn_SystemLoad.UseVisualStyleBackColor = false;
            this.btn_SystemLoad.Click += new System.EventHandler(this.btn_SystemLoad_Click);
            // 
            // btn_SystemSave
            // 
            this.btn_SystemSave.BackColor = System.Drawing.Color.Transparent;
            this.btn_SystemSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SystemSave.Image = ((System.Drawing.Image)(resources.GetObject("btn_SystemSave.Image")));
            this.btn_SystemSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SystemSave.Location = new System.Drawing.Point(11, 763);
            this.btn_SystemSave.Name = "btn_SystemSave";
            this.btn_SystemSave.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btn_SystemSave.Size = new System.Drawing.Size(127, 49);
            this.btn_SystemSave.TabIndex = 960;
            this.btn_SystemSave.Text = "   SAVE";
            this.btn_SystemSave.UseVisualStyleBackColor = false;
            this.btn_SystemSave.Click += new System.EventHandler(this.btn_SystemSave_Click);
            // 
            // btn_SystemTowerLamp
            // 
            this.btn_SystemTowerLamp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemTowerLamp.Bold = true;
            this.btn_SystemTowerLamp.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemTowerLamp.Depth = 0;
            this.btn_SystemTowerLamp.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemTowerLamp.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemTowerLamp.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemTowerLamp.DisabledTextColor = System.Drawing.Color.Gray;
            this.btn_SystemTowerLamp.DrawShadows = true;
            this.btn_SystemTowerLamp.FontSize = 14F;
            this.btn_SystemTowerLamp.ForeColor = System.Drawing.Color.White;
            this.btn_SystemTowerLamp.HighEmphasis = true;
            this.btn_SystemTowerLamp.Icon = null;
            this.btn_SystemTowerLamp.IconLocation = new System.Drawing.Point(-10, 0);
            this.btn_SystemTowerLamp.Location = new System.Drawing.Point(11, 262);
            this.btn_SystemTowerLamp.MouseOnOpacity = 50;
            this.btn_SystemTowerLamp.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btn_SystemTowerLamp.Name = "btn_SystemTowerLamp";
            this.btn_SystemTowerLamp.Selected = false;
            this.btn_SystemTowerLamp.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btn_SystemTowerLamp.Size = new System.Drawing.Size(127, 49);
            this.btn_SystemTowerLamp.TabIndex = 959;
            this.btn_SystemTowerLamp.Text = "TOWER LAMP";
            this.btn_SystemTowerLamp.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btn_SystemTowerLamp.UseVisualStyleBackColor = false;
            this.btn_SystemTowerLamp.Click += new System.EventHandler(this.btn_SystemTowerLamp_Click);
            // 
            // btn_SystemAIO
            // 
            this.btn_SystemAIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemAIO.Bold = true;
            this.btn_SystemAIO.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemAIO.Depth = 0;
            this.btn_SystemAIO.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemAIO.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemAIO.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemAIO.DisabledTextColor = System.Drawing.Color.Gray;
            this.btn_SystemAIO.DrawShadows = true;
            this.btn_SystemAIO.FontSize = 14F;
            this.btn_SystemAIO.ForeColor = System.Drawing.Color.White;
            this.btn_SystemAIO.HighEmphasis = true;
            this.btn_SystemAIO.Icon = null;
            this.btn_SystemAIO.IconLocation = new System.Drawing.Point(-10, 0);
            this.btn_SystemAIO.Location = new System.Drawing.Point(11, 184);
            this.btn_SystemAIO.MouseOnOpacity = 50;
            this.btn_SystemAIO.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btn_SystemAIO.Name = "btn_SystemAIO";
            this.btn_SystemAIO.Selected = false;
            this.btn_SystemAIO.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btn_SystemAIO.Size = new System.Drawing.Size(127, 49);
            this.btn_SystemAIO.TabIndex = 958;
            this.btn_SystemAIO.Text = "AIO";
            this.btn_SystemAIO.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btn_SystemAIO.UseVisualStyleBackColor = false;
            this.btn_SystemAIO.Click += new System.EventHandler(this.btn_SystemAIO_Click);
            // 
            // btn_SystemDIO
            // 
            this.btn_SystemDIO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemDIO.Bold = true;
            this.btn_SystemDIO.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemDIO.Depth = 0;
            this.btn_SystemDIO.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemDIO.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemDIO.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemDIO.DisabledTextColor = System.Drawing.Color.Gray;
            this.btn_SystemDIO.DrawShadows = true;
            this.btn_SystemDIO.FontSize = 14F;
            this.btn_SystemDIO.ForeColor = System.Drawing.Color.White;
            this.btn_SystemDIO.HighEmphasis = true;
            this.btn_SystemDIO.Icon = null;
            this.btn_SystemDIO.IconLocation = new System.Drawing.Point(-10, 0);
            this.btn_SystemDIO.Location = new System.Drawing.Point(11, 109);
            this.btn_SystemDIO.MouseOnOpacity = 50;
            this.btn_SystemDIO.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btn_SystemDIO.Name = "btn_SystemDIO";
            this.btn_SystemDIO.Selected = false;
            this.btn_SystemDIO.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btn_SystemDIO.Size = new System.Drawing.Size(127, 49);
            this.btn_SystemDIO.TabIndex = 957;
            this.btn_SystemDIO.Text = "DIO";
            this.btn_SystemDIO.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btn_SystemDIO.UseVisualStyleBackColor = false;
            this.btn_SystemDIO.Click += new System.EventHandler(this.btn_SystemDIO_Click);
            // 
            // btn_SystemMotor
            // 
            this.btn_SystemMotor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemMotor.Bold = true;
            this.btn_SystemMotor.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemMotor.Depth = 0;
            this.btn_SystemMotor.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btn_SystemMotor.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemMotor.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btn_SystemMotor.DisabledTextColor = System.Drawing.Color.Gray;
            this.btn_SystemMotor.DrawShadows = true;
            this.btn_SystemMotor.FontSize = 14F;
            this.btn_SystemMotor.ForeColor = System.Drawing.Color.White;
            this.btn_SystemMotor.HighEmphasis = true;
            this.btn_SystemMotor.Icon = null;
            this.btn_SystemMotor.IconLocation = new System.Drawing.Point(-10, 0);
            this.btn_SystemMotor.Location = new System.Drawing.Point(11, 37);
            this.btn_SystemMotor.MouseOnOpacity = 50;
            this.btn_SystemMotor.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btn_SystemMotor.Name = "btn_SystemMotor";
            this.btn_SystemMotor.Selected = false;
            this.btn_SystemMotor.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btn_SystemMotor.Size = new System.Drawing.Size(127, 49);
            this.btn_SystemMotor.TabIndex = 956;
            this.btn_SystemMotor.Text = "MOTOR";
            this.btn_SystemMotor.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btn_SystemMotor.UseVisualStyleBackColor = false;
            this.btn_SystemMotor.Click += new System.EventHandler(this.btn_SystemMotor_Click);
            // 
            // frmSystem
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1305, 875);
            this.Controls.Add(this.panSystem);
            this.Controls.Add(this.panSystemRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSystem";
            this.Text = "Manual";
            this.Load += new System.EventHandler(this.frmSystem_Load);
            this.panSystemRight.ResumeLayout(false);
            this.kosesGroupBox1.ResumeLayout(false);
            this.kosesGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panSystem;
        internal System.Windows.Forms.Panel panSystemRight;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private KosesControl.Controls.KosesButton btn_SystemMotor;
        private KosesControl.Controls.KosesButton btn_SystemTowerLamp;
        private KosesControl.Controls.KosesButton btn_SystemAIO;
        private KosesControl.Controls.KosesButton btn_SystemDIO;
        private System.Windows.Forms.Button btn_SystemSave;
        private System.Windows.Forms.Button btn_SystemLoad;
    }
}