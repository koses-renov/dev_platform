﻿namespace KOSES_SEQ.OP
{
    partial class frmTeaching
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTeaching));
            this.panManual = new System.Windows.Forms.Panel();
            this.pgTeaching = new System.Windows.Forms.PropertyGrid();
            this.tvTeaching = new System.Windows.Forms.TreeView();
            this.panManualRight = new System.Windows.Forms.Panel();
            this.btn_TeachingSetEncValue = new System.Windows.Forms.Button();
            this.btn_TeachingMotorStop = new System.Windows.Forms.Button();
            this.btn_TeachingZeroSet = new System.Windows.Forms.Button();
            this.btn_TeachingServoOn = new System.Windows.Forms.Button();
            this.btn_TeachingServoOff = new System.Windows.Forms.Button();
            this.btn_TeachingAlarmClear = new System.Windows.Forms.Button();
            this.btn_TeachingHome = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.l = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_TeachingRecipeSave = new System.Windows.Forms.Button();
            this.timer_MotorStatus = new System.Windows.Forms.Timer(this.components);
            this.kosesGroupBox3 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TeachingJogCw = new System.Windows.Forms.Button();
            this.btn_TeachingJogCcw = new System.Windows.Forms.Button();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TeachingJogRel10 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRel01 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRel05 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelDot01 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelDot02 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelDot03 = new System.Windows.Forms.Button();
            this.txt_TeachingRelValue = new System.Windows.Forms.TextBox();
            this.btn_TeachingJogRelCw = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelCcw = new System.Windows.Forms.Button();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.btn_TeachingAbsGetValue = new System.Windows.Forms.Button();
            this.btn_TeachingJogAbs = new System.Windows.Forms.Button();
            this.txt_TeachingAbsValue = new System.Windows.Forms.TextBox();
            this.led_TeachingHome = new Bulb.LedBulb();
            this.led_TeachingCcwLimit = new Bulb.LedBulb();
            this.led_TeachingCwLimit = new Bulb.LedBulb();
            this.led_TeachingInpos = new Bulb.LedBulb();
            this.led_TeachingAlarm = new Bulb.LedBulb();
            this.led_TeachingServo = new Bulb.LedBulb();
            this.led_TeachingEncoder = new KosesControl.Controls.LedLabel();
            this.led_TeachingCommand = new KosesControl.Controls.LedLabel();
            this.lbl_TeachingAxisName = new KosesControl.Controls.LedLabel();
            this.panManual.SuspendLayout();
            this.panManualRight.SuspendLayout();
            this.kosesGroupBox3.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            this.kosesGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.pgTeaching);
            this.panManual.Controls.Add(this.tvTeaching);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(994, 875);
            this.panManual.TabIndex = 953;
            // 
            // pgTeaching
            // 
            this.pgTeaching.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pgTeaching.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgTeaching.Location = new System.Drawing.Point(326, 4);
            this.pgTeaching.Name = "pgTeaching";
            this.pgTeaching.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.pgTeaching.Size = new System.Drawing.Size(623, 868);
            this.pgTeaching.TabIndex = 1;
            this.pgTeaching.ToolbarVisible = false;
            this.pgTeaching.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.pgTeaching_SelectedGridItemChanged);
            this.pgTeaching.SelectedObjectsChanged += new System.EventHandler(this.pgTeaching_SelectedObjectsChanged);
            // 
            // tvTeaching
            // 
            this.tvTeaching.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvTeaching.Location = new System.Drawing.Point(4, 4);
            this.tvTeaching.Name = "tvTeaching";
            this.tvTeaching.Size = new System.Drawing.Size(316, 868);
            this.tvTeaching.TabIndex = 0;
            // 
            // panManualRight
            // 
            this.panManualRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panManualRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panManualRight.Controls.Add(this.btn_TeachingSetEncValue);
            this.panManualRight.Controls.Add(this.btn_TeachingMotorStop);
            this.panManualRight.Controls.Add(this.kosesGroupBox3);
            this.panManualRight.Controls.Add(this.kosesGroupBox2);
            this.panManualRight.Controls.Add(this.kosesGroupBox1);
            this.panManualRight.Controls.Add(this.btn_TeachingZeroSet);
            this.panManualRight.Controls.Add(this.led_TeachingHome);
            this.panManualRight.Controls.Add(this.led_TeachingCcwLimit);
            this.panManualRight.Controls.Add(this.led_TeachingCwLimit);
            this.panManualRight.Controls.Add(this.led_TeachingInpos);
            this.panManualRight.Controls.Add(this.led_TeachingAlarm);
            this.panManualRight.Controls.Add(this.led_TeachingServo);
            this.panManualRight.Controls.Add(this.btn_TeachingServoOn);
            this.panManualRight.Controls.Add(this.btn_TeachingServoOff);
            this.panManualRight.Controls.Add(this.btn_TeachingAlarmClear);
            this.panManualRight.Controls.Add(this.btn_TeachingHome);
            this.panManualRight.Controls.Add(this.led_TeachingEncoder);
            this.panManualRight.Controls.Add(this.led_TeachingCommand);
            this.panManualRight.Controls.Add(this.label7);
            this.panManualRight.Controls.Add(this.label2);
            this.panManualRight.Controls.Add(this.label6);
            this.panManualRight.Controls.Add(this.label5);
            this.panManualRight.Controls.Add(this.label4);
            this.panManualRight.Controls.Add(this.label3);
            this.panManualRight.Controls.Add(this.l);
            this.panManualRight.Controls.Add(this.label1);
            this.panManualRight.Controls.Add(this.lbl_TeachingAxisName);
            this.panManualRight.Controls.Add(this.btn_TeachingRecipeSave);
            this.panManualRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panManualRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManualRight.Location = new System.Drawing.Point(994, 0);
            this.panManualRight.Margin = new System.Windows.Forms.Padding(0);
            this.panManualRight.Name = "panManualRight";
            this.panManualRight.Size = new System.Drawing.Size(311, 875);
            this.panManualRight.TabIndex = 952;
            // 
            // btn_TeachingSetEncValue
            // 
            this.btn_TeachingSetEncValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingSetEncValue.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingSetEncValue.Image")));
            this.btn_TeachingSetEncValue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingSetEncValue.Location = new System.Drawing.Point(10, 231);
            this.btn_TeachingSetEncValue.Name = "btn_TeachingSetEncValue";
            this.btn_TeachingSetEncValue.Size = new System.Drawing.Size(297, 45);
            this.btn_TeachingSetEncValue.TabIndex = 1005;
            this.btn_TeachingSetEncValue.Text = "SET VALUE";
            this.btn_TeachingSetEncValue.UseVisualStyleBackColor = true;
            this.btn_TeachingSetEncValue.Click += new System.EventHandler(this.btn_TeachingSetEncValue_Click);
            // 
            // btn_TeachingMotorStop
            // 
            this.btn_TeachingMotorStop.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingMotorStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingMotorStop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingMotorStop.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingMotorStop.Image")));
            this.btn_TeachingMotorStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingMotorStop.Location = new System.Drawing.Point(12, 798);
            this.btn_TeachingMotorStop.Name = "btn_TeachingMotorStop";
            this.btn_TeachingMotorStop.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingMotorStop.Size = new System.Drawing.Size(146, 73);
            this.btn_TeachingMotorStop.TabIndex = 1004;
            this.btn_TeachingMotorStop.Text = "MOTOR \r\nSTOP";
            this.btn_TeachingMotorStop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingMotorStop.UseVisualStyleBackColor = false;
            this.btn_TeachingMotorStop.Click += new System.EventHandler(this.btn_TeachingMotorStop_Click);
            // 
            // btn_TeachingZeroSet
            // 
            this.btn_TeachingZeroSet.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingZeroSet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingZeroSet.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingZeroSet.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingZeroSet.Image")));
            this.btn_TeachingZeroSet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingZeroSet.Location = new System.Drawing.Point(208, 159);
            this.btn_TeachingZeroSet.Name = "btn_TeachingZeroSet";
            this.btn_TeachingZeroSet.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingZeroSet.Size = new System.Drawing.Size(98, 70);
            this.btn_TeachingZeroSet.TabIndex = 997;
            this.btn_TeachingZeroSet.Text = "ZERO\r\nSET";
            this.btn_TeachingZeroSet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingZeroSet.UseVisualStyleBackColor = false;
            this.btn_TeachingZeroSet.Click += new System.EventHandler(this.btn_TeachingZeroSet_Click);
            // 
            // btn_TeachingServoOn
            // 
            this.btn_TeachingServoOn.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingServoOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingServoOn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingServoOn.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingServoOn.Image")));
            this.btn_TeachingServoOn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingServoOn.Location = new System.Drawing.Point(9, 278);
            this.btn_TeachingServoOn.Name = "btn_TeachingServoOn";
            this.btn_TeachingServoOn.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingServoOn.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingServoOn.TabIndex = 985;
            this.btn_TeachingServoOn.Text = "SERVO\r\nON";
            this.btn_TeachingServoOn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingServoOn.UseVisualStyleBackColor = false;
            this.btn_TeachingServoOn.Click += new System.EventHandler(this.btn_TeachingServoOn_Click);
            // 
            // btn_TeachingServoOff
            // 
            this.btn_TeachingServoOff.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingServoOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingServoOff.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingServoOff.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingServoOff.Image")));
            this.btn_TeachingServoOff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingServoOff.Location = new System.Drawing.Point(166, 278);
            this.btn_TeachingServoOff.Name = "btn_TeachingServoOff";
            this.btn_TeachingServoOff.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingServoOff.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingServoOff.TabIndex = 984;
            this.btn_TeachingServoOff.Text = "SERVO\r\nOFF";
            this.btn_TeachingServoOff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingServoOff.UseVisualStyleBackColor = false;
            this.btn_TeachingServoOff.Click += new System.EventHandler(this.btn_TeachingServoOff_Click);
            // 
            // btn_TeachingAlarmClear
            // 
            this.btn_TeachingAlarmClear.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingAlarmClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingAlarmClear.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingAlarmClear.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingAlarmClear.Image")));
            this.btn_TeachingAlarmClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingAlarmClear.Location = new System.Drawing.Point(9, 341);
            this.btn_TeachingAlarmClear.Name = "btn_TeachingAlarmClear";
            this.btn_TeachingAlarmClear.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingAlarmClear.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingAlarmClear.TabIndex = 983;
            this.btn_TeachingAlarmClear.Text = "ALARM\r\nCLEAR";
            this.btn_TeachingAlarmClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingAlarmClear.UseVisualStyleBackColor = false;
            this.btn_TeachingAlarmClear.Click += new System.EventHandler(this.btn_TeachingAlarmClear_Click);
            // 
            // btn_TeachingHome
            // 
            this.btn_TeachingHome.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingHome.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingHome.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingHome.Image")));
            this.btn_TeachingHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingHome.Location = new System.Drawing.Point(166, 340);
            this.btn_TeachingHome.Name = "btn_TeachingHome";
            this.btn_TeachingHome.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingHome.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingHome.TabIndex = 982;
            this.btn_TeachingHome.Text = "HOME";
            this.btn_TeachingHome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingHome.UseVisualStyleBackColor = false;
            this.btn_TeachingHome.Click += new System.EventHandler(this.btn_TeachingHome_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 19);
            this.label7.TabIndex = 968;
            this.label7.Text = "ENC";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 19);
            this.label2.TabIndex = 967;
            this.label2.Text = "CMD";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(185, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 19);
            this.label6.TabIndex = 965;
            this.label6.Text = "-LIMIT";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 19);
            this.label5.TabIndex = 963;
            this.label5.Text = "IN-POS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(185, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 19);
            this.label4.TabIndex = 961;
            this.label4.Text = "HOME";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 19);
            this.label3.TabIndex = 959;
            this.label3.Text = "ALARM";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l
            // 
            this.l.AutoSize = true;
            this.l.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l.Location = new System.Drawing.Point(185, 47);
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(70, 19);
            this.l.TabIndex = 957;
            this.l.Text = "+LIMIT";
            this.l.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "SERVO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_TeachingRecipeSave
            // 
            this.btn_TeachingRecipeSave.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingRecipeSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingRecipeSave.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingRecipeSave.Image")));
            this.btn_TeachingRecipeSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingRecipeSave.Location = new System.Drawing.Point(164, 798);
            this.btn_TeachingRecipeSave.Name = "btn_TeachingRecipeSave";
            this.btn_TeachingRecipeSave.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btn_TeachingRecipeSave.Size = new System.Drawing.Size(139, 73);
            this.btn_TeachingRecipeSave.TabIndex = 953;
            this.btn_TeachingRecipeSave.Text = "RECIPE\r\nSAVE";
            this.btn_TeachingRecipeSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingRecipeSave.UseVisualStyleBackColor = false;
            this.btn_TeachingRecipeSave.Click += new System.EventHandler(this.btnRecipeSave_Click);
            // 
            // timer_MotorStatus
            // 
            this.timer_MotorStatus.Interval = 500;
            this.timer_MotorStatus.Tick += new System.EventHandler(this.timer_MotorStatus_Tick);
            // 
            // kosesGroupBox3
            // 
            this.kosesGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox3.Controls.Add(this.btn_TeachingJogCw);
            this.kosesGroupBox3.Controls.Add(this.btn_TeachingJogCcw);
            this.kosesGroupBox3.Location = new System.Drawing.Point(11, 404);
            this.kosesGroupBox3.Name = "kosesGroupBox3";
            this.kosesGroupBox3.Padding = new System.Windows.Forms.Padding(1);
            this.kosesGroupBox3.Radius = 15;
            this.kosesGroupBox3.Size = new System.Drawing.Size(295, 84);
            this.kosesGroupBox3.TabIndex = 1002;
            this.kosesGroupBox3.TabStop = false;
            this.kosesGroupBox3.Text = "JOG";
            this.kosesGroupBox3.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox3.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox3.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox3.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox3.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TeachingJogCw
            // 
            this.btn_TeachingJogCw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogCw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogCw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogCw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogCw.Image")));
            this.btn_TeachingJogCw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogCw.Location = new System.Drawing.Point(151, 22);
            this.btn_TeachingJogCw.Name = "btn_TeachingJogCw";
            this.btn_TeachingJogCw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogCw.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingJogCw.TabIndex = 1001;
            this.btn_TeachingJogCw.Text = "JOG(+)";
            this.btn_TeachingJogCw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogCw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogCw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCw_MouseDown);
            this.btn_TeachingJogCw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCw_MouseUp);
            // 
            // btn_TeachingJogCcw
            // 
            this.btn_TeachingJogCcw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogCcw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogCcw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogCcw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogCcw.Image")));
            this.btn_TeachingJogCcw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogCcw.Location = new System.Drawing.Point(5, 22);
            this.btn_TeachingJogCcw.Name = "btn_TeachingJogCcw";
            this.btn_TeachingJogCcw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogCcw.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingJogCcw.TabIndex = 1000;
            this.btn_TeachingJogCcw.Text = "JOG(-)";
            this.btn_TeachingJogCcw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogCcw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogCcw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCcw_MouseDown);
            this.btn_TeachingJogCcw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCcw_MouseUp);
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRel10);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRel01);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRel05);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRelDot01);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRelDot02);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRelDot03);
            this.kosesGroupBox2.Controls.Add(this.txt_TeachingRelValue);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRelCw);
            this.kosesGroupBox2.Controls.Add(this.btn_TeachingJogRelCcw);
            this.kosesGroupBox2.Location = new System.Drawing.Point(11, 492);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Padding = new System.Windows.Forms.Padding(1);
            this.kosesGroupBox2.Radius = 15;
            this.kosesGroupBox2.Size = new System.Drawing.Size(295, 202);
            this.kosesGroupBox2.TabIndex = 1001;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "RELATIVE";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TeachingJogRel10
            // 
            this.btn_TeachingJogRel10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRel10.Location = new System.Drawing.Point(200, 67);
            this.btn_TeachingJogRel10.Name = "btn_TeachingJogRel10";
            this.btn_TeachingJogRel10.Size = new System.Drawing.Size(90, 45);
            this.btn_TeachingJogRel10.TabIndex = 1007;
            this.btn_TeachingJogRel10.Text = "10";
            this.btn_TeachingJogRel10.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRel10.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRel01
            // 
            this.btn_TeachingJogRel01.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRel01.Location = new System.Drawing.Point(5, 67);
            this.btn_TeachingJogRel01.Name = "btn_TeachingJogRel01";
            this.btn_TeachingJogRel01.Size = new System.Drawing.Size(90, 45);
            this.btn_TeachingJogRel01.TabIndex = 1006;
            this.btn_TeachingJogRel01.Text = "1";
            this.btn_TeachingJogRel01.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRel01.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRel05
            // 
            this.btn_TeachingJogRel05.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRel05.Location = new System.Drawing.Point(104, 67);
            this.btn_TeachingJogRel05.Name = "btn_TeachingJogRel05";
            this.btn_TeachingJogRel05.Size = new System.Drawing.Size(90, 45);
            this.btn_TeachingJogRel05.TabIndex = 1005;
            this.btn_TeachingJogRel05.Text = "5";
            this.btn_TeachingJogRel05.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRel05.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRelDot01
            // 
            this.btn_TeachingJogRelDot01.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelDot01.Location = new System.Drawing.Point(200, 21);
            this.btn_TeachingJogRelDot01.Name = "btn_TeachingJogRelDot01";
            this.btn_TeachingJogRelDot01.Size = new System.Drawing.Size(90, 45);
            this.btn_TeachingJogRelDot01.TabIndex = 1004;
            this.btn_TeachingJogRelDot01.Text = "0.1";
            this.btn_TeachingJogRelDot01.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRelDot01.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRelDot02
            // 
            this.btn_TeachingJogRelDot02.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelDot02.Location = new System.Drawing.Point(104, 21);
            this.btn_TeachingJogRelDot02.Name = "btn_TeachingJogRelDot02";
            this.btn_TeachingJogRelDot02.Size = new System.Drawing.Size(90, 45);
            this.btn_TeachingJogRelDot02.TabIndex = 1003;
            this.btn_TeachingJogRelDot02.Text = "0.01";
            this.btn_TeachingJogRelDot02.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRelDot02.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRelDot03
            // 
            this.btn_TeachingJogRelDot03.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelDot03.Location = new System.Drawing.Point(5, 21);
            this.btn_TeachingJogRelDot03.Name = "btn_TeachingJogRelDot03";
            this.btn_TeachingJogRelDot03.Size = new System.Drawing.Size(90, 45);
            this.btn_TeachingJogRelDot03.TabIndex = 1002;
            this.btn_TeachingJogRelDot03.Text = "0.001";
            this.btn_TeachingJogRelDot03.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRelDot03.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // txt_TeachingRelValue
            // 
            this.txt_TeachingRelValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TeachingRelValue.Location = new System.Drawing.Point(5, 113);
            this.txt_TeachingRelValue.Name = "txt_TeachingRelValue";
            this.txt_TeachingRelValue.Size = new System.Drawing.Size(137, 27);
            this.txt_TeachingRelValue.TabIndex = 1001;
            this.txt_TeachingRelValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_TeachingJogRelCw
            // 
            this.btn_TeachingJogRelCw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogRelCw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogRelCw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelCw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogRelCw.Image")));
            this.btn_TeachingJogRelCw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogRelCw.Location = new System.Drawing.Point(150, 141);
            this.btn_TeachingJogRelCw.Name = "btn_TeachingJogRelCw";
            this.btn_TeachingJogRelCw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogRelCw.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingJogRelCw.TabIndex = 1000;
            this.btn_TeachingJogRelCw.Text = "REL(+)";
            this.btn_TeachingJogRelCw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogRelCw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogRelCw.Click += new System.EventHandler(this.btn_TeachingJogRelCw_Click);
            // 
            // btn_TeachingJogRelCcw
            // 
            this.btn_TeachingJogRelCcw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogRelCcw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogRelCcw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelCcw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogRelCcw.Image")));
            this.btn_TeachingJogRelCcw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogRelCcw.Location = new System.Drawing.Point(3, 141);
            this.btn_TeachingJogRelCcw.Name = "btn_TeachingJogRelCcw";
            this.btn_TeachingJogRelCcw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogRelCcw.Size = new System.Drawing.Size(140, 60);
            this.btn_TeachingJogRelCcw.TabIndex = 987;
            this.btn_TeachingJogRelCcw.Text = "REL(-)";
            this.btn_TeachingJogRelCcw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogRelCcw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogRelCcw.Click += new System.EventHandler(this.btn_TeachingJogRelCcw_Click);
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox1.Controls.Add(this.btn_TeachingAbsGetValue);
            this.kosesGroupBox1.Controls.Add(this.btn_TeachingJogAbs);
            this.kosesGroupBox1.Controls.Add(this.txt_TeachingAbsValue);
            this.kosesGroupBox1.Location = new System.Drawing.Point(11, 698);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Padding = new System.Windows.Forms.Padding(1);
            this.kosesGroupBox1.Radius = 15;
            this.kosesGroupBox1.Size = new System.Drawing.Size(295, 98);
            this.kosesGroupBox1.TabIndex = 1000;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "ABSOLUTELY";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // btn_TeachingAbsGetValue
            // 
            this.btn_TeachingAbsGetValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingAbsGetValue.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingAbsGetValue.Image")));
            this.btn_TeachingAbsGetValue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingAbsGetValue.Location = new System.Drawing.Point(5, 51);
            this.btn_TeachingAbsGetValue.Name = "btn_TeachingAbsGetValue";
            this.btn_TeachingAbsGetValue.Size = new System.Drawing.Size(139, 45);
            this.btn_TeachingAbsGetValue.TabIndex = 1004;
            this.btn_TeachingAbsGetValue.Text = "GET VALUE";
            this.btn_TeachingAbsGetValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingAbsGetValue.UseVisualStyleBackColor = true;
            this.btn_TeachingAbsGetValue.Click += new System.EventHandler(this.btn_TeachingAbsGetValue_Click);
            // 
            // btn_TeachingJogAbs
            // 
            this.btn_TeachingJogAbs.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogAbs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogAbs.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogAbs.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogAbs.Image")));
            this.btn_TeachingJogAbs.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogAbs.Location = new System.Drawing.Point(150, 22);
            this.btn_TeachingJogAbs.Name = "btn_TeachingJogAbs";
            this.btn_TeachingJogAbs.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogAbs.Size = new System.Drawing.Size(140, 73);
            this.btn_TeachingJogAbs.TabIndex = 1003;
            this.btn_TeachingJogAbs.Text = "ABS";
            this.btn_TeachingJogAbs.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogAbs.UseVisualStyleBackColor = false;
            this.btn_TeachingJogAbs.Click += new System.EventHandler(this.btn_TeachingJogAbs_Click);
            // 
            // txt_TeachingAbsValue
            // 
            this.txt_TeachingAbsValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TeachingAbsValue.Location = new System.Drawing.Point(5, 23);
            this.txt_TeachingAbsValue.Name = "txt_TeachingAbsValue";
            this.txt_TeachingAbsValue.Size = new System.Drawing.Size(138, 27);
            this.txt_TeachingAbsValue.TabIndex = 1002;
            this.txt_TeachingAbsValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // led_TeachingHome
            // 
            this.led_TeachingHome.Location = new System.Drawing.Point(264, 78);
            this.led_TeachingHome.Name = "led_TeachingHome";
            this.led_TeachingHome.On = true;
            this.led_TeachingHome.Size = new System.Drawing.Size(39, 33);
            this.led_TeachingHome.TabIndex = 996;
            // 
            // led_TeachingCcwLimit
            // 
            this.led_TeachingCcwLimit.Color = System.Drawing.Color.Red;
            this.led_TeachingCcwLimit.Location = new System.Drawing.Point(264, 116);
            this.led_TeachingCcwLimit.Name = "led_TeachingCcwLimit";
            this.led_TeachingCcwLimit.On = true;
            this.led_TeachingCcwLimit.Size = new System.Drawing.Size(39, 33);
            this.led_TeachingCcwLimit.TabIndex = 995;
            // 
            // led_TeachingCwLimit
            // 
            this.led_TeachingCwLimit.Color = System.Drawing.Color.Red;
            this.led_TeachingCwLimit.Location = new System.Drawing.Point(264, 40);
            this.led_TeachingCwLimit.Name = "led_TeachingCwLimit";
            this.led_TeachingCwLimit.On = true;
            this.led_TeachingCwLimit.Size = new System.Drawing.Size(39, 33);
            this.led_TeachingCwLimit.TabIndex = 994;
            // 
            // led_TeachingInpos
            // 
            this.led_TeachingInpos.Location = new System.Drawing.Point(86, 116);
            this.led_TeachingInpos.Name = "led_TeachingInpos";
            this.led_TeachingInpos.On = true;
            this.led_TeachingInpos.Size = new System.Drawing.Size(39, 33);
            this.led_TeachingInpos.TabIndex = 993;
            // 
            // led_TeachingAlarm
            // 
            this.led_TeachingAlarm.Color = System.Drawing.Color.Red;
            this.led_TeachingAlarm.Location = new System.Drawing.Point(86, 78);
            this.led_TeachingAlarm.Name = "led_TeachingAlarm";
            this.led_TeachingAlarm.On = true;
            this.led_TeachingAlarm.Size = new System.Drawing.Size(39, 33);
            this.led_TeachingAlarm.TabIndex = 992;
            // 
            // led_TeachingServo
            // 
            this.led_TeachingServo.Location = new System.Drawing.Point(86, 40);
            this.led_TeachingServo.Name = "led_TeachingServo";
            this.led_TeachingServo.On = true;
            this.led_TeachingServo.Size = new System.Drawing.Size(39, 33);
            this.led_TeachingServo.TabIndex = 991;
            // 
            // led_TeachingEncoder
            // 
            this.led_TeachingEncoder.BackColor = System.Drawing.Color.Black;
            this.led_TeachingEncoder.Bold = false;
            this.led_TeachingEncoder.DecimalPoint = 3;
            this.led_TeachingEncoder.Font = new System.Drawing.Font("Noto Sans KR", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.led_TeachingEncoder.FontSize = 15F;
            this.led_TeachingEncoder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingEncoder.LedOffColor = System.Drawing.Color.Gray;
            this.led_TeachingEncoder.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingEncoder.Location = new System.Drawing.Point(57, 197);
            this.led_TeachingEncoder.Maximum = 999D;
            this.led_TeachingEncoder.Minimum = 0D;
            this.led_TeachingEncoder.Name = "led_TeachingEncoder";
            this.led_TeachingEncoder.On = true;
            this.led_TeachingEncoder.Size = new System.Drawing.Size(141, 31);
            this.led_TeachingEncoder.TabIndex = 970;
            this.led_TeachingEncoder.Text = "0.00000";
            this.led_TeachingEncoder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_TeachingEncoder.TextType = KosesControl.TextTypes.TEXT;
            // 
            // led_TeachingCommand
            // 
            this.led_TeachingCommand.BackColor = System.Drawing.Color.Black;
            this.led_TeachingCommand.Bold = false;
            this.led_TeachingCommand.DecimalPoint = 3;
            this.led_TeachingCommand.Font = new System.Drawing.Font("Noto Sans KR", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.led_TeachingCommand.FontSize = 15F;
            this.led_TeachingCommand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingCommand.LedOffColor = System.Drawing.Color.Gray;
            this.led_TeachingCommand.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingCommand.Location = new System.Drawing.Point(57, 160);
            this.led_TeachingCommand.Maximum = 999D;
            this.led_TeachingCommand.Minimum = 0D;
            this.led_TeachingCommand.Name = "led_TeachingCommand";
            this.led_TeachingCommand.On = true;
            this.led_TeachingCommand.Size = new System.Drawing.Size(141, 31);
            this.led_TeachingCommand.TabIndex = 969;
            this.led_TeachingCommand.Text = "0.00000";
            this.led_TeachingCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_TeachingCommand.TextType = KosesControl.TextTypes.TEXT;
            // 
            // lbl_TeachingAxisName
            // 
            this.lbl_TeachingAxisName.BackColor = System.Drawing.Color.Black;
            this.lbl_TeachingAxisName.Bold = false;
            this.lbl_TeachingAxisName.DecimalPoint = 3;
            this.lbl_TeachingAxisName.Font = new System.Drawing.Font("Noto Sans KR", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbl_TeachingAxisName.FontSize = 15F;
            this.lbl_TeachingAxisName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lbl_TeachingAxisName.LedOffColor = System.Drawing.Color.Gray;
            this.lbl_TeachingAxisName.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lbl_TeachingAxisName.Location = new System.Drawing.Point(-2, 0);
            this.lbl_TeachingAxisName.Maximum = 999D;
            this.lbl_TeachingAxisName.Minimum = 0D;
            this.lbl_TeachingAxisName.Name = "lbl_TeachingAxisName";
            this.lbl_TeachingAxisName.On = true;
            this.lbl_TeachingAxisName.Size = new System.Drawing.Size(312, 36);
            this.lbl_TeachingAxisName.TabIndex = 955;
            this.lbl_TeachingAxisName.Text = "Axis Name";
            this.lbl_TeachingAxisName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_TeachingAxisName.TextType = KosesControl.TextTypes.TEXT;
            // 
            // frmTeaching
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1305, 875);
            this.Controls.Add(this.panManual);
            this.Controls.Add(this.panManualRight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTeaching";
            this.Text = "Recipe";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmTeaching_FormClosed);
            this.Load += new System.EventHandler(this.frmTeaching_Load);
            this.panManual.ResumeLayout(false);
            this.panManualRight.ResumeLayout(false);
            this.panManualRight.PerformLayout();
            this.kosesGroupBox3.ResumeLayout(false);
            this.kosesGroupBox2.ResumeLayout(false);
            this.kosesGroupBox2.PerformLayout();
            this.kosesGroupBox1.ResumeLayout(false);
            this.kosesGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        internal System.Windows.Forms.Panel panManualRight;
        private System.Windows.Forms.PropertyGrid pgTeaching;
        private System.Windows.Forms.TreeView tvTeaching;
        private System.Windows.Forms.Button btn_TeachingRecipeSave;
        private KosesControl.Controls.LedLabel lbl_TeachingAxisName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label l;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private KosesControl.Controls.LedLabel led_TeachingEncoder;
        private KosesControl.Controls.LedLabel led_TeachingCommand;
        private System.Windows.Forms.Button btn_TeachingServoOn;
        private System.Windows.Forms.Button btn_TeachingServoOff;
        private System.Windows.Forms.Button btn_TeachingAlarmClear;
        private System.Windows.Forms.Button btn_TeachingHome;
        private Bulb.LedBulb led_TeachingInpos;
        private Bulb.LedBulb led_TeachingAlarm;
        private Bulb.LedBulb led_TeachingServo;
        private Bulb.LedBulb led_TeachingHome;
        private Bulb.LedBulb led_TeachingCcwLimit;
        private Bulb.LedBulb led_TeachingCwLimit;
        private System.Windows.Forms.Button btn_TeachingZeroSet;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox3;
        private System.Windows.Forms.Button btn_TeachingJogCw;
        private System.Windows.Forms.Button btn_TeachingJogCcw;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private System.Windows.Forms.Button btn_TeachingJogRel10;
        private System.Windows.Forms.Button btn_TeachingJogRel01;
        private System.Windows.Forms.Button btn_TeachingJogRel05;
        private System.Windows.Forms.Button btn_TeachingJogRelDot01;
        private System.Windows.Forms.Button btn_TeachingJogRelDot02;
        private System.Windows.Forms.Button btn_TeachingJogRelDot03;
        private System.Windows.Forms.TextBox txt_TeachingRelValue;
        private System.Windows.Forms.Button btn_TeachingJogRelCw;
        private System.Windows.Forms.Button btn_TeachingJogRelCcw;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private System.Windows.Forms.Button btn_TeachingJogAbs;
        private System.Windows.Forms.TextBox txt_TeachingAbsValue;
        private System.Windows.Forms.Button btn_TeachingMotorStop;
        private System.Windows.Forms.Timer timer_MotorStatus;
        private System.Windows.Forms.Button btn_TeachingSetEncValue;
        private System.Windows.Forms.Button btn_TeachingAbsGetValue;
    }
}