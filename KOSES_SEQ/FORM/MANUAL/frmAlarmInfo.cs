﻿using KOSES_SEQ.ALARM;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmAlarmInfo : Form
    {
        readonly string ExternalFileName = @"\AlarmList.xml";

        CAlarm _SelectedAlarm;

        public frmAlarmInfo()
        {
            InitializeComponent();

            this.Load += FrmAlarm_Load;
            this.VisibleChanged += FrmAlarm_VisibleChanged;
        }

        /// <summary>
        /// 처음 보여질 때
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmAlarm_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                cbAlarmLevel.DataSource = Enum.GetValues(typeof(AlarmLevel));

                cbUseAlarmCount.Items.Add("0 - Not Use");
                cbUseAlarmCount.Items.Add("1 - Use");
            }
        }

        /// <summary>
        /// 화면 전환 되었을 때
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmAlarm_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (!DesignMode && this.Visible == true)
                {
                    if (dgvAlarmList.RowCount == 0)
                    {
                        ShowAlarmList();
                        dgvAlarmList.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    }
                    UpdateAlarmImageList();

                    dtFromTime.Value = DateTime.Now.AddDays(-1);
                    dtToTime.Value = DateTime.Now;
                }
            }
            catch
            {
            }
        }

        
        /// <summary>
        /// Change tap (Alarm Log Viewer Tap)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlarmLog_Click(object sender, EventArgs e)
        {
            tcAlarm.SelectedIndex = 0;

        }

        /// <summary>
        /// Change tab (Alarm Editor Tap)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditAlarm_Click(object sender, EventArgs e)
        {
            tcAlarm.SelectedIndex = 1;
        }

        /// <summary>
        /// Alarm Editor Save Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlarmInfoUpdate_Click(object sender, EventArgs e)
        {
            if (_SelectedAlarm != null && tbAlarmCode.Text.Equals(_SelectedAlarm.AlarmCode.ToString()))
            {
                _SelectedAlarm.Message = tbAlarmMessage.Text;
                if (cbAlarmLevel.SelectedItem is AlarmLevel level)
                    _SelectedAlarm.AlarmLevel = level;

                _SelectedAlarm.Description = rtbDescription.Text;
                _SelectedAlarm.Action = rtbAction.Text;

                _SelectedAlarm.IsCountAlarm = cbAlarmLevel.SelectedIndex != 0;
                _SelectedAlarm.ImageFileName = cbImageList.SelectedItem == null ? string.Empty : cbImageList.SelectedItem.ToString();

                DBManager.Instance.AlarmInfoDB.UpdateAlarmInfo(_SelectedAlarm);

                ShowAlarmList();
            }
        }

        /// <summary>
        /// Request Alarm Log View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlarmLogView_Click(object sender, EventArgs e)
        {
            DataTable dt = DBManager.Instance.AlarmLogDB.GetAlarmLogs(dtFromTime.Value, dtToTime.Value);

            dgvAlarmLogView.DataSource = dt;
            dgvAlarmLogView.Columns[0].DefaultCellStyle.Format = "yyyy-MM-dd HH:mm.fff";
            dgvAlarmLogView.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvAlarmLogView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        /// <summary>
        /// 외부 파일(xml)로 Alarm List Save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlarmListSend_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = DBManager.Instance.AlarmInfoDB.GetAlarmLists(true);

                SaveFileDialog dlg = new SaveFileDialog();
                dlg.InitialDirectory = DefPath.Data;
                dlg.Filter = "Xml File | *.xml";
                dlg.ShowDialog();

                if (string.IsNullOrEmpty(dlg.FileName))
                {
                    if (dt != null)
                    {
                        dt.WriteXml(DefPath.Data + ExternalFileName, XmlWriteMode.WriteSchema);
                    }
                }
                else
                {
                    if (dt != null)
                    {
                        dt.WriteXml(Path.Combine(DefPath.Data, dlg.FileName), XmlWriteMode.WriteSchema);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// 외부 파일(xml)에서 Alarm List Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAlarmListLoad_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.InitialDirectory = DefPath.Data;
                dlg.Filter = "Xml File | *.xml";
                dlg.ShowDialog();

                string path = DefPath.Data + ExternalFileName;
                if (!string.IsNullOrEmpty(dlg.FileName))
                {
                    path = dlg.FileName;
                }

                if (File.Exists(path))
                {
                    dt.ReadXmlSchema(path);
                    dt.ReadXml(path);

                    DBManager.Instance.AlarmInfoDB.DeleteAlarmLists();

                    DBManager.Instance.AlarmInfoDB.LoadAlarmList(dt);

                    ShowAlarmList();
                }
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// Alarm Editor에서 Alarm 항목 선택 시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAlarmList_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvAlarmList.SelectedRows.Count == 1)
            {
                if (int.TryParse(dgvAlarmList.SelectedRows[0].Cells[0].Value.ToString(), out int code))
                {
                    _SelectedAlarm = DBManager.Instance.AlarmInfoDB.GetAlarm(code);
                    UpdateAlarmInfo(_SelectedAlarm);
                }
            }
            else
            {
                _SelectedAlarm = null;
            }
        }

        /// <summary>
        /// Alarm Image Viewer(picture box) 내 클릭 시 위치 저장
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbAlarmImageView_MouseClick(object sender, MouseEventArgs e)
        {
            if (_SelectedAlarm != null)
            {
                _SelectedAlarm.ImagePivotX = e.Location.X;
                _SelectedAlarm.ImagePivotY = e.Location.Y;

                pbAlarmImageView.Invalidate();
            }
        }

        /// <summary>
        /// Alarm Editor 이미지 파일 선택 시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbImageList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbImageList.SelectedItem is FileInfo fi)
                {
                    pbAlarmImageView.Image = System.Drawing.Image.FromFile(fi.FullName);
                }
                else
                {
                    pbAlarmImageView.Image = pbAlarmImageView.ErrorImage;
                }

                pbAlarmImageView.Invalidate();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Picture Box에 Cross 그리기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pbAlarmImageView_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                if (_SelectedAlarm != null)
                {
                    Point pt1 = new Point((int)(_SelectedAlarm.ImagePivotX), (int)(_SelectedAlarm.ImagePivotY) - 10);
                    Point pt2 = new Point((int)(_SelectedAlarm.ImagePivotX), (int)(_SelectedAlarm.ImagePivotY) + 10);
                    Point pt3 = new Point((int)(_SelectedAlarm.ImagePivotX) - 10, (int)(_SelectedAlarm.ImagePivotY));
                    Point pt4 = new Point((int)(_SelectedAlarm.ImagePivotX) + 10, (int)(_SelectedAlarm.ImagePivotY));

                    Graphics g = e.Graphics;
                    
                    using (Pen redPen = new Pen(Color.Red, 3))
                    {
                        g.DrawLine(redPen, pt1, pt2);
                        g.DrawLine(redPen, pt3, pt4);
                    }
                    
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// View Alarm Log
        /// </summary>
        private void ShowAlarmList()
        {
            DataTable dt = DBManager.Instance.AlarmInfoDB.GetAlarmLists();

            dgvAlarmList.DataSource = dt;

            foreach (DataGridViewColumn item in dgvAlarmList.Columns)
            {
                item.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        /// <summary>
        /// Alarm Edit Alarm 선택 시 상세 내용(편집) 보여줌
        /// </summary>
        /// <param name="alarm"></param>
        private void UpdateAlarmInfo(CAlarm alarm)
        {
            if (alarm != null)
            {
                tbAlarmCode.Text = alarm.AlarmCode.ToString();
                tbAlarmMessage.Text = alarm.Message;
                rtbDescription.Text = alarm.Description;
                rtbAction.Text = alarm.Action;

                cbAlarmLevel.SelectedItem = alarm.AlarmLevel;

                cbUseAlarmCount.SelectedIndex = alarm.IsCountAlarm ? 1 : 0;

                if (_SelectedAlarm != null)
                {
                    cbImageList.SelectedIndex = cbImageList.FindStringExact(_SelectedAlarm.ImageFileName);
                }

                pbAlarmImageView.Invalidate();
            }
        }

        /// <summary>
        /// Image List Load
        /// </summary>
        private void UpdateAlarmImageList()
        {
            if (!Directory.Exists(DefPath.AlarmImage))
            {
                Directory.CreateDirectory(DefPath.AlarmImage);
            }

            DirectoryInfo di = new DirectoryInfo(DefPath.AlarmImage);
            FileInfo[] files = new string[] { "*.jpg", "*.bmp", "*.png" }
                                .SelectMany(ext => di.GetFiles(ext, SearchOption.AllDirectories))
                                .ToArray();

            cbImageList.Items.Clear();

            foreach (FileInfo fileInfo in files)
            {
                cbImageList.Items.Add(fileInfo);
            }
        }
    }
}
