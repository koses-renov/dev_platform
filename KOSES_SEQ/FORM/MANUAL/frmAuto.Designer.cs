﻿namespace KOSES_SEQ.OP
{
    partial class frmAuto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panAuto = new System.Windows.Forms.Panel();
            this.kosesTabControl1 = new KosesControl.Controls.KosesTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.kosesGroupBox4 = new KosesControl.Controls.KosesGroupBox();
            this.ledTextBox1 = new KosesControl.Controls.LedTextBox();
            this.ledButton1 = new KosesControl.Controls.LedButton();
            this.kosesGroupBox3 = new KosesControl.Controls.KosesGroupBox();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.ledLabel2 = new KosesControl.Controls.LedLabel();
            this.ledLabel1 = new KosesControl.Controls.LedLabel();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.led_AutoTroubleTime = new KosesControl.Controls.LedLabel();
            this.led_AutoStopTime = new KosesControl.Controls.LedLabel();
            this.led_AutoRunTime = new KosesControl.Controls.LedLabel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panAuto.SuspendLayout();
            this.kosesTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.kosesGroupBox4.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            this.kosesGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panAuto
            // 
            this.panAuto.BackColor = System.Drawing.Color.Transparent;
            this.panAuto.Controls.Add(this.kosesTabControl1);
            this.panAuto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panAuto.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panAuto.Location = new System.Drawing.Point(0, 0);
            this.panAuto.Name = "panAuto";
            this.panAuto.Padding = new System.Windows.Forms.Padding(1);
            this.panAuto.Size = new System.Drawing.Size(1149, 898);
            this.panAuto.TabIndex = 953;
            // 
            // kosesTabControl1
            // 
            this.kosesTabControl1.Controls.Add(this.tabPage1);
            this.kosesTabControl1.Controls.Add(this.tabPage2);
            this.kosesTabControl1.Controls.Add(this.tabPage3);
            this.kosesTabControl1.Depth = 0;
            this.kosesTabControl1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kosesTabControl1.ItemSize = new System.Drawing.Size(200, 40);
            this.kosesTabControl1.Location = new System.Drawing.Point(2, 12);
            this.kosesTabControl1.Multiline = true;
            this.kosesTabControl1.Name = "kosesTabControl1";
            this.kosesTabControl1.SelectedIndex = 0;
            this.kosesTabControl1.Size = new System.Drawing.Size(1145, 875);
            this.kosesTabControl1.TabIndex = 951;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage1.Controls.Add(this.kosesGroupBox4);
            this.tabPage1.Controls.Add(this.kosesGroupBox3);
            this.tabPage1.Controls.Add(this.kosesGroupBox2);
            this.tabPage1.Controls.Add(this.kosesGroupBox1);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 44);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1137, 827);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "  STATUS  ";
            // 
            // kosesGroupBox4
            // 
            this.kosesGroupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.kosesGroupBox4.Controls.Add(this.ledTextBox1);
            this.kosesGroupBox4.Controls.Add(this.ledButton1);
            this.kosesGroupBox4.Location = new System.Drawing.Point(332, 182);
            this.kosesGroupBox4.Name = "kosesGroupBox4";
            this.kosesGroupBox4.Radius = 15;
            this.kosesGroupBox4.Size = new System.Drawing.Size(323, 170);
            this.kosesGroupBox4.TabIndex = 3;
            this.kosesGroupBox4.TabStop = false;
            this.kosesGroupBox4.Text = "MACHINE INTERFACE";
            this.kosesGroupBox4.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox4.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox4.TitleGradationColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox4.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox4.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // ledTextBox1
            // 
            this.ledTextBox1.BackColor = System.Drawing.Color.Black;
            this.ledTextBox1.Bold = false;
            this.ledTextBox1.DecimalPoint = 3;
            this.ledTextBox1.Font = new System.Drawing.Font("Noto Sans KR", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ledTextBox1.FontSize = 15F;
            this.ledTextBox1.ForeColor = System.Drawing.Color.Gray;
            this.ledTextBox1.LedOffColor = System.Drawing.Color.Gray;
            this.ledTextBox1.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.ledTextBox1.Location = new System.Drawing.Point(20, 95);
            this.ledTextBox1.Maximum = 999D;
            this.ledTextBox1.Minimum = 0D;
            this.ledTextBox1.Name = "ledTextBox1";
            this.ledTextBox1.On = false;
            this.ledTextBox1.Size = new System.Drawing.Size(238, 29);
            this.ledTextBox1.TabIndex = 2;
            this.ledTextBox1.Text = "VISION CONNECTED";
            this.ledTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ledTextBox1.TextType = KosesControl.TextTypes.TEXT;
            // 
            // ledButton1
            // 
            this.ledButton1.Bold = false;
            this.ledButton1.FontSize = 0F;
            this.ledButton1.LedAlign = KosesControl.Controls.LedButton.AlignLeftRight.Left;
            this.ledButton1.LedPadding = new System.Windows.Forms.Padding(5);
            this.ledButton1.LedWidth = 23;
            this.ledButton1.Location = new System.Drawing.Point(20, 32);
            this.ledButton1.Name = "ledButton1";
            this.ledButton1.On = true;
            this.ledButton1.Size = new System.Drawing.Size(154, 40);
            this.ledButton1.TabIndex = 0;
            this.ledButton1.Text = "ledButton1";
            this.ledButton1.UseVisualStyleBackColor = true;
            // 
            // kosesGroupBox3
            // 
            this.kosesGroupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.kosesGroupBox3.Location = new System.Drawing.Point(3, 182);
            this.kosesGroupBox3.Name = "kosesGroupBox3";
            this.kosesGroupBox3.Radius = 15;
            this.kosesGroupBox3.Size = new System.Drawing.Size(323, 170);
            this.kosesGroupBox3.TabIndex = 2;
            this.kosesGroupBox3.TabStop = false;
            this.kosesGroupBox3.Text = "MACHINE COUNT";
            this.kosesGroupBox3.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox3.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox3.TitleGradationColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox3.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox3.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.kosesGroupBox2.Controls.Add(this.ledLabel2);
            this.kosesGroupBox2.Controls.Add(this.ledLabel1);
            this.kosesGroupBox2.Location = new System.Drawing.Point(332, 6);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Radius = 15;
            this.kosesGroupBox2.Size = new System.Drawing.Size(323, 170);
            this.kosesGroupBox2.TabIndex = 1;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "CYCLE TIME";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // ledLabel2
            // 
            this.ledLabel2.BackColor = System.Drawing.Color.Black;
            this.ledLabel2.Bold = false;
            this.ledLabel2.DecimalPoint = 3;
            this.ledLabel2.Font = new System.Drawing.Font("Noto Sans KR", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ledLabel2.FontSize = 18F;
            this.ledLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.ledLabel2.LedOffColor = System.Drawing.Color.Gray;
            this.ledLabel2.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.ledLabel2.Location = new System.Drawing.Point(49, 99);
            this.ledLabel2.Maximum = 999D;
            this.ledLabel2.Minimum = 0D;
            this.ledLabel2.Name = "ledLabel2";
            this.ledLabel2.On = true;
            this.ledLabel2.Size = new System.Drawing.Size(224, 34);
            this.ledLabel2.TabIndex = 4;
            this.ledLabel2.Text = "0 EA";
            this.ledLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ledLabel2.TextType = KosesControl.TextTypes.TEXT;
            // 
            // ledLabel1
            // 
            this.ledLabel1.BackColor = System.Drawing.Color.Black;
            this.ledLabel1.Bold = false;
            this.ledLabel1.DecimalPoint = 3;
            this.ledLabel1.Font = new System.Drawing.Font("Noto Sans KR", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ledLabel1.FontSize = 18F;
            this.ledLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.ledLabel1.LedOffColor = System.Drawing.Color.Gray;
            this.ledLabel1.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.ledLabel1.Location = new System.Drawing.Point(49, 38);
            this.ledLabel1.Maximum = 999D;
            this.ledLabel1.Minimum = 0D;
            this.ledLabel1.Name = "ledLabel1";
            this.ledLabel1.On = true;
            this.ledLabel1.Size = new System.Drawing.Size(224, 34);
            this.ledLabel1.TabIndex = 3;
            this.ledLabel1.Text = "0.0 Sec";
            this.ledLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ledLabel1.TextType = KosesControl.TextTypes.TEXT;
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.kosesGroupBox1.Controls.Add(this.led_AutoTroubleTime);
            this.kosesGroupBox1.Controls.Add(this.led_AutoStopTime);
            this.kosesGroupBox1.Controls.Add(this.led_AutoRunTime);
            this.kosesGroupBox1.Location = new System.Drawing.Point(3, 6);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Radius = 15;
            this.kosesGroupBox1.Size = new System.Drawing.Size(323, 170);
            this.kosesGroupBox1.TabIndex = 0;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "MACHINE TIME";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // led_AutoTroubleTime
            // 
            this.led_AutoTroubleTime.BackColor = System.Drawing.Color.Black;
            this.led_AutoTroubleTime.Bold = false;
            this.led_AutoTroubleTime.DecimalPoint = 3;
            this.led_AutoTroubleTime.Font = new System.Drawing.Font("Noto Sans KR", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.led_AutoTroubleTime.FontSize = 18F;
            this.led_AutoTroubleTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_AutoTroubleTime.LedOffColor = System.Drawing.Color.Gray;
            this.led_AutoTroubleTime.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_AutoTroubleTime.Location = new System.Drawing.Point(49, 126);
            this.led_AutoTroubleTime.Maximum = 999D;
            this.led_AutoTroubleTime.Minimum = 0D;
            this.led_AutoTroubleTime.Name = "led_AutoTroubleTime";
            this.led_AutoTroubleTime.On = true;
            this.led_AutoTroubleTime.Size = new System.Drawing.Size(224, 34);
            this.led_AutoTroubleTime.TabIndex = 5;
            this.led_AutoTroubleTime.Text = "0000 : 00 : 00";
            this.led_AutoTroubleTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_AutoTroubleTime.TextType = KosesControl.TextTypes.TEXT;
            // 
            // led_AutoStopTime
            // 
            this.led_AutoStopTime.BackColor = System.Drawing.Color.Black;
            this.led_AutoStopTime.Bold = false;
            this.led_AutoStopTime.DecimalPoint = 3;
            this.led_AutoStopTime.Font = new System.Drawing.Font("Noto Sans KR", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.led_AutoStopTime.FontSize = 18F;
            this.led_AutoStopTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_AutoStopTime.LedOffColor = System.Drawing.Color.Gray;
            this.led_AutoStopTime.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_AutoStopTime.Location = new System.Drawing.Point(49, 80);
            this.led_AutoStopTime.Maximum = 999D;
            this.led_AutoStopTime.Minimum = 0D;
            this.led_AutoStopTime.Name = "led_AutoStopTime";
            this.led_AutoStopTime.On = true;
            this.led_AutoStopTime.Size = new System.Drawing.Size(224, 34);
            this.led_AutoStopTime.TabIndex = 4;
            this.led_AutoStopTime.Text = "0000 : 00 : 00";
            this.led_AutoStopTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_AutoStopTime.TextType = KosesControl.TextTypes.TEXT;
            // 
            // led_AutoRunTime
            // 
            this.led_AutoRunTime.BackColor = System.Drawing.Color.Black;
            this.led_AutoRunTime.Bold = false;
            this.led_AutoRunTime.DecimalPoint = 3;
            this.led_AutoRunTime.Font = new System.Drawing.Font("Noto Sans KR", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.led_AutoRunTime.FontSize = 18F;
            this.led_AutoRunTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_AutoRunTime.LedOffColor = System.Drawing.Color.Gray;
            this.led_AutoRunTime.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_AutoRunTime.Location = new System.Drawing.Point(49, 34);
            this.led_AutoRunTime.Maximum = 999D;
            this.led_AutoRunTime.Minimum = 0D;
            this.led_AutoRunTime.Name = "led_AutoRunTime";
            this.led_AutoRunTime.On = true;
            this.led_AutoRunTime.Size = new System.Drawing.Size(224, 34);
            this.led_AutoRunTime.TabIndex = 3;
            this.led_AutoRunTime.Text = "0000 : 00 : 00";
            this.led_AutoRunTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_AutoRunTime.TextType = KosesControl.TextTypes.TEXT;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 44);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1137, 827);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = " FLOW / DOOR ";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage3.Location = new System.Drawing.Point(4, 44);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1137, 827);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "  LOG  ";
            // 
            // frmAuto
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1149, 898);
            this.Controls.Add(this.panAuto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAuto";
            this.Text = "Auto";
            this.panAuto.ResumeLayout(false);
            this.kosesTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.kosesGroupBox4.ResumeLayout(false);
            this.kosesGroupBox4.PerformLayout();
            this.kosesGroupBox2.ResumeLayout(false);
            this.kosesGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panAuto;
        private KosesControl.Controls.KosesTabControl kosesTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox4;
        private KosesControl.Controls.LedButton ledButton1;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox3;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private KosesControl.Controls.LedTextBox ledTextBox1;
        private KosesControl.Controls.LedLabel ledLabel2;
        private KosesControl.Controls.LedLabel ledLabel1;
        private KosesControl.Controls.LedLabel led_AutoTroubleTime;
        private KosesControl.Controls.LedLabel led_AutoStopTime;
        private KosesControl.Controls.LedLabel led_AutoRunTime;
    }
}