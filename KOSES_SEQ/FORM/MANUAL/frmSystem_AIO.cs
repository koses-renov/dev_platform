﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.OP
{
    public partial class frmSystem_AIO : Form
    {
        private ucAIO[] AIO_Input = new ucAIO[1];
        private ucAIO[] AIO_Output = new ucAIO[1];

        public frmSystem_AIO()
        {
            InitializeComponent();

            this.VisibleChanged += FrmMotor_VisibleChanged;
        }

        private void frmMotor_Load(object sender, EventArgs e)
        {
            SetDisplayDIO(3);
        }

        private void FrmMotor_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Visible == true)
                {
                    Refresh();
                }
            }
            catch
            {

            }
        }

        private void SetDisplayDIO(int displayIOCount)
        {
            tlpAIO.ColumnCount = displayIOCount + 2;

            AIO_Input = new ucAIO[displayIOCount];
            AIO_Output = new ucAIO[displayIOCount];

            for (int i = 0; i < displayIOCount; i++)
            {
                CreateInputTable(i);
                CreateOutputTable(i);
            }
        }

        private void CreateInputTable(int chNo)
        {
            AIO_Input[chNo] = new ucAIO(ucAIO.IOType.INPUT, chNo);
            AIO_Input[chNo].Dock = DockStyle.Fill;
            tlpAIO.Controls.Add(this.AIO_Input[chNo], chNo + 1, 0);
        }

        private void CreateOutputTable(int chNo)
        {
            AIO_Output[chNo] = new ucAIO(ucAIO.IOType.OUTPUT, chNo);
            AIO_Output[chNo].Dock = DockStyle.Fill;
            tlpAIO.Controls.Add(this.AIO_Output[chNo], chNo + 1, 1);
        }


        public override void Refresh()
        {
            base.Refresh();
        }

        private void btnLeft_Input_Click(object sender, EventArgs e)
        {
            if (this.AIO_Input[0].CurChNo == 0)
                return;

            for (int i = 0; i < this.AIO_Input.Length; i++)
                this.AIO_Input[i].ChangeDownChannel();
        }

        private void btnRight_Input_Click(object sender, EventArgs e)
        {
            if (this.AIO_Input[this.AIO_Input.Length - 1].CurChNo == this.AIO_Input[this.AIO_Input.Length - 1].MaxChCnt)
                return;

            for (int i = 0; i < this.AIO_Input.Length; i++)
                this.AIO_Input[i].ChangeUpChannel();
        }

        private void btnLeft_Output_Click(object sender, EventArgs e)
        {
            if (this.AIO_Output[0].CurChNo == 0)
                return;

            for (int i = 0; i < this.AIO_Output.Length; i++)
                this.AIO_Output[i].ChangeDownChannel();
        }

        private void btnRight_Output_Click(object sender, EventArgs e)
        {
            if (this.AIO_Output[this.AIO_Output.Length - 1].CurChNo == this.AIO_Output[this.AIO_Output.Length - 1].MaxChCnt)
                return;

            for (int i = 0; i < this.AIO_Output.Length; i++)
                this.AIO_Output[i].ChangeUpChannel();
        }
    }
}
