﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace KOSES_SEQ.POPUP
{
    public partial class FrmEditUser : Form
    {
        private enumLogInLevel _SelectLevel = enumLogInLevel.Unknown;
        //private CUser _User = new CUser();

        public FrmEditUser()
        {
            InitializeComponent();
        }

        private void frmChangePass_Load(object sender, EventArgs e)
        {
            tbCurPass.Text = "";
            tbNewPass1.Text = "";
            tbNewPass2.Text = "";
            //txtCurPass.Focus();
            btnOp.BackColor = Color.MediumAquamarine;

            ShowUserList(enumLogInLevel.Level_OP);
        }

        private void ShowUserList(enumLogInLevel level)
        {
            var users = UserManager.Instance.GetUserInfosByLevel(level);
            _SelectLevel = level;

            lstUser.Items.Clear();
            foreach (var user in users)
            {
                lstUser.Items.Add(user.UserID);
            }
        }

        private void btnOp_Click(object sender, EventArgs e)
        {
            Button pBtn = (Button)sender;
            int nTag = 0;
            if (!int.TryParse(pBtn.Tag.ToString(), out nTag)) return;

            btnOp.BackColor = SystemColors.Control;
            btnEng.BackColor = SystemColors.Control;
            btnMaker.BackColor = SystemColors.Control;
            tbCurPass.Text = "";
            tbNewPass1.Text = "";
            tbNewPass2.Text = "";
            if (nTag == 0)
            {
                _SelectLevel = enumLogInLevel.Level_OP;
                btnOp.BackColor = Color.MediumAquamarine;
                tbCurPass.Focus();
            }
            else if (nTag == 1)
            {
                _SelectLevel = enumLogInLevel.Level_ENG;
                btnEng.BackColor = Color.MediumAquamarine;
                tbCurPass.Focus();
            }
            else
            {
                _SelectLevel = enumLogInLevel.Level_ADMIN;
                btnMaker.BackColor = Color.MediumAquamarine;
                tbCurPass.Focus();
            }

            ShowUserList(_SelectLevel);
        }

        private void txtCurPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                tbNewPass1.Focus();
            }
        }

        private void txtNewPass1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                tbNewPass2.Focus();
            }
        }

        private void txtNewPass2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnOk.Focus();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbUserID.Text))
            {
                MessageBox.Show("Please, Confirm User ID.");
                tbUserID.Focus();
                return;
            }


            CUser user;
            if (UserManager.Instance.GetUserInfoByID(tbUserID.Text) != null)
            {
                user = UserManager.Instance.GetUserInfoByID(tbUserID.Text);
                if (user.PassWord != tbCurPass.Text)
                {
                    MessageBox.Show("Please, Check current Password.");
                    tbUserID.Focus();
                    return;
                }

                UserManager.Instance.DeleteUser(user.UserID);
            }

            if (tbNewPass1.Text != tbNewPass2.Text)
            {
                MessageBox.Show("New password mismatch!");
                tbNewPass1.Text = "";
                tbNewPass2.Text = "";
                tbNewPass1.Focus();
                return;
            }

            user = new CUser()
            {
                UserID = tbUserID.Text,
                LogInLevel = _SelectLevel,
                PassWord = tbNewPass1.Text,
            };

            UserManager.Instance.AddUser(user);

            UserManager.Instance.WriteLoginUserList();
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lstUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (lstUser.SelectedItem != null)
            //{
            //    tbUserID.Text = lstUser.SelectedItem.ToString();
            //}
        }

        private void lstUser_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lstUser.SelectedItem != null)
            {
                tbUserID.Text = lstUser.SelectedItem.ToString();
            }
        }
    }
}