﻿namespace KOSES_SEQ.OP
{
    partial class frmSystem_DIO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSystem_DIO));
            this.panManual = new System.Windows.Forms.Panel();
            this.tlpDIO = new KosesControl.Controls.KosesTableLayoutPanel();
            this.btnLeft_Output = new System.Windows.Forms.Button();
            this.btnRight_Input = new System.Windows.Forms.Button();
            this.btnRight_Output = new System.Windows.Forms.Button();
            this.btnLeft_Input = new System.Windows.Forms.Button();
            this.timer_MotorStatus = new System.Windows.Forms.Timer(this.components);
            this.panManual.SuspendLayout();
            this.tlpDIO.SuspendLayout();
            this.SuspendLayout();
            // 
            // panManual
            // 
            this.panManual.BackColor = System.Drawing.Color.Transparent;
            this.panManual.Controls.Add(this.tlpDIO);
            this.panManual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panManual.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panManual.Location = new System.Drawing.Point(0, 0);
            this.panManual.Name = "panManual";
            this.panManual.Padding = new System.Windows.Forms.Padding(1);
            this.panManual.Size = new System.Drawing.Size(926, 625);
            this.panManual.TabIndex = 953;
            // 
            // tlpDIO
            // 
            this.tlpDIO.ColumnCount = 5;
            this.tlpDIO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tlpDIO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDIO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDIO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDIO.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tlpDIO.Controls.Add(this.btnLeft_Output, 0, 1);
            this.tlpDIO.Controls.Add(this.btnRight_Input, 4, 0);
            this.tlpDIO.Controls.Add(this.btnRight_Output, 4, 1);
            this.tlpDIO.Controls.Add(this.btnLeft_Input, 0, 0);
            this.tlpDIO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpDIO.Location = new System.Drawing.Point(1, 1);
            this.tlpDIO.Margin = new System.Windows.Forms.Padding(0);
            this.tlpDIO.Name = "tlpDIO";
            this.tlpDIO.RowCount = 2;
            this.tlpDIO.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDIO.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDIO.Size = new System.Drawing.Size(924, 623);
            this.tlpDIO.TabIndex = 1;
            // 
            // btnLeft_Output
            // 
            this.btnLeft_Output.BackColor = System.Drawing.Color.White;
            this.btnLeft_Output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeft_Output.FlatAppearance.BorderSize = 0;
            this.btnLeft_Output.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeft_Output.Image = ((System.Drawing.Image)(resources.GetObject("btnLeft_Output.Image")));
            this.btnLeft_Output.Location = new System.Drawing.Point(0, 311);
            this.btnLeft_Output.Margin = new System.Windows.Forms.Padding(0);
            this.btnLeft_Output.Name = "btnLeft_Output";
            this.btnLeft_Output.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.btnLeft_Output.Size = new System.Drawing.Size(17, 312);
            this.btnLeft_Output.TabIndex = 71;
            this.btnLeft_Output.UseVisualStyleBackColor = false;
            this.btnLeft_Output.Click += new System.EventHandler(this.btnLeft_Output_Click);
            // 
            // btnRight_Input
            // 
            this.btnRight_Input.BackColor = System.Drawing.Color.White;
            this.btnRight_Input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRight_Input.FlatAppearance.BorderSize = 0;
            this.btnRight_Input.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRight_Input.Image = ((System.Drawing.Image)(resources.GetObject("btnRight_Input.Image")));
            this.btnRight_Input.Location = new System.Drawing.Point(905, 0);
            this.btnRight_Input.Margin = new System.Windows.Forms.Padding(0);
            this.btnRight_Input.Name = "btnRight_Input";
            this.btnRight_Input.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.btnRight_Input.Size = new System.Drawing.Size(19, 311);
            this.btnRight_Input.TabIndex = 69;
            this.btnRight_Input.UseVisualStyleBackColor = false;
            this.btnRight_Input.Click += new System.EventHandler(this.btnRight_Input_Click);
            // 
            // btnRight_Output
            // 
            this.btnRight_Output.BackColor = System.Drawing.Color.White;
            this.btnRight_Output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRight_Output.FlatAppearance.BorderSize = 0;
            this.btnRight_Output.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRight_Output.Image = ((System.Drawing.Image)(resources.GetObject("btnRight_Output.Image")));
            this.btnRight_Output.Location = new System.Drawing.Point(905, 311);
            this.btnRight_Output.Margin = new System.Windows.Forms.Padding(0);
            this.btnRight_Output.Name = "btnRight_Output";
            this.btnRight_Output.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.btnRight_Output.Size = new System.Drawing.Size(19, 312);
            this.btnRight_Output.TabIndex = 70;
            this.btnRight_Output.UseVisualStyleBackColor = false;
            this.btnRight_Output.Click += new System.EventHandler(this.btnRight_Output_Click);
            // 
            // btnLeft_Input
            // 
            this.btnLeft_Input.BackColor = System.Drawing.Color.White;
            this.btnLeft_Input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLeft_Input.FlatAppearance.BorderSize = 0;
            this.btnLeft_Input.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeft_Input.Image = ((System.Drawing.Image)(resources.GetObject("btnLeft_Input.Image")));
            this.btnLeft_Input.Location = new System.Drawing.Point(0, 0);
            this.btnLeft_Input.Margin = new System.Windows.Forms.Padding(0);
            this.btnLeft_Input.Name = "btnLeft_Input";
            this.btnLeft_Input.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.btnLeft_Input.Size = new System.Drawing.Size(17, 311);
            this.btnLeft_Input.TabIndex = 72;
            this.btnLeft_Input.UseVisualStyleBackColor = false;
            this.btnLeft_Input.Click += new System.EventHandler(this.btnLeft_Input_Click);
            // 
            // timer_MotorStatus
            // 
            this.timer_MotorStatus.Interval = 500;
            // 
            // frmSystem_DIO
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(926, 625);
            this.Controls.Add(this.panManual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSystem_DIO";
            this.Text = "Recipe";
            this.Load += new System.EventHandler(this.frmMotor_Load);
            this.panManual.ResumeLayout(false);
            this.tlpDIO.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panManual;
        private System.Windows.Forms.Timer timer_MotorStatus;
        private KosesControl.Controls.KosesTableLayoutPanel tlpDIO;
        private System.Windows.Forms.Button btnLeft_Output;
        private System.Windows.Forms.Button btnRight_Input;
        private System.Windows.Forms.Button btnRight_Output;
        private System.Windows.Forms.Button btnLeft_Input;
    }
}