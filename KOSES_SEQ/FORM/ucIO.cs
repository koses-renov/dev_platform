﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using KosesControl;

namespace KOSES_SEQ.OP
{
    public partial class ucIO : UserControl
    {
        public enum IOType { D_INPUT, D_OUTPUT, A_INPUT, A_OUTPUT };
        /// <summary>
        /// 현재 I/O 채널
        /// </summary>
        public int CurChNo { get; set; }
        /// <summary>
        /// I/O 채널 최대 개수
        /// </summary>
        public int MaxChCnt { get; private set; } = 16;
        /// <summary>
        /// 채널당 IO 최대 개수
        /// </summary>
        public int ListCnt { get; private set; } = 16;

        #region ----- Form 내부 변수 -----
        /// <summary>
        /// I/O 타입
        /// </summary>
        IOType IO_Type;

        bool IsSearch = false;
        object Interlock = new object();
        List<int> SearchIONoList = new List<int>();
        #endregion

        #region ----- Form Initialize -----
        public ucIO(IOType io_type, int chNo, bool check = false)
        {
            InitializeComponent();
            this.IO_Type = io_type;

            this.Check.Visible = check;

            if (chNo < 0)
                this.CurChNo = 0;
            else
                this.CurChNo = chNo;

            ShowIO_List();
        }

        //페이지 로딩 느려보일때 쓰면 좋음
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion

        #region ----- Form Show Event -----
        private void ShowIO_List()
        {
            dispIoTitle();

            tmrIo.Enabled = false;

            if (this.IO_Type == IOType.D_INPUT)
            {
                this.dispIoInputGrid();
            }
            else
            {
                this.dispIoOutputGrid();
            }

            tmrIo.Enabled = true;
        }

        private void dispIoTitle()
        {
            labIoTitle.BackColor = Color.Transparent;
            labIoTitle.ForeColor = Color.White;

            if (this.IO_Type == IOType.D_INPUT)
                panIoTitle.BackColor = Color.SteelBlue;
            else if (this.IO_Type == IOType.D_OUTPUT)
                panIoTitle.BackColor = Color.DarkSalmon;
            if (this.IO_Type == IOType.A_INPUT)
                panIoTitle.BackColor = Color.SteelBlue;
            //panIoTitle.BackColor = Color.CornflowerBlue;
            else if (this.IO_Type == IOType.A_OUTPUT)
                panIoTitle.BackColor = Color.DarkSalmon;
            //panIoTitle.BackColor = Color.DarkOrange;

            labIoTitle.Text = string.Format("{0} (CH {1:00})", this.IO_Type, CurChNo);
        }

        private void dispIoInputGrid()
        {
            //try
            //{
            //    bool success = Program.HandlerSvc.DInList(CurChNo, out List<(int, string)> dInLst);
            //    this.dgvInOut.RowCount = ListCnt;
            //    foreach (DataGridViewRow row in this.dgvInOut.Rows)
            //    {
            //        row.Cells[0].Value = $"X{row.Index + CurChNo * 100}";
            //        row.Cells[1].Value = dInLst[row.Index].Item2; //"Y000 " + 
            //        row.Cells[2].Value = "OFF";     // dOut[i].Item2;
            //        if (Check.Visible)
            //            row.Cells[3].Value = HandlerSystem.InputIONameList[CurChNo].Checks[row.Index];
            //    }
            //    return success;
            //}
            //catch (Exception ex)
            //{
            //    Logger.Log(Logger.Module.Laser, Logger.Type.Error, $"{IO_Type} Not Find List : {ex.Message}");
            //    return false;
            //}
        }

        private void dispIoOutputGrid()
        {
            //try
            //{
            //    bool success = Program.HandlerSvc.DOutList(CurChNo, out List<(int, string)> dOutLst);

            //    this.dgvInOut.RowCount = ListCnt;
            //    foreach (DataGridViewRow row in this.dgvInOut.Rows)
            //    {
            //        row.Cells[0].Value = $"Y{row.Index + CurChNo * 100}";
            //        row.Cells[1].Value = dOutLst[row.Index].Item2; //"Y000 " + 
            //        row.Cells[2].Value = "OFF";     // dOut[i].Item2;
            //        if(Check.Visible)
            //            row.Cells[3].Value = HandlerSystem.OutputIONameList[CurChNo].Checks[row.Index];
            //    }
            //    return success;
            //}
            //catch (Exception ex)
            //{
            //    Logger.Log(Logger.Module.Laser, Logger.Type.Error, $"{IO_Type} Not Find List : {ex.Message}");
            //    return false;
            //}
        }
        #endregion

        #region ----- Form Update Timer -----
        private void tmrIo_Tick(object sender, EventArgs e)
        {
            if (this.Visible && this.Created)
            {
                //if (this.IO_Type == IOEnum.OUTPUT)
                //    UpdateDOutput();
                //else
                //    UpdateDInput();
            }
            else
                tmrIo.Stop();
        }
        private void IOView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
                tmrIo.Start();
        }

        private void UpdateDInput()
        {
            int chIndex = (CurChNo * 100);
            for (int i = 0; i < MaxChCnt; i++)
            {
                this.dgvInOut.Rows[i].Cells[2].Value = SysOperator.Instance.motion.GetInput(chIndex + i) ? "ON" : "OFF";
            }
        }

        private void UpdateDOutput()
        {
            int chIndex = (CurChNo * 100);
            for (int i = 0; i < MaxChCnt; i++)
            {
                this.dgvInOut.Rows[i].Cells[2].Value = SysOperator.Instance.motion.GetOutput(chIndex + i) ? "ON" : "OFF";
            }
        }
        #endregion

        #region ----- Form Button & Grid Event -----
        private void btnIoChDown_Click(object sender, EventArgs e)
        {
            if (CurChNo > 0)
            {
                IsSearch = false;
                //this.btnSearch.Icon = Properties.Resources.outline_search_white_18dp;
                CurChNo--;
                ShowIO_List();
            }
        }
        private void btnIoChUp_Click(object sender, EventArgs e)
        {
            if (CurChNo < MaxChCnt - 1)
            {
                IsSearch = false;
                //this.btnSearch.Icon = Properties.Resources.outline_search_white_18dp;
                CurChNo++;
                ShowIO_List();
            }
        }
        public void ChangeUpChannel()
        {
            this.CurChNo++;
            ShowIO_List();
        }
        public void ChangeDownChannel()
        {
            this.CurChNo--;
            ShowIO_List();
        }
        /// <summary>
        /// I/O Output 버튼 클릭 -> Ouput신호 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInOut_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0 || this.IO_Type != IOType.D_OUTPUT)
                return;
            //else
            //{
            //    if (2 == e.ColumnIndex)
            //    {
            //        string strNo = dgvInOut[0, e.RowIndex].Value.ToString();
            //        int ioNo = Convert.ToInt32(strNo.Replace("Y", ""));
            //        int chNo = ioNo / 100;
            //        int indexNo = ioNo % 100;

            //        Program.HandlerSvc.DOut(chNo, out List<(int, bool)> dOutputs);

            //        bool ioOn = dOutputs[indexNo].Item2 ? false : true;
            //        Program.HandlerSvc.DOutSet(ioNo, ioOn);
            //    }

            //    UpdateDOutput();
            //}
        }


        /// <summary>
        /// I/O 검색 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if(!IsSearch)
            //    {
            //        if (this.tbSearchNo.Text == this.tbSearchNo.HintText) this.tbSearchNo.Text = "";
            //        if (this.tbSearchName.Text == this.tbSearchName.HintText) this.tbSearchName.Text = "";

            //        List<(int, string)> ioList;
            //        if (this.IO_Type == IOEnum.OUTPUT)
            //            Program.HandlerSvc.DOutList(-1, out ioList);
            //        else
            //            Program.HandlerSvc.DInList(-1, out ioList);

            //        ioList = ioList.FindAll(list => list.Item1.ToString().IndexOf(tbSearchNo.Text, StringComparison.OrdinalIgnoreCase) >= 0).
            //                        FindAll(list => list.Item2.IndexOf(tbSearchName.Text, StringComparison.OrdinalIgnoreCase) >= 0);


            //        this.dgvInOut.Rows.Clear();
            //        this.SearchIONoList.Clear();
            //        this.dgvInOut.RowCount = ioList.Count;
            //        foreach (DataGridViewRow row in this.dgvInOut.Rows)
            //        {
            //            row.Cells[0].Value = ioList[row.Index].Item1;
            //            row.Cells[1].Value = ioList[row.Index].Item2; //"Y000 " + 
            //            row.Cells[2].Value = "OFF";     // dOut[i].Item2;
            //            SearchIONoList.Add(ioList[row.Index].Item1);
            //        }
            //        this.IsSearch = true;

            //        this.btnSearch.Icon = Properties.Resources.outline_undo_white_18dp;
            //    }
            //    else
            //    {
            //        if (this.IO_Type == IOEnum.OUTPUT)
            //            dispIoOutputGrid();
            //        else
            //            dispIoInputGrid();

            //        this.IsSearch = false;
            //        this.btnSearch.Icon = Properties.Resources.outline_search_white_18dp;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageView.ShowMessage($"I can't find it.{ex.Message}", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }
        /// <summary>
        /// I/O 이름 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvInOut_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //string strNo = dgvInOut[0, e.RowIndex].Value.ToString();
            //int ioNo = 0;
            //if (this.IO_Type == IOEnum.OUTPUT)
            //    ioNo = Convert.ToInt32(strNo.Replace("Y", ""));
            //else
            //    ioNo = Convert.ToInt32(strNo.Replace("X", ""));

            //if (this.IO_Type == IOEnum.OUTPUT && e.ColumnIndex == 1)
            //    Program.HandlerSvc.DOutName_Set(ioNo, dgvInOut[e.ColumnIndex, e.RowIndex].Value.ToString());
            //else if (this.IO_Type == IOEnum.INPUT && e.ColumnIndex == 1)
            //    Program.HandlerSvc.DInName_Set(ioNo, dgvInOut[e.ColumnIndex, e.RowIndex].Value.ToString());
        }


        private void dgvInOut_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.ColumnIndex == Check.Index && e.RowIndex != -1)
            //{
            //    if (this.IO_Type == IOEnum.INPUT)
            //        HandlerSystem.InputIONameList[CurChNo].Checks[e.RowIndex] = !(bool)dgvInOut.Rows[e.RowIndex].Cells[3].Value;
            //    else if (this.IO_Type == IOEnum.OUTPUT)
            //        HandlerSystem.OutputIONameList[CurChNo].Checks[e.RowIndex] = !(bool)dgvInOut.Rows[e.RowIndex].Cells[3].Value;
            //}
        }

        private void dgvInOut_SelectionChanged(object sender, EventArgs e)
        {
            this.dgvInOut.ClearSelection();
        }
        #endregion

        #region ----- 단축키 Event -----
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Keys key = keyData & ~(Keys.Shift | Keys.Control);
            switch (key)
            {
                case Keys.Enter:
                    this.IsSearch = false;
                    this.btnSearch.PerformClick();
                    break;
                case Keys.Escape:
                    this.IsSearch = true;
                    this.btnSearch.PerformClick();
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion
    }
}
