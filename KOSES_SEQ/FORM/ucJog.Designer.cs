﻿namespace KOSES_SEQ.OP
{
    partial class ucJog
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucJog));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachingMotorStop = new System.Windows.Forms.Button();
            this.btn_TeachingRecipeSave = new System.Windows.Forms.Button();
            this.kosesGroupBox3 = new KosesControl.Controls.KosesGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachingJogCcw = new System.Windows.Forms.Button();
            this.btn_TeachingJogCw = new System.Windows.Forms.Button();
            this.kosesGroupBox2 = new KosesControl.Controls.KosesGroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachingJogRel10 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelDot01 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelDot03 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRel01 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelDot02 = new System.Windows.Forms.Button();
            this.btn_TeachingJogRel05 = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txt_TeachingRelValue = new System.Windows.Forms.TextBox();
            this.btn_TeachingJogRelCcw = new System.Windows.Forms.Button();
            this.btn_TeachingJogRelCw = new System.Windows.Forms.Button();
            this.kosesGroupBox1 = new KosesControl.Controls.KosesGroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachingAbsGetValue = new System.Windows.Forms.Button();
            this.txt_TeachingAbsValue = new System.Windows.Forms.TextBox();
            this.btn_TeachingJogAbs = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_TeachingServoOn = new System.Windows.Forms.Button();
            this.btn_TeachingAlarmClear = new System.Windows.Forms.Button();
            this.btn_TeachingServoOff = new System.Windows.Forms.Button();
            this.btn_TeachingHome = new System.Windows.Forms.Button();
            this.tlpMotorCommand = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.led_TeachingCommand = new KosesControl.Controls.LedLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.led_TeachingEncoder = new KosesControl.Controls.LedLabel();
            this.btn_TeachingZeroSet = new System.Windows.Forms.Button();
            this.tlpMotorStatus = new System.Windows.Forms.TableLayoutPanel();
            this.led_TeachingServo = new Bulb.LedBulb();
            this.label1 = new System.Windows.Forms.Label();
            this.l = new System.Windows.Forms.Label();
            this.led_TeachingCwLimit = new Bulb.LedBulb();
            this.led_TeachingAlarm = new Bulb.LedBulb();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.led_TeachingHome = new Bulb.LedBulb();
            this.led_TeachingCcwLimit = new Bulb.LedBulb();
            this.led_TeachingInpos = new Bulb.LedBulb();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_TeachingAxisName = new KosesControl.Controls.LedLabel();
            this.tlpJog = new System.Windows.Forms.TableLayoutPanel();
            this.tmrMotorStatus = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel2.SuspendLayout();
            this.kosesGroupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.kosesGroupBox2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.kosesGroupBox1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tlpMotorCommand.SuspendLayout();
            this.tlpMotorStatus.SuspendLayout();
            this.tlpJog.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btn_TeachingMotorStop, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_TeachingRecipeSave, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 774);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.05263F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(301, 73);
            this.tableLayoutPanel2.TabIndex = 1009;
            // 
            // btn_TeachingMotorStop
            // 
            this.btn_TeachingMotorStop.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingMotorStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingMotorStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingMotorStop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingMotorStop.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingMotorStop.Image")));
            this.btn_TeachingMotorStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingMotorStop.Location = new System.Drawing.Point(1, 1);
            this.btn_TeachingMotorStop.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingMotorStop.Name = "btn_TeachingMotorStop";
            this.btn_TeachingMotorStop.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingMotorStop.Size = new System.Drawing.Size(148, 71);
            this.btn_TeachingMotorStop.TabIndex = 1004;
            this.btn_TeachingMotorStop.Text = "MOTOR \r\nSTOP";
            this.btn_TeachingMotorStop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingMotorStop.UseVisualStyleBackColor = false;
            this.btn_TeachingMotorStop.Click += new System.EventHandler(this.btn_TeachingMotorStop_Click);
            // 
            // btn_TeachingRecipeSave
            // 
            this.btn_TeachingRecipeSave.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingRecipeSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingRecipeSave.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingRecipeSave.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingRecipeSave.Image")));
            this.btn_TeachingRecipeSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingRecipeSave.Location = new System.Drawing.Point(151, 1);
            this.btn_TeachingRecipeSave.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingRecipeSave.Name = "btn_TeachingRecipeSave";
            this.btn_TeachingRecipeSave.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.btn_TeachingRecipeSave.Size = new System.Drawing.Size(149, 71);
            this.btn_TeachingRecipeSave.TabIndex = 953;
            this.btn_TeachingRecipeSave.Text = "RECIPE\r\nSAVE";
            this.btn_TeachingRecipeSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingRecipeSave.UseVisualStyleBackColor = false;
            // 
            // kosesGroupBox3
            // 
            this.kosesGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox3.Controls.Add(this.tableLayoutPanel3);
            this.kosesGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox3.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.kosesGroupBox3.Location = new System.Drawing.Point(3, 351);
            this.kosesGroupBox3.Name = "kosesGroupBox3";
            this.kosesGroupBox3.Padding = new System.Windows.Forms.Padding(1);
            this.kosesGroupBox3.Radius = 0;
            this.kosesGroupBox3.Size = new System.Drawing.Size(301, 91);
            this.kosesGroupBox3.TabIndex = 1002;
            this.kosesGroupBox3.TabStop = false;
            this.kosesGroupBox3.Text = "JOG";
            this.kosesGroupBox3.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox3.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox3.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox3.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox3.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.btn_TeachingJogCcw, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btn_TeachingJogCw, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(1, 20);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(299, 70);
            this.tableLayoutPanel3.TabIndex = 1008;
            // 
            // btn_TeachingJogCcw
            // 
            this.btn_TeachingJogCcw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogCcw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogCcw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogCcw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogCcw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogCcw.Image")));
            this.btn_TeachingJogCcw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogCcw.Location = new System.Drawing.Point(1, 4);
            this.btn_TeachingJogCcw.Margin = new System.Windows.Forms.Padding(1, 4, 1, 1);
            this.btn_TeachingJogCcw.Name = "btn_TeachingJogCcw";
            this.btn_TeachingJogCcw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogCcw.Size = new System.Drawing.Size(147, 65);
            this.btn_TeachingJogCcw.TabIndex = 1000;
            this.btn_TeachingJogCcw.Text = "JOG(-)";
            this.btn_TeachingJogCcw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogCcw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogCcw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCcw_MouseDown);
            this.btn_TeachingJogCcw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCcw_MouseUp);
            // 
            // btn_TeachingJogCw
            // 
            this.btn_TeachingJogCw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogCw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogCw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogCw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogCw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogCw.Image")));
            this.btn_TeachingJogCw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogCw.Location = new System.Drawing.Point(150, 4);
            this.btn_TeachingJogCw.Margin = new System.Windows.Forms.Padding(1, 4, 1, 1);
            this.btn_TeachingJogCw.Name = "btn_TeachingJogCw";
            this.btn_TeachingJogCw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogCw.Size = new System.Drawing.Size(148, 65);
            this.btn_TeachingJogCw.TabIndex = 1001;
            this.btn_TeachingJogCw.Text = "JOG(+)";
            this.btn_TeachingJogCw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogCw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogCw.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCw_MouseDown);
            this.btn_TeachingJogCw.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_TeachingJogCw_MouseUp);
            // 
            // kosesGroupBox2
            // 
            this.kosesGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox2.Controls.Add(this.tableLayoutPanel7);
            this.kosesGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.kosesGroupBox2.Location = new System.Drawing.Point(3, 448);
            this.kosesGroupBox2.Name = "kosesGroupBox2";
            this.kosesGroupBox2.Padding = new System.Windows.Forms.Padding(1);
            this.kosesGroupBox2.Radius = 0;
            this.kosesGroupBox2.Size = new System.Drawing.Size(301, 209);
            this.kosesGroupBox2.TabIndex = 1001;
            this.kosesGroupBox2.TabStop = false;
            this.kosesGroupBox2.Text = "RELATIVE";
            this.kosesGroupBox2.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox2.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox2.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox2.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(1, 20);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.77052F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.22948F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(299, 188);
            this.tableLayoutPanel7.TabIndex = 1011;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Controls.Add(this.btn_TeachingJogRel10, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_TeachingJogRelDot01, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_TeachingJogRelDot03, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_TeachingJogRel01, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btn_TeachingJogRelDot02, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_TeachingJogRel05, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.38095F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.61905F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(299, 82);
            this.tableLayoutPanel4.TabIndex = 1009;
            // 
            // btn_TeachingJogRel10
            // 
            this.btn_TeachingJogRel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRel10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRel10.Location = new System.Drawing.Point(199, 43);
            this.btn_TeachingJogRel10.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingJogRel10.Name = "btn_TeachingJogRel10";
            this.btn_TeachingJogRel10.Size = new System.Drawing.Size(99, 38);
            this.btn_TeachingJogRel10.TabIndex = 1007;
            this.btn_TeachingJogRel10.Text = "10";
            this.btn_TeachingJogRel10.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRel10.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRelDot01
            // 
            this.btn_TeachingJogRelDot01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRelDot01.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelDot01.Location = new System.Drawing.Point(199, 4);
            this.btn_TeachingJogRelDot01.Margin = new System.Windows.Forms.Padding(1, 4, 1, 1);
            this.btn_TeachingJogRelDot01.Name = "btn_TeachingJogRelDot01";
            this.btn_TeachingJogRelDot01.Size = new System.Drawing.Size(99, 37);
            this.btn_TeachingJogRelDot01.TabIndex = 1004;
            this.btn_TeachingJogRelDot01.Text = "0.1";
            this.btn_TeachingJogRelDot01.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRelDot01.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRelDot03
            // 
            this.btn_TeachingJogRelDot03.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRelDot03.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelDot03.Location = new System.Drawing.Point(1, 4);
            this.btn_TeachingJogRelDot03.Margin = new System.Windows.Forms.Padding(1, 4, 1, 1);
            this.btn_TeachingJogRelDot03.Name = "btn_TeachingJogRelDot03";
            this.btn_TeachingJogRelDot03.Size = new System.Drawing.Size(97, 37);
            this.btn_TeachingJogRelDot03.TabIndex = 1002;
            this.btn_TeachingJogRelDot03.Text = "0.001";
            this.btn_TeachingJogRelDot03.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRelDot03.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRel01
            // 
            this.btn_TeachingJogRel01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRel01.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRel01.Location = new System.Drawing.Point(1, 43);
            this.btn_TeachingJogRel01.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingJogRel01.Name = "btn_TeachingJogRel01";
            this.btn_TeachingJogRel01.Size = new System.Drawing.Size(97, 38);
            this.btn_TeachingJogRel01.TabIndex = 1006;
            this.btn_TeachingJogRel01.Text = "1";
            this.btn_TeachingJogRel01.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRel01.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRelDot02
            // 
            this.btn_TeachingJogRelDot02.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRelDot02.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelDot02.Location = new System.Drawing.Point(100, 4);
            this.btn_TeachingJogRelDot02.Margin = new System.Windows.Forms.Padding(1, 4, 1, 1);
            this.btn_TeachingJogRelDot02.Name = "btn_TeachingJogRelDot02";
            this.btn_TeachingJogRelDot02.Size = new System.Drawing.Size(97, 37);
            this.btn_TeachingJogRelDot02.TabIndex = 1003;
            this.btn_TeachingJogRelDot02.Text = "0.01";
            this.btn_TeachingJogRelDot02.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRelDot02.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // btn_TeachingJogRel05
            // 
            this.btn_TeachingJogRel05.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRel05.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRel05.Location = new System.Drawing.Point(100, 43);
            this.btn_TeachingJogRel05.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingJogRel05.Name = "btn_TeachingJogRel05";
            this.btn_TeachingJogRel05.Size = new System.Drawing.Size(97, 38);
            this.btn_TeachingJogRel05.TabIndex = 1005;
            this.btn_TeachingJogRel05.Text = "5";
            this.btn_TeachingJogRel05.UseVisualStyleBackColor = true;
            this.btn_TeachingJogRel05.Click += new System.EventHandler(this.btn_TeachingJogRelDot03_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.txt_TeachingRelValue, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn_TeachingJogRelCcw, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn_TeachingJogRelCw, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 82);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(299, 106);
            this.tableLayoutPanel5.TabIndex = 1010;
            // 
            // txt_TeachingRelValue
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.txt_TeachingRelValue, 2);
            this.txt_TeachingRelValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_TeachingRelValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TeachingRelValue.Location = new System.Drawing.Point(1, 1);
            this.txt_TeachingRelValue.Margin = new System.Windows.Forms.Padding(1);
            this.txt_TeachingRelValue.Name = "txt_TeachingRelValue";
            this.txt_TeachingRelValue.Size = new System.Drawing.Size(297, 27);
            this.txt_TeachingRelValue.TabIndex = 1001;
            this.txt_TeachingRelValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_TeachingJogRelCcw
            // 
            this.btn_TeachingJogRelCcw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogRelCcw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogRelCcw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRelCcw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelCcw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogRelCcw.Image")));
            this.btn_TeachingJogRelCcw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogRelCcw.Location = new System.Drawing.Point(1, 30);
            this.btn_TeachingJogRelCcw.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingJogRelCcw.Name = "btn_TeachingJogRelCcw";
            this.btn_TeachingJogRelCcw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogRelCcw.Size = new System.Drawing.Size(147, 75);
            this.btn_TeachingJogRelCcw.TabIndex = 987;
            this.btn_TeachingJogRelCcw.Text = "REL(-)";
            this.btn_TeachingJogRelCcw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogRelCcw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogRelCcw.Click += new System.EventHandler(this.btn_TeachingJogRelCcw_Click);
            // 
            // btn_TeachingJogRelCw
            // 
            this.btn_TeachingJogRelCw.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogRelCw.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogRelCw.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogRelCw.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogRelCw.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogRelCw.Image")));
            this.btn_TeachingJogRelCw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogRelCw.Location = new System.Drawing.Point(150, 30);
            this.btn_TeachingJogRelCw.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingJogRelCw.Name = "btn_TeachingJogRelCw";
            this.btn_TeachingJogRelCw.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingJogRelCw.Size = new System.Drawing.Size(148, 75);
            this.btn_TeachingJogRelCw.TabIndex = 1000;
            this.btn_TeachingJogRelCw.Text = "REL(+)";
            this.btn_TeachingJogRelCw.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogRelCw.UseVisualStyleBackColor = false;
            this.btn_TeachingJogRelCw.Click += new System.EventHandler(this.btn_TeachingJogRelCw_Click);
            // 
            // kosesGroupBox1
            // 
            this.kosesGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.kosesGroupBox1.Controls.Add(this.tableLayoutPanel6);
            this.kosesGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kosesGroupBox1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.kosesGroupBox1.Location = new System.Drawing.Point(3, 663);
            this.kosesGroupBox1.Name = "kosesGroupBox1";
            this.kosesGroupBox1.Padding = new System.Windows.Forms.Padding(1);
            this.kosesGroupBox1.Radius = 0;
            this.kosesGroupBox1.Size = new System.Drawing.Size(301, 105);
            this.kosesGroupBox1.TabIndex = 1000;
            this.kosesGroupBox1.TabStop = false;
            this.kosesGroupBox1.Text = "ABSOLUTELY";
            this.kosesGroupBox1.TitleBackColor = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleForeColor = System.Drawing.Color.White;
            this.kosesGroupBox1.TitleGradationColor1 = System.Drawing.Color.SteelBlue;
            this.kosesGroupBox1.TitleGradationColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(207)))), ((int)(((byte)(226)))));
            this.kosesGroupBox1.TitleGradationStyle = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.btn_TeachingAbsGetValue, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.txt_TeachingAbsValue, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btn_TeachingJogAbs, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(1, 20);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(299, 84);
            this.tableLayoutPanel6.TabIndex = 1011;
            // 
            // btn_TeachingAbsGetValue
            // 
            this.btn_TeachingAbsGetValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingAbsGetValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingAbsGetValue.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingAbsGetValue.Image")));
            this.btn_TeachingAbsGetValue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingAbsGetValue.Location = new System.Drawing.Point(1, 36);
            this.btn_TeachingAbsGetValue.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingAbsGetValue.Name = "btn_TeachingAbsGetValue";
            this.btn_TeachingAbsGetValue.Size = new System.Drawing.Size(147, 47);
            this.btn_TeachingAbsGetValue.TabIndex = 1004;
            this.btn_TeachingAbsGetValue.Text = "GET VALUE";
            this.btn_TeachingAbsGetValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingAbsGetValue.UseVisualStyleBackColor = true;
            this.btn_TeachingAbsGetValue.Click += new System.EventHandler(this.btn_TeachingAbsGetValue_Click);
            // 
            // txt_TeachingAbsValue
            // 
            this.txt_TeachingAbsValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_TeachingAbsValue.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TeachingAbsValue.Location = new System.Drawing.Point(1, 7);
            this.txt_TeachingAbsValue.Margin = new System.Windows.Forms.Padding(1, 7, 1, 1);
            this.txt_TeachingAbsValue.Name = "txt_TeachingAbsValue";
            this.txt_TeachingAbsValue.Size = new System.Drawing.Size(147, 27);
            this.txt_TeachingAbsValue.TabIndex = 1002;
            this.txt_TeachingAbsValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_TeachingJogAbs
            // 
            this.btn_TeachingJogAbs.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingJogAbs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingJogAbs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingJogAbs.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingJogAbs.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingJogAbs.Image")));
            this.btn_TeachingJogAbs.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingJogAbs.Location = new System.Drawing.Point(150, 3);
            this.btn_TeachingJogAbs.Margin = new System.Windows.Forms.Padding(1, 3, 1, 1);
            this.btn_TeachingJogAbs.Name = "btn_TeachingJogAbs";
            this.btn_TeachingJogAbs.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel6.SetRowSpan(this.btn_TeachingJogAbs, 2);
            this.btn_TeachingJogAbs.Size = new System.Drawing.Size(148, 80);
            this.btn_TeachingJogAbs.TabIndex = 1003;
            this.btn_TeachingJogAbs.Text = "ABS";
            this.btn_TeachingJogAbs.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingJogAbs.UseVisualStyleBackColor = false;
            this.btn_TeachingJogAbs.Click += new System.EventHandler(this.btn_TeachingJogAbs_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btn_TeachingServoOn, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_TeachingAlarmClear, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btn_TeachingServoOff, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_TeachingHome, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 223);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(307, 125);
            this.tableLayoutPanel1.TabIndex = 1008;
            // 
            // btn_TeachingServoOn
            // 
            this.btn_TeachingServoOn.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingServoOn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingServoOn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingServoOn.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingServoOn.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingServoOn.Image")));
            this.btn_TeachingServoOn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingServoOn.Location = new System.Drawing.Point(1, 1);
            this.btn_TeachingServoOn.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingServoOn.Name = "btn_TeachingServoOn";
            this.btn_TeachingServoOn.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingServoOn.Size = new System.Drawing.Size(151, 60);
            this.btn_TeachingServoOn.TabIndex = 985;
            this.btn_TeachingServoOn.Text = "SERVO\r\nON";
            this.btn_TeachingServoOn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingServoOn.UseVisualStyleBackColor = false;
            this.btn_TeachingServoOn.Click += new System.EventHandler(this.btn_TeachingServoOn_Click);
            // 
            // btn_TeachingAlarmClear
            // 
            this.btn_TeachingAlarmClear.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingAlarmClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingAlarmClear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingAlarmClear.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingAlarmClear.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingAlarmClear.Image")));
            this.btn_TeachingAlarmClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingAlarmClear.Location = new System.Drawing.Point(1, 63);
            this.btn_TeachingAlarmClear.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingAlarmClear.Name = "btn_TeachingAlarmClear";
            this.btn_TeachingAlarmClear.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingAlarmClear.Size = new System.Drawing.Size(151, 61);
            this.btn_TeachingAlarmClear.TabIndex = 983;
            this.btn_TeachingAlarmClear.Text = "ALARM\r\nCLEAR";
            this.btn_TeachingAlarmClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingAlarmClear.UseVisualStyleBackColor = false;
            this.btn_TeachingAlarmClear.Click += new System.EventHandler(this.btn_TeachingAlarmClear_Click);
            // 
            // btn_TeachingServoOff
            // 
            this.btn_TeachingServoOff.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingServoOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingServoOff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingServoOff.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingServoOff.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingServoOff.Image")));
            this.btn_TeachingServoOff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingServoOff.Location = new System.Drawing.Point(154, 1);
            this.btn_TeachingServoOff.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingServoOff.Name = "btn_TeachingServoOff";
            this.btn_TeachingServoOff.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingServoOff.Size = new System.Drawing.Size(152, 60);
            this.btn_TeachingServoOff.TabIndex = 984;
            this.btn_TeachingServoOff.Text = "SERVO\r\nOFF";
            this.btn_TeachingServoOff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingServoOff.UseVisualStyleBackColor = false;
            this.btn_TeachingServoOff.Click += new System.EventHandler(this.btn_TeachingServoOff_Click);
            // 
            // btn_TeachingHome
            // 
            this.btn_TeachingHome.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_TeachingHome.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingHome.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingHome.Image")));
            this.btn_TeachingHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingHome.Location = new System.Drawing.Point(154, 63);
            this.btn_TeachingHome.Margin = new System.Windows.Forms.Padding(1);
            this.btn_TeachingHome.Name = "btn_TeachingHome";
            this.btn_TeachingHome.Padding = new System.Windows.Forms.Padding(5);
            this.btn_TeachingHome.Size = new System.Drawing.Size(152, 61);
            this.btn_TeachingHome.TabIndex = 982;
            this.btn_TeachingHome.Text = "HOME";
            this.btn_TeachingHome.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingHome.UseVisualStyleBackColor = false;
            this.btn_TeachingHome.Click += new System.EventHandler(this.btn_TeachingHome_Click);
            // 
            // tlpMotorCommand
            // 
            this.tlpMotorCommand.ColumnCount = 3;
            this.tlpMotorCommand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpMotorCommand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMotorCommand.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpMotorCommand.Controls.Add(this.label2, 0, 0);
            this.tlpMotorCommand.Controls.Add(this.led_TeachingCommand, 1, 0);
            this.tlpMotorCommand.Controls.Add(this.label7, 0, 1);
            this.tlpMotorCommand.Controls.Add(this.led_TeachingEncoder, 1, 1);
            this.tlpMotorCommand.Controls.Add(this.btn_TeachingZeroSet, 2, 0);
            this.tlpMotorCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMotorCommand.Location = new System.Drawing.Point(0, 149);
            this.tlpMotorCommand.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMotorCommand.Name = "tlpMotorCommand";
            this.tlpMotorCommand.RowCount = 2;
            this.tlpMotorCommand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpMotorCommand.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpMotorCommand.Size = new System.Drawing.Size(307, 74);
            this.tlpMotorCommand.TabIndex = 1007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 37);
            this.label2.TabIndex = 967;
            this.label2.Text = "CMD";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // led_TeachingCommand
            // 
            this.led_TeachingCommand.BackColor = System.Drawing.Color.Black;
            this.led_TeachingCommand.Bold = false;
            this.led_TeachingCommand.DecimalPoint = 3;
            this.led_TeachingCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingCommand.Font = new System.Drawing.Font("LED Dot-Matrix", 17F);
            this.led_TeachingCommand.FontSize = 17F;
            this.led_TeachingCommand.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingCommand.LedOffColor = System.Drawing.Color.Gray;
            this.led_TeachingCommand.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingCommand.Location = new System.Drawing.Point(55, 3);
            this.led_TeachingCommand.Margin = new System.Windows.Forms.Padding(3);
            this.led_TeachingCommand.Maximum = 999D;
            this.led_TeachingCommand.Minimum = 0D;
            this.led_TeachingCommand.Name = "led_TeachingCommand";
            this.led_TeachingCommand.On = true;
            this.led_TeachingCommand.Size = new System.Drawing.Size(145, 31);
            this.led_TeachingCommand.TabIndex = 969;
            this.led_TeachingCommand.Text = "0.000";
            this.led_TeachingCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_TeachingCommand.TextType = KosesControl.TextTypes.NUMBER;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 37);
            this.label7.TabIndex = 968;
            this.label7.Text = "ENC";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // led_TeachingEncoder
            // 
            this.led_TeachingEncoder.BackColor = System.Drawing.Color.Black;
            this.led_TeachingEncoder.Bold = false;
            this.led_TeachingEncoder.DecimalPoint = 3;
            this.led_TeachingEncoder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingEncoder.Font = new System.Drawing.Font("LED Dot-Matrix", 17F);
            this.led_TeachingEncoder.FontSize = 17F;
            this.led_TeachingEncoder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingEncoder.LedOffColor = System.Drawing.Color.Gray;
            this.led_TeachingEncoder.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.led_TeachingEncoder.Location = new System.Drawing.Point(55, 40);
            this.led_TeachingEncoder.Margin = new System.Windows.Forms.Padding(3);
            this.led_TeachingEncoder.Maximum = 999D;
            this.led_TeachingEncoder.Minimum = 0D;
            this.led_TeachingEncoder.Name = "led_TeachingEncoder";
            this.led_TeachingEncoder.On = true;
            this.led_TeachingEncoder.Size = new System.Drawing.Size(145, 31);
            this.led_TeachingEncoder.TabIndex = 970;
            this.led_TeachingEncoder.Text = "0.000";
            this.led_TeachingEncoder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.led_TeachingEncoder.TextType = KosesControl.TextTypes.NUMBER;
            // 
            // btn_TeachingZeroSet
            // 
            this.btn_TeachingZeroSet.BackColor = System.Drawing.Color.Transparent;
            this.btn_TeachingZeroSet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_TeachingZeroSet.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TeachingZeroSet.Image = ((System.Drawing.Image)(resources.GetObject("btn_TeachingZeroSet.Image")));
            this.btn_TeachingZeroSet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_TeachingZeroSet.Location = new System.Drawing.Point(206, 3);
            this.btn_TeachingZeroSet.Name = "btn_TeachingZeroSet";
            this.btn_TeachingZeroSet.Padding = new System.Windows.Forms.Padding(5);
            this.tlpMotorCommand.SetRowSpan(this.btn_TeachingZeroSet, 2);
            this.btn_TeachingZeroSet.Size = new System.Drawing.Size(98, 64);
            this.btn_TeachingZeroSet.TabIndex = 997;
            this.btn_TeachingZeroSet.Text = "ZERO\r\nSET";
            this.btn_TeachingZeroSet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_TeachingZeroSet.UseVisualStyleBackColor = false;
            this.btn_TeachingZeroSet.Click += new System.EventHandler(this.btn_TeachingZeroSet_Click);
            // 
            // tlpMotorStatus
            // 
            this.tlpMotorStatus.ColumnCount = 4;
            this.tlpMotorStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.36921F));
            this.tlpMotorStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.63079F));
            this.tlpMotorStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.36921F));
            this.tlpMotorStatus.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.63079F));
            this.tlpMotorStatus.Controls.Add(this.led_TeachingServo, 1, 0);
            this.tlpMotorStatus.Controls.Add(this.label1, 0, 0);
            this.tlpMotorStatus.Controls.Add(this.l, 2, 0);
            this.tlpMotorStatus.Controls.Add(this.led_TeachingCwLimit, 3, 0);
            this.tlpMotorStatus.Controls.Add(this.led_TeachingAlarm, 1, 1);
            this.tlpMotorStatus.Controls.Add(this.label3, 0, 1);
            this.tlpMotorStatus.Controls.Add(this.label4, 2, 1);
            this.tlpMotorStatus.Controls.Add(this.led_TeachingHome, 3, 1);
            this.tlpMotorStatus.Controls.Add(this.led_TeachingCcwLimit, 3, 2);
            this.tlpMotorStatus.Controls.Add(this.led_TeachingInpos, 1, 2);
            this.tlpMotorStatus.Controls.Add(this.label5, 0, 2);
            this.tlpMotorStatus.Controls.Add(this.label6, 2, 2);
            this.tlpMotorStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMotorStatus.Location = new System.Drawing.Point(0, 30);
            this.tlpMotorStatus.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMotorStatus.Name = "tlpMotorStatus";
            this.tlpMotorStatus.RowCount = 3;
            this.tlpMotorStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMotorStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMotorStatus.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpMotorStatus.Size = new System.Drawing.Size(307, 119);
            this.tlpMotorStatus.TabIndex = 1006;
            // 
            // led_TeachingServo
            // 
            this.led_TeachingServo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingServo.Location = new System.Drawing.Point(117, 3);
            this.led_TeachingServo.Name = "led_TeachingServo";
            this.led_TeachingServo.On = true;
            this.led_TeachingServo.Size = new System.Drawing.Size(32, 33);
            this.led_TeachingServo.TabIndex = 991;
            this.led_TeachingServo.Text = "ledBu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "SERVO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l
            // 
            this.l.AutoSize = true;
            this.l.Dock = System.Windows.Forms.DockStyle.Fill;
            this.l.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l.Location = new System.Drawing.Point(155, 0);
            this.l.Name = "l";
            this.l.Size = new System.Drawing.Size(108, 39);
            this.l.TabIndex = 957;
            this.l.Text = "+LIMIT";
            this.l.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // led_TeachingCwLimit
            // 
            this.led_TeachingCwLimit.Color = System.Drawing.Color.Red;
            this.led_TeachingCwLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingCwLimit.Location = new System.Drawing.Point(269, 3);
            this.led_TeachingCwLimit.Name = "led_TeachingCwLimit";
            this.led_TeachingCwLimit.On = true;
            this.led_TeachingCwLimit.Size = new System.Drawing.Size(35, 33);
            this.led_TeachingCwLimit.TabIndex = 994;
            this.led_TeachingCwLimit.Text = "ledBulb4";
            // 
            // led_TeachingAlarm
            // 
            this.led_TeachingAlarm.Color = System.Drawing.Color.Red;
            this.led_TeachingAlarm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingAlarm.Location = new System.Drawing.Point(117, 42);
            this.led_TeachingAlarm.Name = "led_TeachingAlarm";
            this.led_TeachingAlarm.On = true;
            this.led_TeachingAlarm.Size = new System.Drawing.Size(32, 33);
            this.led_TeachingAlarm.TabIndex = 992;
            this.led_TeachingAlarm.Text = "ledBulb2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 39);
            this.label3.TabIndex = 959;
            this.label3.Text = "ALARM";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(155, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 39);
            this.label4.TabIndex = 961;
            this.label4.Text = "HOME";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // led_TeachingHome
            // 
            this.led_TeachingHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingHome.Location = new System.Drawing.Point(269, 42);
            this.led_TeachingHome.Name = "led_TeachingHome";
            this.led_TeachingHome.On = true;
            this.led_TeachingHome.Size = new System.Drawing.Size(35, 33);
            this.led_TeachingHome.TabIndex = 996;
            this.led_TeachingHome.Text = "ledBulb6";
            // 
            // led_TeachingCcwLimit
            // 
            this.led_TeachingCcwLimit.Color = System.Drawing.Color.Red;
            this.led_TeachingCcwLimit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingCcwLimit.Location = new System.Drawing.Point(269, 81);
            this.led_TeachingCcwLimit.Name = "led_TeachingCcwLimit";
            this.led_TeachingCcwLimit.On = true;
            this.led_TeachingCcwLimit.Size = new System.Drawing.Size(35, 35);
            this.led_TeachingCcwLimit.TabIndex = 995;
            this.led_TeachingCcwLimit.Text = "ledBulb5";
            // 
            // led_TeachingInpos
            // 
            this.led_TeachingInpos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.led_TeachingInpos.Location = new System.Drawing.Point(117, 81);
            this.led_TeachingInpos.Name = "led_TeachingInpos";
            this.led_TeachingInpos.On = true;
            this.led_TeachingInpos.Size = new System.Drawing.Size(32, 35);
            this.led_TeachingInpos.TabIndex = 993;
            this.led_TeachingInpos.Text = "ledBulb3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 41);
            this.label5.TabIndex = 963;
            this.label5.Text = "IN-POS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(155, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 41);
            this.label6.TabIndex = 965;
            this.label6.Text = "-LIMIT";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_TeachingAxisName
            // 
            this.lbl_TeachingAxisName.BackColor = System.Drawing.Color.Black;
            this.lbl_TeachingAxisName.Bold = false;
            this.lbl_TeachingAxisName.DecimalPoint = 3;
            this.lbl_TeachingAxisName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_TeachingAxisName.Font = new System.Drawing.Font("Noto Sans KR", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbl_TeachingAxisName.FontSize = 12F;
            this.lbl_TeachingAxisName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lbl_TeachingAxisName.LedOffColor = System.Drawing.Color.Gray;
            this.lbl_TeachingAxisName.LedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(54)))));
            this.lbl_TeachingAxisName.Location = new System.Drawing.Point(0, 0);
            this.lbl_TeachingAxisName.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_TeachingAxisName.Maximum = 999D;
            this.lbl_TeachingAxisName.Minimum = 0D;
            this.lbl_TeachingAxisName.Name = "lbl_TeachingAxisName";
            this.lbl_TeachingAxisName.On = true;
            this.lbl_TeachingAxisName.Size = new System.Drawing.Size(307, 30);
            this.lbl_TeachingAxisName.TabIndex = 955;
            this.lbl_TeachingAxisName.Text = "Axis Name";
            this.lbl_TeachingAxisName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_TeachingAxisName.TextType = KosesControl.TextTypes.TEXT;
            // 
            // tlpJog
            // 
            this.tlpJog.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tlpJog.ColumnCount = 1;
            this.tlpJog.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpJog.Controls.Add(this.lbl_TeachingAxisName, 0, 0);
            this.tlpJog.Controls.Add(this.tableLayoutPanel2, 0, 7);
            this.tlpJog.Controls.Add(this.kosesGroupBox1, 0, 6);
            this.tlpJog.Controls.Add(this.kosesGroupBox2, 0, 5);
            this.tlpJog.Controls.Add(this.kosesGroupBox3, 0, 4);
            this.tlpJog.Controls.Add(this.tlpMotorStatus, 0, 1);
            this.tlpJog.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.tlpJog.Controls.Add(this.tlpMotorCommand, 0, 2);
            this.tlpJog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpJog.Location = new System.Drawing.Point(0, 0);
            this.tlpJog.Name = "tlpJog";
            this.tlpJog.RowCount = 8;
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.52428F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.143919F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.33803F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.83914F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.3319F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.54624F));
            this.tlpJog.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276483F));
            this.tlpJog.Size = new System.Drawing.Size(307, 850);
            this.tlpJog.TabIndex = 954;
            this.tlpJog.VisibleChanged += new System.EventHandler(this.tlpJog_VisibleChanged);
            // 
            // tmrMotorStatus
            // 
            this.tmrMotorStatus.Interval = 1000;
            this.tmrMotorStatus.Tick += new System.EventHandler(this.tmrMotorStatus_Tick);
            // 
            // ucJog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpJog);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ucJog";
            this.Size = new System.Drawing.Size(307, 850);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.kosesGroupBox3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.kosesGroupBox2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.kosesGroupBox1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tlpMotorCommand.ResumeLayout(false);
            this.tlpMotorCommand.PerformLayout();
            this.tlpMotorStatus.ResumeLayout(false);
            this.tlpMotorStatus.PerformLayout();
            this.tlpJog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_TeachingMotorStop;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox3;
        private System.Windows.Forms.Button btn_TeachingJogCw;
        private System.Windows.Forms.Button btn_TeachingJogCcw;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox2;
        private System.Windows.Forms.Button btn_TeachingJogRel10;
        private System.Windows.Forms.Button btn_TeachingJogRel01;
        private System.Windows.Forms.Button btn_TeachingJogRel05;
        private System.Windows.Forms.Button btn_TeachingJogRelDot01;
        private System.Windows.Forms.Button btn_TeachingJogRelDot02;
        private System.Windows.Forms.Button btn_TeachingJogRelDot03;
        private System.Windows.Forms.TextBox txt_TeachingRelValue;
        private System.Windows.Forms.Button btn_TeachingJogRelCw;
        private System.Windows.Forms.Button btn_TeachingJogRelCcw;
        private KosesControl.Controls.KosesGroupBox kosesGroupBox1;
        private System.Windows.Forms.Button btn_TeachingAbsGetValue;
        private System.Windows.Forms.Button btn_TeachingJogAbs;
        private System.Windows.Forms.TextBox txt_TeachingAbsValue;
        private System.Windows.Forms.Button btn_TeachingZeroSet;
        private Bulb.LedBulb led_TeachingHome;
        private Bulb.LedBulb led_TeachingCcwLimit;
        private Bulb.LedBulb led_TeachingCwLimit;
        private Bulb.LedBulb led_TeachingInpos;
        private Bulb.LedBulb led_TeachingAlarm;
        private Bulb.LedBulb led_TeachingServo;
        private KosesControl.Controls.LedLabel led_TeachingEncoder;
        private KosesControl.Controls.LedLabel led_TeachingCommand;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label l;
        private System.Windows.Forms.Label label1;
        private KosesControl.Controls.LedLabel lbl_TeachingAxisName;
        private System.Windows.Forms.Button btn_TeachingRecipeSave;
        private System.Windows.Forms.TableLayoutPanel tlpMotorStatus;
        private System.Windows.Forms.TableLayoutPanel tlpMotorCommand;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_TeachingServoOn;
        private System.Windows.Forms.Button btn_TeachingServoOff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btn_TeachingAlarmClear;
        private System.Windows.Forms.Button btn_TeachingHome;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tlpJog;
        private System.Windows.Forms.Timer tmrMotorStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
    }
}
