﻿namespace KOSES_SEQ.OP
{
    partial class ucIO
{
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucIO));
            this.tmrIo = new System.Windows.Forms.Timer(this.components);
            this.panIoTitle = new System.Windows.Forms.Panel();
            this.labIoTitle = new System.Windows.Forms.Label();
            this.btnIoChUp = new System.Windows.Forms.Button();
            this.btnIoChDown = new System.Windows.Forms.Button();
            this.panIoData = new System.Windows.Forms.Panel();
            this.dgvInOut = new KosesControl.Controls.KosesDataGridView();
            this.columnNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Check = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbSearchNo = new KosesControl.Controls.KosesTextBox();
            this.tbSearchName = new KosesControl.Controls.KosesTextBox();
            this.btnSearch = new KosesControl.Controls.KosesButton();
            this.panIoTitle.SuspendLayout();
            this.panIoData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInOut)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrIo
            // 
            this.tmrIo.Interval = 300;
            this.tmrIo.Tick += new System.EventHandler(this.tmrIo_Tick);
            // 
            // panIoTitle
            // 
            this.panIoTitle.BackColor = System.Drawing.Color.DarkSalmon;
            this.panIoTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panIoTitle.Controls.Add(this.labIoTitle);
            this.panIoTitle.Controls.Add(this.btnIoChUp);
            this.panIoTitle.Controls.Add(this.btnIoChDown);
            this.panIoTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panIoTitle.Location = new System.Drawing.Point(0, 0);
            this.panIoTitle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panIoTitle.Name = "panIoTitle";
            this.panIoTitle.Size = new System.Drawing.Size(278, 31);
            this.panIoTitle.TabIndex = 10;
            // 
            // labIoTitle
            // 
            this.labIoTitle.BackColor = System.Drawing.Color.Transparent;
            this.labIoTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labIoTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labIoTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labIoTitle.ForeColor = System.Drawing.Color.White;
            this.labIoTitle.Location = new System.Drawing.Point(40, 0);
            this.labIoTitle.Name = "labIoTitle";
            this.labIoTitle.Size = new System.Drawing.Size(196, 29);
            this.labIoTitle.TabIndex = 8;
            this.labIoTitle.Text = "INPUT (CH 01)";
            this.labIoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnIoChUp
            // 
            this.btnIoChUp.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnIoChUp.FlatAppearance.BorderSize = 0;
            this.btnIoChUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIoChUp.ForeColor = System.Drawing.Color.White;
            this.btnIoChUp.Location = new System.Drawing.Point(236, 0);
            this.btnIoChUp.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnIoChUp.Name = "btnIoChUp";
            this.btnIoChUp.Size = new System.Drawing.Size(40, 29);
            this.btnIoChUp.TabIndex = 7;
            this.btnIoChUp.Text = "▶";
            this.btnIoChUp.UseVisualStyleBackColor = true;
            this.btnIoChUp.Click += new System.EventHandler(this.btnIoChUp_Click);
            // 
            // btnIoChDown
            // 
            this.btnIoChDown.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnIoChDown.FlatAppearance.BorderSize = 0;
            this.btnIoChDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIoChDown.ForeColor = System.Drawing.Color.White;
            this.btnIoChDown.Location = new System.Drawing.Point(0, 0);
            this.btnIoChDown.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnIoChDown.Name = "btnIoChDown";
            this.btnIoChDown.Size = new System.Drawing.Size(40, 29);
            this.btnIoChDown.TabIndex = 6;
            this.btnIoChDown.Text = "◀";
            this.btnIoChDown.UseVisualStyleBackColor = true;
            this.btnIoChDown.Click += new System.EventHandler(this.btnIoChDown_Click);
            // 
            // panIoData
            // 
            this.panIoData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panIoData.Controls.Add(this.dgvInOut);
            this.panIoData.Controls.Add(this.tableLayoutPanel2);
            this.panIoData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panIoData.Font = new System.Drawing.Font("Arial", 10F);
            this.panIoData.Location = new System.Drawing.Point(0, 31);
            this.panIoData.Margin = new System.Windows.Forms.Padding(0);
            this.panIoData.Name = "panIoData";
            this.panIoData.Size = new System.Drawing.Size(278, 461);
            this.panIoData.TabIndex = 11;
            // 
            // dgvInOut
            // 
            this.dgvInOut.AllowUserToAddRows = false;
            this.dgvInOut.AllowUserToDeleteRows = false;
            this.dgvInOut.AllowUserToResizeColumns = false;
            this.dgvInOut.AllowUserToResizeRows = false;
            this.dgvInOut.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInOut.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvInOut.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInOut.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvInOut.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(232)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInOut.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInOut.ColumnHeadersHeight = 32;
            this.dgvInOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvInOut.ColumnHeadersVisible = false;
            this.dgvInOut.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnNo,
            this.columnName,
            this.columnStatus,
            this.Check});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInOut.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvInOut.DisabledBackColor = System.Drawing.SystemColors.Control;
            this.dgvInOut.DisabledTextColor = System.Drawing.SystemColors.GrayText;
            this.dgvInOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInOut.EnableHeadersVisualStyles = false;
            this.dgvInOut.GridColor = System.Drawing.Color.Black;
            this.dgvInOut.Location = new System.Drawing.Point(0, 0);
            this.dgvInOut.Margin = new System.Windows.Forms.Padding(0);
            this.dgvInOut.MultiSelect = false;
            this.dgvInOut.Name = "dgvInOut";
            this.dgvInOut.RowHeadersVisible = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 10F);
            this.dgvInOut.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvInOut.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 10F);
            this.dgvInOut.RowTemplate.Height = 20;
            this.dgvInOut.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInOut.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInOut.Size = new System.Drawing.Size(276, 436);
            this.dgvInOut.TabIndex = 10;
            this.dgvInOut.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInOut_CellEndEdit);
            this.dgvInOut.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInOut_CellMouseDoubleClick);
            this.dgvInOut.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInOut_CellMouseUp);
            this.dgvInOut.SelectionChanged += new System.EventHandler(this.dgvInOut_SelectionChanged);
            // 
            // columnNo
            // 
            this.columnNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.columnNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.columnNo.FillWeight = 52F;
            this.columnNo.HeaderText = "No";
            this.columnNo.Name = "columnNo";
            this.columnNo.ReadOnly = true;
            this.columnNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.columnNo.Width = 50;
            // 
            // columnName
            // 
            this.columnName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.columnName.DefaultCellStyle = dataGridViewCellStyle3;
            this.columnName.FillWeight = 186.9246F;
            this.columnName.HeaderText = "Name";
            this.columnName.Name = "columnName";
            this.columnName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // columnStatus
            // 
            this.columnStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.columnStatus.DefaultCellStyle = dataGridViewCellStyle4;
            this.columnStatus.FillWeight = 50F;
            this.columnStatus.HeaderText = "Status";
            this.columnStatus.Name = "columnStatus";
            this.columnStatus.ReadOnly = true;
            this.columnStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Check
            // 
            this.Check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Check.HeaderText = "Ck";
            this.Check.Name = "Check";
            this.Check.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Check.Width = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Controls.Add(this.tbSearchNo, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbSearchName, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSearch, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 436);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(276, 23);
            this.tableLayoutPanel2.TabIndex = 12;
            // 
            // tbSearchNo
            // 
            this.tbSearchNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSearchNo.ForeColor = System.Drawing.Color.Gray;
            this.tbSearchNo.HintText = "No";
            this.tbSearchNo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbSearchNo.Location = new System.Drawing.Point(0, 0);
            this.tbSearchNo.Margin = new System.Windows.Forms.Padding(0);
            this.tbSearchNo.Name = "tbSearchNo";
            this.tbSearchNo.Size = new System.Drawing.Size(50, 23);
            this.tbSearchNo.TabIndex = 3;
            this.tbSearchNo.Text = "No";
            this.tbSearchNo.TextBoxType = KosesControl.NumberTypes.ALL;
            // 
            // tbSearchName
            // 
            this.tbSearchName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSearchName.ForeColor = System.Drawing.Color.Gray;
            this.tbSearchName.HintText = "Enter IO Name";
            this.tbSearchName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tbSearchName.Location = new System.Drawing.Point(50, 0);
            this.tbSearchName.Margin = new System.Windows.Forms.Padding(0);
            this.tbSearchName.Name = "tbSearchName";
            this.tbSearchName.Size = new System.Drawing.Size(202, 23);
            this.tbSearchName.TabIndex = 1;
            this.tbSearchName.Text = "Enter IO Name";
            this.tbSearchName.TextBoxType = KosesControl.NumberTypes.ALL;
            this.tbSearchName.WordWrap = false;
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = false;
            this.btnSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnSearch.Bold = true;
            this.btnSearch.BolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnSearch.Depth = 0;
            this.btnSearch.DeselectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(87)))), ((int)(((byte)(126)))));
            this.btnSearch.DisabledBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnSearch.DisabledBolderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnSearch.DisabledTextColor = System.Drawing.Color.Gray;
            this.btnSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch.DrawShadows = true;
            this.btnSearch.FontSize = 14F;
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.HighEmphasis = true;
            this.btnSearch.Icon = ((System.Drawing.Image)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.IconLocation = new System.Drawing.Point(-10, 0);
            this.btnSearch.Location = new System.Drawing.Point(253, 0);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.btnSearch.MouseOnOpacity = 50;
            this.btnSearch.MouseState = KosesControl.Animations.MouseState.HOVER;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Selected = false;
            this.btnSearch.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(48)))), ((int)(((byte)(70)))));
            this.btnSearch.Size = new System.Drawing.Size(23, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.TypeButton = KosesControl.Controls.KosesButton.ButtonType.CONTAINED;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // ucIO
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.Controls.Add(this.panIoData);
            this.Controls.Add(this.panIoTitle);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ucIO";
            this.Size = new System.Drawing.Size(278, 492);
            this.VisibleChanged += new System.EventHandler(this.IOView_VisibleChanged);
            this.panIoTitle.ResumeLayout(false);
            this.panIoData.ResumeLayout(false);
            this.panIoData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInOut)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer tmrIo;
        private System.Windows.Forms.Panel panIoTitle;
        private System.Windows.Forms.Label labIoTitle;
        private System.Windows.Forms.Button btnIoChUp;
        private System.Windows.Forms.Button btnIoChDown;
        private System.Windows.Forms.Panel panIoData;
        private KosesControl.Controls.KosesDataGridView dgvInOut;
        private KosesControl.Controls.KosesTextBox tbSearchName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private KosesControl.Controls.KosesTextBox tbSearchNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnStatus;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Check;
        private KosesControl.Controls.KosesButton btnSearch;
    }
}
