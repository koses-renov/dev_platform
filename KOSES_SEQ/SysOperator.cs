﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using KOSES_SEQ.CONFIG;
using KOSES_SEQ.MOTION;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TASK_AUTO;
using KOSES_SEQ.TASK_DATA;
using KOSES_SEQ.TASK_HOME;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MESSAGE;
using KOSES_SEQ.TASK_MANUAL;
using KOSES_SEQ.TEACHING;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using KOSES_SEQ.POPUP;
using KOSES_SEQ.HARDWARE;

namespace KOSES_SEQ.OP
{
    public static class FRM
    {
        public static frmBase Main;

        public static frmManual Manual;
        public static frmAuto Auto;
        public static frmTeaching Teach;
        public static frmJob Job;
        public static frmSystem System;
        public static frmAlarmInfo Alarm;
        public static frmMotorStatus MotorStatus;
        //public static frmSystem_TowerLamp Tower;

        public static frmMsg Msg;
        public static FrmLogin Login;
        public static FrmErrorPopup ErrorPopup;



        public static void SetupPagedForm(System.Windows.Forms.Form F)
        {
            F.AutoScaleMode = AutoScaleMode.None;
            F.FormBorderStyle = FormBorderStyle.None;
            F.Size = Main.panBase.Size;
            F.StartPosition = FormStartPosition.Manual;
            F.ShowInTaskbar = false;
            F.Location = new Point(0, Main.panBaseTop.Height);
            //F.DoubleBufferingAllDataGrid();
        }

        internal static void Init()
        {
            Main = new frmBase();
            Manual = new frmManual();
            Auto = new frmAuto();
            Msg = new frmMsg();
            Teach = new frmTeaching();
            Job = new frmJob();
            System = new frmSystem();
            MotorStatus = new frmMotorStatus();
            Alarm = new frmAlarmInfo();

            Login = new FrmLogin();
            ErrorPopup = new FrmErrorPopup();
            //Tower = new frmSystem_TowerLamp();


            SetupPagedForm(Main);
            SetupPagedForm(Manual);
            SetupPagedForm(Auto);
            SetupPagedForm(Teach);
            SetupPagedForm(Job);
            SetupPagedForm(System);
            SetupPagedForm(MotorStatus);

            Msg.Show();
            Msg.Hide();

            Login.Show();
            Login.Hide();

            ErrorPopup.Show();
            ErrorPopup.Hide();
            //Tower.Show();
            //Tower.Hide();
            //Alarm.Show();
            //Alarm.Hide();
        }
    }

    public static class FormHelper
    {
        /// <summary>
        /// Show Fom in parent control and Close/Hide previous insided forms
        /// </summary>
        public static void ShowInside(this Form form, Control parent)
        {
            form.TopLevel = false;
            parent.Controls.Add(form);
            form.Dock = DockStyle.Fill;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            form.Show();

            string formname = form.Name;
            if (formname != $"frmAuto")
            {
                form.BringToFront();
            }
        }

        public static void ShowAndFront(this Form self, Form main)
        {
            if (self.Visible) self.BringToFront();
            else self.Show(main);
        }

        public static void HideChildForms(this Control ctrl)
        {
            if (ctrl != null)
                foreach (Control f in ctrl.Controls)
                {
                    if (f is Form)
                    {
                        (f as Form).Hide();
                    }
                }
        }
    }

    public delegate void deleRecipeChanged(string groupName, string recipeName);
    class SysOperator
    {
        public event deleRecipeChanged OnRecipeChanged;
        private static SysOperator _Instance;
        // 내부용
        private int[] _BtnIOs = null;
        private bool[] _BtnFlags = null;

        public MotorParameter motorPara = null;
        public MotionController motion = null;
        public CTenkey Tenkey = new CTenkey();

        public DefaultInfo defaultInfo = new DefaultInfo();

        private static object Singleton = new object();
        public static SysOperator Instance
        {
            get
            {
                lock (Singleton)
                {
                    if (_Instance == null)
                    {
                        _Instance = new SysOperator();
                    }

                    return _Instance;
                }
            }
        }

        protected SysOperator()
        {
            if (Program.IsSimulation)
                motion = new MotionController_SIMUL();
            else
                motion = new MotionController_WMX2();

        }

        public bool Init()
        {

            bool ret = true; // 리턴값 성공으로 초기화

            //_BtnIOs = new int[3] { DI.BTN_START, DI.BTN_STOP, DI.BTN_RESET };
            _BtnFlags = new bool[3] { false, false, false };


            // 폴더 만들기
            //DIR.Log = APP.Path + "Log\\";
            //DIR.Cassette = APP.Path + "Cassette\\";
            //DIR.Db = APP.Path + "Db\\";
            //Directory.CreateDirectory(DIR.Log);
            //Directory.CreateDirectory(DIR.Cassette);
            //Directory.CreateDirectory(DIR.Db);

            //// 파일경로
            //FILE.CFG = APP.Path + "CFG.ini";
            //FILE.AlarmList = APP.Path + "AlarmList.db";
            //FILE.IO = APP.Path + "io.db";
            //FILE.Comp = APP.Path + "Comp.cmp";
            //FILE.DbLot = DIR.Db + "Lot.db";
            //FILE.DbNg = DIR.Db + "Ng.db";
            //FILE.DbAlarm = DIR.Db + "Alarm.db";


            // 폼 초기화
            FRM.Init();


            FRM.Main.ProgramInit_Task();

            // 시퀀스 초기화
            //SEQ.Init(MTS_AX.MotorCount);



            //////////////////////////////////////////////////
            //  모터 파라메터
            //////////////////////////////////////////////////
            //ProgramInit_Log($"모터 파라메터 초기화");
            //motorPara.LoadAll();
            //motion.SetParameterAll();
            //motion.SetExtraParameter();


            // 옵셋 초기화
            // OFFSET.Init(MaxBlockSize);

            // 이전 설비의 기본 상태 복구 (레시피 등)
            LoadDefaultInfo();

            UserManager.Instance.ReadLoginUserList();

            // 레시피 초기화
            SetRecipeName(defaultInfo.RecipeGroupName, defaultInfo.RecipeName);
            TeachingManager.Instance.Load();
            TeachingManager.Instance.TeachingData.SetTeachingNodeName();
            //RCP.Dir = APP.Path + "Recipe\\";
            //RCP.Init(FILE.CFG, RCP.Dir + "COMMON_RECIPE.DAT", OnRecipeChanged);
            //for (int i = 0; i < MTS_AX.MotorCount; i++)
            //    FA.RCP.MotorNames[i] = MTS_AX.Axes[i].AxisName;

            // DB Open
            DBManager.Instance.Open();

            // Alarm 관련 Table Default Set (Table 없을 시 생성)
            DBManager.Instance.AlarmInfoDB.MakeTable();
            DBManager.Instance.AlarmInfoDB.MakeEmptyAlarmList();

            DBManager.Instance.AlarmLogDB.MakeTable();


            List<MotorInfo> motorList = new List<MotorInfo>();
            int i = 0;
            foreach (string name in Enum.GetNames(typeof(Def.Motor)))
            {
                MotorInfo mi = new MotorInfo();
                mi.Name = name;
                mi.Num = i++;
                motorList.Add(mi);
                mi = null;
            }
            SysOperator.Instance.motorPara = new MotorParameter(motorList, DefPath.Motor, DefPath.MotorHelp);
            //// 사용자 초기화
            //USER.Init(FILE.CFG);
            //USER.Authority = Authority.Engineer;

            // Tenkey 추가
            TenKeyIO io = new TenKeyIO();
            // 시작 I/O 번호는 0, 0 Default (예외 생길 시 가변적으로 수정 필요)
            io.SetTenKeyIO(0, 0);
            Tenkey.SetTenkeyIO(io);


            Thread.Sleep(100);

            // 쓰레드 연결 및 스타트
            //TRD.DoProc = DoMainThread;
            //TRD.Start();

            //// 초기화 성공 여부 (리턴값 체크)
            //if (ret)
            //    LOG.Debug("SYS", "Initialized successfully");
            //else
            //    LOG.Debug("SYS", "Fail to Initialize");

            return ret;
        }

        // 종료화
        public static void Final()
        {
           
            
          //

        }


        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  TASK
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
       // public static SafetySensorTask safetySensorTask;
       // public static DoorSensorTask doorSensorTask;
        public static HomeTask homeTask;
        public static HomeData homeData;
        public static ManualTask manualTask;
        public static AutoTask autoTask;
        public static AutoData autoData;
        public static AutoUse autoUse;

        public static Def.TaskType WhoIsLive()
        {
            Def.TaskType taskType = Def.TaskType.NONE;

            if (true == autoTask.IsLive)
            {
                return Def.TaskType.AUTO;
            }

            if (true == homeTask.Task.IsLive)
            {
                return Def.TaskType.HOME;
            }

            if (true == manualTask.IsLive)
            {
                return Def.TaskType.MANUAL;
            }

            return taskType;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  USER STOP
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static bool userStopFlag = false;
        public static void SetUserStop(bool b = true)
        {
            userStopFlag = b;
        }
        public static void ResetUserStop()
        {
            SetUserStop(false);
        }
        public static bool GetUserStop()
        {
            return userStopFlag;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  CHECK CONDITION
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
        #region CheckCondition : Door
        public static Def.Code CheckCondition_Door()
        {
            //if (Program.teaching.common.doorCheck == false)
            //{
            //    return Def.Code.OK;
            //}

            //if (Program.motion.frame.door.IsOpen())
            //{
            //    string doorString = Program.motion.frame.door.GetOpenDoorString();
            //    Program.MessageBox($"DOOR OPENED", doorString, DefColor.LightRed);
            //    return Def.Code.DOOR_OPENED;
            //}
            return Def.Code.OK;
        }
        #endregion
        #region CheckCondition : MotorInit
        public static Def.Code CheckCondition_MotorInit()
        {

            bool b;
            string title = "CHECK CONDITION : MOTOR INIT";
            string msg = "";
            string motorName = "";

            //b = Program.motion.IsHomeAllCompleted(ref motorName);
            //if (b == false)
            //{
            //    msg = $"CHECK MOTOR INIT STATUS : {motorName}";
            //    MessageBox(title, msg, DefColor.LightRed);
            //    return Def.Code.MOTOR_NOT_INIT;
            //}

            return Def.Code.OK;
        }
        #endregion
        #region CheckCondition : General
        public static Def.Code CheckCondition_General()
        {
            Def.Code code;
            bool b;
            string title = "CHECK CONDITION : GENERAL";
            string msg = "";

            //b = Program.motion.frame.mainAir.Get();
            //if (b == false)
            //{
            //    msg = "CHECK MAIN AIR";
            //    MessageBox(title, msg, DefColor.LightRed);
            //    return Def.Code.MAIN_AIR_PRESSURE_LOW;
            //}

            //b = Program.motion.IsServoOnAll();
            //if (b == false)
            //{
            //    msg = "CHECK MOTOR SERVO STATUS";
            //    MessageBox(title, msg, DefColor.LightRed);
            //    return Def.Code.MOTOR_SERVO_OFF;
            //}

            //code = CheckCondition_Door();
            //if (code != Def.Code.OK)
            //{
            //    return code;
            //}

            //bool safeSensorCur = Program.motion.frame.areaSensor.Get();
            //if (Program.config.virtualMode)
            //{
            //    safeSensorCur = false;
            //}
            //if (safeSensorCur)
            //{
            //    msg = "SAFETY SENSOR DETECTED";
            //    MessageBox(title, msg, DefColor.LightRed);
            //    return Def.Code.SAFETY_SENSOR_DETECTED;
            //}

            return Def.Code.OK;
        }
        #endregion
        #region CheckCondition : Home
        public static Def.Code CheckCondition_Home()
        {
            Def.Code code;

            code = CheckCondition_General();
            if (code != Def.Code.OK)
            {
                return code;
            }

            return Def.Code.OK;
        }
        #endregion
        #region CheckCondition : Start
        public static Def.Code CheckCondition_Start()
        {
            Def.Code code;

            //if (motion.frame.mainAir.Get() == false)
            //{
            //    return Def.Code.MAIN_AIR_PRESSURE_LOW;
            //}

            code = CheckCondition_Home();
            if (code != Def.Code.OK)
            {
                return code;
            }

            code = CheckCondition_MotorInit();
            if (code != Def.Code.OK)
            {
                return code;
            }

            return Def.Code.OK;
        }
        #endregion
        #region CheckCondition : ManualStart
        public static Def.Code CheckCondition_ManualStart()
        {
            Def.Code code;

            EquipmentStatus.State es = GetEquipState();
            switch (es)
            {
                case EquipmentStatus.State.READY:
                case EquipmentStatus.State.STOP:
                case EquipmentStatus.State.ERROR_RESET:
                case EquipmentStatus.State.JOB_COMPLETED:
                    break;

                default:
                    FRM.Msg.ShowMsg($"MANUAL CAN NOT MANUAL OPERATION WHEN {es}");
                    return Def.Code.INVALID_STATUS;
            }

            code = CheckCondition_General();
            if (code != Def.Code.OK)
            {
                return code;
            }

            code = CheckCondition_MotorInit();
            if (code != Def.Code.OK)
            {
                return code;
            }

            //bool safeSensorCur = motion.frame.areaSensor.Get();
            bool safeSensorCur = false;
            if (safeSensorCur)
            {              
                FRM.Msg.ShowMsg($"MANUAL SAFETY SENSOR DETECTED {es}");
                return Def.Code.SAFETY_SENSOR_DETECTED;
            }

            return Def.Code.OK;
        }
        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  MANAGER
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static EquipmentStatus equipmentstatus = new EquipmentStatus();

        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  EQUIPMENT STATUS
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void SetEquipState(EquipmentStatus.State es)
        {
            equipmentstatus.States = es;

           // Program.log.EquipmentState(es);

           // TowerLampInformation_3C tl = Program.towerLamp.Get(es);
            //Program.motion.frame.towerLamp.SetLamp(tl.red, tl.yellow, tl.green);
        }
        public static EquipmentStatus.State GetEquipState()
        {
            return equipmentstatus.States;
        }
          #region Start
        // 오토 스타트
        public void StartAuto()
        {
            //Program.log.Action($"Main Start : Begin");
            string msg = "";

            Def.Code code;
            string title = "START";

            code = CheckCondition_General();
            if (code != Def.Code.OK)
            {
                //Program.log.Action($"Main Start : {code}");
                return;
            }

            if (homeTask.Task.IsLive)
            {
                msg = ($"초기화 동작 중입니다. 잠시 후 다시 시도하세요.");
               
                FRM.Msg.ShowMsg($"{msg}");
                //Program.log.Action($"Main Start : {msg}");
                return;
            }

            if (manualTask.IsLive)
            {
                msg = ($"매뉴얼 동작 중입니다. 잠시 후 다시 시도하세요.");
                
                FRM.Msg.ShowMsg($"{msg}");
                //Program.log.Action($"Main Start : {msg}");
                return;
            }

            //MotorAlarmCheck_Start(); 

            code = CheckCondition_Start();
            if (code != Def.Code.OK)
            {
                msg = $"CheckCondition_Start : {code}";
                
                FRM.Msg.ShowMsg($"{msg}");
                //Program.log.Action($"Main Start : {code}");
                return;
            }

            EquipmentStatus.State es = GetEquipState();
            //Program.log.Action($"Main Start : EquipmentStatus ({es})");

            switch (es)
            {
                case EquipmentStatus.State.ERROR:
                    msg = string.Format("NEED ERROR RESET");
                   // Program.log.Action($"Main Start : {msg}");
                    return;

                case EquipmentStatus.State.READY:
                    ResetUserStop();
                    //autoData.lotEnd.State = false;
                    autoData.PauseInput = false;
                    autoTask.Start();
                    SetEquipState(EquipmentStatus.State.EXECUTING);
                    break;

                case EquipmentStatus.State.JOB_COMPLETED:
                    ResetUserStop();
                    SetEquipState(EquipmentStatus.State.EXECUTING);
                    break;

                case EquipmentStatus.State.ERROR_RESET:
                case EquipmentStatus.State.STOP:
                    ResetUserStop();
                    SetEquipState(EquipmentStatus.State.EXECUTING);
                    break;

                case EquipmentStatus.State.EXECUTING:
                    msg = ($"이미 오토런 동작 중 입니다.");
                    
                    FRM.Msg.ShowMsg($"{msg}");
                    return;

                default:
                    msg = ($"장비 상태 정보를 확인하세요.");
                    //FRM.Msg.ShowMsg($"{msg}");

                    // Alarm Test용
                    AlarmManager.Instance.SetError((int)Def.Code.CODE_ERROR, msg);
                    return;
            }

            //Program.equipTime.WorkTimeStart();

            //Program.log.Action($"Main Start : End");
        }
        #endregion
        #region Stop
        private void StopFunc(bool taskStop, bool machineStop)
        {
            if (taskStop == true)
            {
                autoTask.Stop();
                homeTask.Task.Stop();
                manualTask.Stop();
            }
            if (machineStop == true)
            {
                motion.StopAll(Def.MaxHomeStepCount * 3);
            }
        }
        private void EmoStop()
        {
            motion.EmoStopAll(Def.MaxHomeStepCount * 3);
        }

        // 모두 멈춤
        public  void StopAll(bool isEStop = false)
        {
            //Program.log.Action($"Main Stop Begin");
            string title = $"STOP";
            //motion.frame.button.SetSwitchLamp(LibDef.LampMotion.Off, LibDef.LampMotion.On, LibDef.LampMotion.Off);

            EquipmentStatus.State es = GetEquipState();

            if (manualTask.IsLive)
            {
                SetUserStop(true);
                StopFunc(true, true);
                //Program.log.Action($"Main Stop : Manual Task is working");
                return;
            }

            if (homeTask.Task.IsLive)
            {
                SetUserStop(true);
                StopFunc(true, true);
                //Program.log.Action($"Main Stop : Home Task is working");
                //Program.NoticeStop();
                homeTask.Stop();
                SetEquipState(EquipmentStatus.State.NEED_INITIALIZATION);
                return;
            }

            //Program.log.Action($"Main Stop : EquipmentStatus ({es})");
            switch (es)
            {
                case EquipmentStatus.State.UNKNOWN:
                    SetUserStop(true);
                    StopFunc(true, true);
                    //equipTime.IdleTimeStart();
                    break;

                case EquipmentStatus.State.MOTION_CONNECT_FAIL:
                    SetUserStop(true);
                    StopFunc(true, true);
                    //equipTime.IdleTimeStart();
                    break;

                case EquipmentStatus.State.NEED_INITIALIZATION:
                    SetUserStop(true);
                    StopFunc(true, true);
                    //equipTime.IdleTimeStart();
                    break;

                case EquipmentStatus.State.NOW_INITIALIZING:
                    SetUserStop(true);
                    StopFunc(true, true);
                    SetEquipState(EquipmentStatus.State.NEED_INITIALIZATION);
                    //equipTime.IdleTimeStart();
                    break;

                case EquipmentStatus.State.READY:
                    StopFunc(true, true);
                    //equipTime.IdleTimeStart();
                    break;

                case EquipmentStatus.State.EXECUTING:
                    SetEquipState(EquipmentStatus.State.STOP);
                    //equipTime.StopTimeStart();
                    break;

                case EquipmentStatus.State.ERROR:
                    //SetUserStop(true);
                    //msg = Program.lang.Trans($"리셋 버튼를 누르고 다시 시도하세요.");
                    //Program.MessageBox(title, msg, DefColor.MsgWarning);
                    break;

                case EquipmentStatus.State.ERROR_RESET:
                case EquipmentStatus.State.STOP:
                case EquipmentStatus.State.JOB_COMPLETED:
                    //SetUserStop(true);
                    //if (Program.MessageBoxYesNo($"작업 종료", $"현재 진행중인 작업을 종료할까요 ?", DefColor.Yellow))
                    //{
                    //    autoTask.Finish();
                    //    SetEquipStatus(Equipment.State.NEED_INITIALIZATION);
                    //}
                    break;

                default:
                    SetEquipState(EquipmentStatus.State.STOP);
                    StopFunc(true, true);
                    //equipTime.StopTimeStart();
                    break;
            }

            //Program.log.Action($"Main Stop : End");
        }
        #endregion
        #region Reset
        // 리셋
        public void Reset()
        {
            //Program.log.Action($"Main Reset Begin");
            //Program.motion.frame.button.SetSwitchLamp(LibDef.LampMotion.Off, LibDef.LampMotion.Off, LibDef.LampMotion.On);

            EquipmentStatus.State es = GetEquipState();
            //Program.log.Action($"Main Reset : EquipmentStatus ({es})");
            switch (es)
            {
                case EquipmentStatus.State.ERROR:
                    ResetUserStop();
                    SetEquipState(EquipmentStatus.State.ERROR_RESET);
                    //Program.baseForm.lastErrorForm.DoHide();
                    //equipTime.IdleTimeStart();
                    break;
                default:
                    break;
            }

            AlarmManager.Instance.ResetAllErrors();
            //Program.log.Action($"Main Reset End");
        }
        #endregion

        #region Home
        public void Home()
        {
            //Program.log.Action($"Main Home Begin");
            string msg = "";

            //if (Program.config.virtualMode)
            //{
            //    Program.autoTask.Init();
            //    //foreach (Def.TrayDir td in Enum.GetValues(typeof(Def.TrayDir)))
            //    //{
            //    //    Program.autoTask.task.dataMgr.Tray_MakeNew(td);
            //    //    Program.autoTask.task.dataMgr.FailTray_MakeNew(td);
            //    //}
            //    return;
            //}

            //Program.motion.frame.button.SetSwitchLamp(LibDef.LampMotion.Off, LibDef.LampMotion.Off, LibDef.LampMotion.Off);           

            Def.Code code;
            string title = "HOME";

            code = CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                //Program.log.Action($"Main Home : CheckCodition_Door ({code})");
                return;
            }


            if (homeTask.Task.IsLive)
            {
                msg = ($"초기화 동작이 이미 진행중입니다."); //Program.lang.Trans
                FRM.Msg.ShowMsg(msg);
                //Program.log.Action($"Main Home : {msg}");
                return;
            }

            if (manualTask.IsLive)
            {
               // msg = Program.lang.Trans($"매뉴얼 동작 진행 중입니다. 잠시 후 다시 시도하세요.");
                msg = ($"매뉴얼 동작 진행 중입니다. 잠시 후 다시 시도하세요.");
                FRM.Msg.ShowMsg(msg);              
                //Program.log.Action($"Main Home : CheckCodition_Door ({msg})");
                return;
            }

            //FRM.Main.MotorAlarmCheck_Start();

            code = CheckCondition_Home();
            if (code != Def.Code.OK)
            {
                //Program.log.Action($"Main Home : CheckCondition_Home ({code})");
                return;
            }

            EquipmentStatus.State es = GetEquipState();

            //Program.log.Action($"Main Home : EquipmentStatus ({es})");
            switch (es)
            {
                case EquipmentStatus.State.UNKNOWN:
                    break;

                case EquipmentStatus.State.MOTION_CONNECT_FAIL:
                    break;

                case EquipmentStatus.State.NOW_INITIALIZING:

                    break;

                case EquipmentStatus.State.EXECUTING:
                    //msg = Program.lang.Trans($"중지 버튼을 누르고 다시 시도하세요.");
                    msg = ($"중지 버튼을 누르고 다시 시도하세요.");
                    FRM.Msg.Warning(msg);
                    break;

                case EquipmentStatus.State.ERROR:
                    //msg = Program.lang.Trans($"리셋 버튼을 누르고 다시 시도하세요.");
                    msg = ($"리셋 버튼을 누르고 다시 시도하세요.");
                    FRM.Msg.Warning(msg);
                    break;

                case EquipmentStatus.State.ERROR_RESET:
                case EquipmentStatus.State.STOP:
                case EquipmentStatus.State.JOB_COMPLETED:
                    //msg = Program.lang.Trans($"오토런 동작이 취소됩니다. 진행할까요 ?");
                    msg = ($"오토런 동작이 취소됩니다. 진행할까요 ?");
                    if (FRM.Msg.Confirm(msg))
                    {
                        //msg = Program.lang.Trans($"초기화 동작 중입니다...");
                        msg = ($"초기화 동작 중입니다...");
                        //NoticeStart(msg, Def.NoticeMediumFontSize, DefColor.LightGreen, 0);
                        SetUserStop(true);
                        autoTask.Finish();
                        homeTask.Start();
                    }
                    break;


                default:
                    //msg = Program.lang.Trans($"초기화 동작 중입니다...");
                    msg = ($"초기화 동작 중입니다...");
                    //NoticeStart(msg, Def.NoticeMediumFontSize, DefColor.LightGreen, 0);
                    SetUserStop(true);
                    homeTask.Start();
                    break;
            }

           // Program.log.Action($"Main Home End");
        }
        #endregion

        // 타워램프 실행부
        public static void DoTowerLamp()
        {
            //int index = Convert.ToInt32(TPM.Status);

            //switch (TOWERLAMP.Lamps[index, 0])
            //{
            //    case LampMode.Off: DIO.O[DO.TOWER_R] = false; break;
            //    case LampMode.On: DIO.O[DO.TOWER_R] = true; break;
            //    case LampMode.Blink: DIO.O[DO.TOWER_R] = FLICKER.IsOn; break;

            //}
            //switch (TOWERLAMP.Lamps[index, 1])
            //{
            //    case LampMode.Off: DIO.O[DO.TOWER_Y] = false; break;
            //    case LampMode.On: DIO.O[DO.TOWER_Y] = true; break;
            //    case LampMode.Blink: DIO.O[DO.TOWER_Y] = FLICKER.IsOn; break;
            //}
            //switch (TOWERLAMP.Lamps[index, 2])
            //{
            //    case LampMode.Off: DIO.O[DO.TOWER_G] = false; break;
            //    case LampMode.On: DIO.O[DO.TOWER_G] = true; break;
            //    case LampMode.Blink: DIO.O[DO.TOWER_G] = FLICKER.IsOn; break;
            //}

            //if (OPT.Buzzer && TOWERLAMP.StartBuzzer)
            //{
            //    DIO.O[DO.BUZZER1] = TOWERLAMP.Buzzers[index, 0];
            //    DIO.O[DO.BUZZER2] = TOWERLAMP.Buzzers[index, 1];
            //    DIO.O[DO.BUZZER3] = TOWERLAMP.Buzzers[index, 2];
            //}
            //else
            //{
            //    DIO.O[DO.BUZZER1] = false;
            //    DIO.O[DO.BUZZER2] = false;
            //    DIO.O[DO.BUZZER3] = false;
            //}
        }

        public void SetRecipeName(string groupName, string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "Recipe";
            }

            if (string.IsNullOrEmpty(groupName))
            {
                groupName = "Group";
            }

            SysOperator.Instance.defaultInfo.RecipeGroupName = groupName;
            SysOperator.Instance.defaultInfo.RecipeName = name;
            SysOperator.Instance.SaveDefaultInfo();

            OnRecipeChanged?.Invoke(groupName, name);
        }

        public void SaveDefaultInfo()
        {
            try
            {
                string strPath = DefPath.Config + @"\DefaultInfo.bin";

                // Directory 없을 때 생성
                if (!Directory.Exists(DefPath.Config))
                {
                    Directory.CreateDirectory(DefPath.Config);
                }

                BinaryFormatter bf = new BinaryFormatter();

                using (MemoryStream ms = new MemoryStream())
                {
                    bf.Serialize(ms, defaultInfo);
                    ms.Position = 0;
                    using (BinaryWriter BW = new BinaryWriter(File.Open(strPath, FileMode.Create))) 
                    { 
                        BW.Write(ms.ToArray()); 
                    }
                }

            }
            catch (Exception ex) 
            { 
                MessageBox.Show(ex.ToString()); 
            }
        }

        public void LoadDefaultInfo()
        {
            try
            {
                string strPath = DefPath.Config + @"\DefaultInfo.bin";

                if (!File.Exists(strPath) || !File.Exists(strPath)) 
                {
                    SaveDefaultInfo(); 
                }
                using (BinaryReader br = new BinaryReader(new FileStream(strPath, FileMode.Open)))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    defaultInfo = (DefaultInfo)bf.Deserialize(br.BaseStream);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
