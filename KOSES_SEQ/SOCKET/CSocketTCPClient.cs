﻿using KOSES_SEQ.MANAGER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.SOCKET
{
    public class CSocketTCPClient : IDisposable
    {
        public int MAX_BUFFER = 1500;

        public delegate void sendMsgDelegate(string strSend, int index);
        public delegate void recvMsgDelegate(string strRecv, int index);

        public event sendMsgDelegate OnSendMsg;
        public event recvMsgDelegate OnRecvMsg;

        public int _SocketIndex;
        private Socket _Socket;
        private EndPoint epLocal, epRemote;

        protected string _logCategory;


        public string _LastSendMsg;
        public string _LastRecvMsg;
        protected string strPreRecvMsg;


        public CSocketTCPClient(string elementName = "")
        {
            _Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            if(string.IsNullOrEmpty(elementName))
            {
                _logCategory = this.GetType().Name;
            }
            else
            {
                _logCategory = elementName;
            }
        }

        public void Dispose()
        {
            if(_Socket != null)
                _Socket.Dispose();
        }

        public string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }

        public virtual bool IsConnected()
        {
            return _Socket.Connected;
        }

        byte[] _buffer;

        /// <summary>
        /// UDP 연결
        /// </summary>
        /// <param name="strLocalIP : 내 IP"></param>
        /// <param name="strRemoteIP : 상대 IP"></param>
        /// <param name="nPort"></param>
        /// <returns></returns>
        public virtual bool Connect(string strLocalIP, string strRemoteIP, int nPort)
        {
            try
            {
                if (epLocal != null)
                    epLocal = null;

                if (epRemote != null)
                    epLocal = null;

                if (string.IsNullOrEmpty(strLocalIP) || string.IsNullOrEmpty(strRemoteIP))
                    return false;

                epLocal = new IPEndPoint(IPAddress.Parse(strLocalIP), nPort);
                _Socket.Bind(epLocal);

                epRemote = new IPEndPoint(IPAddress.Parse(strRemoteIP), nPort);
                _Socket.Connect(epRemote);
            }
            catch (Exception ex)
            {
                try
                {
                    Dispose();

                    _Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                    LogManager.ExLog(ex);
                    return false;
                }
                catch (Exception ex2)
                {
                    LogManager.ExLog(ex);
                    LogManager.ExLog(ex2);
                    return false;
                }
            }

            try
            {
                if (epLocal == null || epRemote == null)
                    return false;

                //Receive Start
                _buffer = new byte[MAX_BUFFER];
                Array.Clear(_buffer, 0, _buffer.Length);
                _Socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(MessageCallback), _buffer);

                LogManager.Log(enumLogLevel.COMM, "connect: " + strRemoteIP + " (Port:" + nPort.ToString() + ")", this.GetType().Name);

                return true;
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return false;
            }
            finally
            {

            }
        }

        //Receive
        protected void MessageCallback(IAsyncResult aResult)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                int size = _Socket.EndReceive(aResult);

                if (size > 0)
                {
                    byte[] receivedData = new byte[MAX_BUFFER];
                    receivedData = (byte[])aResult.AsyncState;
                    ASCIIEncoding eEncoding = new ASCIIEncoding();
                    string receivedMessage = eEncoding.GetString(receivedData);

                    receivedMessage = receivedMessage.Replace("\0", string.Empty);

                    strPreRecvMsg = receivedMessage;
                    _LastRecvMsg += strPreRecvMsg;

                    RaiseRecvMsg(receivedMessage, _SocketIndex);

                    string strLog = "Recv <= (" + _SocketIndex.ToString() + "," + epRemote.ToString() + "):" + receivedMessage.Trim('\0');
                    LogManager.Log(enumLogLevel.COMM, strLog, _logCategory);
                }

                _buffer = new byte[MAX_BUFFER];
                Array.Clear(_buffer, 0, _buffer.Length);
                _Socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(MessageCallback), _buffer);
            }
            catch (Exception ex)
            {
                try
                {
                    Dispose();

                    _Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                    LogManager.ExLog(ex);
                }
                catch(Exception ex2)
                {
                    LogManager.ExLog(ex);
                    LogManager.ExLog(ex2);
                }
            }
        }

        //Send Data
        public virtual void SendData(object eCmd)
        {
            try
            {
                string strMsg = eCmd.ToString();

                SendData(strMsg);

            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }
        byte[] _msg;
        //Send Data
        public virtual int SendData(string strMsg)
        {
            try
            {
                _LastSendMsg = strMsg;
                _LastRecvMsg = "";
                strPreRecvMsg = "";

                ASCIIEncoding enc = new ASCIIEncoding();

                _msg = new byte[MAX_BUFFER];
                _msg = enc.GetBytes(strMsg);

                string strLog = "Send => (" + _SocketIndex.ToString() + "," + epRemote.ToString() + "):" + strMsg;
                LogManager.Log(enumLogLevel.COMM, strLog, _logCategory);

                _Socket.Send(_msg);
                _LastSendMsg = strMsg;
                RaiseSendMsg(strMsg, _SocketIndex);
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return CReturnCode.Exception;
            }

            return CReturnCode.Success;
        }

        protected void RaiseSendMsg(string strMsg, int index)
        {
            if(OnSendMsg != null)
            {
                OnSendMsg(strMsg, index);
            }
        }

        protected void RaiseRecvMsg(string strRecv, int index)
        {
            if(OnRecvMsg != null)
            {
                OnRecvMsg(strRecv, index);
            }
        }
    }
}
