﻿using KOSES_SEQ.MANAGER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KOSES_SEQ.SOCKET
{
    public class CSocketTCPServer : IDisposable
    {
        private readonly int MAX_BUFFER = 1500;

        public delegate void recvMsgDelegate(string strRecv, int index);
        public delegate void sendMsgDelegate(string strSend, int index);

        public event recvMsgDelegate OnRecievedMessage;
        public event sendMsgDelegate OnSendMsg;


        bool _ListenerRun;
        protected string _logCategory;
        protected string strPreRecvMsg;

        public string _LastRecievedMassage;
        public string _LastSendMsg;
        public string _LastRecvMsg;

        NetworkStream _ClientStream; // Client로 전송되는 값 받아오는 객체
        TcpListener _TcpListener;
        Thread _ListenerThread;
        EndPoint epLocal;//, epRemote;


        public CSocketTCPServer(string elementName = "")
        {
            if(string.IsNullOrEmpty(elementName))
            {
                _logCategory = this.GetType().Name;
            }
            else
            {
                _logCategory = elementName;
            }
        }

        public void Dispose()
        {
            if (_TcpListener != null)
            {
                _TcpListener.Stop();
                _TcpListener.Server.Dispose();
            }
        }

        public string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }

        public virtual bool IsConnected()
        {
            return _TcpListener.Server.Connected;
        }

        /// <summary>
        /// UDP 연결
        /// </summary>
        /// <param name="strRemoteIP : 상대 IP"></param>
        /// <param name="nPort"></param>
        /// <returns></returns>
        public virtual bool Connect(string strLocalIP, int nPort)
        {
            try
            {
                if (epLocal != null)
                    epLocal = null;

                if (string.IsNullOrEmpty(strLocalIP))
                    return false;

                _TcpListener = new TcpListener(IPAddress.Parse(strLocalIP), nPort);
                StartListener();
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return false;
            }

            return true;
        }

        protected void StartListener()
        {
            try
            {
                _ListenerRun = true;
                _TcpListener.Start();

                _ListenerThread = new Thread(ListenerThread);
                _ListenerThread.IsBackground = true;
                _ListenerThread.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private void ListenerThread()// Listner Thread 정의
        {
            try
            {
                while (_ListenerRun)
                {
                    TcpClient client = this._TcpListener.AcceptTcpClient(); // 클라이언트 접속

                    Thread startClientThread = new Thread(new ParameterizedThreadStart(HandleClientComm)); // Client로 부터 접속
                    startClientThread.IsBackground = true;
                    startClientThread.Start(client); // 시작.
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }
        }

        // 클라이언트 Thread 정의
        private void HandleClientComm(object client)
        {
            try
            {
                TcpClient tcpClient = (TcpClient)client; // tcp Client 생성
                _ClientStream = tcpClient.GetStream(); // Client로 부터 Stream 받아오기
                if (_ClientStream != null)
                {
                    byte[] message = new byte[MAX_BUFFER]; // Message Byte 생성

                    while (_ListenerRun)
                    {
                        Array.Clear(message, 0, message.Length);

                        _ClientStream.Read(message, 0, MAX_BUFFER);
                        ASCIIEncoding eEncoding = new ASCIIEncoding();
                        string receivedMessage = eEncoding.GetString(message);
                        if (!string.IsNullOrEmpty(receivedMessage))
                        {
                            _LastRecievedMassage = receivedMessage;
                            _LastRecievedMassage = _LastRecievedMassage.Replace("\0", string.Empty);
                            Console.WriteLine(_LastRecievedMassage);

                            RaiseRecvMsg(_LastRecievedMassage, 0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //Send Data
        public virtual int SendData(string strMsg)
        {
            try
            {
                _LastSendMsg = strMsg;
                _LastRecvMsg = string.Empty;
                strPreRecvMsg = string.Empty;

                ASCIIEncoding enc = new ASCIIEncoding();

                byte[] message = enc.GetBytes(strMsg);

                string strLog = "Send => " + strMsg;
                LogManager.Log(enumLogLevel.COMM, strLog, _logCategory);

                if (_ClientStream != null)
                {
                    _ClientStream.Write(message, 0, message.Length);
                }

                _LastSendMsg = strMsg;
                RaiseSendMsg(strMsg, 0);
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return CReturnCode.Exception;
            }

            return CReturnCode.Success;
        }

        protected void RaiseSendMsg(string strMsg, int index)
        {
            if(OnSendMsg != null)
            {
                OnSendMsg(strMsg, index);
            }
        }

        protected void RaiseRecvMsg(string strRecv, int index)
        {
            if(OnRecievedMessage != null)
            {
                OnRecievedMessage(strRecv, index);
            }
        }
    }
}
