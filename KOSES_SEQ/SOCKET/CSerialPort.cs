﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.MANAGER;

namespace KOSES_SEQ.SOCKET
{
    public class CSerialPort : IDisposable
    {
        readonly int _RecieveDelay = 30;

        SerialPort _SerialPort = null;

        #region Comport 설정 값

        string _strPortName = string.Empty;
        int _SerialBaudRate = 9600;
        int _SerialDataBits = 8;
        StopBits _SerialStopBits    = StopBits.None;
        Parity _SerialParity        = Parity.None;

        bool _SerialRtsEnable = false;
        bool _SerialDtrEnable = false;

        #endregion

        bool _IsReceivedMessage = false;

        string _RcvMessage = string.Empty;

        public string RcvMessage { get => _RcvMessage; }
        public string PortName { get => this._strPortName; set => this._strPortName = value; }

        public bool IsReceivedMessage { get => _IsReceivedMessage; }

        public CSerialPort()
        {
        }

        public void Dispose()
        {
            _SerialPort?.Dispose();
        }

        public int SetSerialPort(string strPortName, 
                                int iBaudRate = 9600, 
                                int iDataBits = 8, 
                                StopBits stopBits = StopBits.One, 
                                Parity parity = Parity.None,
                                bool rtsEnable = false,
                                bool dtrEnable = false)
        {
            int iRet = CReturnCode.Success;
            try
            {
                _strPortName        = strPortName;
                _SerialBaudRate     = iBaudRate;
                _SerialDataBits     = iDataBits;
                _SerialStopBits     = stopBits;
                _SerialParity       = parity;
                _SerialRtsEnable    = rtsEnable;
                _SerialDtrEnable    = dtrEnable;
            }
            catch(Exception ex)
            {
                LogManager.ExLog(ex);
                return CReturnCode.Exception;
            }

            return iRet;
        }

        /// <summary>
        /// Data 수신
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void _SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(_RecieveDelay);
            _RcvMessage = _SerialPort.ReadExisting();

            LogManager.Log(enumLogLevel.COMM, $"Recv <= {_RcvMessage.Replace("\r\n", "\\r\\n")}", _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);

            _IsReceivedMessage = true;
        }

        /// <summary>
        /// 멤버 변수로 Port Open
        /// SetSerialPort(..) 실행 후 OpenPort()하도록 할것
        /// </summary>
        public void OpenPort()
        {
            LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Open start", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            try
            {
                _SerialPort = new SerialPort(_strPortName, _SerialBaudRate, _SerialParity, _SerialDataBits, _SerialStopBits)
                {
                    RtsEnable = _SerialRtsEnable,
                    DtrEnable = _SerialDtrEnable
                };

                _SerialPort.Open();

                _SerialPort.DataReceived -= _SerialPort_DataReceived;
                _SerialPort.DataReceived += _SerialPort_DataReceived;

                LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Open", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            }
            catch(Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// PortName만으로 Port Open
        /// </summary>
        public void OpenPort(string strPortName)
        {
            LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Open start", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            try
            {
                _SerialPort = new SerialPort(strPortName)
                {
                    RtsEnable = _SerialRtsEnable,
                    DtrEnable = _SerialDtrEnable
                };

                _SerialPort.Open();

                _SerialPort.DataReceived -= _SerialPort_DataReceived;
                _SerialPort.DataReceived += _SerialPort_DataReceived;

                LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Open", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// PortName과 BaudRate만으로 Port Open
        /// 추가가 필요하면 함수 더 만들면 될 듯
        /// SetSerialPort(...) 후 PortOpen() 추천...
        /// </summary>
        public int OpenPort(string strPortName, int iBaudRate)
        {
            LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Open start", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            try
            {
                _SerialPort = new SerialPort(strPortName, iBaudRate)
                {
                    RtsEnable = _SerialRtsEnable,
                    DtrEnable = _SerialDtrEnable
                };

                _SerialPort.Open();

                _SerialPort.DataReceived -= _SerialPort_DataReceived;
                _SerialPort.DataReceived += _SerialPort_DataReceived;

                LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Open", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return CReturnCode.Exception;
            }

            return CReturnCode.Success;
        }

        /// <summary>
        /// Port Close
        /// </summary>
        public int ClosePort()
        {
            LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Close Start", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);

            if (_SerialPort != null)
            {
                if(_SerialPort.IsOpen)
                    _SerialPort.Close();

                Dispose();

                _SerialPort = null;
                LogManager.Log(enumLogLevel.COMM, string.Format("Serial {0} Port Closed", _strPortName), _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);
            }

            return CReturnCode.Success;
        }

        /// <summary>
        /// Message 송신
        /// </summary>
        /// <param name="msg">Message</param>
        /// <param name="bAsync">비동기 여부</param>
        /// <param name="timeOut">TimeOut 시간 (단위 : ms)</param>
        /// <returns></returns>
        public virtual int SendMsg(string msg, bool bAsync = false, int timeOut = 3000)
        {
            _IsReceivedMessage = false;

            try
            {
                if(_SerialPort == null)
                {
                    OpenPort(this._strPortName);
                    Thread.Sleep(500);
                }
                if(!_SerialPort.IsOpen)
                {
                    _SerialPort.Open();
                    Thread.Sleep(500);
                }

                _SerialPort.Write(msg);

                LogManager.Log(enumLogLevel.COMM, $"Send => {msg.Replace("\r\n", "\\r\\n")}", _strPortName != string.Empty ? this.GetType().Name + _strPortName : this.GetType().Name);


                if (!bAsync)
                {
                    int iTick = Environment.TickCount;
                    while (!_IsReceivedMessage)
                    {
                        if (CTimeOut.IsTimeOut(iTick, timeOut))
                            return CReturnCode.TimeOut;

                        Thread.Sleep(10);
                    }
                }
            }
            catch(Exception ex)
            {
                _SerialPort?.Dispose();
                LogManager.ExLog(ex);
                return CReturnCode.Exception;
            }

            return CReturnCode.Success;
        }

    }
}
