﻿using KOSES_SEQ.MANAGER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.SOCKET
{

    public static class CSocketMSGSender
    {
        public delegate void deleSendMsg(object oSender, string strMSG);
        public delegate void deleRecvMsg(object oSender, string strMSG);

        public static event deleSendMsg OndeleSendMsg;
        public static event deleRecvMsg OndeleRecvMsg;

        public static void RaiseOndeleSendMsg(object oSender, string strMsg)
        {
            if (OndeleSendMsg != null)
                OndeleSendMsg(oSender, strMsg);
        }

        public static void RaiseOndeleRecvMsg(object oSender, string strMsg)
        {
            if (OndeleRecvMsg != null)
                OndeleRecvMsg(oSender, strMsg);
        }

    }
    public class CSocketUDP : IDisposable
    {
        public int index;
        private Socket sck;
        private EndPoint epLocal, epRemote;

        protected string _logCategory;

        public delegate void sendMsgDelegate(string strSend, int index);

        public event sendMsgDelegate sendMsg;

        public delegate void recvMsgDelegate(string strRecv, int index);

        public event recvMsgDelegate recvMsg;

        public string strLastSendMsg = "";
        public string strLastRecvMsg = "";
        protected string strPreRecvMsg = "";

        protected string m_strReceiveLog = "";
        protected string m_strSendLog = "";

        //public bool isWaitReply = false; //메세지 Send 후 true, Receive 후 false

        public CSocketUDP(string unitName = "")
        {
            sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            if(string.IsNullOrEmpty(unitName))
            {
                _logCategory = this.GetType().Name;
            }
            else
            {
                _logCategory = unitName;
            }
        }

        public void Dispose()
        {
            if(sck != null)
                sck.Dispose();
        }

        public string GetLocalIP()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }

        public virtual bool IsConnected()
        {
            return sck.Connected;
        }

        byte[] _buffer;

        /// <summary>
        /// UDP 연결
        /// </summary>
        /// <param name="strLocalIP : 내 IP"></param>
        /// <param name="strRemoteIP : 상대 IP"></param>
        /// <param name="nPort"></param>
        /// <returns></returns>
        public virtual bool Connect(string strLocalIP, string strRemoteIP, int nPort)
        {
            try
            {
                if (epLocal != null)
                    epLocal = null;

                if (epRemote != null)
                    epLocal = null;

                if (string.IsNullOrEmpty(strLocalIP) || string.IsNullOrEmpty(strRemoteIP))
                    return false;

                epLocal = new IPEndPoint(IPAddress.Parse(strLocalIP), nPort);
                sck.Bind(epLocal);

                epRemote = new IPEndPoint(IPAddress.Parse(strRemoteIP), nPort);
                sck.Connect(epRemote);
            }
            catch (Exception ex)
            {
                try
                {
                    Dispose();
                    //if (sck != null && sck.IsBound)
                    //    sck.Disconnect(false);

                    sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                    LogManager.ExLog(ex);
                    return false;
                }
                catch (Exception ex2)
                {
                    LogManager.ExLog(ex);
                    LogManager.ExLog(ex2);
                    return false;
                }
            }

            try
            {
                if (epLocal == null || epRemote == null)
                    return false;
                //Receive Start
                /*byte[] */_buffer = new byte[1500];
                sck.BeginReceiveFrom(_buffer, 0, _buffer.Length, SocketFlags.None, ref epRemote, new AsyncCallback(MessageCallback), _buffer);

                LogManager.Log(enumLogLevel.COMM, "connect: " + strRemoteIP + " (Port:" + nPort.ToString() + ")", this.GetType().Name);

                return true;
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return false;
            }
            finally
            {

            }
        }

        //Receive
        protected void MessageCallback(IAsyncResult aResult)
        {
            try
            {
                //yspark Exception relation ... existing connection was forcibly closed by the remote host 
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                int size = sck.EndReceiveFrom(aResult, ref epRemote);

                if (size > 0)
                {
                    byte[] receivedData = new byte[1500];
                    receivedData = (byte[])aResult.AsyncState;
                    ASCIIEncoding eEncoding = new ASCIIEncoding();
                    string receivedMessage = eEncoding.GetString(receivedData);
                    //this.Invoke((MethodInvoker)delegate()
                    //{
                    //    listMessage.Items.Add("Friend:" + receivedMessage);
                    //});

                    receivedMessage = receivedMessage.Replace("\0", string.Empty);

                    strPreRecvMsg = receivedMessage;
                    strLastRecvMsg += strPreRecvMsg;

                    recvMsg(receivedMessage, index);

                    string strLog = "Recv <= (" + index.ToString() + "," + epRemote.ToString() + "):" + receivedMessage.Trim('\0');
                    //if (strLog != m_strReceiveLog)
                    {
                        //FrmMain.logAdd(DF.COMM_LOG, strLog);
                        LogManager.Log(enumLogLevel.COMM, strLog, _logCategory);
                        m_strReceiveLog = strLog;
                    }
                    //isWaitReply = false;
                }
                /*byte[] */_buffer = new byte[1500];
                sck.BeginReceiveFrom(_buffer, 0, _buffer.Length, SocketFlags.None, ref epRemote, new AsyncCallback(MessageCallback), _buffer);
            }
            catch (Exception ex)
            {
                try
                {
                    Dispose();

                    sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                    //sck.Disconnect(false);
                    LogManager.ExLog(ex);
                }
                catch(Exception ex2)
                {
                    LogManager.ExLog(ex);
                    LogManager.ExLog(ex2);
                }
            }
        }

        //Send Data
        public virtual void SendData(object eCmd)
        {
            try
            {
                string strMsg = eCmd.ToString();

                SendData(strMsg);

            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }
        byte[] _msg;
        //Send Data
        public virtual void SendData(string strMsg)
        {
            try
            {
                //For main form receive message log clear
                //---------------------------------------
                recvMsg("", index);
                //---------------------------------------

                strLastSendMsg = strMsg;
                strLastRecvMsg = "";
                strPreRecvMsg = "";

                //if (!isWaitReply) return; //sendData를 호출하는 측에서 true 후 호출할것!!!
                System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                /*byte[] */_msg = new byte[1500];
                _msg = enc.GetBytes(strMsg);

                string strLog = "Send => (" + index.ToString() + "," + epRemote.ToString() + "):" + strMsg;
                LogManager.Log(enumLogLevel.COMM, strLog, _logCategory);
                m_strSendLog = strLog;

                sck.Send(_msg);
                strLastSendMsg = strMsg;
                sendMsg(strMsg, index);
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }
    }
}
