﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public class KosesDataGridView : DataGridView
    {
        Color disabledBackColor = SystemColors.Control;
        [Category("Setting"), Description("DisabledBackColor")]
        public Color DisabledBackColor
        {
            get { return this.disabledBackColor; }

            set
            {
                this.disabledBackColor = value;
                this.Invalidate();
            }
        }
        Color disabledTextColor = SystemColors.GrayText;
        [Category("Setting"), Description("DisabledTextColor")]
        public Color DisabledTextColor
        {
            get { return this.disabledTextColor; }

            set
            {
                this.disabledTextColor = value;
                this.Invalidate();
            }
        }
        Color TempBackColor;
        Color TempTextColor;

        public KosesDataGridView()
        {
            this.EnableHeadersVisualStyles = false;

            base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);

            TempBackColor = this.DefaultCellStyle.BackColor;
            TempTextColor = this.DefaultCellStyle.ForeColor;

            //DisEnable일때 Cell의 색을 전부 특정색으로 바꿔주기 위해
            this.EnabledChanged += (sender, args) =>
            {

                if (!this.Enabled)
                {
                    this.DefaultCellStyle.BackColor = this.disabledBackColor;
                    this.DefaultCellStyle.ForeColor = this.disabledTextColor;
                    this.CurrentCell = null;
                }
                else
                {
                    this.DefaultCellStyle.BackColor = TempBackColor;
                    this.DefaultCellStyle.ForeColor = TempTextColor;
                }
                Invalidate();
            };
        }
    }
}
