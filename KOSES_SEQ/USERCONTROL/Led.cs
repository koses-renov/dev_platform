﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace KosesControl.Controls
{
	public partial class Led : Control
	{
		#region Public and Private Members

		private Color _color;
		private bool _on = true;
		private Color _reflectionColor = Color.FromArgb(180, 255, 255, 255);
		private Color[] _surroundColor = new Color[] { Color.FromArgb(0, 255, 255, 255) };
		private Timer _timer = new Timer();

        public enum Shape { CIRCLE, RECTENGLE }
		Shape CurLedShape;
		[Category("Shape"), Description("Led Shape")]
		public Shape LedShape
		{
			get { return this.CurLedShape; }

			set
			{
				this.CurLedShape = value;
				this.Invalidate();
			}
		}

		int radius;
		[Category("Shape"), Description("Shape Radius")]
		public int Radius
		{
			get { return radius; }
			set
			{
				radius = value;
				this.Invalidate();
			}
		}

		[Category("Shape"), Description("Led Color")]
		[DefaultValue(typeof(Color), "153, 255, 54")]
		public Color Color {
			get { return _color; }
			set
			{ 
				_color = value;
				this.DarkColor = ControlPaint.Dark(_color, 0.2f);
				this.DarkDarkColor = ControlPaint.Dark(_color, 0.7f);
				this.Invalidate();	// Redraw the control
			} 
		}
		
		public Color DarkColor { get; protected set; }
		
		public Color DarkDarkColor { get; protected set; }

		[Category("Status"), Description("Led On/Off")]
		public bool On { 
			get { return _on; } 
			set { _on = value; this.Invalidate(); }
		}

		#endregion

		#region Constructor

		public Led() {
			SetStyle(ControlStyles.DoubleBuffer
			| ControlStyles.AllPaintingInWmPaint
			| ControlStyles.ResizeRedraw
			| ControlStyles.UserPaint
			| ControlStyles.SupportsTransparentBackColor, true);
			this.Color = Color.FromArgb(255, 153, 255, 54);
			this.Size = new Size(23, 23);
			this.BackColor = Color.Transparent;
			this.Padding = new Padding(0, 0, 0, 0);
			this.Margin = new Padding(0, 0, 0, 0);
			_timer.Tick += new EventHandler(
				(object sender, EventArgs e) => { this.On = !this.On; }
			);
		}
		
		#endregion

		#region Methods

		protected override void OnPaint(PaintEventArgs e)
		{
            try
            {
                if (Parent != null)
                {
                    Bitmap behind = new Bitmap(Parent.Width, Parent.Height);
                    //Bitmap behind = new Bitmap(this.Width, this.Height);
                    foreach (Control c in Parent.Controls)
                        if (c.Bounds.IntersectsWith(this.Bounds) & c != this)
                            c.DrawToBitmap(behind, c.Bounds);
                    e.Graphics.DrawImage(behind, -Left, -Top);
                    behind.Dispose();
                }
                Bitmap offScreenBmp = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);
                using (Graphics g = Graphics.FromImage(offScreenBmp))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    // Draw the control
                    if (this.CurLedShape == Shape.CIRCLE)
                        drawControl_Circle(g, this.On);
                    else if (this.CurLedShape == Shape.RECTENGLE)
                        drawControl_Rectangle(g, this.On);

                    // Draw the image to the screen
                    e.Graphics.DrawImageUnscaled(offScreenBmp, 0, 0);
                }
            }
            catch (Exception ex)
            {

            }
		}

		/// <summary>
		/// Renders the control to an image
		/// </summary>
		private void drawControl_Circle(Graphics g, bool on) {
			// Is the bulb on or off
			Color lightColor = (on)? this.Color : Color.FromArgb(150, this.DarkColor);
			Color darkColor = (on) ? this.DarkColor : this.DarkDarkColor;
			
			// Calculate the dimensions of the bulb
			int width = this.Width - (this.Padding.Left + this.Padding.Right);
			int height = this.Height - (this.Padding.Top + this.Padding.Bottom);
			// Diameter is the lesser of width and height
			int diameter = Math.Min(width, height);
			// Subtract 1 pixel so ellipse doesn't get cut off
			diameter = Math.Max(diameter - 1, 1);

			// Draw the background ellipse
			var rectangle = new Rectangle(this.Padding.Left + (width / 2 - diameter / 2), this.Padding.Top + (height / 2 - diameter / 2), diameter, diameter);
			g.FillEllipse(new SolidBrush(darkColor), rectangle);

			// Draw the glow gradient
			var path = new GraphicsPath();
			path.AddEllipse(rectangle);
			var pathBrush = new PathGradientBrush(path);
			pathBrush.CenterColor = lightColor;
			pathBrush.SurroundColors = new Color[] { Color.FromArgb(0, lightColor) };
			g.FillEllipse(pathBrush, rectangle);

			// Draw the white reflection gradient
			var offset = Convert.ToInt32(diameter * .15F);
			var diameter1 = Convert.ToInt32(rectangle.Width * .8F);
			var whiteRect = new Rectangle(rectangle.X - offset, rectangle.Y - offset, diameter1, diameter1);
			var path1 = new GraphicsPath();
			path1.AddEllipse(whiteRect);
			var pathBrush1 = new PathGradientBrush(path);
			pathBrush1.CenterColor = _reflectionColor;
			pathBrush1.SurroundColors = _surroundColor;
			g.FillEllipse(pathBrush1, whiteRect);

			// Draw the border
			g.SetClip(this.ClientRectangle);
			if (this.On) g.DrawEllipse(new Pen(Color.FromArgb(85, Color.Black),1F), rectangle);
		}
		private void drawControl_Rectangle(Graphics g, bool on)
		{
			Color lightColor = (on) ? this.Color : Color.FromArgb(150, this.DarkColor);
			Color darkColor = (on) ? this.DarkColor : this.DarkDarkColor;

			// Draw the background ellipse
			var rectangle = new Rectangle(this.Padding.Left, this.Padding.Top, this.Width - this.Padding.Right - this.Padding.Left - 1, this.Height - this.Padding.Bottom - this.Padding.Top - 1);
			g.FillPath(new SolidBrush(darkColor), GetRoundedRectanglePath(rectangle, radius));

			// Draw the glow gradient
			var path = new GraphicsPath();
			path.AddEllipse(new Rectangle(-this.Width / 2, -this.Height / 2, this.Width * 2, this.Height * 2));
			var pathBrush = new PathGradientBrush(path);
			pathBrush.CenterColor = lightColor;
			pathBrush.SetSigmaBellShape(1f, 1f);

			pathBrush.SurroundColors = new Color[] { Color.FromArgb(0, lightColor) };
			g.FillPath(pathBrush, GetRoundedRectanglePath(rectangle, radius));

			// Draw the border
			path = GetRoundedRectanglePath(rectangle, radius);
			path.AddPath(GetRoundedRectanglePath(rectangle, radius), true);
			g.SetClip(path);
			if (this.On) g.DrawRectangle(new Pen(Color.FromArgb(85, Color.Black), 1F), rectangle);
		}

		public GraphicsPath GetRoundedRectanglePath(Rectangle rectangle, int radius)
		{
			GraphicsPath gp = new GraphicsPath();
			if (radius <= 0)
				radius = 1;

			int diameter = 2 * radius;

			if (diameter > this.Height)
				diameter = this.Height;

			if (diameter > this.Width)
				diameter = this.Width;

			gp.AddArc(rectangle.X, rectangle.Y, diameter, diameter, 180, 90);
			gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y, diameter, diameter, 270, 90);
			gp.AddArc(rectangle.X + rectangle.Width - diameter, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 0, 90);
			gp.AddArc(rectangle.X, rectangle.Y + rectangle.Height - diameter, diameter, diameter, 90, 90);
			gp.CloseFigure();

			return gp;
		}

		/// <summary>
		/// Causes the Led to start blinking
		/// </summary>
		/// <param name="milliseconds">Number of milliseconds to blink for. 0 stops blinking</param>
		public void Blink(int milliseconds)
		{
			if (milliseconds > 0) {
				this.On = true;
				_timer.Interval = milliseconds;
				_timer.Enabled = true;
			}
			else {
				_timer.Enabled = false;
				this.On = false;
			}
		}
        #endregion
    }
}
