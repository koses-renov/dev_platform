﻿
namespace KOSES_SEQ.USERCONTROL
{
    partial class KosesTenkeyItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSequencName = new System.Windows.Forms.Label();
            this.btnStep = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.lblTenkeyNo = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.615385F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.69231F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.34615F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.34616F));
            this.tableLayoutPanel1.Controls.Add(this.lblSequencName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnStep, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnHome, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTenkeyNo, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(570, 60);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblSequencName
            // 
            this.lblSequencName.AutoSize = true;
            this.lblSequencName.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lblSequencName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSequencName.Location = new System.Drawing.Point(59, 4);
            this.lblSequencName.Margin = new System.Windows.Forms.Padding(3);
            this.lblSequencName.Name = "lblSequencName";
            this.lblSequencName.Size = new System.Drawing.Size(319, 52);
            this.lblSequencName.TabIndex = 1;
            this.lblSequencName.Text = "SEQUENCE NAME";
            this.lblSequencName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStep
            // 
            this.btnStep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStep.Location = new System.Drawing.Point(385, 4);
            this.btnStep.Name = "btnStep";
            this.btnStep.Size = new System.Drawing.Size(86, 52);
            this.btnStep.TabIndex = 2;
            this.btnStep.Text = "STEP *";
            this.btnStep.UseVisualStyleBackColor = true;
            this.btnStep.Click += new System.EventHandler(this.btnStep_Click);
            // 
            // btnHome
            // 
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnHome.Location = new System.Drawing.Point(478, 4);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(88, 52);
            this.btnHome.TabIndex = 3;
            this.btnHome.Text = "HOME #";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // lblTenkeyNo
            // 
            this.lblTenkeyNo.AutoSize = true;
            this.lblTenkeyNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTenkeyNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTenkeyNo.Location = new System.Drawing.Point(4, 4);
            this.lblTenkeyNo.Margin = new System.Windows.Forms.Padding(3);
            this.lblTenkeyNo.Name = "lblTenkeyNo";
            this.lblTenkeyNo.Size = new System.Drawing.Size(48, 52);
            this.lblTenkeyNo.TabIndex = 4;
            this.lblTenkeyNo.Text = "00";
            this.lblTenkeyNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KosesTenkeyItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "KosesTenkeyItem";
            this.Size = new System.Drawing.Size(570, 60);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblSequencName;
        private System.Windows.Forms.Button btnStep;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Label lblTenkeyNo;
    }
}
