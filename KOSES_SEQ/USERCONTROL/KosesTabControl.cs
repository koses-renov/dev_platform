﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public class KosesTabControl : TabControl
    {
        public KosesTabControl()
        {
            Multiline = true;
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);

            var page = e.Control as TabPage;
            if (page != null)
            {
                page.UseVisualStyleBackColor = false;
                page.BackColor = Color.White;
            }
        }

        [Browsable(false)]
        public int Depth { get; set; }

        protected override void WndProc(ref Message m)
        {
            //if (m.Msg == 0x1328 && !DesignMode) m.Result = (IntPtr)1;
            //else base.WndProc(ref m);
           
            base.WndProc(ref m);
        }
    }
}