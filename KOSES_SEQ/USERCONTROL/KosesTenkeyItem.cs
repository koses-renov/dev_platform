﻿using KOSES_SEQ.OP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.USERCONTROL
{
    public partial class KosesTenkeyItem : UserControl
    {
        [Category("Custom")]
        public int TenkeyNo
        {
            get 
            {
                if (int.TryParse(lblTenkeyNo.Text, out int no))
                    return no;

                return 0;
            }
            set
            {
                lblTenkeyNo.Text = value.ToString("D2");
            }
        }

        [Category("Custom")]
        public string SequenceName
        {
            get { return lblSequencName.Text; }
            set
            {
                lblSequencName.Text = value;
            }
        }

        bool _UseStep;
        [Category("Custom")]
        public bool UseStep
        {
            get 
            {
                return _UseStep; 
            }
            set
            {
                _UseStep = value;
                btnStep.Visible = value;
            }
        }

        [Category("Custom")]
        public string StepText
        {
            get { return btnStep.Text; }
            set
            {
                btnStep.Text = value;
            }
        }

        bool _UseHome;
        [Category("Custom")]
        public bool UseHome
        {
            get { return _UseHome; }
            set
            {
                _UseHome = value;
                btnHome.Visible = value;
            }

        }

        [Category("Custom")]
        public string HomeText
        {
            get { return btnHome.Text; }
            set
            {
                btnHome.Text = value;
            }
        }

        public KosesTenkeyItem()
        {
            InitializeComponent();
        }

        private void btnStep_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.Tenkey.ExternRequestStep(TenkeyNo);
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.Tenkey.ExternRequestHome(TenkeyNo);
        }
    }
}
