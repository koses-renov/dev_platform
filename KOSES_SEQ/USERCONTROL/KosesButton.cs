﻿using KosesControl.Animations;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;
using static KosesControl.FontManager;

namespace KosesControl.Controls
{
    public partial class KosesButton : Button
    {
        public enum ButtonType
        {
            TEXT,
            OUTLINED,
            CONTAINED
        }

        private ButtonType typeButton;
        [Category("Status"), Description("Button Type")]
        public ButtonType TypeButton
        {
            get { return typeButton; }
            set { typeButton = value; Invalidate(); }
        }

        private bool highEmphasis;
        [Category("Status"), Description("Effect On/Off")]
        public bool HighEmphasis
        {
            get { return highEmphasis; }
            set { highEmphasis = value; Invalidate(); }
        }

        private bool drawShadows;
        [Category("Status"), Description("Shadow On/Off")]
        public bool DrawShadows
        {
            get { return drawShadows; }
            set { drawShadows = value; Invalidate(); }
        }

        bool selected;
        [Category("Status"), Description("Selected")]
        public bool Selected
        {
            get { return selected; }
            set 
            {
                if(selected != value)
                {
                    selected = value;
                    this.BackColor = selected ? selectedColor : deselectedColor;  
                }
            }
        }


        private float fontSize;
        [Category("Font"), Description("Font Size")]
        public float FontSize
        {
            get { return fontSize; }
            set 
            { 
                fontSize = value;
                _textSize = CreateGraphics().MeasureString(this.Text, FontManager.GetFontByNotoSans(this.fontSize, this.Bold));
                if (AutoSize)
                {
                    Refresh();
                }
                this.Invalidate(); 
            }
        }
        private bool bold;
        [Category("Font"), Description("Bold On/Off")]
        public bool Bold
        {
            get { return bold; }
            set 
            { 
                bold = value;
                _textSize = CreateGraphics().MeasureString(this.Text, FontManager.GetFontByNotoSans(this.FontSize, this.bold));
                if (AutoSize)
                {
                    Refresh();
                }
                this.Invalidate();
            }
        }

        Color bolderColor;
        [Category("Color"), Description("BolderColor")]
        public Color BolderColor
        {
            get { return bolderColor; }
            set
            {
                bolderColor = value;
                this.Invalidate();
            }
        }

        Color disabledBolderColor;
        [Category("Color"), Description("DisabledBolderColor")]
        public Color DisabledBolderColor
        {
            get { return disabledBolderColor; }
            set
            {
                disabledBolderColor = value;
                this.Invalidate();
            }
        }

        Color selectedColor = Color.FromArgb(27, 48, 70);
        [Category("Color"), Description("Selected Color")]
        public Color SelectedColor
        {
            get { return selectedColor; }
            set
            {
                selectedColor = value;
                this.Invalidate();
            }
        }

        Color deselectedColor = Color.FromArgb(49, 87, 126);
        [Category("Color"), Description("DeSelected Color")]
        public Color DeselectedColor
        {
            get { return deselectedColor; }
            set
            {
                deselectedColor = value;
                ColorScheme = new ColorManager(deselectedColor, ControlPaint.Dark(deselectedColor), ControlPaint.Light(deselectedColor, 1f), TextShade.WHITE);
                this.Invalidate();
            }
        }
        Color DisabledbackgroundColor;
        [Category("Disable"), Description("DisableBackgroundColor")]
        public Color DisabledBackgroundColor
        {
            get { return this.DisabledbackgroundColor; }

            set
            {
                this.DisabledbackgroundColor = value;
                this.Invalidate();
            }
        }
        Color disabledTextColor = Color.Gray;
        [Category("Disable"), Description("DisableTextColor")]
        public Color DisabledTextColor
        {
            get { return this.disabledTextColor; }

            set
            {
                this.disabledTextColor = value;
                this.Invalidate();
            }
        }
        int mouseOnOpacity = 50;
        [Category("MouseOn"), Description("Opacity")]
        public int MouseOnOpacity
        {
            get { return mouseOnOpacity; }
            set
            {
                mouseOnOpacity = value;
            }
        }


        protected override void InitLayout()
        {
            base.InitLayout();
            Invalidate();
            LocationChanged += (sender, e) => { if (DrawShadows) Parent?.Invalidate(); };
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if (drawShadows && Parent != null) AddShadowPaintEvent(Parent, drawShadowOnParent);
            if (_oldParent != null) RemoveShadowPaintEvent(_oldParent, drawShadowOnParent);
            _oldParent = Parent;
        }

        private Control _oldParent;

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (Parent == null) return;
            if (Visible)
                AddShadowPaintEvent(Parent, drawShadowOnParent);
            else
                RemoveShadowPaintEvent(Parent, drawShadowOnParent);
        }

        private bool _shadowDrawEventSubscribed = false;

        private void AddShadowPaintEvent(Control control, PaintEventHandler shadowPaintEvent)
        {
            if (_shadowDrawEventSubscribed) return;
            control.Paint += shadowPaintEvent;
            control.Invalidate();
            _shadowDrawEventSubscribed = true;
        }

        private void RemoveShadowPaintEvent(Control control, PaintEventHandler shadowPaintEvent)
        {
            if (!_shadowDrawEventSubscribed) return;
            control.Paint -= shadowPaintEvent;
            control.Invalidate();
            _shadowDrawEventSubscribed = false;
        }

        private readonly AnimationManager _hoverAnimationManager = null;
        private readonly AnimationManager _animationManager = null;

        /// <summary>
        /// Defines the _textSize
        /// </summary>
        private SizeF _textSize;

        /// <summary>
        /// Defines the _icon
        /// </summary>
        private Image _icon;
        private GraphicsUnit graphicsUnit;

        /// <summary>
        /// Gets or sets the Icon
        /// </summary>
        public Image Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                if (AutoSize)
                {
                    Refresh();
                }

                Invalidate();
            }
        }
        private Point iconLocation = new Point(-10, 0);
        public Point IconLocation
        {
            get { return iconLocation; }
            set
            {
                iconLocation = value;
                if (AutoSize)
                {
                    Refresh();
                }

                Invalidate();
            }
        }

        [DefaultValue(true)]
        public override bool AutoSize
        {
            get => base.AutoSize;
            set => base.AutoSize = value;
        }

        /// <summary>
        /// Gets or sets the Depth
        /// </summary>
        [Browsable(false)]
        public int Depth { get; set; }
        public FontManager FontManager => FontManager.Instance;

        /// <summary>
        /// Gets or sets the MouseState
        /// </summary>
        [Browsable(false)]
        public MouseState MouseState { get; set; }

        private ColorManager ColorScheme;

        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialButton"/> class.
        /// </summary>
        public KosesButton()
        {
            DrawShadows = true;
            HighEmphasis = true;
            this.DoubleBuffered = true;
            TypeButton = ButtonType.CONTAINED;
            this.BackColor = deselectedColor;
            this.BolderColor = Color.FromArgb(150, 0, 0, 0);
            this.DisabledBolderColor = Color.FromArgb(30, 0, 0, 0);
            this.DisabledBackgroundColor = Color.FromArgb(30, 0, 0, 0);
            this.ForeColor = Color.White;
            this.fontSize = 14f;
            this.bold = true;
            ColorScheme = new ColorManager(this.BackColor, ControlPaint.Dark(this.BackColor), ControlPaint.LightLight(this.BackColor), TextShade.WHITE);

            base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);

            _animationManager = new AnimationManager(false)
            {
                Increment = 0.03,
                AnimationType = AnimationType.EaseOut
            };
            _hoverAnimationManager = new AnimationManager
            {
                Increment = 0.12,
                AnimationType = AnimationType.Linear
            };

            _hoverAnimationManager.OnAnimationProgress += sender => Invalidate();
            _animationManager.OnAnimationProgress += sender => Invalidate();

            AutoSize = true;
            Margin = new Padding(3,3,3,3);
            Padding = new Padding(0);
        }

        /// <summary>
        /// Gets or sets the Text
        /// </summary>
        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                _textSize = CreateGraphics().MeasureString(value, FontManager.GetFontByNotoSans(this.fontSize, this.Bold));
                if (AutoSize)
                {
                    Refresh();
                }

                Invalidate();
            }
        }
        [Browsable(false)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                base.BackColor = value;
                ColorScheme = new ColorManager(this.BackColor, ControlPaint.Dark(this.BackColor), ControlPaint.Light(this.BackColor, 1f), TextShade.WHITE);

                Invalidate();
            }
        }

        private void drawShadowOnParent(object sender, PaintEventArgs e)
        {
            if (Parent == null)
            {
                RemoveShadowPaintEvent((Control)sender, drawShadowOnParent);
                return;
            }

            if (!DrawShadows || TypeButton != ButtonType.CONTAINED || Parent == null) return;

            // paint shadow on parent
            Graphics gp = e.Graphics;
            Rectangle rect = new Rectangle(Location, ClientRectangle.Size);
            gp.SmoothingMode = SmoothingMode.AntiAlias;
            DrawHelper.DrawSquareShadow(gp, rect);
        }

        /// <summary>
        /// The OnPaint
        /// </summary>
        /// <param name="pevent">The pevent<see cref="PaintEventArgs"/></param>
        protected override void OnPaint(PaintEventArgs pevent)
        {
            var g = pevent.Graphics;

            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            g.SmoothingMode = SmoothingMode.AntiAlias;

            double hoverAnimProgress = _hoverAnimationManager.GetProgress();

            g.Clear(Parent.BackColor);

            // button rectand path
            RectangleF buttonRectF = new RectangleF(ClientRectangle.Location, ClientRectangle.Size);
            buttonRectF.X -= 0.5f;
            buttonRectF.Y -= 0.5f;
            GraphicsPath buttonPath = DrawHelper.GetRoundRectPath(buttonRectF, 10);

            // button shadow (blend with form shadow)
            DrawHelper.DrawSquareShadow(g, ClientRectangle);

            if (TypeButton == ButtonType.CONTAINED)
            {
                // draw button rect
                // Disabled
                if (!Enabled)
                {
                    using (SolidBrush disabledBrush = new SolidBrush(DrawHelper.BlendColor(Parent.BackColor, this.DisabledBackgroundColor, this.DisabledBackgroundColor.A)))
                    {
                        g.FillPath(disabledBrush, buttonPath);
                    }
                }
                // High emphasis
                else if (HighEmphasis)
                {
                    g.FillPath(ColorScheme.PrimaryBrush, buttonPath);
                }
                // Mormal
                else
                {
                    using (SolidBrush normalBrush = new SolidBrush(this.BackColor))
                    {
                        g.FillPath(normalBrush, buttonPath);
                    }
                }
            }
            else
            {
                g.Clear(Parent.BackColor);
            }

            //Hover
            using (SolidBrush hoverBrush = new SolidBrush(Color.FromArgb(
                (int)(hoverAnimProgress * mouseOnOpacity), (TypeButton == ButtonType.CONTAINED && HighEmphasis ? ColorScheme.DarkPrimaryColor : // Contained with Emphasis without accent
                ColorScheme.PrimaryColor).RemoveAlpha()))) // Normal or Emphasis without accent
            {
                g.FillPath(hoverBrush, buttonPath);
            }

            if (TypeButton == ButtonType.OUTLINED)
            {
                using (Pen outlinePen = new Pen(Enabled ? this.BolderColor : this.DisabledBolderColor, 1))
                {
                    buttonRectF.X += 0.5f;
                    buttonRectF.Y += 0.5f;
                    g.DrawPath(outlinePen, buttonPath);
                }
            }

            //Ripple
            //if (_animationManager.IsAnimating())
            //{
            //    g.Clip = new Region(buttonRectF);
            //    for (var i = 0; i < _animationManager.GetAnimationCount(); i++)
            //    {
            //        var animationValue = _animationManager.GetProgress(i);
            //        var animationSource = _animationManager.GetSource(i);

            //        using (Brush rippleBrush = new SolidBrush(
            //            Color.FromArgb((int)(100 - (animationValue * 100)), // Alpha animation
            //            (TypeButton == ButtonType.CONTAINED && HighEmphasis ?
            //                ColorScheme.LightPrimaryColor : ColorScheme.PrimaryColor)))) // Normal
            //        {
            //            var rippleSize = (int)(animationValue * Width * 2);
            //            g.FillEllipse(rippleBrush, new Rectangle(animationSource.X - rippleSize / 2, animationSource.Y - rippleSize / 2, rippleSize, rippleSize));
            //        }
            //    }
            //    g.ResetClip();
            //}


            //Text
            var textRect = ClientRectangle;
            if (Icon != null)
            {
                textRect.Width -= 8 + 24 + 4 + 8; // left padding + icon width + space between Icon and Text + right padding
                textRect.X += 8 + 24 + 4; // left padding + icon width + space between Icon and Text
            }

            Color textColor = Enabled ? this.ForeColor : this.disabledTextColor; // Disabled
            using (NativeTextRenderer NativeText = new NativeTextRenderer(g))
            {
                NativeText.DrawMultilineTransparentText(Text, FontManager.GetFontByNotoSans(FontSize, Bold),
                    textColor,
                    textRect.Location,
                    textRect.Size,
                    NativeTextRenderer.TextAlignFlags.Center | NativeTextRenderer.TextAlignFlags.Middle);
            }

            //Icon
            if (Icon != null)
            {
                // Center Icon
                var rect = Icon.GetBounds(ref graphicsUnit);
                if (string.IsNullOrEmpty(Text))
                {
                    g.DrawImage(Icon, ClientRectangle.Width / 2 - rect.Width / 2, ClientRectangle.Height / 2 - rect.Height / 2, rect, graphicsUnit);
                }
                else
                {
                    g.DrawImage(Icon, ClientRectangle.Width / 2 - textRect.X / 2 + iconLocation.X, ClientRectangle.Height / 2 - rect.Height / 2 + iconLocation.Y, rect, graphicsUnit);
                }
            }
        }

        /// <summary>
        /// The GetPreferredSize
        /// </summary>
        /// <returns>The <see cref="Size"/></returns>
        private Size GetPreferredSize()
        {
            return GetPreferredSize(Size);
        }

        /// <summary>
        /// The GetPreferredSize
        /// </summary>
        /// <param name="proposedSize">The proposedSize<see cref="Size"/></param>
        /// <returns>The <see cref="Size"/></returns>
        public override Size GetPreferredSize(Size proposedSize)
        {
            Size s = base.GetPreferredSize(proposedSize);

            // Provides extra space for proper padding for content
            var extra = 16;

            if (Icon != null)
            {
                // 24 is for icon size
                // 4 is for the space between icon & text
                extra += Icon.Width + 4;
            }

            if (AutoSize)
            {
                s.Width = (int)Math.Ceiling(_textSize.Width);
                s.Width += extra;
                s.Height = 36;
            }
            else
            {
                s.Width += extra;
                s.Height = 36;
            }

            return s;
        }

        /// <summary>
        /// The OnCreateControl
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            if (DesignMode)
            {
                return;
            }

            MouseState = MouseState.OUT;
            MouseEnter += (sender, args) =>
            {
                MouseState = MouseState.HOVER;
                _hoverAnimationManager.StartNewAnimation(AnimationDirection.In);
                Invalidate();
            };
            MouseLeave += (sender, args) =>
            {
                MouseState = MouseState.OUT;
                _hoverAnimationManager.StartNewAnimation(AnimationDirection.Out);
                Invalidate();
            };
            MouseDown += (sender, args) =>
            {
                if (args.Button == MouseButtons.Left)
                {
                    MouseState = MouseState.DOWN;

                    _animationManager.StartNewAnimation(AnimationDirection.In, args.Location);
                    Invalidate();
                }
            };
            MouseUp += (sender, args) =>
            {
                MouseState = MouseState.HOVER;

                Invalidate();
            };

            GotFocus += (sender, args) =>
            {
                _hoverAnimationManager.StartNewAnimation(AnimationDirection.In);
                Invalidate();
            };
            LostFocus += (sender, args) =>
            {
                _hoverAnimationManager.StartNewAnimation(AnimationDirection.Out);
                Invalidate();
            };

            KeyDown += (object sender, KeyEventArgs e) =>
            {
                if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
                {
                    _animationManager.StartNewAnimation(AnimationDirection.In, new Point(ClientRectangle.Width >> 1, ClientRectangle.Height >> 1));
                    Invalidate();
                }
            };
        }
    }
}
