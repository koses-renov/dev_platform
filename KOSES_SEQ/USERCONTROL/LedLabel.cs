﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static KosesControl.Controls.KosesLabel;

namespace KosesControl.Controls
{
    public class LedLabel : Label
    {
        private TextTypes _textType;
        [Category("Font"), Description("Font Type")]
        public TextTypes TextType
        {
            get { return _textType; }
            set
            {
                _textType = value;
                Refresh();
            }
        }
        private float fontSize;
        [Category("Font"), Description("Font Size")]
        public float FontSize
        {
            get { return fontSize; }
            set
            {
                fontSize = value;
                Refresh();
            }
        }
        private bool bold;
        [Category("Font"), Description("Bold On/Off")]
        public bool Bold
        {
            get { return bold; }
            set
            {
                bold = value;
                Refresh();
            }
        }
        public FontManager FontManager => FontManager.Instance;

        [Category("Value Setting"), Description("최소값")]
        public double Minimum { get; set; } = 0;

        [Category("Value Setting"), Description("최대값")]
        public double Maximum { get; set; } = 999;

        [Category("Value Setting"), Description("소수점")]
        public int DecimalPoint { get; set; } = 3;

        private Color _ledOnColor;
        [Category("Led"), Description("Led On Color")]
        public Color LedOnColor
        {
            get { return _ledOnColor; }
            set
            {
                _ledOnColor = value;
                this.Invalidate();
            }
        }

        private Color _ledOffColor;
        [Category("Led"), Description("Led Off Color")]
        public Color LedOffColor
        {
            get { return _ledOffColor; }
            set
            {
                _ledOffColor = value;
                this.Invalidate();
            }
        }

        private bool _on;
        [Category("Led"), Description("On Off")]
        public bool On
        {
            get { return _on; }
            set
            {
                _on = value;
                this.ForeColor = _on ? LedOnColor : LedOffColor;
                this.Invalidate();
            }
        }

        public override string Text
        {
            get => base.Text; 
            set
            {
                if(TextType == TextTypes.NUMBER)
                {
                    if (double.TryParse(value, out double num))
                    {
                        if (num <= Minimum)
                            num = Minimum;
                        else if (num > Maximum)
                            num = Maximum;
                        base.Text = num.ToString($"F{DecimalPoint}");
                    }
                    else
                        base.Text = "0.0";
                }
                else
                    base.Text = value;
            }
        }

        [Browsable(false)]
        public override Font Font
        {
            get => base.Font;
            set => base.Font = value;
        }

        public LedLabel()
        {
            this.BackColor = Color.Black;
            this.fontSize = 15;
            this.ForeColor = LedOnColor = Color.FromArgb(255, 153, 255, 54);
            LedOffColor = Color.Gray;
            On = true;
            this.TextAlign = ContentAlignment.MiddleCenter;
            this.Refresh();
        }

        public override void Refresh()
        {
            base.Refresh();
            if (TextType == TextTypes.NUMBER)
                this.Font = FontManager.GetFontByLEDDot(FontSize);
            else
                this.Font = FontManager.GetFontByNotoSans(FontSize, Bold);
        }
    }
}
