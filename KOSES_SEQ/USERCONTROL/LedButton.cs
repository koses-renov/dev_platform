﻿using KosesControl.Animations;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public partial class LedButton : Button
    {
        Led Led;
        public enum AlignLeftRight { Left, Right }
        private AlignLeftRight ledAlign;
        [Category("Led"), Description("Led Setting")]
        public AlignLeftRight LedAlign
        {
            get
            {
                return ledAlign;
            }
            set
            {
                ledAlign = value;

                if (ledAlign == AlignLeftRight.Left)
                {
                    Led.Dock = DockStyle.Left;
                }
                else if (ledAlign == AlignLeftRight.Right)
                {
                    Led.Dock = DockStyle.Right;
                }
                Led.Padding = new Padding(Led.Padding.Right, Led.Padding.Top, Led.Padding.Left, Led.Padding.Bottom);
            }
        }

        private float fontSize;
        [Category("Font"), Description("Font Size")]
        public float FontSize
        {
            get { return fontSize; }
            set
            {
                fontSize = value;
                this.Invalidate();
            }
        }
        private bool bold;
        [Category("Font"), Description("Bold On/Off")]
        public bool Bold
        {
            get { return bold; }
            set
            {
                bold = value;
                this.Invalidate();
            }
        }

        [Category("Led"), Description("Led Width")]
        public int LedWidth
        {
            get => Led.Width;
            set => Led.Width = value;
        }

        [Category("Led"), Description("Led Padding")]
        public Padding LedPadding
        {
            get => Led.Padding;
            set => Led.Padding = value;
        }

        [Category("Led"), Description("Led Color")]
        public Color LedColor
        {
            get { return Led.Color; }
            set
            {
                Led.Color = value;
                this.Invalidate();
            }
        }

        [Category("Led"), Description("On Off")]
        public bool On
        {
            get { return Led.On; }
            set
            {
                Led.On = value;
                this.Invalidate();
            }
        }

        public LedButton()
        {
            Led = new Led()
            {
                LedShape = Led.Shape.RECTENGLE,
                Padding = new Padding(5, 5, 5, 5),
            };
            this.Size = new Size(120, 40);
            LedAlign = AlignLeftRight.Left;
            this.Controls.Add(Led);
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);
        }
    }
}
