﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public partial class KosesGroupBox : GroupBox
    {
        private Color titleBackColor;
        [Category("Title"), Description("Title BackColor")]
        public Color TitleBackColor
        {
            get { return titleBackColor; }
            set
            {
                titleBackColor = value;
                this.Invalidate();
            }
        }
        [Category("Title"), Description("Title GradationStyle")]
        public LinearGradientMode TitleGradationStyle { get; set; }
        private Color titleGradationColor1;
        [Category("Title"), Description("Title BackColor1")]
        public Color TitleGradationColor1
        {
            get { return titleGradationColor1; }
            set
            {
                titleGradationColor1 = value;
                this.Invalidate();
            }
        }

        private Color titleGradationColor2;
        [Category("Title"), Description("Title BackColor2")]
        public Color TitleGradationColor2
        {
            get { return titleGradationColor2; }
            set
            {
                titleGradationColor2 = value;
                this.Invalidate();
            }
        }
        //[Category("Title"), Description("Title HatchStyle")]
        //public HatchStyle TitleHatchStyle { get; set; }

        private Color titleForeColor;
        [Category("Title"), Description("Title ForeColor")]
        public Color TitleForeColor
        {
            get { return titleForeColor; }
            set
            {
                titleForeColor = value;
                this.Invalidate();
            }
        }
        [Category("Border"), Description("Border Radius")]
        public int Radius { get; set; }

        public KosesGroupBox()
        {
            this.DoubleBuffered = true;
            TitleBackColor = Color.SteelBlue;
            TitleGradationColor1 = Color.SteelBlue;
            TitleGradationColor2 = Color.FromArgb(184, 207, 226);
            TitleGradationStyle = LinearGradientMode.Horizontal;
            TitleForeColor = Color.White;

            //this.TitleHatchStyle = HatchStyle.Percent60;
            this.Radius = 15;
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);

            GroupBoxRenderer.DrawParentBackground(pevent.Graphics, this.ClientRectangle, this);
            var rect = ClientRectangle;
            using (var path = DrawHelper.CreateRoundRectPath(this.ClientRectangle, Radius))
            {
                pevent.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

                rect = new Rectangle(0, 0, rect.Width, Font.Height);
                if (this.BackColor != Color.Transparent)
                    using (var brush = new SolidBrush(BackColor))
                        pevent.Graphics.FillPath(brush, path);

                var clip = pevent.Graphics.ClipBounds;
                pevent.Graphics.SetClip(rect);

                using (var brush = new LinearGradientBrush(this.ClientRectangle, TitleGradationColor1, TitleGradationColor2, TitleGradationStyle))
                    pevent.Graphics.FillPath(brush, path);

                //using (var brush = new HatchBrush(TitleHatchStyle, TitleBackColor, ControlPaint.Light(TitleBackColor)))
                //    pevent.Graphics.FillPath(brush, path);
                using (var pen = new Pen(TitleBackColor, 1))
                    pevent.Graphics.DrawPath(pen, path);

                TextRenderer.DrawText(pevent.Graphics, Text, Font, rect, TitleForeColor);
                pevent.Graphics.SetClip(clip);

                using (var pen = new Pen(TitleBackColor, 1))
                    pevent.Graphics.DrawPath(pen, path);
            }
        }
    }
}
