﻿using KosesControl.Animations;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public class KosesRadioButton : RadioButton
    {
        private float fontSize;
        [Category("Font"), Description("Font Size")]
        public float FontSize
        {
            get { return fontSize; }
            set
            {
                fontSize = value;
                radioButtonSize = (int)fontSize;
                _textSize = CreateGraphics().MeasureString(this.Text, FontManager.GetFontByNotoSans(this.fontSize, this.Bold));
                _boxOffset = Height / 2 - (int)(radioButtonSize / 2);
                if (AutoSize)
                {
                    Refresh();
                }
                this.Invalidate();
            }
        }
        private bool bold;
        [Category("Font"), Description("Bold On/Off")]
        public bool Bold
        {
            get { return bold; }
            set
            {
                bold = value;
                _textSize = CreateGraphics().MeasureString(this.Text, FontManager.GetFontByNotoSans(this.FontSize, this.bold));
                if (AutoSize)
                {
                    Refresh();
                }
                this.Invalidate();
            }
        }

        Color disabledTextColor;
        [Category("Font"), Description("DisableTextColor")]
        [DefaultValue(typeof(Color), "Gray")]
        public Color DisabledTextColor
        {
            get { return this.disabledTextColor; }

            set
            {
                this.disabledTextColor = value;
                this.Invalidate();
            }
        }
        Color radioButtonOnColor;
        [Category("RadioButton"), Description("RadioButtonOnColor")]
        public Color RadioButtonOnColor
        {
            get { return radioButtonOnColor; }
            set
            {
                radioButtonOnColor = value;
                this.Invalidate();
            }
        }
        Color radioButtonOffColor;
        [Category("RadioButton"), Description("RadioButtonOffColor")]
        public Color RadioButtonOffColor
        {
            get { return radioButtonOffColor; }
            set
            {
                radioButtonOffColor = value;
                this.Invalidate();
            }
        }
        Color disableRadioButtonColor;
        [Category("RadioButton"), Description("DisableRadioButtonColor")]
        public Color DisableRadioButtonColor
        {
            get { return disableRadioButtonColor; }
            set
            {
                disableRadioButtonColor = value;
                this.Invalidate();
            }
        }

        private int radioButtonSize = 14;
        [Browsable(false)]
        [Category("RadioButton"), Description("RadioButtonSize")]
        public int RadioButtonSize
        {
            get { return radioButtonSize; }
            set
            {
                radioButtonSize = value;
                if (radioButtonSize % 2 != 0)
                    radioButtonSize--;
                this.Invalidate();
            }
        }

        [Browsable(false)]
        public MouseState MouseState { get; set; }
        [Browsable(false)]
        public Point MouseLocation { get; set; }

        private bool ripple;

        [Browsable(false)]
        [Category("Behavior")]
        public bool Ripple
        {
            get { return ripple; }
            set
            {
                ripple = value;
                AutoSize = AutoSize; //Make AutoSize directly set the bounds.

                if (value)
                {
                    Margin = new Padding(0);
                }

                Invalidate();
            }
        }

        /// <summary>
        /// Defines the _textSize
        /// </summary>
        private SizeF _textSize;

        public FontManager FontManager => FontManager.Instance;

        private ColorManager ColorScheme;

        // animation managers
        private readonly AnimationManager _checkAM;

        private readonly AnimationManager _rippleAM;
        private readonly AnimationManager _hoverAM;

        // size related variables which should be recalculated onsizechanged
        private Rectangle _radioButtonBounds;

        private int _boxOffset;

        // size constants
        private const int HEIGHT_RIPPLE = 37;

        private const int HEIGHT_NO_RIPPLE = 20;
        private const int RADIOBUTTON_SIZE = 18;
        private const int RADIOBUTTON_SIZE_HALF = RADIOBUTTON_SIZE / 2;
        private const int RADIOBUTTON_OUTER_CIRCLE_WIDTH = 2;
        private const int RADIOBUTTON_INNER_CIRCLE_SIZE = RADIOBUTTON_SIZE - (2 * RADIOBUTTON_OUTER_CIRCLE_WIDTH);
        private const int TEXT_OFFSET = 26;

        public KosesRadioButton()
        {
            this.DisabledTextColor = Color.Gray;
            this.RadioButtonOnColor = Color.FromArgb(49, 87, 126);
            this.RadioButtonOffColor = Color.FromArgb(138, 0, 0, 0);
            this.DisableRadioButtonColor = Color.FromArgb(66, 0, 0, 0);
            this.fontSize = 14f;
            this.bold = false;
            ColorScheme = new ColorManager(this.BackColor, ControlPaint.Dark(this.BackColor), ControlPaint.LightLight(this.BackColor), TextShade.WHITE);

            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);

            _checkAM = new AnimationManager
            {
                AnimationType = AnimationType.EaseInOut,
                Increment = 0.06
            };
            _hoverAM = new AnimationManager(true)
            {
                AnimationType = AnimationType.Linear,
                Increment = 0.10
            };
            _rippleAM = new AnimationManager(false)
            {
                AnimationType = AnimationType.Linear,
                Increment = 0.10,
                SecondaryIncrement = 0.08
            };

            _checkAM.OnAnimationProgress += sender => Invalidate();
            _hoverAM.OnAnimationProgress += sender => Invalidate();
            _rippleAM.OnAnimationProgress += sender => Invalidate();

            TabStopChanged += (sender, e) => TabStop = true;

            CheckedChanged += (sender, args) =>
            {
                if (Ripple)
                    _checkAM.StartNewAnimation(Checked ? AnimationDirection.In : AnimationDirection.Out);
            };

            SizeChanged += OnSizeChanged;

            Ripple = true;
            MouseLocation = new Point(-1, -1);
        }

        private void OnSizeChanged(object sender, EventArgs eventArgs)
        {
            _boxOffset = Height / 2 - (int)(radioButtonSize / 2);
        }

        public override Size GetPreferredSize(Size proposedSize)
        {
            Size strSize;

            using (NativeTextRenderer NativeText = new NativeTextRenderer(CreateGraphics()))
            {
                strSize = NativeText.MeasureString(Text, FontManager.GetFontByNotoSans(FontSize, Bold));
            }
            strSize.Width += radioButtonSize + _boxOffset * 2;
            strSize.Height += radioButtonSize;

            return strSize;
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;

            // clear the control
            g.Clear(Parent.BackColor);

            int RADIOBUTTON_CENTER = _boxOffset + radioButtonSize / 2;
            Point animationSource = new Point(RADIOBUTTON_CENTER, RADIOBUTTON_CENTER);

            double animationProgress = _checkAM.GetProgress();

            int colorAlpha = Enabled ? (int)(animationProgress * 255.0) : disableRadioButtonColor.A;
            int backgroundAlpha = Enabled ? (int)(radioButtonOffColor.A * (1.0 - animationProgress)) : disableRadioButtonColor.A;
            float animationSize = (float)(animationProgress * radioButtonSize * 0.7);
            float animationSizeHalf = animationSize / 2;
            int rippleHeight = (HEIGHT_RIPPLE % 2 == 0) ? HEIGHT_RIPPLE - 3 : HEIGHT_RIPPLE - 2;

            Color RadioColor = Color.FromArgb(colorAlpha, Enabled ? radioButtonOnColor : disableRadioButtonColor);

            // draw hover animation
            if (Ripple)
            {
                double animationValue = _hoverAM.GetProgress();
                int rippleSize = (int)(rippleHeight * (0.7 + (0.3 * animationValue)));

                using (SolidBrush rippleBrush = new SolidBrush(Color.FromArgb((int)(40 * animationValue),
                    !Checked ? Color.White : RadioColor)))
                {
                    g.FillEllipse(rippleBrush, new Rectangle(animationSource.X - rippleSize / 2, animationSource.Y - rippleSize / 2, rippleSize - 1, rippleSize - 1));
                }
            }

            // draw ripple animation
            if (Ripple && _rippleAM.IsAnimating())
            {
                for (int i = 0; i < _rippleAM.GetAnimationCount(); i++)
                {
                    double animationValue = _rippleAM.GetProgress(i);
                    int rippleSize = (_rippleAM.GetDirection(i) == AnimationDirection.InOutIn) ? (int)(rippleHeight * (0.7 + (0.3 * animationValue))) : rippleHeight;

                    using (SolidBrush rippleBrush = new SolidBrush(Color.FromArgb((int)((animationValue * 40)), !Checked ? Color.White : RadioColor)))
                    {
                        g.FillEllipse(rippleBrush, new Rectangle(animationSource.X - rippleSize / 2, animationSource.Y - rippleSize / 2, rippleSize - 1, rippleSize - 1));
                    }
                }
            }

            // draw radiobutton circle
            using (Pen pen = new Pen(DrawHelper.BlendColor(Parent.BackColor, Enabled ? radioButtonOffColor : disableRadioButtonColor, backgroundAlpha), 2))
            {
                g.DrawEllipse(pen, new Rectangle(_boxOffset, _boxOffset, radioButtonSize, radioButtonSize));
            }

            if (Enabled)
            {
                using (Pen pen = new Pen(RadioColor, 2))
                {
                    g.DrawEllipse(pen, new Rectangle(_boxOffset, _boxOffset, radioButtonSize, radioButtonSize));
                }
            }

            if (Checked)
            {
                using (SolidBrush brush = new SolidBrush(RadioColor))
                {
                    g.FillEllipse(brush, new RectangleF(RADIOBUTTON_CENTER - animationSizeHalf, RADIOBUTTON_CENTER - animationSizeHalf, animationSize, animationSize));
                }
            }

            // Text
            using (NativeTextRenderer NativeText = new NativeTextRenderer(g))
            {
                Rectangle textLocation = new Rectangle(_boxOffset + radioButtonSize + 5, 0, Width, Height);
                NativeText.DrawTransparentText(Text, FontManager.GetFontByNotoSans(FontSize, Bold),
                    Enabled ? this.ForeColor : this.disabledTextColor,
                    textLocation.Location,
                    textLocation.Size,
                    NativeTextRenderer.TextAlignFlags.Left | NativeTextRenderer.TextAlignFlags.Middle);
            }
        }

        private bool IsMouseInCheckArea()
        {
            return ClientRectangle.Contains(MouseLocation);
        }

        private bool hovered = false;

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            if (DesignMode) return;

            MouseState = MouseState.OUT;

            GotFocus += (sender, AddingNewEventArgs) =>
            {
                if (Ripple && !hovered)
                {
                    _hoverAM.StartNewAnimation(AnimationDirection.In, new object[] { Checked });
                    hovered = true;
                }
            };

            LostFocus += (sender, args) =>
            {
                if (Ripple && hovered)
                {
                    _hoverAM.StartNewAnimation(AnimationDirection.Out, new object[] { Checked });
                    hovered = false;
                }
            };

            MouseEnter += (sender, args) =>
            {
                MouseState = MouseState.HOVER;
                //if (Ripple && !hovered)
                //{
                //    _hoverAM.StartNewAnimation(AnimationDirection.In, new object[] { Checked });
                //    hovered = true;
                //}
            };

            MouseLeave += (sender, args) =>
            {
                MouseLocation = new Point(-1, -1);
                MouseState = MouseState.OUT;
                //if (Ripple && hovered)
                //{
                //    _hoverAM.StartNewAnimation(AnimationDirection.Out, new object[] { Checked });
                //    hovered = false;
                //}
            };

            MouseDown += (sender, args) =>
            {
                MouseState = MouseState.DOWN;
                if (Ripple)
                {
                    _rippleAM.SecondaryIncrement = 0;
                    _rippleAM.StartNewAnimation(AnimationDirection.InOutIn, new object[] { Checked });
                }
            };

            KeyDown += (sender, args) =>
            {
                if (Ripple && (args.KeyCode == Keys.Space) && _rippleAM.GetAnimationCount() == 0)
                {
                    _rippleAM.SecondaryIncrement = 0;
                    _rippleAM.StartNewAnimation(AnimationDirection.InOutIn, new object[] { Checked });
                }
            };

            MouseUp += (sender, args) =>
            {
                if (Ripple)
                {
                    MouseState = MouseState.HOVER;
                    _rippleAM.SecondaryIncrement = 0.08;
                    _hoverAM.StartNewAnimation(AnimationDirection.Out, new object[] { Checked });
                    hovered = false;
                }
            };

            KeyUp += (sender, args) =>
            {
                if (Ripple && (args.KeyCode == Keys.Space))
                {
                    MouseState = MouseState.HOVER;
                    _rippleAM.SecondaryIncrement = 0.08;
                }
            };

            MouseMove += (sender, args) =>
            {
                MouseLocation = args.Location;
                Cursor = IsMouseInCheckArea() ? Cursors.Hand : Cursors.Default;
            };
        }
    }
}