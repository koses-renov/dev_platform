﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public class KosesPropertyGrid : PropertyGrid
    {
        public KosesPropertyGrid()
        {
        }

        public GridItemCollection GetAllGridEntries()
        {
            if (this == null)
                throw new ArgumentNullException("grid");

            object view = GetType().BaseType.GetField("gridView", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(this);
            return (GridItemCollection)view.GetType().InvokeMember("GetAllGridEntries", BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, view, null);
        }
    }
}
