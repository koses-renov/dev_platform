﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public class KosesTextBox : TextBox
    {
        
        string hintText;
        [Category("Setting"), Description("HintText")]
        public string HintText
        {
            get { return hintText; }
            set
            {
                hintText = value;
                this.Text = hintText;
                this.Invalidate();
            }
        }

        KeyPressEventHandler KeyPressHandler;
        EventHandler TextChangedHandler;
        NumberTypes textboxType;
        [Category("Setting"), Description("HintText")]
        public NumberTypes TextBoxType
        {
            get { return textboxType; }
            set
            {
                textboxType = value;
                EventHandle(out KeyPressHandler, out TextChangedHandler);
                if (textboxType == NumberTypes.INT || textboxType == NumberTypes.UINT)
                {
                    this.Text = "0";
                    ImeMode = ImeMode.Disable;
                }
                else if (textboxType == NumberTypes.FLOAT || textboxType == NumberTypes.UFLOAT)
                {
                    this.Text = "0.000";
                    this.MaxLength = 10;
                    ImeMode = ImeMode.Disable;
                }
                else
                    ImeMode = ImeMode.NoControl;

                this.KeyPress += KeyPressHandler;
                this.TextChanged += TextChangedHandler;
            }
        }
        private void EventHandle(out KeyPressEventHandler keyPress, out EventHandler textChanged)
        {
            keyPress = (sender, e) =>
            {
                if (textboxType != NumberTypes.ALL)
                {
                    if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                    {
                        if (textboxType == NumberTypes.INT || textboxType == NumberTypes.FLOAT)
                        {
                            if (!(e.KeyChar == '-' && !(sender as TextBox).Text.Contains("-")))
                                e.Handled = true;
                            else
                            {
                                e.Handled = false;
                                return;
                            }
                        }
                        if (textboxType == NumberTypes.UFLOAT || textboxType == NumberTypes.FLOAT)
                        {
                            if (!(e.KeyChar == '.' && !(sender as TextBox).Text.Contains(".")))
                                e.Handled = true;
                            else
                            {
                                e.Handled = false;
                                return;
                            }
                        }
                        e.Handled = true;
                    }
                }
            };

            textChanged = (sender, e) =>
            {
                if (textboxType == NumberTypes.UFLOAT || textboxType == NumberTypes.FLOAT)
                {
                    if (this.Text.Contains("."))
                    {
                        if (this.Text.IndexOf('.') == 0)
                        {
                            this.Text = this.Text.Replace(".", "0.");
                            this.Select(this.Text.Length, 0);
                        }
                    }
                }
                if (this.Text.Contains("-"))
                {
                    if (this.Text.IndexOf('-') != 0)
                    {
                        this.Text = this.Text.Replace("-", "");
                        this.Text = this.Text.Insert(0, "-");
                        this.Select(this.Text.Length, 0);
                    }
                }
            };
        }

        Color DefaultColor;
        public KosesTextBox()
        {
            // get default color of text
            DefaultColor = this.ForeColor;
            HintText = "";
            // Add event handler for when the control gets focus
            this.GotFocus += (object sender, EventArgs e) =>
            {
                if(this.Text == HintText)
                    this.Text = String.Empty;
                this.ForeColor = DefaultColor;
            };

            // add event handling when focus is lost
            this.LostFocus += (Object sender, EventArgs e) => {
                if (String.IsNullOrEmpty(this.Text) || this.Text == HintText)
                {
                    this.ForeColor = Color.Gray;
                    this.Text = HintText;
                }
                else
                {
                    this.ForeColor = DefaultColor;
                }
            };

           if (!string.IsNullOrEmpty(hintText))
            {
                // change style   
                this.ForeColor = Color.Gray;
                // Add text
                HintText = hintText;
                this.Text = hintText;
            }
        }
    }
}
