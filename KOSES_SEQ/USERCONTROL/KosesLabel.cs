﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace KosesControl.Controls
{
    public class KosesLabel : Label
    {
        private TextTypes _textType;
        [Category("ValueSetting"), Description("Value Type")]
        public TextTypes TextType
        {
            get { return _textType; }
            set
            {
                _textType = value;
                if (_textType == TextTypes.TEXT)
                    Text = this.Text;
                else if (_textType == TextTypes.NUMBER)
                    Text = innerValue.ToString(ValueFormatStr);
                Invalidate();
            }
        }

        private double innerValue;

        [Category("ValueSetting")]
        public double ValueMin { get; set; } = 0;

        [Category("ValueSetting")]
        public double ValueMax { get; set; } = 999;

        [Category("ValueSetting")]
        public string ValueFormatStr { get; set; } = "F3";

        [Browsable(false)]
        public double Value
        {
            get => this.innerValue;
            set
            {
                if(_textType == TextTypes.NUMBER)
                {
                    if (value <= ValueMin)
                        this.innerValue = ValueMin;
                    else if (value > ValueMax)
                        this.innerValue = ValueMax;
                    else
                        this.innerValue = value;
                    Invalidate();
                }
            }
        }

        [DefaultValue(typeof(ContentAlignment), "MiddleCenter")]
        public override ContentAlignment TextAlign
        {
            get
            {
                return _TextAlign;
            }
            set
            {
                _TextAlign = value;
                updateAligment();
                Invalidate();
            }
        }

        private float fontSize;
        [Category("Font"), Description("Font Size")]
        public float FontSize
        {
            get { return fontSize; }
            set
            {
                fontSize = value;
                _textSize = CreateGraphics().MeasureString(this.Text, FontManager.GetFontByNotoSans(this.fontSize, this.bold));
                if (AutoSize)
                {
                    Refresh();
                }
                this.Invalidate();
            }
        }
        private bool bold;
        [Category("Font"), Description("Bold On/Off")]
        public bool Bold
        {
            get { return bold; }
            set
            {
                bold = value;
                _textSize = CreateGraphics().MeasureString(this.Text, FontManager.GetFontByNotoSans(this.fontSize, this.bold));
                if (AutoSize)
                {
                    Refresh();
                }
                this.Invalidate();
            }
        }
        public enum BorderTypes { NONE, RECTENGLE, ROUNDRECTENGLE }

        private BorderTypes _borderType;

        [Category("Border"), Description("Border Type")]
        public BorderTypes BorderType
        {
            get { return _borderType; }
            set
            {
                _borderType = value;
                Invalidate();
            }
        }

        private int _borderWidth = 1;
        [Category("Border"), Description("Border Size")]
        public int BorderWidth
        {
            get { return _borderWidth; }
            set
            {
                _borderWidth = value;
                Invalidate();
            }
        }

        private Color _borderColor = Color.Black;
        [Category("Border"), Description("Border Color")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                _borderColor = value;
                Invalidate();
            }
        }

        private int _borderRadius = 1;
        [Category("Border"), Description("RountRectBord Radius")]
        public int BorderRadius
        {
            get { return _borderRadius; }
            set
            {
                _borderRadius = value;
                Invalidate();
            }
        }

        Color disabledTextColor = Color.Gray;
        [Category("Font"), Description("DisableTextColor")]
        public Color DisabledTextColor
        {
            get { return this.disabledTextColor; }

            set
            {
                this.disabledTextColor = value;
                this.Invalidate();
            }
        }
        private FontManager FontManager => FontManager.Instance;

        private SizeF _textSize;

        public KosesLabel()
        {
            this.fontSize = 14f;
            TextAlign = ContentAlignment.MiddleCenter;
        }

        public override Size GetPreferredSize(Size proposedSize)
        {
            if (AutoSize)
            {
                Size strSize;
                using (NativeTextRenderer NativeText = new NativeTextRenderer(CreateGraphics()))
                {
                    strSize = NativeText.MeasureString(Text, FontManager.GetFontByNotoSans(this.fontSize, this.bold));
                    strSize.Width += 1; // necessary to avoid a bug when autosize = true
                }
                return strSize;
            }
            else
            {
                return proposedSize;
            }
        }

        private ContentAlignment _TextAlign = ContentAlignment.TopLeft;


        private NativeTextRenderer.TextAlignFlags Alignment;

        private void updateAligment()
        {
            switch (_TextAlign)
            {
                case ContentAlignment.TopLeft:
                    Alignment = NativeTextRenderer.TextAlignFlags.Top | NativeTextRenderer.TextAlignFlags.Left;
                    break;

                case ContentAlignment.TopCenter:
                    Alignment = NativeTextRenderer.TextAlignFlags.Top | NativeTextRenderer.TextAlignFlags.Center;
                    break;

                case ContentAlignment.TopRight:
                    Alignment = NativeTextRenderer.TextAlignFlags.Top | NativeTextRenderer.TextAlignFlags.Right;
                    break;

                case ContentAlignment.MiddleLeft:
                    Alignment = NativeTextRenderer.TextAlignFlags.Middle | NativeTextRenderer.TextAlignFlags.Left;
                    break;

                case ContentAlignment.MiddleCenter:
                    Alignment = NativeTextRenderer.TextAlignFlags.Middle | NativeTextRenderer.TextAlignFlags.Center;
                    break;

                case ContentAlignment.MiddleRight:
                    Alignment = NativeTextRenderer.TextAlignFlags.Middle | NativeTextRenderer.TextAlignFlags.Right;
                    break;

                case ContentAlignment.BottomLeft:
                    Alignment = NativeTextRenderer.TextAlignFlags.Bottom | NativeTextRenderer.TextAlignFlags.Left;
                    break;

                case ContentAlignment.BottomCenter:
                    Alignment = NativeTextRenderer.TextAlignFlags.Bottom | NativeTextRenderer.TextAlignFlags.Center;
                    break;

                case ContentAlignment.BottomRight:
                    Alignment = NativeTextRenderer.TextAlignFlags.Bottom | NativeTextRenderer.TextAlignFlags.Right;
                    break;

                default:
                    Alignment = NativeTextRenderer.TextAlignFlags.Top | NativeTextRenderer.TextAlignFlags.Left;
                    break;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(Parent.BackColor);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            if (this.BorderType == BorderTypes.RECTENGLE)
            {
                //base pos
                int xy = 0;

                int width = this.ClientSize.Width;
                int height = this.ClientSize.Height;

                //BackGround
                var brush = new SolidBrush(this.BackColor);
                g.FillRectangle(brush, ClientRectangle);

                //Border
                Pen pen = new Pen(_borderColor);
                for (int i = 0; i < _borderWidth; i++)
                    g.DrawRectangle(pen, xy + i, xy + i, width - (i << 1) - 1, height - (i << 1) - 1);
            }
            else if (this.BorderType == BorderTypes.ROUNDRECTENGLE)
            {
                //Set Path
                RectangleF LabelRectF = new RectangleF(ClientRectangle.Location, ClientRectangle.Size);
                GraphicsPath LabelPath = DrawHelper.GetRoundRectPath(LabelRectF, _borderRadius);
                GraphicsPath LabelBorderPath = DrawHelper.CreateRoundRectBorderPath(this.ClientRectangle, _borderRadius, _borderWidth);

                //BackGround
                var brush = new SolidBrush(this.BackColor);
                g.FillPath(brush, LabelPath);

                //Border
                Pen pen = new Pen(_borderColor, _borderWidth);
                g.DrawPath(pen, LabelBorderPath);
            }
            else
            {
                //BackGround
                var brush = new SolidBrush(this.BackColor);
                g.FillRectangle(brush, ClientRectangle);
            }

            // Draw Text
            using (NativeTextRenderer NativeText = new NativeTextRenderer(g))
            {
                var textArea = new Rectangle(ClientRectangle.Left + Padding.Left, ClientRectangle.Top + Padding.Top, ClientRectangle.Width - Padding.Right, ClientRectangle.Height - Padding.Bottom);
                NativeText.DrawMultilineTransparentText(
                    Text,
                    FontManager.GetFontByNotoSans(this.fontSize, this.bold),
                    Enabled ?
                    this.ForeColor : // Normal
                    this.DisabledTextColor, // Disabled
                    textArea.Location,
                    textArea.Size,
                    Alignment);
            }
        }

        protected override void InitLayout()
        {
            Font = FontManager.GetFontByNotoSans(this.fontSize, this.bold);
            BackColorChanged += (sender, args) => Refresh();
        }
    }
}