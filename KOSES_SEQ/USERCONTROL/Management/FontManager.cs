﻿using KOSES_SEQ.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KosesControl
{
    public class FontManager
    {
        private static FontManager _instance;
        public static FontManager Instance => _instance ?? (_instance = new FontManager());

        public int FORM_PADDING = 14;

        //NativeTextRenderer.LogFont lfont = new NativeTextRenderer.LogFont();

        private FontManager()
        {
            AddFont(Resources.NotoSansKR_Regular);
            //AddFont(Resources.NotoSansKR_Bold);
            //AddFont(Resources.NotoSansKR_Black);
            //AddFont(Resources.NotoSansKR_Thin);
            //AddFont(Resources.NotoSansKR_Light);
            //AddFont(Resources.NotoSansKR_Medium);
            AddFont(Resources.LEDDot);
            AddFont(Resources.SevenSegment_Regular);

            FontFamilies = new Dictionary<string, FontFamily>();
            foreach (FontFamily ff in FontCollection.Families.ToArray())
            {
                FontFamilies.Add(ff.Name.Replace(' ', '_'), ff);
            }
        }

        // Destructor
        ~FontManager()
        {
        }

        #region Font

        public Font GetFontByNotoSans(float fontSize, bool bold)
        {
            if (bold)
                return new Font(FontFamilies["Noto_Sans_KR"], fontSize, FontStyle.Bold, GraphicsUnit.Pixel);
            else
                return new Font(FontFamilies["Noto_Sans_KR"], fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
        }
        public Font GetFontByLEDDot(float fontSize)
        {
            return new Font(FontFamilies["LED_Dot-Matrix"], fontSize);
        }
        public Font GetFontBySevenSegment(float fontSize)
        {
            return new Font(FontFamilies["DSEG7_Classic_Mini"], fontSize);
        }

        private Dictionary<string, FontFamily> FontFamilies;

        private PrivateFontCollection FontCollection = new PrivateFontCollection();

        //public IntPtr GetLogFontByType(float fontSize, bool bold)
        //{
        //    if(bold)
        //        return createLogicalFont("Noto_Sans_KR_Bold", Convert.ToInt32(fontSize), NativeTextRenderer.logFontWeight.FW_BOLD);
        //    else
        //        return createLogicalFont("Noto_Sans_KR_Medium", Convert.ToInt32(fontSize), NativeTextRenderer.logFontWeight.FW_MEDIUM);
        //}

        //private IntPtr createLogicalFont(string fontName, int size, NativeTextRenderer.logFontWeight weight)
        //{
        //    // Logical font:
        //    lfont.lfFaceName = fontName;
        //    lfont.lfHeight = -size;
        //    lfont.lfWeight = (int)weight;
        //    return NativeTextRenderer.CreateFontIndirect(lfont);
        //}

        private void AddFont(byte[] fontdata)
        {
            // Add font to system table in memory
            int dataLength = fontdata.Length;

            IntPtr ptrFont = Marshal.AllocCoTaskMem(dataLength);
            Marshal.Copy(fontdata, 0, ptrFont, dataLength);

            // GDI Font
            NativeTextRenderer.AddFontMemResourceEx(fontdata, dataLength, IntPtr.Zero, out _);

            // GDI+ Font
            FontCollection.AddMemoryFont(ptrFont, dataLength);
        }
        #endregion
    }
}
