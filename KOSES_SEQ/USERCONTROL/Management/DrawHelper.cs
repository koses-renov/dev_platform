﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace KosesControl
{
    /// <summary>
    /// Defines the <see cref="DrawHelper" />
    /// </summary>
    internal static class DrawHelper
    {
        static bool isFillLeftTop = false;//왼쪽위 사각으로 채우기(라운드 적용X)
        static bool isFillRightTop = false;//오른쪽위 사각으로 채우기(라운드 적용X)
        static bool isFillLeftBtm = false;//왼쪽아래 사각으로 채우기(라운드 적용X)
        static bool isFillRightBtm = false;//오른쪽아래 사각으로 채우기(라운드 적용X)

        /// <summary>
        /// The CreateRoundRectBorder
        /// </summary>
        /// <param name="rectangle">The rectangle<see cref="Rectangle"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <param name="borderWidth">The borderWidth<see cref="int"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateRoundRectPath(Rectangle rectangle, float radius = 1, int borderWidth = 0)
        {
            var gp = new GraphicsPath();
            if (radius < 1) radius = 1;

            int left = rectangle.X + borderWidth - 1;
            int top = rectangle.Y + borderWidth - 1;
            int right = rectangle.X + rectangle.Width - borderWidth + 1;
            int bottom = rectangle.Y + rectangle.Height - borderWidth + 1;

            if (isFillLeftTop)
            {//좌상
                gp.AddLine(left, top + radius, left, top);
                gp.AddLine(left, top, left + radius, top);
            }
            else
            {
                gp.AddArc(left, top, radius, radius, 180, 90);
            }
            if (isFillRightTop)
            {//우상
                gp.AddLine(right - radius, top, right, top);
                gp.AddLine(right, top, right, top + radius);
            }
            else
            {
                gp.AddArc(left + right - radius - borderWidth, top, radius, radius, 270, 90);
            }
            if (isFillRightBtm)
            {//우하
                gp.AddLine(right, bottom - radius, right, bottom);
                gp.AddLine(right, bottom, right - radius, bottom);
            }
            else
            {
                gp.AddArc(left + right - radius - borderWidth, top + bottom - radius - borderWidth, radius, radius, 0, 90);
            }
            if (isFillLeftBtm)
            {//좌하
                gp.AddLine(left + radius, bottom, left, bottom);
                gp.AddLine(left, bottom, left, bottom - radius);
            }
            else
            {
                gp.AddArc(left, top + bottom - radius - borderWidth, radius, radius, 90, 90);
            }

            gp.CloseAllFigures();
            return gp;
        }

        /// <summary>
        /// The CreateRoundRectBorder
        /// </summary>
        /// <param name="rectangle">The rectangle<see cref="RectangleF"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <param name="borderWidth">The borderWidth<see cref="int"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateRoundRectPath(RectangleF rectangle, float radius = 1, int borderWidth = 0)
        {
            var gp = new GraphicsPath();
            if(radius < 1) radius = 1;

            float left = rectangle.X + borderWidth;
            float top = rectangle.Y + borderWidth;
            float right = rectangle.X + rectangle.Width - borderWidth;
            float bottom = rectangle.Y + rectangle.Height - borderWidth;

            if (isFillLeftTop)
            {//좌상
                gp.AddLine(left, top + radius, left, top);
                gp.AddLine(left, top, left + radius, top);
            }
            else
            {
                gp.AddArc(left, top, radius, radius, 180, 90);
            }
            if (isFillRightTop)
            {//우상
                gp.AddLine(right - radius, top, right, top);
                gp.AddLine(right, top, right, top + radius);
            }
            else
            {
                gp.AddArc(left + rectangle.Width - radius - borderWidth, top, radius, radius, 270, 90);
            }
            if (isFillRightBtm)
            {//우하
                gp.AddLine(right, bottom - radius, right, bottom);
                gp.AddLine(right, bottom, right - radius, bottom);
            }
            else
            {
                gp.AddArc(left + rectangle.Width - radius - borderWidth,top + rectangle.Height - radius - borderWidth, radius, radius, 0, 90);
            }
            if (isFillLeftBtm)
            {//좌하
                gp.AddLine(left + radius, bottom, left, bottom);
                gp.AddLine(left, bottom, left, bottom - radius);
            }
            else
            {
                gp.AddArc(left, top + rectangle.Height - radius - borderWidth, radius, radius, 90, 90);
            }

            gp.CloseAllFigures();
            return gp;
        }

        /// <summary>
        /// The CreateRoundRectBorder
        /// </summary>
        /// <param name="rectangle">The rectangle<see cref="Rectangle"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateRoundRectPath(Rectangle rectangle, float radius = 1)
        {
            var gp = new GraphicsPath();
            if (radius < 1) radius = 1;
            gp.AddArc(rectangle.X + rectangle.Width - radius - 1, rectangle.Y, radius, radius, 270, 90);
            gp.AddArc(rectangle.X + rectangle.Width - radius - 1, rectangle.Y + rectangle.Height - radius - 1, radius, radius, 0, 90);
            gp.AddArc(rectangle.X, rectangle.Y + rectangle.Height - radius - 1, radius, radius, 90, 90);
            gp.AddArc(rectangle.X, rectangle.Y, radius, radius, 180, 90);
            gp.CloseFigure();
            return gp;
        }

        /// <summary>
        /// The CreateRoundRect
        /// </summary>
        /// <param name="x">The x<see cref="float"/></param>
        /// <param name="y">The y<see cref="float"/></param>
        /// <param name="width">The width<see cref="float"/></param>
        /// <param name="height">The height<see cref="float"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateShadowRoundRect(float x, float y, float width, float height, float radius)
        {
            var gp = new GraphicsPath();
            gp.AddArc(x + width - (radius * 2), y, radius * 2, radius * 2, 270, 90);
            gp.AddArc(x + width - (radius * 2), y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            gp.AddArc(x, y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            gp.AddArc(x, y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();
            return gp;
        }

        /// <summary>
        /// The CreateTopRoundRect
        /// </summary>
        /// <param name="rect">The rect<see cref="Rectangle"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath GetRoundRectPath(Rectangle rect, float radius)
        {
            isFillLeftTop = false;//왼쪽위 사각으로 채우기(라운드 적용X)
            isFillRightTop = false;//오른쪽위 사각으로 채우기(라운드 적용X)
            isFillLeftBtm = false;//왼쪽아래 사각으로 채우기(라운드 적용X)
            isFillRightBtm = false;//오른쪽아래 사각으로 채우기(라운드 적용X)
            return CreateRoundRectPath(rect, radius);
        }

        /// <summary>
        /// The CreateTopRoundRect
        /// </summary>
        /// <param name="rect">The rect<see cref="RectangleF"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath GetRoundRectPath(RectangleF rect, float radius)
        {
            isFillLeftTop = false;//왼쪽위 사각으로 채우기(라운드 적용X)
            isFillRightTop = false;//오른쪽위 사각으로 채우기(라운드 적용X)
            isFillLeftBtm = false;//왼쪽아래 사각으로 채우기(라운드 적용X)
            isFillRightBtm = false;//오른쪽아래 사각으로 채우기(라운드 적용X)
            return CreateRoundRectPath(rect, radius);
        }

        /// <summary>
        /// The CreateTopRoundRect
        /// </summary>
        /// <param name="rect">The rect<see cref="RectangleF"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateTopRoundRectPath(RectangleF rect, float radius)
        {
            isFillLeftTop = false;//왼쪽위 사각으로 채우기(라운드 적용X)
            isFillRightTop = false;//오른쪽위 사각으로 채우기(라운드 적용X)
            isFillLeftBtm = true;//왼쪽아래 사각으로 채우기(라운드 적용X)
            isFillRightBtm = true;//오른쪽아래 사각으로 채우기(라운드 적용X)
            return CreateRoundRectPath(rect, radius);
        }

        /// <summary>
        /// The CreateRoundRectBorder
        /// </summary>
        /// <param name="rect">The rect<see cref="Rectangle"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <param name="borderWidth">The borderWidth<see cref="int"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateRoundRectBorderPath(Rectangle rect, float radius, int borderWidth)
        {
            return CreateRoundRectPath(rect, radius, borderWidth);
        }

        /// <summary>
        /// The CreateRoundRect
        /// </summary>
        /// <param name="rect">The rect<see cref="RectangleF"/></param>
        /// <param name="radius">The radius<see cref="float"/></param>
        /// <returns>The <see cref="GraphicsPath"/></returns>
        public static GraphicsPath CreateShadowRoundRect(RectangleF rect, float radius)
        {
            return CreateShadowRoundRect(rect.X, rect.Y, rect.Width, rect.Height, radius);
        }


        /// <summary>
        /// The BlendColor
        /// </summary>
        /// <param name="backgroundColor">The backgroundColor<see cref="Color"/></param>
        /// <param name="frontColor">The frontColor<see cref="Color"/></param>
        /// <param name="blend">The blend<see cref="double"/></param>
        /// <returns>The <see cref="Color"/></returns>
        public static Color BlendColor(Color backgroundColor, Color frontColor, double blend)
        {
            var ratio = blend / 255d;
            var invRatio = 1d - ratio;
            var r = (int)((backgroundColor.R * invRatio) + (frontColor.R * ratio));
            var g = (int)((backgroundColor.G * invRatio) + (frontColor.G * ratio));
            var b = (int)((backgroundColor.B * invRatio) + (frontColor.B * ratio));
            return Color.FromArgb(r, g, b);
        }

        /// <summary>
        /// The BlendColor
        /// </summary>
        /// <param name="backgroundColor">The backgroundColor<see cref="Color"/></param>
        /// <param name="frontColor">The frontColor<see cref="Color"/></param>
        /// <returns>The <see cref="Color"/></returns>
        public static Color BlendColor(Color backgroundColor, Color frontColor)
        {
            return BlendColor(backgroundColor, frontColor, frontColor.A);
        }

        public static void DrawSquareShadow(Graphics g, Rectangle bounds)
        {
            using (SolidBrush shadowBrush = new SolidBrush(Color.FromArgb(12, 0, 0, 0)))
            {
                GraphicsPath path;
                path = DrawHelper.CreateShadowRoundRect(new RectangleF(bounds.X - 3.5f, bounds.Y - 1.5f, bounds.Width + 6, bounds.Height + 3), 8);
                g.FillPath(shadowBrush, path);
                path = DrawHelper.CreateShadowRoundRect(new RectangleF(bounds.X - 2.5f, bounds.Y - 1.5f, bounds.Width + 4, bounds.Height + 2), 6);
                g.FillPath(shadowBrush, path);
                path = DrawHelper.CreateShadowRoundRect(new RectangleF(bounds.X - 1.5f, bounds.Y - 0.5f, bounds.Width + 2, bounds.Height + 1), 4);
                g.FillPath(shadowBrush, path);
                path = DrawHelper.CreateShadowRoundRect(new RectangleF(bounds.X - 0.5f, bounds.Y + 1.5f, bounds.Width + 0, bounds.Height + 0), 4);
                g.FillPath(shadowBrush, path);
                path = DrawHelper.CreateShadowRoundRect(new RectangleF(bounds.X - 0.5f, bounds.Y + 2.5f, bounds.Width + 0, bounds.Height + 0), 4);
                g.FillPath(shadowBrush, path);
                path.Dispose();
            }
        }

        public static void DrawRoundShadow(Graphics g, Rectangle bounds)
        {
            using (SolidBrush shadowBrush = new SolidBrush(Color.FromArgb(12, 0, 0, 0)))
            {
                g.FillEllipse(shadowBrush, new Rectangle(bounds.X - 2, bounds.Y - 1, bounds.Width + 4, bounds.Height + 6));
                g.FillEllipse(shadowBrush, new Rectangle(bounds.X - 1, bounds.Y - 1, bounds.Width + 2, bounds.Height + 4));
                g.FillEllipse(shadowBrush, new Rectangle(bounds.X - 0, bounds.Y - 0, bounds.Width + 0, bounds.Height + 2));
                g.FillEllipse(shadowBrush, new Rectangle(bounds.X - 0, bounds.Y + 2, bounds.Width + 0, bounds.Height + 0));
                g.FillEllipse(shadowBrush, new Rectangle(bounds.X - 0, bounds.Y + 1, bounds.Width + 0, bounds.Height + 0));
            }
        }
    }
}