﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using KOSES_SEQ.OP;
using System.Collections.Specialized;
using System.Configuration;

namespace KOSES_SEQ
{
    static class Program
    {
        public static bool IsSimulation;
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool onlyOne = false;
            Mutex mutex = new Mutex(true, "KOSES_SEQ", out onlyOne);

            if (onlyOne)
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                if (appSettings["SIMULATION_MODE"] != null && bool.TryParse(appSettings["SIMULATION_MODE"], out bool simul))
                    IsSimulation = simul;

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(FRM.Main = new frmBase());
            }
            else
            {
                MessageBox.Show("프로그램이 실행중입니다.");
                return;
            }
        }
    }
}
