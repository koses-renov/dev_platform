﻿using KOSES_SEQ.MANAGER;
using KOSES_SEQ.OP;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KOSES_SEQ.HARDWARE
{
    /// <summary>
    /// Koses Tenkey 기본 I/O
    /// </summary>
    public class TenKeyIO
    {
        #region Input 버튼 누를 시 COL + ROW 의 조합으로 Keypad 판단
        public int _I_TENKEY_START_NO = -1;
        public int _I_TENKEY_COL_1 = -1;
        public int _I_TENKEY_COL_2 = -1;
        public int _I_TENKEY_COL_3 = -1;

        public int _I_TENKEY_ROW_1 = -1;
        public int _I_TENKEY_ROW_2 = -1;
        public int _I_TENKEY_ROW_3 = -1;
        public int _I_TENKEY_ROW_4 = -1;
        #endregion

        #region Output 기본 두자리 영역을 Display
        public int _O_TENKEY_START_NO = -1;

        // 0~9 (1의 자리)
        public int _O_TENKEY_LOW_BIT_0 = -1;
        public int _O_TENKEY_LOW_BIT_1 = -1;
        public int _O_TENKEY_LOW_BIT_2 = -1;
        public int _O_TENKEY_LOW_BIT_3 = -1;

        // 10~90 (10의 자리)
        public int _O_TENKEY_HIGH_BIT_0 = -1;
        public int _O_TENKEY_HIGH_BIT_1 = -1;
        public int _O_TENKEY_HIGH_BIT_2 = -1;
        public int _O_TENKEY_HIGH_BIT_3 = -1;
        #endregion


        /// <summary>
        /// 시작 I/O 번호 기준으로 1씩 증가
        /// </summary>
        /// <param name="startInput"></param>
        /// <param name="startOutput"></param>
        public void SetTenKeyIO(int startInput, int startOutput)
        {
            _I_TENKEY_START_NO = startInput;
            _I_TENKEY_COL_1 = startInput;
            _I_TENKEY_COL_2 = startInput + 1;
            _I_TENKEY_COL_3 = startInput + 2;

            _I_TENKEY_ROW_1 = startInput + 3;
            _I_TENKEY_ROW_2 = startInput + 4;
            _I_TENKEY_ROW_3 = startInput + 5;
            _I_TENKEY_ROW_4 = startInput + 6;

            _O_TENKEY_START_NO = startOutput;
            _O_TENKEY_LOW_BIT_0 = startOutput;
            _O_TENKEY_LOW_BIT_1 = startOutput + 1;
            _O_TENKEY_LOW_BIT_2 = startOutput + 2;
            _O_TENKEY_LOW_BIT_3 = startOutput + 3;

            _O_TENKEY_HIGH_BIT_0 = startOutput + 4;
            _O_TENKEY_HIGH_BIT_1 = startOutput + 5;
            _O_TENKEY_HIGH_BIT_2 = startOutput + 6;
            _O_TENKEY_HIGH_BIT_3 = startOutput + 7;
        }
    }

    public delegate void PushedTenkeyStepButton(int num, bool isPush);
    public delegate void PushedTenkeyHomeButton(int num, bool isPush);
    public delegate void DisplayChanged(int num);

    /// <summary>
    /// Koses Tenkey Class
    /// </summary>
    public class CTenkey
    {
        #region KeyPad Info
        /*****************************************************
         * 
         *   버튼 배치
         *   1   2   3
         *   4   5   6
         *   7   8   9
         * STEP  0  HOME
         * 
         * ***************************************************/

        const int ROW_COUNT = 4;
        const int COLUMN_COUNT = 3;

        protected readonly int[,] KeyPadArray = new int[ROW_COUNT, COLUMN_COUNT]
        {
            { 1,  2,  3 },
            { 4,  5,  6 },
            { 7,  8,  9 },
            { 10, 0, 11 },
        };

        protected enum TenKeyPad
        {
            None = -9,

            Num_0 = 0,
            Num_1 = 1,
            Num_2 = 2,
            Num_3 = 3,
            Num_4 = 4,
            Num_5 = 5,
            Num_6 = 6,
            Num_7 = 7,
            Num_8 = 8,
            Num_9 = 9,
            STEP = 10,
            HOME = 11,
        }

        protected bool IsDisableNumMode;

        /// <summary>
        /// Tenkey Home 버튼 눌림 (Jog -, Step Reset 용도?)
        /// </summary>
        public event PushedTenkeyHomeButton OnPushedTenkeyHomeButton;

        /// <summary>
        /// Tenkey Step 버튼 눌림 (Jog +, Step 진행 용도?)
        /// </summary>
        public event PushedTenkeyStepButton OnPushedTenkeyStepButton;

        /// <summary>
        /// Tenkey 번호 변경 됨
        /// </summary>
        public event DisplayChanged OnDisplayChanged;

        #endregion

        /// <summary>
        /// Digital I/O
        /// </summary>
        protected TenKeyIO _TenKeyIO;

        /// <summary>
        /// Key pad 이전 입력 값
        /// </summary>
        protected TenKeyPad _PrevKey = TenKeyPad.None;

        protected ushort _CurrentDisplayValue = 0;
        /// <summary>
        /// 현재 Tenkey에 Display 되는 값
        /// </summary>
        public ushort CurrentDisplayValue
        {
            get { return _CurrentDisplayValue; }
            set
            {
                _CurrentDisplayValue = value;
            }
        }

        protected ushort _RequestDisplayValue = 0;

        /// <summary>
        /// 변경할 Tenkey Display 값
        /// </summary>
        public ushort RequestDisplayValue
        {
            get { return _RequestDisplayValue; }
            set
            {
                _RequestDisplayValue = value;
            }
        }

        protected Thread _ThrCheckKey;

        public CTenkey()
        {
        }

        protected void RaiseOnPushedHomeButton(bool isPush)
        {
            Action action = new Action(delegate ()
            {
                OnPushedTenkeyHomeButton?.Invoke(CurrentDisplayValue, isPush);
            });
            action.BeginInvoke(null, null);
        }

        protected void RaiseOnPushedStepButton(bool isPush)
        {
            Action action = new Action(delegate ()
            {
                OnPushedTenkeyStepButton?.Invoke(CurrentDisplayValue, isPush);
            });
            action.BeginInvoke(null, null);
        }

        protected void RaiseOnDisplayChanged()
        {
            Action action = new Action(delegate ()
            {
                OnDisplayChanged?.Invoke(CurrentDisplayValue);
            });
            action.BeginInvoke(null, null);
        }

        public void SetTenkeyIO(TenKeyIO io)
        {
            _TenKeyIO = io;
            StartKeyCheckThread();
        }

        /// <summary>
        /// Loop Thread 시작
        /// </summary>
        protected void StartKeyCheckThread()
        {
            _ThrCheckKey = new Thread(LoopKeyCheck);
            _ThrCheckKey.IsBackground = true;
            _ThrCheckKey.Start();
        }

        /// <summary>
        /// 눌린 Pad 위치 확인
        /// </summary>
        /// <returns></returns>
        protected TenKeyPad GetPushedPad()
        {
            int startNo = _TenKeyIO._I_TENKEY_START_NO;
            int column = -1;
            int row = -1;
            TenKeyPad pushedPad;

            try
            {
                var value = SysOperator.Instance.motion.GetInputByte(startNo);

                BitArray bitArray = new BitArray(new byte[] { value });
                bool[] boolArray = new bool[bitArray.Length];

                bitArray.CopyTo(boolArray, 0);

                // Pad 눌림 이상 상태 체크( 기본적으로 Col 1개 Row 1개 On이 정상)
                if (boolArray.Where(a => a == true).Count() != 2)
                    return TenKeyPad.None;

                // 눌린 Pad의 열
                if (boolArray[_TenKeyIO._I_TENKEY_COL_1 - startNo])
                    column = 0;
                else if (boolArray[_TenKeyIO._I_TENKEY_COL_2 - startNo])
                    column = 1;
                else if (boolArray[_TenKeyIO._I_TENKEY_COL_3 - startNo])
                    column = 2;


                // 눌린 Pad의 행
                if (boolArray[_TenKeyIO._I_TENKEY_ROW_1 - startNo])
                    row = 0;
                else if (boolArray[_TenKeyIO._I_TENKEY_ROW_2 - startNo])
                    row = 1;
                else if (boolArray[_TenKeyIO._I_TENKEY_ROW_3 - startNo])
                    row = 2;
                else if (boolArray[_TenKeyIO._I_TENKEY_ROW_4 - startNo])
                    row = 3;


                if (!Enumerable.Range(0, ROW_COUNT).Contains(row)
                    || !Enumerable.Range(0, COLUMN_COUNT).Contains(column))
                {
                    return TenKeyPad.None;
                }

                // 해당 행 열 위치의 Pad 가져옴
                pushedPad = (TenKeyPad)KeyPadArray[row, column];
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return TenKeyPad.None;
            }

            return pushedPad;
        }

        /// <summary>
        /// 현재 Display 되는 1의 자리가 10의 자리가 되고
        /// 새로 누른 번호가 1의 자리가 됨
        /// </summary>
        /// <param name="no"></param>
        protected void SetNewNumber(int no)
        {
            var temp = _CurrentDisplayValue % 10;
            _RequestDisplayValue = (ushort)(temp * 10 + no);
        }

        /// <summary>
        /// 번호키 사용 막음
        /// </summary>
        /// <param name="isDisable"></param>
        public void ChangeDisableNumMode(bool isDisable)
        {
            IsDisableNumMode = isDisable;

            if (IsDisableNumMode)
                ResetDisplay();
        }

        /// <summary>
        /// Display "00" 초기화
        /// </summary>
        protected void ResetDisplay()
        {
            _RequestDisplayValue = 0;
            Display(false);
        }

        /// <summary>
        /// Tenkey 눌림 체크, Tenkey Display
        /// </summary>
        protected void LoopKeyCheck()
        {
            ResetDisplay();

            while (true)
            {
                try
                {
                    TenKeyPad pad = GetPushedPad();

                    if (_PrevKey != pad)
                    {
                        switch (pad)
                        {
                            case TenKeyPad.Num_0:
                            case TenKeyPad.Num_1:
                            case TenKeyPad.Num_2:
                            case TenKeyPad.Num_3:
                            case TenKeyPad.Num_4:
                            case TenKeyPad.Num_5:
                            case TenKeyPad.Num_6:
                            case TenKeyPad.Num_7:
                            case TenKeyPad.Num_8:
                            case TenKeyPad.Num_9:

                                // 번호 입력 불가 모드
                                if (IsDisableNumMode)
                                {
                                    continue;
                                }
                                else
                                {
                                    SetNewNumber((int)pad);
                                }
                                break;

                            case TenKeyPad.HOME:
                                RaiseOnPushedHomeButton(true);
                                break;

                            case TenKeyPad.STEP:
                                RaiseOnPushedStepButton(true);
                                break;

                            case TenKeyPad.None:
                                if (_PrevKey == TenKeyPad.HOME)
                                    RaiseOnPushedHomeButton(false);
                                else if (_PrevKey == TenKeyPad.STEP)
                                    RaiseOnPushedStepButton(false);
                                break;
                        }

                        _PrevKey = pad;
                        LogManager.Log(enumLogLevel.Info, pad + " Pushed.", this.GetType().Name);
                        this.Display();
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ExLog(ex);
                }

                Thread.Sleep(50);
            }
        }

        protected void Display(bool check = true)
        {
            try
            {
                ushort value = _RequestDisplayValue;
                if (!check || _CurrentDisplayValue != value)
                {
                    DisplayDecimal(value % 10, value / 10);
                    _CurrentDisplayValue = value;

                    RaiseOnDisplayChanged();
                }
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// 10진수 디스플레이
        /// </summary>
        /// <param name="low">1의 자리</param>
        /// <param name="high">10의 자리</param>
        public void DisplayDecimal(int low, int high)
        {
            if (high > 10 || low > 10)
                return;

            try
            {
                byte lowByte = BitConverter.GetBytes(low)[0];
                high <<= 4; // HIBYTE shift
                byte highByte = BitConverter.GetBytes(high)[0];

                var result = lowByte + highByte;

                // B 접점 반전
                result ^= 0xFF;

                SysOperator.Instance.motion.SetOutputByte(_TenKeyIO._O_TENKEY_START_NO, BitConverter.GetBytes(result)[0]);
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// 외부 로직에서 Home 버튼 눌림으로 요청
        /// </summary>
        /// <param name="num"></param>
        public void ExternRequestHome(int num)
        {
            _RequestDisplayValue = (ushort)num;
            Display();

            _PrevKey = TenKeyPad.HOME;
            RaiseOnPushedHomeButton(true);
        }

        /// <summary>
        /// 외부 로직에서 Step 버튼 눌림으로 요청
        /// </summary>
        /// <param name="num"></param>
        public void ExternRequestStep(int num)
        {
            _RequestDisplayValue = (ushort)num;
            Display();

            _PrevKey = TenKeyPad.STEP;
            RaiseOnPushedStepButton(true);
        }
    }
}
