﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.HARDWARE.MC;
//using KOSES_SEQ.HARDWARE.MC.AJIN;
//using static KOSES_SEQ.HARDWARE.MC.AJIN.AJIN_PEM_DEFINE;
using KOSES_SEQ.CONFIG;
using System.Diagnostics;
using KOSES_SEQ.OP;
using wmxCLRLibrary;
using EcApiCLRLib;

namespace KOSES_SEQ.MANAGER
{
    public enum AnalogDataType
    {
        WeidmullerS5 = 16384,
        WeidmullerS7 = 27648,
        Delta = 65535,

        //WAGO_AO_Volt = 32767,
        WAGO_AO_Volt = 65535,   // 16bit
        WAGO_AO_Current = 32768, // 15bit setting할시
        WAGO_AI_Volt = 32760,
        WAGO_AI_Current = 32760
    }

    public class MotionController_WMX2 : MotionController
    {
        static string MOTION_CFG_PATH = ".\\MotionConfig.xml";
        static string MOTION_EX_PATH = "C:\\Program Files\\SoftServo\\WMX2\\";

        #region Parameter로 뺄 것
        static int SLAVE_COUNT = 32;
        static int DIGITAL_BYTE = 8;
        static short DIGITAL_COUNT = 26;
        #endregion

        private WMXLib _Wmx2Lib = new WMXLib();
        private EcApiLib _EcApi = new EcApiLib();

        private wmxCLRLibrary.Status _ServoStatus = new wmxCLRLibrary.Status();
        private ActiveDeviceList _ActiveDeviceList = new ActiveDeviceList();
        private static EcMasterInfo _EcMasterInfo = new EcMasterInfo();

        private Thread UpdateStatus;

        private AnalogDataType _InputAnalogDataType = AnalogDataType.WAGO_AI_Volt;
        public AnalogDataType InputAnalogDataType
        {
            get { return _InputAnalogDataType; }
            set
            {
                _InputAnalogDataType = value;
            }
        }

        private AnalogDataType _OutputAnalogDataType = AnalogDataType.WAGO_AO_Current;
        public AnalogDataType OutputAnalogDataType
        {
            get { return _OutputAnalogDataType; }
            set
            {
                _OutputAnalogDataType = value;
            }
        }

        public MotionController_WMX2()
        {
            StartUpdateStatus();
        }

        private void StartUpdateStatus()
        {
            UpdateStatus = new Thread(UpdateStatusLoop);
            UpdateStatus.IsBackground = true;
            UpdateStatus.Start();
        }

        private void UpdateStatusLoop()
        {
            while (true)
            {
                try
                {
                    if (IsOpen())
                    {
                        _Wmx2Lib.GetStatus(ref _ServoStatus);
                    }
                }
                catch (Exception ex)
                {
                    Console.Write("WMX2 OPEN FAIL!!!");
                }

                Thread.Sleep(5);
            }
        }

        public override void SetMotionController()
        {
        }

        public override bool IsOpen()
        {
            Status status = null;
            _Wmx2Lib.GetStatus(ref status);

            if (status!= null && status.OpState[0] == OperationState.Offline)
                return false;

            return true;
        }

        public override bool MotionControllerOpen()
        {
            int error = 0;
            try
            {
                error = _Wmx2Lib.GetActiveDeviceList(ref _ActiveDeviceList);
                if (error != (int)ErrorCode.None)
                {
                    return false;
                }

                if (_ActiveDeviceList.Count > 1)
                {
                    for (int i = 1; i < _ActiveDeviceList.Count; i++)
                    {
                        if (0 == string.Compare(_ActiveDeviceList.NameList[i], "Wmx2_Lib", true))
                            _Wmx2Lib.ExecForceCloseDevice(_ActiveDeviceList.IDList[i]);
                    }
                }

                // 디바이스 생성
                error = _Wmx2Lib.CreateDeviceEx(MOTION_EX_PATH, PlatformType.EtherCATEngine);
                if (error != (int)ErrorCode.None)
                {
                    return false;
                }

                // Set Device Name.
                _Wmx2Lib.SetDeviceName("device");

                // Get created device state.
                _Wmx2Lib.GetActiveDeviceList(ref _ActiveDeviceList);

                // 설정 파일 로드 (경로 확인 필요)
                error = _Wmx2Lib.config.ImportAndSetAll(MOTION_CFG_PATH);
                if (error != (int)ErrorCode.None)
                {
                    return false;
                }

                // 통신 시작
                error = _Wmx2Lib.StartCommunication();
                if (error != (int)ErrorCode.None)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write("WMX2 MOTION OPEN FAIL!!!");
            }

            return true;
        }

        /*-------------------------------------------------------------------*/
        /* Function    : IsAllAlive                                          */
        /* Description : Check if all slaves are alive.                      */
        /*-------------------------------------------------------------------*/
        private bool IsAllAlive(ref bool isLost)
        {
            bool bRet = true;

            _EcApi.GetStatus(_EcMasterInfo);//GetMasterInfo(_EcMasterInfo);

            // Check Slave alive.
            isLost = false;

            if (_EcMasterInfo.NumOfSlaves < SLAVE_COUNT)
                return false;

            for (int i = 0; i < _EcMasterInfo.NumOfSlaves; i++)
            {
                int error = 0;
                if (_EcMasterInfo.Slaves[i].State != EcStateMachine.Op)
                {
                    if (_EcMasterInfo.Slaves[i].State == EcStateMachine.PreOp || _EcMasterInfo.Slaves[i].State == EcStateMachine.SafeOp)
                    {
                        _EcApi.ChangeSlaveState(i, EcStateMachine.Op, ref error);
                    }

                    bRet = false;

                    // Check Slave lost. 
                    if ((_EcMasterInfo.Slaves[i].State == EcStateMachine.None) ||
                         (_EcMasterInfo.Slaves[i].State == EcStateMachine.Init))
                    {
                        isLost = true;
                    }
                }
            }

            return bRet;
        }

        bool _IsAlive;
        bool _PrevAlive;
        public int CheckServoConnection()
        {
            //int error = (int)ErrorCode.None;
            try
            {
                int iTick = Environment.TickCount;

                MOTION.Motion_Wait timer = new MOTION.Motion_Wait(30000, Def.Code.OK);

                while (true)
                {
                    bool isLost = false;
                    // Check if all slaves are alive.
                    _IsAlive = IsAllAlive(ref isLost);
                    if (_IsAlive)
                    {
                        Console.WriteLine("All the slaves are alive.");
                    }
                    else
                    {
                        Console.WriteLine("One of the slave is not alive.");
                    }

                    _ = _EcApi.StartHotconnect();

                    // Check slave revived.
                    if (_IsAlive && !_PrevAlive)
                    {
                        Console.WriteLine("Slave revived.");
                    }

                    //-------------------------------------------------------
                    // Since an amp alarm may occur on the Slave side, 
                    // Please clear the amp alarm.
                    //-------------------------------------------------------

                    _PrevAlive = _IsAlive;
                    Thread.Sleep(1000);

                    if (_IsAlive)
                        break;

                    if (timer.IsBreak)
                    {
                        return (int)Def.Code.CODE_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return (int)Def.Code.OK;
        }

        public override void MotionControllerClose()
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            _Wmx2Lib.StopCommunication();
            Thread.Sleep(1000);
            _Wmx2Lib.CloseDevice();
        }

        private short GetInputModuleNo(int port)
        {
            short moduleNo = (short)(port / DIGITAL_BYTE);

            return (short)(moduleNo);
        }

        private short GetInputPortNo(int port)
        {
            return (short)(port % DIGITAL_BYTE);
        }

        private short GetOutputModuleNo(int port)
        {
            short moduleNo = (short)(port / DIGITAL_BYTE);

            return (short)(moduleNo);
        }

        private short GetOutputPortNo(int port)
        {
            return (short)(port % DIGITAL_BYTE);
        }

        public override bool GetInput(int port, Def.SensorType type = Def.SensorType.A)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            //b = axl.GetInput(port);
            byte ioData = 0;
            int error = _Wmx2Lib.io.GetInBit(GetInputModuleNo(port), GetInputPortNo(port), ref ioData);

            b = ioData > 0;
            if (type == Def.SensorType.B)
            {
                b = !b;
            }
            return b;
        } 

        public override bool GetOutput(int port)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            //b = axl.GetInput(port);
            byte ioData = 0;
            int error = _Wmx2Lib.io.GetOutBit(GetOutputModuleNo(port), GetOutputPortNo(port), ref ioData);

            b = ioData > 0;

            return b;
        }

        public override void SetOutput(int port, bool b)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}
            byte value = b ? (byte)1 : (byte)0;
            _Wmx2Lib.io.SetOutBit(GetOutputPortNo(port), GetOutputPortNo(port), value);
            //axl.SetOutput(port, b);
        }

        public override byte GetInputByte(int port)
        {
            if (port % 8 != 0)
                return 0;

            byte ioData = 0;

            int iRet = _Wmx2Lib.io.GetInByte(GetInputModuleNo(port), ref ioData);
            if (iRet != CReturnCode.Success)
            {
                string errString = string.Empty;
                _Wmx2Lib.GetLastErrorString(ref errString, 0);
                LogManager.Log(enumLogLevel.Error, $"GetInputByte Failed. [{_Wmx2Lib.GetLastError()}][{errString}]", this.GetType().Name);
            }

            return ioData;
        }

        public override byte GetOutputByte(int port)
        {
            if (port % 8 != 0)
                return 0;

            byte ioData = 0;

            int iRet = _Wmx2Lib.io.GetOutByte(GetInputModuleNo(port), ref ioData);
            if (iRet != CReturnCode.Success)
            {
                string errString = string.Empty;
                _Wmx2Lib.GetLastErrorString(ref errString, 0);
                LogManager.Log(enumLogLevel.Error, $"GetOutputByte Failed. [{_Wmx2Lib.GetLastError()}][{errString}]", this.GetType().Name);
            }

            return ioData;
        }

        public override void SetOutputByte(int port, byte value)
        {
            if (port % 8 != 0)
                return;

            int iRet = _Wmx2Lib.io.SetOutByte(GetOutputModuleNo(port), value);
            if (iRet != CReturnCode.Success)
            {
                string errString = string.Empty;
                _Wmx2Lib.GetLastErrorString(ref errString, 0);
                LogManager.Log(enumLogLevel.Error, $"SetOutputByte Failed. [{_Wmx2Lib.GetLastError()}][{errString}]", this.GetType().Name);
            }
        }

        public override double GetInputAnalog(int port)
        {
            double val = 0;

            try
            {
                byte[] ioData = new byte[2];
                int iRet = _Wmx2Lib.io.GetInBytes((short)(port * 2 + DIGITAL_COUNT), 2, ref ioData);
                if (iRet != (int)ErrorCode.None)
                {
                }

                val = (double)(ioData[0] | (ioData[1] << 8)) / (int)_InputAnalogDataType * 10; // 파라미터화 할 것
            }
            catch (Exception ex)
            {
            }

            return val;
        }

        public override double GetOutputAnalog(int port)
        {
            double val = 0;

            try
            {
                byte[] ioData = new byte[2];
                int iRet = _Wmx2Lib.io.GetOutBytes((short)(port * 2 + DIGITAL_COUNT), 2, ref ioData);
                if (iRet != (int)ErrorCode.None)
                {
                }

                val = (double)(ioData[0] | (ioData[1] << 8)) / (int)_OutputAnalogDataType * 10; // 파라미터화 할 것
            }
            catch (Exception ex)
            {
            }

            return val;
        }

        public override void SetOutputAnalog(int port, double voltage)
        {
            try
            {
                ushort val = (ushort)(voltage / 10 * (int)_OutputAnalogDataType);
                byte[] ioData = new byte[2];
                //if(ioData.Length > 2)
                //{
                //    LogManager.Log(enumLogLevel.Error, $"SetAnalogOutput Byte Length Failed. [{_WmxLib.GetLastError()}]", this.GetType().Name);
                //    return;
                //}
                ioData[0] = (byte)((ushort)val & 0xff);
                ioData[1] = (byte)(((ushort)val >> 8) & 0xff);

                int iRet = _Wmx2Lib.io.SetOutBytes((short)(port * 2 + DIGITAL_COUNT), 2, ioData);
                if (iRet != (int)ErrorCode.None)
                {
                }
            }
            catch (Exception ex)
            {
            }
        }

        public override double GetCmdPos(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return 0.0;
            //}

            if (_ServoStatus != null && _ServoStatus.PosCommand.Length > (int)motor)
            {
                return _ServoStatus.PosCommand[(int)motor];
            }
            else
            {
                return 0.0;
            }
        }

        public override double GetEncPos(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return 0.0;
            //}

            if (_ServoStatus != null && _ServoStatus.ActualPos.Length > (int)motor)
            {
                return _ServoStatus.ActualPos[(int)motor];
            }
            else
            {
                return 0.0;
            }
        }

        #region MoveAbs
        /// <summary>
        /// 모터 파라메터 속도에 입력 비율로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="speedRate"></param>
        /// <returns></returns>
        public override bool MoveAbs(Def.Motor motor, double pos, int speedRate = 100)
        {
            bool b = false;
            double acc = 0;
            double vel = 0;
            double dec = 0;
            Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

            //  전역 비율 속도
            b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
            if (b == false)
            {
                return false;
            }

            //  입력 비율 속도
            acc = acc * speedRate / 100;
            vel = vel * speedRate / 100;
            dec = dec * speedRate / 100;

            b = MoveAbs(motor, pos, acc, vel, dec, profile);
            if (false == b)
            {
                return false;
            }
            return true;
        }

        //public bool MoveAbs_Block(Def.Motor motor, double pos, int speedRate = 100)
        //{
        //    bool b = false;
        //    double acc = 0;
        //    double vel = 0;
        //    double dec = 0;
        //    Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

        //    //  전역 비율 속도
        //    b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
        //    if (b == false)
        //    {
        //        return false;
        //    }

        //    //  입력 비율 속도
        //    acc = acc * speedRate / 100;
        //    vel = vel * speedRate / 100;
        //    dec = dec * speedRate / 100;

        //    b = MoveAbs_Block(motor, pos, acc, vel, dec, profile);
        //    if (false == b)
        //    {
        //        return false;
        //    }
        //    return true;
        //}
        /// <summary>
        /// 입력 속도로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public override bool MoveAbs(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p)
        {
            bool b = false;

            int error = _Wmx2Lib.jerkRatioMotion.StartPos((short)motor, ProfileType.SCurve, pos, vel, acc, dec, 0.9, 0.9, 0, 0);
            if (error != (int)ErrorCode.None)
            {
                Stop(motor);
            }

            b = true;
            Thread.Sleep(20);
            return b;
        }

        public override bool MoveAbs_Block(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p)
        {
            return MoveAbs(motor, pos, acc, vel, dec, p);
        }
        #endregion

        #region MoveRel
        ///// <summary>
        ///// 모터 파라메터 속도에 입력 비율로 상대위치 이동
        ///// </summary>
        ///// <param name="motor"></param>
        ///// <param name="dir"></param>
        ///// <param name="dis"></param>
        ///// <param name="speedRate"></param>
        ///// <returns></returns>
        //public bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, int speedRate = 100)
        //{
        //    bool b = false;
        //    double acc = 0;
        //    double vel = 0;
        //    double dec = 0;
        //    Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

        //    //  전역 비율 속도
        //    b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
        //    if (false == b)
        //    {
        //        return false;
        //    }

        //    //  입력 비율 속도
        //    acc = acc * speedRate / 100;
        //    vel = vel * speedRate / 100;
        //    dec = dec * speedRate / 100;

        //    b = MoveRel(motor, dir, dis, acc, vel, dec, profile);
        //    Thread.Sleep(20);
        //    return b;
        //}

        /// <summary>
        /// 입력 속도로 상대 위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="dis"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public override bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, double acc, double vel, double dec, Def.MotorProfile profile)
        {
            bool b = false;
            if (Def.MotorDir.MINUS == dir)
            {
                dis = -dis;
            }

            double target = GetCmdPos(motor) + dis;

            b = MoveAbs(motor, target, acc, vel, dec, profile);
            if (b == false)
            {
                Stop(motor);
            }
            Thread.Sleep(20);
            return b;
        }
        #endregion

        #region MoveJog
        ///// <summary>
        ///// 모터 파라메터 속도에 입력 비율로 속도 이동
        ///// </summary>
        ///// <param name="motor"></param>
        ///// <param name="dir"></param>
        ///// <returns></returns>
        //public bool MoveJog(Def.Motor motor, Def.MotorDir dir, int speedRate = 100)
        //{
        //    bool b = false;
        //    double acc = 0;
        //    double vel = 0;
        //    double dec = 0;

        //    //  전역 비율 속도
        //    b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
        //    if (false == b)
        //    {
        //        return false;
        //    }

        //    //  입력 비율 속도
        //    acc = acc * speedRate / 100;
        //    vel = vel * speedRate / 100;
        //    dec = dec * speedRate / 100;

        //    b = MoveJog(motor, dir, acc, vel, dec);
        //    return b;
        //}

        /// <summary>
        /// 입력 속도로 속도 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <returns></returns>
        public override bool MoveJog(Def.Motor motor, Def.MotorDir dir, double acc, double vel, double dec)
        {
            bool b = false;

            if (dir == Def.MotorDir.MINUS)
            {
                vel *= -1;
            }

            int error = _Wmx2Lib.jerkRatioMotion.StartJog((short)motor, ProfileType.JerkLimited, vel, acc, 0.9);
            if (error != (int)ErrorCode.None)
            {
                return false;
            }
            b = true;
            return b;
        }
        #endregion



        public Def.Code MoveAbsWait_TorqueLimit(Def.Motor motor, double destPos, double torqueLimit)
        {
            throw new NotImplementedException();
            return MoveWait_TorqueLimit(motor, destPos, torqueLimit);
        }
        //public Def.Code MoveAbsWait(Def.Motor motor, double destPos, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        //{
        //    return MoveWait(motor, destPos, inputCheck1, inputCheck2);
        //}
        //public Def.Code MoveRelWait(Def.Motor motor, double relValue, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        //{
        //    double curPos = GetEncPos(motor);
        //    double destPos = curPos + relValue;
        //    return MoveWait(motor, destPos, inputCheck1, inputCheck2);
        //}

        //Def.Code MoveWait(Def.Motor motor, double destPos, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        //{          
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    double movetimeover = 10000;
        //    //double movetimeover = Program.teaching.motor.moveWaitTime;
        //    //double tolerance = Program.teaching.motor.moveTolerance;
        //    double tolerance = 0.05;

        //    bool b;
        //    bool bInputCheck;

        //    while (true)
        //    {
        //        Thread.Sleep(10);

        //        if (sw.ElapsedMilliseconds > movetimeover)
        //        {
        //            Stop(motor);
        //            Def.Code code = GetMotorTimeOver(motor);
        //            return code;
        //        }

        //        if (SysOperator.GetUserStop())
        //        {
        //            Stop(motor);
        //            return Def.Code.USER_STOP;
        //        }

        //        if (IsStop(motor) == false)
        //        {
        //            continue;
        //        }


        //        ////////////////////////////////
        //        /// 모터 정지 상태
        //        ////////////////////////////////
        //        if (IsAlarm(motor) == true)
        //        {
        //            Stop(motor);
        //            return Def.Code.MOTOR_ALARM;
        //        }

        //        if (inputCheck1 != Def.NotUsed)
        //        {
        //            bInputCheck = GetInput(inputCheck1, Def.SensorType.B);
        //            if (bInputCheck)
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTOR_EXT_SENSOR_1;
        //            }
        //        }

        //        if (inputCheck2 != Def.NotUsed)
        //        {
        //            bInputCheck = GetInput(inputCheck2);
        //            if (bInputCheck)
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTOR_EXT_SENSOR_2;
        //            }
        //        }

        //        if (SysOperator.motorPara.para[(int)motor].motorType == (int)Def.ServoType.SERVO)
        //        {
        //            b = IsPosition(motor, destPos, tolerance);
        //            if (b)
        //            {
        //                ////////////////////////////////
        //                /// 모터 정상 정지
        //                ////////////////////////////////
        //                return Def.Code.OK;
        //            }
        //            else
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTION_MOVE_FAIL;
        //            }
        //        }

        //        ////////////////////////////////
        //        /// 모터 정상 정지
        //        ////////////////////////////////
        //        return Def.Code.OK;
        //    }
        //}

        //Def.Code MoveWait_TorqueLimit(Def.Motor motor, double destPos, double torqueLimit)
        //{          
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    double movetimeover = 10000;
        //    //double movetimeover = Program.teaching.motor.moveWaitTime;

        //    while (true)
        //    {
        //        if (GetInMotionStatus(motor) == false)
        //        {
        //            if (IsAlarm(motor) == true)
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTOR_ALARM;
        //            }

        //            if(IsTorqueOver(motor, torqueLimit))
        //            {
        //                EmoStop(motor);
        //            }

        //            break;
        //        }

        //        if (sw.ElapsedMilliseconds > movetimeover)                   
        //        {
        //            Stop(motor);
        //            Def.Code code = GetMotorTimeOver(motor);
        //            return code;
        //        }

        //        if (SysOperator.GetUserStop())
        //        {
        //            Stop(motor);
        //            return Def.Code.USER_STOP;
        //        }
        //        Thread.Sleep(1);
        //    }

        //    return Def.Code.OK;
        //}

        //Def.Code GetMotorTimeOver(Def.Motor motor)
        //{
        //    return Def.GetMotorError(motor);
        //}

        public override bool IsPosition(Def.Motor motor, double destPos, double tolerance)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            if (SysOperator.Instance.motorPara.para[(int)motor].motorType == (int)Def.ServoType.STEP)
            {
                return true;
            }

            double curPos = GetEncPos(motor);

            bool b;
            b = Def.IsRange(curPos, destPos, tolerance);
            return b;
        }


        public override void SetTorqueLimit(Def.Motor motor, double plusTorqueLimit, double minusTorqueLimit)
        {
            throw new NotImplementedException();
            //axl.SetTorqueLimit((int)motor, plusTorqueLimit, minusTorqueLimit);
            //Thread.Sleep(10);
        }


        public override void Stop(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            try
            {
                _Wmx2Lib.basicMotion.Stop((int)motor);
            }
            catch (Exception ex)
            {

            }
        }

        public override void EmoStop(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            try
            {
                _Wmx2Lib.basicMotion.ExecQuickStop((int)motor);
            }
            catch (Exception ex)
            {

            }
        }

        //public bool IsStop(Def.Motor motor)
        //{
        //    bool b = false;
        //    if (GetInPositionStatus(motor) == true && GetInMotionStatus(motor) == false)
        //    {
        //        b = true;
        //    }
        //    return b;
        //}

        public override bool GetInPositionStatus(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;
            b = Convert.ToBoolean(_ServoStatus.InPos[(int)motor]);//axl.GetInPositionStatus((int)motor);
            return b;
        }

        public override bool GetInMotionStatus(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = !(_ServoStatus.OpState[(int)motor] == OperationState.Idle);//axl.GetInMotionStatus((int)motor);
            return b;
        }

        public override bool Home(Def.Motor motor)
        {
            MotorParameterData motorParaData = new MotorParameterData(null);
            GetMotorPara(motor, out motorParaData);

            bool b = false;

            int error = _Wmx2Lib.home.StartHome((int)motor);
            if (error != (int)ErrorCode.None)
            {
                return false;
            }

            b = true;

            if (b == false)
            {
                //Program.MessageBox($"HOME", "HOME FAILED", DefColor.LightRed);
            }

            return b;
        }

        public override bool HomeWait(Def.Motor motor)
        {          
            Stopwatch sw = new Stopwatch();
            sw.Start();

            double movetimeover = 10000;
            //double movetimeover = Program.teaching.motor.homeWaitTime;

            int nStopCount = 5;

            while (true)
            {
                if (true == IsHomeCompleted(motor) && true == IsStop(motor))
                {
                    break;
                }

                if (sw.ElapsedMilliseconds > movetimeover)                  
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }

                if (SysOperator.GetUserStop() == true)
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }
                Thread.Sleep(1);
            }

            return true;
        }

        public override bool IsHomeCompleted(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;

            b = Convert.ToBoolean(_ServoStatus.HomeDone[(int)motor]);
            return b;
        }

        //public bool IsHomeAllCompleted(ref string motorName)
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return true;
        //    //}

        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        if (false == IsHomeCompleted(motor))
        //        {
        //            motorName = motor.ToString();
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        public override double GetCurrentTorque(Def.Motor motor)
        {

            throw new NotImplementedException();
            //double curTorque;

            //curTorque = axl.GetCurrentTorque((int)motor);
            //return curTorque;
        }
        //public bool IsTorqueOver(Def.Motor motor, double torqueLimit)
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return false;
        //    //}

        //    double curTorque = GetCurrentTorque(motor);
        //    if (torqueLimit <= curTorque)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        public override bool IsAlarm(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;

            b = Convert.ToBoolean(_ServoStatus.AmpAlarm[(int)motor]);
            return b;

            //bool b = false;
            //b = axl.GetAlarmStatus((int)motor);
            //return b;
        }

        //public void StopAll(int stopCount = 3)
        //{
        //    for (int i = 0; i < stopCount; i++)
        //    {
        //        foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //        {
        //            Stop(motor);
        //        }
        //        Thread.Sleep(10);
        //    }
        //}
        //public void EmoStopAll(int stopCount = 1)
        //{
        //    for (int i = 0; i < stopCount; i++)
        //    {
        //        foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //        {
        //            EmoStop(motor);
        //        }
        //        Thread.Sleep(10);
        //    }
        //}


        public override bool IsPlusSensor(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;

            b = Convert.ToBoolean(_ServoStatus.PositiveLS[(int)motor]);
            return b;

            //bool b = false;
            //b = axl.GetPlusLimitSensorStatus((int)motor);
            //return b;
        }

        public override bool IsHomeSensor(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;

            b = Convert.ToBoolean(_ServoStatus.HomeSwitch[(int)motor]);
            return b;

            //bool b = false;
            //b = axl.GetHomeSensorStatus((int)motor);
            //return b;
        }

        public override bool IsMinusSensor(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;

            b = Convert.ToBoolean(_ServoStatus.NegativeLS[(int)motor]);
            return b;

            //bool b = false;
            //b = axl.GetMinusLimitSensorStatus((int)motor);
            //return b;
        }

        //public bool IsServoOnAll()
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return true;
        //    //}

        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        if (IsServoOn(motor) == false)
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        public override bool IsServoOn(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;

            b = Convert.ToBoolean(_ServoStatus.ServoOn[(int)motor]);
            return b;

            //bool b = false;
            //b = axl.GetServoStatus((int)motor);
            //return b;
        }

        public override void ServoOn(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            _Wmx2Lib.axisControl.SetServoOn((int)motor, 1);
            //axl.SetServo((int)motor, true);
        }

        public override void ServoOff(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            _Wmx2Lib.axisControl.SetServoOn((int)motor, 0);

            //axl.SetServo((int)motor, false);
        }

        //public void ServoOnAll()
        //{
        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        ServoOn(motor);
        //    }
        //}

        //public void ServoOffAll()
        //{
        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        ServoOff(motor);
        //    }
        //}

        //public void AlarmClearAll()
        //{
        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        ResetAlarm(motor);
        //    }
        //}

        public override void SetPosZero(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            _Wmx2Lib.home.SetCommandPos((short)motor, 0.0);
            _Wmx2Lib.home.SetFeedbackPos((short)motor, 0.0);

            //axl.SetPosZero((int)motor);
        }

        public override void ResetAlarm(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            _Wmx2Lib.axisControl.ClearAmpAlarm((short)motor);
            _Wmx2Lib.axisControl.ClearAxisAlarm((short)motor);

            //axl.ResetAlarm((int)motor);
        }

        //public void SignalSetInpos(Def.Motor motor, int level)
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return;
        //    //}
        //    wmxCLRLibrary.common.AxisParam param = new wmxCLRLibrary.common.AxisParam();
        //    _Wmx2Lib.config.GetAxisParam((short)motor, ref param);

        //    param.

        //    _Wmx2Lib.config.SetAxisParam((short)motor, )

        //    axl.SignalSetInpos((int)motor, level);
        //}

        public override void SetParameter(Def.Motor motor)
        {
            ////if (Program.config.virtualMode)
            ////{
            ////    return;
            ////}

            //wmxCLRLibrary.common.SystemParam param = new wmxCLRLibrary.common.SystemParam();
            //_Wmx2Lib.config.GetDefaultParam(ref param);

            //// 지정 축의 펄스 당 움직이는 거리를 설정한다.
            ////CAXM.AxmMotSetMoveUnitPerPulse(m, para.unitPerPulse, 1);


            //// 주의사항: 최소속도를 UNIT/PULSE 보다 작게할 경우 최소단위가 UNIT/PULSE로 맞추어지기때문에 최소 속도가 UNIT/PULSE 가 된다.
            //// 지정 축에 초기 속도를 설정한다.
            ////CAXM.AxmMotSetMinVel(m, 1);
            //param.MotionParam[(short)motor].MinVelocity = 1;


            //// 지정 축에 등속도 구동 함수에서의 최고 속도를 설정한다.
            //// 주의사항 : 입력 최대 속도 값이 PPS가 아니라 UNIT 이다.
            //// ex) 최대 출력 주파수(PCI-N804/404 : 10 MPPS)
            //// ex) 최대 출력 Unit/Sec(PCI-N804/404 : 10MPPS * Unit/Pulse)
            ////CAXM.AxmMotSetMaxVel(m, para.vel);
            ////param.MotionParam[(short)motor].EndVelocity = 1;

            //// 지정 축의 펄스 출력 방식을 설정한다.
            //// uMethod  0 :OneHighLowHigh, 1 :OneHighHighLow, 2 :OneLowLowHigh, 3 :OneLowHighLow, 4 :TwoCcwCwHigh
            ////          5 :TwoCcwCwLow, 6 :TwoCwCcwHigh, 7 :TwoCwCcwLow, 8 :TwoPhase, 9 :TwoPhaseReverse
            ////        OneHighLowHigh                = 0x0,                    // 1펄스 방식, PULSE(Active High), 정방향(DIR=Low)  / 역방향(DIR=High)
            ////        OneHighHighLow                = 0x1,                    // 1펄스 방식, PULSE(Active High), 정방향(DIR=High) / 역방향(DIR=Low)
            ////        OneLowLowHigh                 = 0x2,                    // 1펄스 방식, PULSE(Active Low),  정방향(DIR=Low)  / 역방향(DIR=High)
            ////        OneLowHighLow                 = 0x3,                    // 1펄스 방식, PULSE(Active Low),  정방향(DIR=High) / 역방향(DIR=Low)
            ////        TwoCcwCwHigh                  = 0x4,                    // 2펄스 방식, PULSE(CCW:역방향),  DIR(CW:정방향),  Active High
            ////        TwoCcwCwLow                   = 0x5,                    // 2펄스 방식, PULSE(CCW:역방향),  DIR(CW:정방향),  Active Low
            ////        TwoCwCcwHigh                  = 0x6,                    // 2펄스 방식, PULSE(CW:정방향),   DIR(CCW:역방향), Active High
            ////        TwoCwCcwLow                   = 0x7,                    // 2펄스 방식, PULSE(CW:정방향),   DIR(CCW:역방향), Active Low
            ////        TwoPhase                      = 0x8,                    // 2상(90' 위상차),  PULSE lead DIR(CW: 정방향), PULSE lag DIR(CCW:역방향)
            ////        TwoPhaseReverse               = 0x9                     // 2상(90' 위상차),  PULSE lead DIR(CCW: 정방향), PULSE lag DIR(CW:역방향)
            ////CAXM.AxmMotSetPulseOutMethod(m, (uint)para.pulseMode);

            //// 지정 축의 가속도 단위를 설정한다.
            //// AccelUnit : UNIT_SEC2   '0' - 가감속 단위를 unit/sec2 사용
            ////             SEC         '1' - 가감속 단위를 sec 사용
            ////CAXM.AxmMotSetAccelUnit(m, (uint)para.accUnit);

            //// 지정 축의 end limit sensor의 사용 유무 및 신호의 입력 레벨을 설정한다. 
            //// end limit sensor 신호 입력 시 감속정지 또는 급정지에 대한 설정도 가능하다.
            //// uStopMode: EMERGENCY_STOP(0), SLOWDOWN_STOP(1)
            //// uPositiveLevel, uNegativeLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)
            ////CAXM.AxmSignalSetLimit(m, 0, (uint)para.plusSignalLevel, (uint)para.minusSignalLevel);
            //param.LimitParam[(short)motor].PositiveLSType

            //// 지정 축의 Inpositon 신호 사용 여부 및 신호 입력 레벨을 설정한다
            //// uLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)
            //CAXM.AxmSignalSetInpos(m, (uint)para.inposSignalLevel);

            //// 지정 축의 Home 센서 Level 을 설정한다.
            //// uLevel : LOW(0), HIGH(1)
            //CAXM.AxmHomeSetSignalLevel(m, (uint)para.homeSignalLevel);

            //// 지정 축의 알람 신호 입력 시 비상 정지의 사용 여부 및 신호 입력 레벨을 설정한다.
            //// uLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)
            //CAXM.AxmSignalSetServoAlarm(m, (uint)para.alarmSignalLevel);

            //// 지정 축의 Servo-On신호의 출력 레벨을 설정한다.
            //// uLevel : LOW(0), HIGH(1)
            //CAXM.AxmSignalSetServoOnLevel(m, (uint)para.servoSignalLevel);

            //// 지정 축의 외부(Actual) 카운트의 증가 방향 설정을 포함하여 지정 축의 Encoder 입력 방식을 설정한다.
            //// uMethod : 0 - 7 설정.
            ////        ObverseUpDownMode             = 0x0,                     // 정방향 Up/Down
            ////        ObverseSqr1Mode               = 0x1,                     // 정방향 1체배
            ////        ObverseSqr2Mode               = 0x2,                     // 정방향 2체배
            ////        ObverseSqr4Mode               = 0x3,                     // 정방향 4체배
            ////        ReverseUpDownMode             = 0x4,                     // 역방향 Up/Down
            ////        ReverseSqr1Mode               = 0x5,                     // 역방향 1체배
            ////        ReverseSqr2Mode               = 0x6,                     // 역방향 2체배
            ////        ReverseSqr4Mode               = 0x7                      // 역방향 4체배
            //CAXM.AxmMotSetEncInputMethod(m, (uint)para.encoderCount);


            ////  급정지 신호의 사용우무 및 Active Level 설정
            ////CAXM.AxmSignalSetStop(m, (uint)para.)

            ////
            ////CAXM.Axm

            //axl.SetMotorParameter((int)motor, SysOperator.Instance.motorPara.para[(int)motor]);
        }

        //public void SetParameterAll()
        //{
        //    int count = Enum.GetNames(typeof(Def.Motor)).Length;
        //    string[] motors = Enum.GetNames(typeof(Def.Motor));
        //    foreach (string motorString in motors)
        //    {
        //        try
        //        {
        //            Def.Motor motor = (Def.Motor)Enum.Parse(typeof(Def.Motor), motorString);
        //            SetParameter(motor);
        //        }
        //        catch (ArgumentException)
        //        {
        //            Thread.Sleep(1);
        //        }
        //    }
        //}

        public void SetExtraParameter()
        {
            Def.MotorType type;

            foreach (Def.Motor m in Enum.GetValues(typeof(Def.Motor)))
            {
                type = Def.GetMotorType(m);
                switch (type)
                {
                    case Def.MotorType.PCIB:
                        break;
                    case Def.MotorType.ML3:
                        SetExtraParameter_ML3(m);
                        break;
                    case Def.MotorType.ECAT:
                        break;
                    default:
                        break;
                }
            }
        }

        //int bank = 36;      //  36 : ENCODER_TYPE
        //uint value = 0;     //  0 : TYPE_INCREMENTAL, 1 : TYPE_ABSOLUTE
        public void SetExtraParameter_ML3(Def.Motor m)
        {
            // INCREMENTAL / ABSOLUTE 모드 전환
            //  SET ZERO 안되어서 넣어 보려고 했는데. 안해도 SET ZERO 됨.
            //CAXM.AxmInterruptSetUserEnable((int)m, bank, value);
        }

        // 모터 인터락
        //public bool CheckCrashMotor(Def.Motor m, ref string errMsg)
        //{
        //    bool bNotCrash = true;
        //    Def.Motor mt;
        //    //switch (m)
        //    //{
        //    //    case Def.Motor.PPLoader_P1_R:
        //    //    case Def.Motor.PPLoader_P2_R:
        //    //    case Def.Motor.PPLoader_P3_R:
        //    //    case Def.Motor.PPLoader_P4_R:
        //    //        {
        //    //            int pi = (int)m - (int)Def.Motor.PPLoader_P1_R;
        //    //            mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            bNotCrash &= bOrg;
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_P1_R:
        //    //    case Def.Motor.PPUnloader_P2_R:
        //    //    case Def.Motor.PPUnloader_P3_R:
        //    //    case Def.Motor.PPUnloader_P4_R:
        //    //        {
        //    //            int pi = (int)m - (int)Def.Motor.PPUnloader_P1_R;
        //    //            mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.AutoCal_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }

        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + (Def.Motor.Dispenser_Right_Z).ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }

        //    //                 bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + (Def.Motor.Dispenser_Right_Z).ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_1_Camera_Z:
        //    //    case Def.Motor.Attach_2_Camera_Z:
        //    //    case Def.Motor.Attach_3_Camera_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_1_P1_Z:
        //    //    case Def.Motor.Attach_1_P2_Z:
        //    //    case Def.Motor.Attach_1_P3_Z:
        //    //    case Def.Motor.Attach_1_P4_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_2_P1_Z:
        //    //    case Def.Motor.Attach_2_P2_Z:
        //    //    case Def.Motor.Attach_2_P3_Z:
        //    //    case Def.Motor.Attach_2_P4_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_3_P1_Z:
        //    //    case Def.Motor.Attach_3_P2_Z:
        //    //    case Def.Motor.Attach_3_P3_Z:
        //    //    case Def.Motor.Attach_3_P4_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_1_P1_R:
        //    //    case Def.Motor.Attach_1_P2_R:
        //    //    case Def.Motor.Attach_1_P3_R:
        //    //    case Def.Motor.Attach_1_P4_R:
        //    //        {
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            int pi = (int)m - (int)Def.Motor.Attach_1_P1_R;
        //    //            mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                 bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_2_P1_R:
        //    //    case Def.Motor.Attach_2_P2_R:
        //    //    case Def.Motor.Attach_2_P3_R:
        //    //    case Def.Motor.Attach_2_P4_R:
        //    //        {
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            int pi = (int)m - (int)Def.Motor.Attach_2_P1_R;
        //    //            mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                 bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;
        //    //    case Def.Motor.Attach_3_P1_R:
        //    //    case Def.Motor.Attach_3_P2_R:
        //    //    case Def.Motor.Attach_3_P3_R:
        //    //    case Def.Motor.Attach_3_P4_R:
        //    //        {
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            int pi = (int)m - (int)Def.Motor.Attach_3_P1_R;
        //    //            mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                 bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_P1_Z:
        //    //    case Def.Motor.PPLoader_P2_Z:
        //    //    case Def.Motor.PPLoader_P3_Z:
        //    //    case Def.Motor.PPLoader_P4_Z:
        //    //    case Def.Motor.PPLoader_Camera_Z:
        //    //        break;

        //    //    case Def.Motor.PPUnloader_P1_Z:
        //    //    case Def.Motor.PPUnloader_P2_Z:
        //    //    case Def.Motor.PPUnloader_P3_Z:
        //    //    case Def.Motor.PPUnloader_P4_Z:
        //    //    case Def.Motor.PPUnloader_Camera_Z:
        //    //        break;

        //    //    case Def.Motor.Dispenser_Left_X:
        //    //    case Def.Motor.Dispenser_Left_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Dispenser_Left_Z:
        //    //        break;

        //    //    case Def.Motor.Dispenser_Right_X:
        //    //    case Def.Motor.Dispenser_Right_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Dispenser_Right_Z:
        //    //        break;

        //    //    case Def.Motor.Dispenser_Laser_Y:
        //    //        break;

        //    //    case Def.Motor.LensTrayElevator_1:
        //    //    case Def.Motor.LensTrayElevator_2:
        //    //    case Def.Motor.LensTrayElevator_3:
        //    //        break;

        //    //    case Def.Motor.LensTrayShuttle_1:
        //    //    case Def.Motor.LensTrayShuttle_2:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.LensTrayElevator_2))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.LensTrayElevator_2.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.LensTrayElevator_2))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.LensTrayElevator_2.ToString();
        //    //                return false;
        //    //            }


        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.LensTrayElevator_2);
        //    //                 bOrg |= Program.motion.lens.stacker.IsStandby_Z2();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.LensTrayElevator_2.ToString();
        //    //                return false;
        //    //            }

        //    //            double tolerance = Program.teaching.motor.moveTolerance;
      
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                     bOrg = Program.motion.IsHomeSensor(mt);
        //    //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                     bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                     bOrg = Program.motion.IsHomeSensor(mt);
        //    //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                     bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                     bOrg = Program.motion.IsHomeSensor(mt);
        //    //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                     bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.MagLoader_Z:
        //    //        {
        //    //            bNotCrash = !Program.motion.rail.loader.top.GetCarrierExist_Entry();

        //    //            bNotCrash &= Program.motion.magLoader.GetMagSlider(Def.MagPos.Top);
        //    //            bNotCrash &= Program.motion.magLoader.GetMagSliderLock(Def.MagPos.Top);

        //    //            bNotCrash &= Program.motion.magLoader.GetMagSlider(Def.MagPos.Btm);
        //    //            bNotCrash &= Program.motion.magLoader.GetMagSliderLock(Def.MagPos.Btm);
        //    //        }
        //    //        break;

        //    //    case Def.Motor.MagUnloader_Z:
        //    //        {
        //    //            bNotCrash = !Program.motion.rail.unloader.top.GetCarrierExist_Entry();

        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSlider(Def.MagPos.Top);
        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSliderLock(Def.MagPos.Top);

        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSlider(Def.MagPos.Btm);
        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSliderLock(Def.MagPos.Btm);
        //    //        }
        //    //        break;

        //    //    case Def.Motor.RailIndex_1_X:
        //    //    case Def.Motor.RailIndex_2_X:
        //    //    case Def.Motor.RailIndex_3_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPLoader_Camera_Z);
        //    //                 bOrg |= Program.motion.ppLoader.conv.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }


        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                    bOrg = Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.RailIndex_4_X:
        //    //    case Def.Motor.RailIndex_5_X:
        //    //    case Def.Motor.RailIndex_6_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPUnloader_Camera_Z);
        //    //            bOrg |= Program.motion.ppUnloader.camera.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                    bOrg = Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Shuttle_1_X:
        //    //    case Def.Motor.Shuttle_2_X:
        //    //    case Def.Motor.Shuttle_3_X:
        //    //    case Def.Motor.Shuttle_4_X:
        //    //    case Def.Motor.Shuttle_5_X:
        //    //    case Def.Motor.Shuttle_6_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }

        //    //                 bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            int si = (int)m - (int)Def.Motor.Shuttle_1_X;
        //    //            int ai = (int)(si / 2);
        //    //            bNotCrash &= Program.motion.mask[si].upDown.Get(Def.UpDown.Up);
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }
                           
        //    //                    bOrg = Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bOrg = Program.motion.IsMinusSensor(mt);
        //    //                bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                double tolerance = Program.teaching.motor.moveTolerance;

        //    //                if (ai == (int)Def.AttachIdx.A1)
        //    //                {
        //    //                    mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }
      
        //    //                        bOrg = Program.motion.IsHomeSensor(mt);
        //    //                        bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                        bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                        bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else if (ai == (int)Def.AttachIdx.A2)
        //    //                {
        //    //                    mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                    bOrg = Program.motion.IsHomeSensor(mt);
        //    //                    bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                    bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                    bOrg = Program.motion.IsHomeSensor(mt);
        //    //                    bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                    bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_Camera_Y:
        //    //    case Def.Motor.PPLoader_Conv_W:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPLoader_Camera_Z);
        //    //                 bOrg |= Program.motion.ppLoader.conv.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_Y:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_Camera_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
                        
        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPUnloader_Camera_Z);
        //    //                 bOrg |= Program.motion.ppUnloader.camera.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_Y:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_1_X:
        //    //    case Def.Motor.Attach_2_X:
        //    //    case Def.Motor.Attach_3_X:
        //    //        {
        //    //            int ai = (int)m - (int)Def.Motor.Attach_1_X;
        //    //            mt = Def.Motor.Attach_1_Y + ai;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[ai].IsStandby_Y();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                if (ai == (int)Def.AttachIdx.A1)
        //    //                {
        //    //                    mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                        bOrg = Program.motion.IsHomeSensor(mt);
        //    //                        bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                        bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                        bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else if (ai == (int)Def.AttachIdx.A2)
        //    //                {
        //    //                    mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_1_Y:
        //    //    case Def.Motor.Attach_2_Y:
        //    //    case Def.Motor.Attach_3_Y:
        //    //        {
        //    //            int ai = (int)m - (int)Def.Motor.Attach_1_Y;
        //    //            bool bOrg = true;
        //    //            //if (Program.motion.IsHomeCompleted(Def.Motor.Attach_1_X + ai))
        //    //            //{
        //    //            //    bNotCrash &= Program.motion.attach[ai].IsStandby_X();
        //    //            //}
        //    //            //
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                if (ai == (int)Def.AttachIdx.A1)
        //    //                {
        //    //                    mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else if (ai == (int)Def.AttachIdx.A2)
        //    //                {
        //    //                    mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Inspection_1_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Inspection_1_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Inspection_1_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Inspection_1_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Inspection_1_Z.ToString();
        //    //                return false;
        //    //            }
                        
        //    //            bool bOrg = Program.motion.IsMinusSensor(Def.Motor.Inspection_1_Z);
        //    //                 bOrg |= Program.motion.inspection[(int)Def.InspectionIdx.I1].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Inspection_1_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Inspection_2_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Inspection_2_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Inspection_2_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Inspection_2_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Inspection_2_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsMinusSensor(Def.Motor.Inspection_2_Z);
        //    //                 bOrg |= Program.motion.inspection[(int)Def.InspectionIdx.I2].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Inspection_2_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Inspection_1_Z:
        //    //    case Def.Motor.Inspection_2_Z:
        //    //        break;

        //    //    case Def.Motor.Reject_X:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_Bottom_Camera_X:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_Bottom_Camera_X:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //}

        //    return bNotCrash;
        //}


    }
}
