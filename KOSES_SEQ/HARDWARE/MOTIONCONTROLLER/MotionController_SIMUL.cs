﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.HARDWARE.MC;
//using KOSES_SEQ.HARDWARE.MC.AJIN;
//using static KOSES_SEQ.HARDWARE.MC.AJIN.AJIN_PEM_DEFINE;
using KOSES_SEQ.CONFIG;
using System.Diagnostics;
using KOSES_SEQ.OP;
using System.Windows.Forms;

namespace KOSES_SEQ.MANAGER
{
    public class MotionController_SIMUL : MotionController
    {
        static readonly int MAX_AXES = 128;
        static readonly int MAX_DIGITAL = 512;
        static readonly int MAX_ANALOG = 32;

        bool[] InputDigital = new bool[MAX_DIGITAL];
        bool[] OutputDigital = new bool[MAX_DIGITAL];
        double[] InputAnalog = new double[MAX_ANALOG];
        double[] OutputAnalog = new double[MAX_ANALOG];

        double[] CurrentPosition = new double[MAX_AXES];
        double[] CommandPosition = new double[MAX_AXES];

        bool[] IsBusy = new bool[MAX_AXES];
        bool[] ServoOnStatus = new bool[MAX_AXES];

        private Thread UpdateStatus;
        public MotionController_SIMUL()
        {
            StartUpdateStatus();
        }

        private void StartUpdateStatus()
        {
            UpdateStatus = new Thread(UpdateStatusLoop);
            UpdateStatus.IsBackground = true;
            UpdateStatus.Start();
        }

        private void UpdateStatusLoop()
        {
            while (true)
            {
                try
                {
                    if (IsOpen())
                    {
                    }
                }
                catch (Exception ex)
                {
                }

                Thread.Sleep(5);
            }
        }

        public override void SetMotionController()
        {
        }

        public override bool IsOpen()
        {
            return true;
        }

        public override bool MotionControllerOpen()
        {
            return true;
        }

        public override void MotionControllerClose()
        {
        }
        public override bool GetInput(int port, Def.SensorType type = Def.SensorType.A)
        {
            bool b = InputDigital[port];
            if (type == Def.SensorType.B)
            {
                b = !b;
            }
            return b;
        }

        public override byte GetInputByte(int port)
        {
            if (port % 8 != 0)
                return 0;

            uint value = 0;

            for (int i = 0; i < 8; i++)
            {
                if (InputDigital[i + port])
                    value |= (uint)1 << i;
            }

            return BitConverter.GetBytes(value)[0];
        }

        public override bool GetOutput(int port)
        {

            bool b = OutputDigital[port];

            return b;
        }

        public override byte GetOutputByte(int port)
        {
            if (port % 8 != 0)
                return 0;

            uint value = 0;

            for (int i = 0; i < 8; i++)
            {
                if (OutputDigital[i + port])
                    value |= (uint)1 << i;
            }

            return BitConverter.GetBytes(value)[0];
        }

        public override void SetOutput(int port, bool b)
        {
            OutputDigital[port] = b;
        }

        public override void SetOutputByte(int port, byte value)
        {
            if (port % 8 != 0)
                return;

            for (int i = 0; i < 8; i++)
            {
                OutputDigital[i + port] = (value & (1 << i)) == 0 ? false : true;
            }
        }

        public override double GetInputAnalog(int port)
        {
            double val = InputAnalog[port];

            return val;
        }

        public override double GetOutputAnalog(int port)
        {
            double val = OutputAnalog[port];

            return val;
        }

        public override void SetOutputAnalog(int port, double voltage)
        {
            OutputAnalog[port] = voltage;
        }

        public override double GetCmdPos(Def.Motor motor)
        {
            return CommandPosition[(int)motor];
        }

        public override double GetEncPos(Def.Motor motor)
        {
            return CurrentPosition[(int)motor];
        }

        #region MoveAbs
        /// <summary>
        /// 모터 파라메터 속도에 입력 비율로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="speedRate"></param>
        /// <returns></returns>
        public override bool MoveAbs(Def.Motor motor, double pos, int speedRate = 100)
        {
            bool b = false;
            double acc = 0;
            double vel = 0;
            double dec = 0;
            Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

            //  전역 비율 속도
            b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
            if (b == false)
            {
                return false;
            }

            //  입력 비율 속도
            acc = acc * speedRate / 100;
            vel = vel * speedRate / 100;
            dec = dec * speedRate / 100;

            b = MoveAbs(motor, pos, acc, vel, dec, profile);
            if (false == b)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 입력 속도로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public override bool MoveAbs(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p)
        {
            bool b = true;

            if (IsBusy[(int)motor] || !IsServoOn(motor))
                return false;

            Action action = new Action(delegate ()
            {
                UpdateCurrentPosition(motor, pos, acc, vel, dec);
            });
            action.BeginInvoke(null, null);

            Thread.Sleep(20);

            return b;
        }

        private void UpdateCurrentPosition(Def.Motor motor, double pos, double acc, double vel, double dec)
        {
            double dCurPos = CurrentPosition[(int)motor];
            double dVel = (vel == 0) ? 10 : vel;
            double dVelAdpt = (dVel / 100); //10ms속도로 환산(sleep을 10ms로 하기 때문)

            double dInterval = pos - dCurPos;

            IsBusy[(int)motor] = true;

            #region 가속도 시간 보상

            double dMinDistMaxSpeed = dVel * dVel / (acc * 1000);  // 최고 속도에 도달하기 위한 최소한의 거리 mm
            double dAccDelaySec;

            if (dInterval >= dMinDistMaxSpeed)
                dAccDelaySec = dVel / (acc * 1000);
            else
                dAccDelaySec = Math.Sqrt(dInterval / (acc * 1000));
            int index = 0;
            int delayStep = 1;
            do
            {
                Thread.Sleep(delayStep);
                index++;
            }
            while ((index * delayStep) < (dAccDelaySec * 1000));
            #endregion

            try
            {
                do
                {
                    // 현재 위치와 목적지 사이의 거리
                    dInterval = pos - CommandPosition[(int)motor];

                    // 목적지와의 거리가 작으면 Position을 같게 셋팅
                    if (Math.Abs(dVelAdpt) > Math.Abs(dInterval))
                    {
                        CommandPosition[(int)motor] = pos;
                        CurrentPosition[(int)motor] = pos;
                    }
                    else
                    {
                        // +방향으로 이동할지 -방향으로 이동할지...
                        if (dInterval >= 0)
                            CommandPosition[(int)motor] += dVelAdpt;
                        else
                            CommandPosition[(int)motor] -= dVelAdpt;

                        // Encoder Position은 Command Position을 동일하게 셋팅
                        CurrentPosition[(int)motor] = CommandPosition[(int)motor];
                    }

                    Application.DoEvents();

                    Thread.Sleep(10);

                } while (CurrentPosition[(int)motor] != pos && IsBusy[(int)motor]); // 목적지 포지션에 도달 안했거나 정지 명령이 안들어 왔을 때
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsBusy[(int)motor] = false;
            }
        }

        public override bool MoveAbs_Block(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p)
        {
            return MoveAbs(motor, pos, acc, vel, dec, p);
        }
        #endregion

        #region MoveRel

        /// <summary>
        /// 입력 속도로 상대 위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="dis"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public override bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, double acc, double vel, double dec, Def.MotorProfile profile)
        {
            bool b = true;
            if (Def.MotorDir.MINUS == dir)
            {
                dis = -dis;
            }

            double target = GetCmdPos(motor) + dis;

            b = MoveAbs(motor, target, acc, vel, dec, profile);
            if (b == false)
            {
                Stop(motor);
            }
            Thread.Sleep(20);
            return b;
        }
        #endregion

        #region MoveJog

        /// <summary>
        /// 입력 속도로 속도 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <returns></returns>
        public override bool MoveJog(Def.Motor motor, Def.MotorDir dir, double acc, double vel, double dec)
        {
            bool b = false;

            double pos = 99999;
            if (dir == Def.MotorDir.MINUS)
            {
                pos *= -1;
            }

            b = MoveAbs(motor, pos, acc, vel, dec, Def.MotorProfile.ASYMMETRIC_S_CURVE);

            return b;
        }
        #endregion

        public override bool IsPosition(Def.Motor motor, double destPos, double tolerance)
        {

            if (SysOperator.Instance.motorPara.para[(int)motor].motorType == (int)Def.ServoType.STEP)
            {
                return true;
            }

            double curPos = GetEncPos(motor);

            bool b;
            b = Def.IsRange(curPos, destPos, tolerance);
            return b;
        }


        public override void SetTorqueLimit(Def.Motor motor, double plusTorqueLimit, double minusTorqueLimit)
        {
            throw new NotImplementedException(); ;
        }


        public override void Stop(Def.Motor motor)
        {
            IsBusy[(int)motor] = false;
        }

        public override void EmoStop(Def.Motor motor)
        {
            IsBusy[(int)motor] = false;
        }

        public override bool GetInPositionStatus(Def.Motor motor)
        {
            return !IsBusy[(int)motor];
        }

        public override bool GetInMotionStatus(Def.Motor motor)
        {
            return IsBusy[(int)motor];
        }

        public override bool Home(Def.Motor motor)
        {
            bool b = MoveAbs(motor, 0);

            return b;
        }

        public override bool HomeWait(Def.Motor motor)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            double movetimeover = 10000;

            int nStopCount = 5;

            while (true)
            {
                if (!IsBusy[(int)motor])
                {
                    break;
                }

                if (sw.ElapsedMilliseconds > movetimeover)
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }

                if (SysOperator.GetUserStop() == true)
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }
                Thread.Sleep(1);
            }

            return true;
        }

        public override bool IsHomeCompleted(Def.Motor motor)
        {
            return true;
        }

        public override double GetCurrentTorque(Def.Motor motor)
        {

            return 0.0;
        }

        public override bool IsAlarm(Def.Motor motor)
        {
            return false;
        }

        public override bool IsPlusSensor(Def.Motor motor)
        {
            return false;
        }

        public override bool IsHomeSensor(Def.Motor motor)
        {
            return false;
        }

        public override bool IsMinusSensor(Def.Motor motor)
        {
            return false;
        }

        public override bool IsServoOn(Def.Motor motor)
        {
            return ServoOnStatus[(int)motor];
        }

        public override void ServoOn(Def.Motor motor)
        {
            ServoOnStatus[(int)motor] = true;
        }

        public override void ServoOff(Def.Motor motor)
        {
            ServoOnStatus[(int)motor] = false;
        }

        public override void SetPosZero(Def.Motor motor)
        {
            CommandPosition[(int)motor] = 0.0;
            CurrentPosition[(int)motor] = 0.0;
        }

        public override void ResetAlarm(Def.Motor motor)
        {
        }

        public override void SetParameter(Def.Motor motor)
        {

        }

        public void SetExtraParameter()
        {
        }

    }
}


