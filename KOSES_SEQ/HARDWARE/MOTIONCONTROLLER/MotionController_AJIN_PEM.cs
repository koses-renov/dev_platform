﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.HARDWARE.MC;
using KOSES_SEQ.HARDWARE.MC.AJIN;
using static KOSES_SEQ.HARDWARE.MC.AJIN.AJIN_PEM_DEFINE;
using KOSES_SEQ.CONFIG;
using System.Diagnostics;
using KOSES_SEQ.OP;

namespace KOSES_SEQ.MANAGER
{
    public class MotionController_AJIN_PEM : MotionController
    {
        AJIN_PEM axl = null;

        public MotionController_AJIN_PEM()
        {
        }

        public override void SetMotionController()
        {
            axl = new AJIN_PEM();
        }


        public override bool MotionControllerOpen()
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;
            b = axl.Open();
            return b;
        }

        public override void MotionControllerClose()
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.Close();
        }

        public override bool GetInput(int port, Def.SensorType type = Def.SensorType.A)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = axl.GetInput(port);
            if (type == Def.SensorType.B)
            {
                b = !b;
            }
            return b;
        }

        public override byte GetInputByte(int port)
        {
            throw new NotImplementedException();
        }

        public override byte GetOutputByte(int port)
        {
            throw new NotImplementedException();
        }

        public override void SetOutputByte(int port, byte value)
        {
            throw new NotImplementedException();
        }

        public override bool GetOutput(int port)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = axl.GetOutput(port);
            return b;
        }

        public override void SetOutput(int port, bool b)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.SetOutput(port, b);
        }

        public override double GetCmdPos(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return 0.0;
            //}

            double pos = 0.0;
            pos = axl.GetCmdPos((int)motor);
            return pos;
        }

        public override double GetEncPos(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return 0.0;
            //}

            double pos = 0.0;
            pos = axl.GetEncPos((int)motor);
            return pos;
        }

        #region MoveAbs
        /// <summary>
        /// 모터 파라메터 속도에 입력 비율로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="speedRate"></param>
        /// <returns></returns>
        public override bool MoveAbs(Def.Motor motor, double pos, int speedRate = 100)
        {
            bool b = false;
            double acc = 0;
            double vel = 0;
            double dec = 0;
            Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

            //  전역 비율 속도
            b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
            if (b == false)
            {
                return false;
            }

            //  입력 비율 속도
            acc = acc * speedRate / 100;
            vel = vel * speedRate / 100;
            dec = dec * speedRate / 100;

            b = MoveAbs(motor, pos, acc, vel, dec, profile);
            if (false == b)
            {
                return false;
            }
            return true;
        }

        //public bool MoveAbs_Block(Def.Motor motor, double pos, int speedRate = 100)
        //{
        //    bool b = false;
        //    double acc = 0;
        //    double vel = 0;
        //    double dec = 0;
        //    Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

        //    //  전역 비율 속도
        //    b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
        //    if (b == false)
        //    {
        //        return false;
        //    }

        //    //  입력 비율 속도
        //    acc = acc * speedRate / 100;
        //    vel = vel * speedRate / 100;
        //    dec = dec * speedRate / 100;

        //    b = MoveAbs_Block(motor, pos, acc, vel, dec, profile);
        //    if (false == b)
        //    {
        //        return false;
        //    }
        //    return true;
        //}
        /// <summary>
        /// 입력 속도로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public override bool MoveAbs(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p)
        {
            bool b = false;
            AJIN_PEM_DEFINE.MotorProfile mp = AJIN_PEM_DEFINE.TransMotorProfile(p);
            b = axl.MoveAbs((int)motor, pos, acc, vel, dec, mp);
            if (b == false)
            {
                Stop(motor);
            }
            Thread.Sleep(20);
            return b;
        }

        public override bool MoveAbs_Block(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p)
        {
            bool b = false;
            AJIN_PEM_DEFINE.MotorProfile mp = AJIN_PEM_DEFINE.TransMotorProfile(p);
            b = axl.MoveAbs_Block((int)motor, pos, acc, vel, dec, mp);
            if (b == false)
            {
                Stop(motor);
            }
            return b;
        }
        #endregion
        #region MoveRel
        ///// <summary>
        ///// 모터 파라메터 속도에 입력 비율로 상대위치 이동
        ///// </summary>
        ///// <param name="motor"></param>
        ///// <param name="dir"></param>
        ///// <param name="dis"></param>
        ///// <param name="speedRate"></param>
        ///// <returns></returns>
        //public bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, int speedRate = 100)
        //{
        //    bool b = false;
        //    double acc = 0;
        //    double vel = 0;
        //    double dec = 0;
        //    Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

        //    //  전역 비율 속도
        //    b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
        //    if (false == b)
        //    {
        //        return false;
        //    }

        //    //  입력 비율 속도
        //    acc = acc * speedRate / 100;
        //    vel = vel * speedRate / 100;
        //    dec = dec * speedRate / 100;

        //    b = MoveRel(motor, dir, dis, acc, vel, dec, profile);
        //    Thread.Sleep(20);
        //    return b;
        //}

        /// <summary>
        /// 입력 속도로 상대 위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="dis"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public override bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, double acc, double vel, double dec, Def.MotorProfile profile)
        {
            bool b = false;
            AJIN_PEM_DEFINE.MotorDir ajinDir = AJIN_PEM_DEFINE.TransMotorDir(dir);
            AJIN_PEM_DEFINE.MotorProfile ajinProfile = AJIN_PEM_DEFINE.TransMotorProfile(profile);
            b = axl.MoveRel((int)motor, ajinDir, dis, acc, vel, dec, ajinProfile);
            if (b == false)
            {
                Stop(motor);
            }
            Thread.Sleep(20);
            return b;
        }
        #endregion
        #region MoveJog
        ///// <summary>
        ///// 모터 파라메터 속도에 입력 비율로 속도 이동
        ///// </summary>
        ///// <param name="motor"></param>
        ///// <param name="dir"></param>
        ///// <returns></returns>
        //public bool MoveJog(Def.Motor motor, Def.MotorDir dir, int speedRate = 100)
        //{
        //    bool b = false;
        //    double acc = 0;
        //    double vel = 0;
        //    double dec = 0;

        //    //  전역 비율 속도
        //    b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
        //    if (false == b)
        //    {
        //        return false;
        //    }

        //    //  입력 비율 속도
        //    acc = acc * speedRate / 100;
        //    vel = vel * speedRate / 100;
        //    dec = dec * speedRate / 100;

        //    b = MoveJog(motor, dir, acc, vel, dec);
        //    return b;
        //}

        /// <summary>
        /// 입력 속도로 속도 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <returns></returns>
        public override bool MoveJog(Def.Motor motor, Def.MotorDir dir, double acc, double vel, double dec)
        {
            bool b = false;
            AJIN_PEM_DEFINE.MotorDir ajinDir = AJIN_PEM_DEFINE.TransMotorDir(dir);
            b = axl.MoveJog((int)motor, ajinDir, acc, vel, dec);
            return b;
        }
        #endregion



        public Def.Code MoveAbsWait_TorqueLimit(Def.Motor motor, double destPos, double torqueLimit)
        {
            return MoveWait_TorqueLimit(motor, destPos, torqueLimit);
        }
        //public Def.Code MoveAbsWait(Def.Motor motor, double destPos, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        //{
        //    return MoveWait(motor, destPos, inputCheck1, inputCheck2);
        //}
        //public Def.Code MoveRelWait(Def.Motor motor, double relValue, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        //{
        //    double curPos = GetEncPos(motor);
        //    double destPos = curPos + relValue;
        //    return MoveWait(motor, destPos, inputCheck1, inputCheck2);
        //}

        //Def.Code MoveWait(Def.Motor motor, double destPos, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        //{          
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    double movetimeover = 10000;
        //    //double movetimeover = Program.teaching.motor.moveWaitTime;
        //    //double tolerance = Program.teaching.motor.moveTolerance;
        //    double tolerance = 0.05;

        //    bool b;
        //    bool bInputCheck;

        //    while (true)
        //    {
        //        Thread.Sleep(10);

        //        if (sw.ElapsedMilliseconds > movetimeover)
        //        {
        //            Stop(motor);
        //            Def.Code code = GetMotorTimeOver(motor);
        //            return code;
        //        }

        //        if (SysOperator.GetUserStop())
        //        {
        //            Stop(motor);
        //            return Def.Code.USER_STOP;
        //        }

        //        if (IsStop(motor) == false)
        //        {
        //            continue;
        //        }


        //        ////////////////////////////////
        //        /// 모터 정지 상태
        //        ////////////////////////////////
        //        if (IsAlarm(motor) == true)
        //        {
        //            Stop(motor);
        //            return Def.Code.MOTOR_ALARM;
        //        }

        //        if (inputCheck1 != Def.NotUsed)
        //        {
        //            bInputCheck = GetInput(inputCheck1, Def.SensorType.B);
        //            if (bInputCheck)
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTOR_EXT_SENSOR_1;
        //            }
        //        }

        //        if (inputCheck2 != Def.NotUsed)
        //        {
        //            bInputCheck = GetInput(inputCheck2);
        //            if (bInputCheck)
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTOR_EXT_SENSOR_2;
        //            }
        //        }

        //        if (SysOperator.motorPara.para[(int)motor].motorType == (int)Def.ServoType.SERVO)
        //        {
        //            b = IsPosition(motor, destPos, tolerance);
        //            if (b)
        //            {
        //                ////////////////////////////////
        //                /// 모터 정상 정지
        //                ////////////////////////////////
        //                return Def.Code.OK;
        //            }
        //            else
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTION_MOVE_FAIL;
        //            }
        //        }

        //        ////////////////////////////////
        //        /// 모터 정상 정지
        //        ////////////////////////////////
        //        return Def.Code.OK;
        //    }
        //}

        //Def.Code MoveWait_TorqueLimit(Def.Motor motor, double destPos, double torqueLimit)
        //{          
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    double movetimeover = 10000;
        //    //double movetimeover = Program.teaching.motor.moveWaitTime;

        //    while (true)
        //    {
        //        if (GetInMotionStatus(motor) == false)
        //        {
        //            if (IsAlarm(motor) == true)
        //            {
        //                Stop(motor);
        //                return Def.Code.MOTOR_ALARM;
        //            }

        //            if(IsTorqueOver(motor, torqueLimit))
        //            {
        //                EmoStop(motor);
        //            }

        //            break;
        //        }

        //        if (sw.ElapsedMilliseconds > movetimeover)                   
        //        {
        //            Stop(motor);
        //            Def.Code code = GetMotorTimeOver(motor);
        //            return code;
        //        }

        //        if (SysOperator.GetUserStop())
        //        {
        //            Stop(motor);
        //            return Def.Code.USER_STOP;
        //        }
        //        Thread.Sleep(1);
        //    }

        //    return Def.Code.OK;
        //}

        //Def.Code GetMotorTimeOver(Def.Motor motor)
        //{
        //    return Def.GetMotorError(motor);
        //}

        public override bool IsPosition(Def.Motor motor, double destPos, double tolerance)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            if (SysOperator.Instance.motorPara.para[(int)motor].motorType == (int)Def.ServoType.STEP)
            {
                return true;
            }

            double curPos = GetEncPos(motor);

            bool b;
            b = Def.IsRange(curPos, destPos, tolerance);
            return b;
        }


        public override void SetTorqueLimit(Def.Motor motor, double plusTorqueLimit, double minusTorqueLimit)
        {
            axl.SetTorqueLimit((int)motor, plusTorqueLimit, minusTorqueLimit);
            Thread.Sleep(10);
        }


        public override void Stop(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.Stop((int)motor);
        }

        public override void EmoStop(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.EmergencyStop((int)motor);
        }

        //public bool IsStop(Def.Motor motor)
        //{
        //    bool b = false;
        //    if (GetInPositionStatus(motor) == true && GetInMotionStatus(motor) == false)
        //    {
        //        b = true;
        //    }
        //    return b;
        //}

        public override bool GetInPositionStatus(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;
            b = axl.GetInPositionStatus((int)motor);
            return b;
        }

        public override bool GetInMotionStatus(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = axl.GetInMotionStatus((int)motor);
            return b;
        }

        public override bool Home(Def.Motor motor)
        {
            MotorParameterData motorParaData = new MotorParameterData(null);
            GetMotorPara(motor, out motorParaData);

            bool b = false;

            /////////////////////////////////////////////////////
            //  AJIN AXT
            /////////////////////////////////////////////////////
            Def.Rotate homeDir = Def.GetRotate(motorParaData.homeDir.ToString());
            b = axl.Home(
                (int)motor,
                homeDir,
                (uint)motorParaData.homeMethod,
                (uint)motorParaData.zPhase,
                motorParaData.homeClearTime,
                motorParaData.homeOffset,
                motorParaData.multiHomeVel,
                motorParaData.multiHomeAcc);


            if (b == false)
            {
                //Program.MessageBox($"HOME", "HOME FAILED", DefColor.LightRed);
            }

            return b;
        }

        public override bool HomeWait(Def.Motor motor)
        {          
            Stopwatch sw = new Stopwatch();
            sw.Start();

            double movetimeover = 10000;
            //double movetimeover = Program.teaching.motor.homeWaitTime;

            int nStopCount = 5;

            while (true)
            {
                if (true == IsHomeCompleted(motor) && true == IsStop(motor))
                {
                    break;
                }

                if (sw.ElapsedMilliseconds > movetimeover)                  
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }

                if (SysOperator.GetUserStop() == true)
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }
                Thread.Sleep(1);
            }

            return true;
        }

        public override bool IsHomeCompleted(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;
            b = axl.GetHomeComplete((int)motor);
            return b;
        }

        //public bool IsHomeAllCompleted(ref string motorName)
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return true;
        //    //}

        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        if (false == IsHomeCompleted(motor))
        //        {
        //            motorName = motor.ToString();
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        public override double GetCurrentTorque(Def.Motor motor)
        {
            double curTorque;
            curTorque = axl.GetCurrentTorque((int)motor);
            return curTorque;
        }
        //public bool IsTorqueOver(Def.Motor motor, double torqueLimit)
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return false;
        //    //}

        //    double curTorque = GetCurrentTorque(motor);
        //    if (torqueLimit <= curTorque)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        public override bool IsAlarm(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = axl.GetAlarmStatus((int)motor);
            return b;
        }

        //public void StopAll(int stopCount = 3)
        //{
        //    for (int i = 0; i < stopCount; i++)
        //    {
        //        foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //        {
        //            Stop(motor);
        //        }
        //        Thread.Sleep(10);
        //    }
        //}
        //public void EmoStopAll(int stopCount = 1)
        //{
        //    for (int i = 0; i < stopCount; i++)
        //    {
        //        foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //        {
        //            EmoStop(motor);
        //        }
        //        Thread.Sleep(10);
        //    }
        //}


        public override bool IsPlusSensor(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = axl.GetPlusLimitSensorStatus((int)motor);
            return b;
        }

        public override bool IsHomeSensor(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;
            b = axl.GetHomeSensorStatus((int)motor);
            return b;
        }

        public override bool IsMinusSensor(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return false;
            //}

            bool b = false;
            b = axl.GetMinusLimitSensorStatus((int)motor);
            return b;
        }

        //public bool IsServoOnAll()
        //{
        //    //if (Program.config.virtualMode)
        //    //{
        //    //    return true;
        //    //}

        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        if (IsServoOn(motor) == false)
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}
        public override bool IsServoOn(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            bool b = false;
            b = axl.GetServoStatus((int)motor);
            return b;
        }

        public override void ServoOn(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.SetServo((int)motor, true);
        }

        public override void ServoOff(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.SetServo((int)motor, false);
        }

        //public void ServoOnAll()
        //{
        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        ServoOn(motor);
        //    }
        //}

        //public void ServoOffAll()
        //{
        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        ServoOff(motor);
        //    }
        //}

        //public void AlarmClearAll()
        //{
        //    foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
        //    {
        //        ResetAlarm(motor);
        //    }
        //}

        public override void SetPosZero(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.SetPosZero((int)motor);
        }

        public override void ResetAlarm(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.ResetAlarm((int)motor);
        }

        public void SignalSetInpos(Def.Motor motor, Level level)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.SignalSetInpos((int)motor, level);
        }

        public override void SetParameter(Def.Motor motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return;
            //}

            axl.SetMotorParameter((int)motor, SysOperator.Instance.motorPara.para[(int)motor]);
        }

        //public void SetParameterAll()
        //{
        //    int count = Enum.GetNames(typeof(Def.Motor)).Length;
        //    string[] motors = Enum.GetNames(typeof(Def.Motor));
        //    foreach (string motorString in motors)
        //    {
        //        try
        //        {
        //            Def.Motor motor = (Def.Motor)Enum.Parse(typeof(Def.Motor), motorString);
        //            SetParameter(motor);
        //        }
        //        catch (ArgumentException)
        //        {
        //            Thread.Sleep(1);
        //        }
        //    }
        //}

        public void SetExtraParameter()
        {
            Def.MotorType type;

            foreach (Def.Motor m in Enum.GetValues(typeof(Def.Motor)))
            {
                type = Def.GetMotorType(m);
                switch (type)
                {
                    case Def.MotorType.PCIB:
                        break;
                    case Def.MotorType.ML3:
                        SetExtraParameter_ML3(m);
                        break;
                    case Def.MotorType.ECAT:
                        break;
                    default:
                        break;
                }
            }
        }

        //int bank = 36;      //  36 : ENCODER_TYPE
        //uint value = 0;     //  0 : TYPE_INCREMENTAL, 1 : TYPE_ABSOLUTE
        public void SetExtraParameter_ML3(Def.Motor m)
        {
            // INCREMENTAL / ABSOLUTE 모드 전환
            //  SET ZERO 안되어서 넣어 보려고 했는데. 안해도 SET ZERO 됨.
            //CAXM.AxmInterruptSetUserEnable((int)m, bank, value);
        }

        // 모터 인터락
        //public bool CheckCrashMotor(Def.Motor m, ref string errMsg)
        //{
        //    bool bNotCrash = true;
        //    Def.Motor mt;
        //    //switch (m)
        //    //{
        //    //    case Def.Motor.PPLoader_P1_R:
        //    //    case Def.Motor.PPLoader_P2_R:
        //    //    case Def.Motor.PPLoader_P3_R:
        //    //    case Def.Motor.PPLoader_P4_R:
        //    //        {
        //    //            int pi = (int)m - (int)Def.Motor.PPLoader_P1_R;
        //    //            mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            bNotCrash &= bOrg;
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_P1_R:
        //    //    case Def.Motor.PPUnloader_P2_R:
        //    //    case Def.Motor.PPUnloader_P3_R:
        //    //    case Def.Motor.PPUnloader_P4_R:
        //    //        {
        //    //            int pi = (int)m - (int)Def.Motor.PPUnloader_P1_R;
        //    //            mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.AutoCal_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }

        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + (Def.Motor.Dispenser_Right_Z).ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + (Def.Motor.Dispenser_Left_Z).ToString();
        //    //                return false;
        //    //            }

        //    //                 bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + (Def.Motor.Dispenser_Right_Z).ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_1_Camera_Z:
        //    //    case Def.Motor.Attach_2_Camera_Z:
        //    //    case Def.Motor.Attach_3_Camera_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_1_P1_Z:
        //    //    case Def.Motor.Attach_1_P2_Z:
        //    //    case Def.Motor.Attach_1_P3_Z:
        //    //    case Def.Motor.Attach_1_P4_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_2_P1_Z:
        //    //    case Def.Motor.Attach_2_P2_Z:
        //    //    case Def.Motor.Attach_2_P3_Z:
        //    //    case Def.Motor.Attach_2_P4_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_3_P1_Z:
        //    //    case Def.Motor.Attach_3_P2_Z:
        //    //    case Def.Motor.Attach_3_P3_Z:
        //    //    case Def.Motor.Attach_3_P4_Z:
        //    //        break;

        //    //    case Def.Motor.Attach_1_P1_R:
        //    //    case Def.Motor.Attach_1_P2_R:
        //    //    case Def.Motor.Attach_1_P3_R:
        //    //    case Def.Motor.Attach_1_P4_R:
        //    //        {
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            int pi = (int)m - (int)Def.Motor.Attach_1_P1_R;
        //    //            mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                 bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_2_P1_R:
        //    //    case Def.Motor.Attach_2_P2_R:
        //    //    case Def.Motor.Attach_2_P3_R:
        //    //    case Def.Motor.Attach_2_P4_R:
        //    //        {
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            int pi = (int)m - (int)Def.Motor.Attach_2_P1_R;
        //    //            mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                 bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;
        //    //    case Def.Motor.Attach_3_P1_R:
        //    //    case Def.Motor.Attach_3_P2_R:
        //    //    case Def.Motor.Attach_3_P3_R:
        //    //    case Def.Motor.Attach_3_P4_R:
        //    //        {
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            int pi = (int)m - (int)Def.Motor.Attach_3_P1_R;
        //    //            mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                 bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_P1_Z:
        //    //    case Def.Motor.PPLoader_P2_Z:
        //    //    case Def.Motor.PPLoader_P3_Z:
        //    //    case Def.Motor.PPLoader_P4_Z:
        //    //    case Def.Motor.PPLoader_Camera_Z:
        //    //        break;

        //    //    case Def.Motor.PPUnloader_P1_Z:
        //    //    case Def.Motor.PPUnloader_P2_Z:
        //    //    case Def.Motor.PPUnloader_P3_Z:
        //    //    case Def.Motor.PPUnloader_P4_Z:
        //    //    case Def.Motor.PPUnloader_Camera_Z:
        //    //        break;

        //    //    case Def.Motor.Dispenser_Left_X:
        //    //    case Def.Motor.Dispenser_Left_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Dispenser_Left_Z:
        //    //        break;

        //    //    case Def.Motor.Dispenser_Right_X:
        //    //    case Def.Motor.Dispenser_Right_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Dispenser_Right_Z:
        //    //        break;

        //    //    case Def.Motor.Dispenser_Laser_Y:
        //    //        break;

        //    //    case Def.Motor.LensTrayElevator_1:
        //    //    case Def.Motor.LensTrayElevator_2:
        //    //    case Def.Motor.LensTrayElevator_3:
        //    //        break;

        //    //    case Def.Motor.LensTrayShuttle_1:
        //    //    case Def.Motor.LensTrayShuttle_2:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.LensTrayElevator_2))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.LensTrayElevator_2.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.LensTrayElevator_2))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.LensTrayElevator_2.ToString();
        //    //                return false;
        //    //            }


        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.LensTrayElevator_2);
        //    //                 bOrg |= Program.motion.lens.stacker.IsStandby_Z2();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.LensTrayElevator_2.ToString();
        //    //                return false;
        //    //            }

        //    //            double tolerance = Program.teaching.motor.moveTolerance;
      
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                     bOrg = Program.motion.IsHomeSensor(mt);
        //    //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                     bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                     bOrg = Program.motion.IsHomeSensor(mt);
        //    //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                     bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                     bOrg = Program.motion.IsHomeSensor(mt);
        //    //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                     bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.MagLoader_Z:
        //    //        {
        //    //            bNotCrash = !Program.motion.rail.loader.top.GetCarrierExist_Entry();

        //    //            bNotCrash &= Program.motion.magLoader.GetMagSlider(Def.MagPos.Top);
        //    //            bNotCrash &= Program.motion.magLoader.GetMagSliderLock(Def.MagPos.Top);

        //    //            bNotCrash &= Program.motion.magLoader.GetMagSlider(Def.MagPos.Btm);
        //    //            bNotCrash &= Program.motion.magLoader.GetMagSliderLock(Def.MagPos.Btm);
        //    //        }
        //    //        break;

        //    //    case Def.Motor.MagUnloader_Z:
        //    //        {
        //    //            bNotCrash = !Program.motion.rail.unloader.top.GetCarrierExist_Entry();

        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSlider(Def.MagPos.Top);
        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSliderLock(Def.MagPos.Top);

        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSlider(Def.MagPos.Btm);
        //    //            bNotCrash &= Program.motion.magUnloader.GetMagSliderLock(Def.MagPos.Btm);
        //    //        }
        //    //        break;

        //    //    case Def.Motor.RailIndex_1_X:
        //    //    case Def.Motor.RailIndex_2_X:
        //    //    case Def.Motor.RailIndex_3_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPLoader_Camera_Z);
        //    //                 bOrg |= Program.motion.ppLoader.conv.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }


        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                    bOrg = Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.RailIndex_4_X:
        //    //    case Def.Motor.RailIndex_5_X:
        //    //    case Def.Motor.RailIndex_6_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPUnloader_Camera_Z);
        //    //            bOrg |= Program.motion.ppUnloader.camera.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                    bOrg = Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Shuttle_1_X:
        //    //    case Def.Motor.Shuttle_2_X:
        //    //    case Def.Motor.Shuttle_3_X:
        //    //    case Def.Motor.Shuttle_4_X:
        //    //    case Def.Motor.Shuttle_5_X:
        //    //    case Def.Motor.Shuttle_6_X:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Left_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }

        //    //                 bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
        //    //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Right_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            int si = (int)m - (int)Def.Motor.Shuttle_1_X;
        //    //            int ai = (int)(si / 2);
        //    //            bNotCrash &= Program.motion.mask[si].upDown.Get(Def.UpDown.Up);
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }
                           
        //    //                    bOrg = Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bOrg = Program.motion.IsMinusSensor(mt);
        //    //                bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                double tolerance = Program.teaching.motor.moveTolerance;

        //    //                if (ai == (int)Def.AttachIdx.A1)
        //    //                {
        //    //                    mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }
      
        //    //                        bOrg = Program.motion.IsHomeSensor(mt);
        //    //                        bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                        bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                        bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else if (ai == (int)Def.AttachIdx.A2)
        //    //                {
        //    //                    mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                    bOrg = Program.motion.IsHomeSensor(mt);
        //    //                    bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                    bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                    bOrg = Program.motion.IsHomeSensor(mt);
        //    //                    bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                    bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                    bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_Camera_Y:
        //    //    case Def.Motor.PPLoader_Conv_W:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPLoader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPLoader_Camera_Z);
        //    //                 bOrg |= Program.motion.ppLoader.conv.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPLoader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_Y:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_Camera_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPUnloader_Camera_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
                        
        //    //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPUnloader_Camera_Z);
        //    //                 bOrg |= Program.motion.ppUnloader.camera.IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.PPUnloader_Camera_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_Y:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_1_X:
        //    //    case Def.Motor.Attach_2_X:
        //    //    case Def.Motor.Attach_3_X:
        //    //        {
        //    //            int ai = (int)m - (int)Def.Motor.Attach_1_X;
        //    //            mt = Def.Motor.Attach_1_Y + ai;
        //    //            if (!Program.motion.IsServoOn(mt))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(mt))
        //    //            {
        //    //                errMsg = "Is Not Home : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsHomeSensor(mt);
        //    //                 bOrg |= Program.motion.attach[ai].IsStandby_Y();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                return false;
        //    //            }

        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                if (ai == (int)Def.AttachIdx.A1)
        //    //                {
        //    //                    mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                        bOrg = Program.motion.IsHomeSensor(mt);
        //    //                        bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                        bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                        bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else if (ai == (int)Def.AttachIdx.A2)
        //    //                {
        //    //                    mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Attach_1_Y:
        //    //    case Def.Motor.Attach_2_Y:
        //    //    case Def.Motor.Attach_3_Y:
        //    //        {
        //    //            int ai = (int)m - (int)Def.Motor.Attach_1_Y;
        //    //            bool bOrg = true;
        //    //            //if (Program.motion.IsHomeCompleted(Def.Motor.Attach_1_X + ai))
        //    //            //{
        //    //            //    bNotCrash &= Program.motion.attach[ai].IsStandby_X();
        //    //            //}
        //    //            //
        //    //            double tolerance = Program.teaching.motor.moveTolerance;

        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                if (ai == (int)Def.AttachIdx.A1)
        //    //                {
        //    //                    mt = Def.Motor.Attach_1_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else if (ai == (int)Def.AttachIdx.A2)
        //    //                {
        //    //                    mt = Def.Motor.Attach_2_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    mt = Def.Motor.Attach_3_P1_Z + pi;
        //    //                    if (!Program.motion.IsServoOn(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                    if (!Program.motion.IsHomeCompleted(mt))
        //    //                    {
        //    //                        errMsg = "Is Not Home : " + mt.ToString();
        //    //                        return false;
        //    //                    }

        //    //                         bOrg = Program.motion.IsHomeSensor(mt);
        //    //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
        //    //                         bOrg |= Program.motion.IsMinusSensor(mt);
        //    //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
        //    //                    if (!bOrg)
        //    //                    {
        //    //                        errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                        return false;
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Inspection_1_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Inspection_1_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Inspection_1_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Inspection_1_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Inspection_1_Z.ToString();
        //    //                return false;
        //    //            }
                        
        //    //            bool bOrg = Program.motion.IsMinusSensor(Def.Motor.Inspection_1_Z);
        //    //                 bOrg |= Program.motion.inspection[(int)Def.InspectionIdx.I1].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Inspection_1_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Inspection_2_Y:
        //    //        {
        //    //            if (!Program.motion.IsServoOn(Def.Motor.Inspection_2_Z))
        //    //            {
        //    //                errMsg = "Is Not Servo On : " + Def.Motor.Inspection_2_Z.ToString();
        //    //                return false;
        //    //            }
        //    //            if (!Program.motion.IsHomeCompleted(Def.Motor.Inspection_2_Z))
        //    //            {
        //    //                errMsg = "Is Not Home : " + Def.Motor.Inspection_2_Z.ToString();
        //    //                return false;
        //    //            }

        //    //            bool bOrg = Program.motion.IsMinusSensor(Def.Motor.Inspection_2_Z);
        //    //                 bOrg |= Program.motion.inspection[(int)Def.InspectionIdx.I2].IsStandby_Z();
        //    //            if (!bOrg)
        //    //            {
        //    //                errMsg = "Is Not Org Pos : " + Def.Motor.Inspection_2_Z.ToString();
        //    //                return false;
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.Inspection_1_Z:
        //    //    case Def.Motor.Inspection_2_Z:
        //    //        break;

        //    //    case Def.Motor.Reject_X:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPLoader_Bottom_Camera_X:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPLoader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //    case Def.Motor.PPUnloader_Bottom_Camera_X:
        //    //        {
        //    //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
        //    //            {
        //    //                mt = Def.Motor.PPUnloader_P1_Z + pi;
        //    //                if (!Program.motion.IsServoOn(mt))
        //    //                {
        //    //                    errMsg = "Is Not Servo On : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //                if (!Program.motion.IsHomeCompleted(mt))
        //    //                {
        //    //                    errMsg = "Is Not Home : " + mt.ToString();
        //    //                    return false;
        //    //                }

        //    //                bool bOrg = Program.motion.IsMinusSensor(mt);
        //    //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
        //    //                if (!bOrg)
        //    //                {
        //    //                    errMsg = "Is Not Org Pos : " + mt.ToString();
        //    //                    return false;
        //    //                }
        //    //            }
        //    //        }
        //    //        break;

        //    //}

        //    return bNotCrash;
        //}


    }
}
