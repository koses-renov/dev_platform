﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.HARDWARE.MC;
using KOSES_SEQ.HARDWARE.MC.AJIN;
//using static KOSES_SEQ.HARDWARE.MC.AJIN.AJIN_PEM_DEFINE;
using KOSES_SEQ.CONFIG;
using System.Diagnostics;
using KOSES_SEQ.OP;

namespace KOSES_SEQ.MANAGER
{
    public enum MotionControllerType
    {
        AJIN_PEM,
        WMX2,
        WMX3
    }

    public abstract class MotionController
    {
        public MotionController()
        {
        }

        bool motionOpen = false;

        public virtual bool IsOpen()
        {
            //  AJIN 계열은 컨트롤러가 연결되지 않은 환경에서 실행하는 경우 예외상황 발생
            //  함수를 try ~ catch 로 예외처리를 하면 프로그램 동작중 아래의 함수에서 CPU 점유율이 높아짐
            //   - UpdateInputStatus
            //   - UpdateOutputStatus
            //   - UpdateMotorStatus
            //  상기 함수에서 모션 컨트롤러 연결이 실패한 경우 바로 리턴 시켜 디버그시 CPU 점유율을 낮춘다.
            return motionOpen;
        }


        public bool IsMotionControllerOpen()
        {
            return motionOpen;
        }

        public abstract void SetMotionController();

        public abstract bool MotionControllerOpen();

        public abstract void MotionControllerClose();

        public abstract bool GetInput(int port, Def.SensorType type = Def.SensorType.A);

        public abstract byte GetInputByte(int port);

        public abstract bool GetOutput(int port);

        public abstract byte GetOutputByte(int port);

        public abstract void SetOutput(int port, bool b);

        public abstract void SetOutputByte(int port, byte value);

        public virtual double GetInputAnalog(int port)
        {
            return 0.0;
        }

        public virtual double GetOutputAnalog(int port)
        {
            return 0.0;
        }

        public virtual void SetOutputAnalog(int port, double val)
        {

        }

        public abstract double GetCmdPos(Def.Motor motor);

        public abstract double GetEncPos(Def.Motor motor);

        public void GetMotorPara(Def.Motor motor, out MotorParameterData motorParaData)
        {
            motorParaData = SysOperator.Instance.motorPara.para[(int)motor];
        }

        public void SetMotorPara(Def.Motor motor, MotorParameterData motorParaData)
        {
            SysOperator.Instance.motorPara.para[(int)motor] = motorParaData;
        }

        protected int GetZoneSpeed()
        {
            //int speed = Program.config.motorSpeed;
            // return speed;
            return 100;
        }

        /// <summary>
        /// 입력 속도를 FormMotorStatus 에서 설정된 속도 비율로 변경
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="transAcc"></param>
        /// <param name="transVel"></param>
        /// <param name="transDec"></param>
        /// <returns></returns>
        public bool GetMotorSpeedWithSpeedRate(Def.Motor motor, out double transAcc, out double transVel, out double transDec)
        {
            MotorParameterData motorParaData;// = new MotorParameterData(null);
            GetMotorPara(motor, out motorParaData);

            transAcc = 0;
            transVel = 0;
            transDec = 0;

            int zoneSpeed = GetZoneSpeed();
            transAcc = motorParaData.acc * zoneSpeed / 100;
            transVel = motorParaData.vel * zoneSpeed / 100;
            transDec = motorParaData.dec * zoneSpeed / 100;

            return true;
        }

        #region MoveAbs
        /// <summary>
        /// 모터 파라메터 속도에 입력 비율로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="speedRate"></param>
        /// <returns></returns>
        public abstract bool MoveAbs(Def.Motor motor, double pos, int speedRate = 100);

        public bool MoveAbs_Block(Def.Motor motor, double pos, int speedRate = 100)
        {
            bool b = false;
            double acc = 0;
            double vel = 0;
            double dec = 0;
            Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

            //  전역 비율 속도
            b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
            if (b == false)
            {
                return false;
            }

            //  입력 비율 속도
            acc = acc * speedRate / 100;
            vel = vel * speedRate / 100;
            dec = dec * speedRate / 100;

            b = MoveAbs_Block(motor, pos, acc, vel, dec, profile);
            if (false == b)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 입력 속도로 절대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="pos"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public abstract bool MoveAbs(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p);

        public abstract bool MoveAbs_Block(Def.Motor motor, double pos, double acc, double vel, double dec, Def.MotorProfile p);
        #endregion
        #region MoveRel
        /// <summary>
        /// 모터 파라메터 속도에 입력 비율로 상대위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="dis"></param>
        /// <param name="speedRate"></param>
        /// <returns></returns>
        public virtual bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, int speedRate = 100)
        {
            bool b = false;
            double acc = 0;
            double vel = 0;
            double dec = 0;
            Def.MotorProfile profile = Def.MotorProfile.SYMMETRIC_TRAPEZOID;

            //  전역 비율 속도
            b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
            if (false == b)
            {
                return false;
            }

            //  입력 비율 속도
            acc = acc * speedRate / 100;
            vel = vel * speedRate / 100;
            dec = dec * speedRate / 100;

            b = MoveRel(motor, dir, dis, acc, vel, dec, profile);
            Thread.Sleep(20);
            return b;
        }

        /// <summary>
        /// 입력 속도로 상대 위치 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="dis"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public abstract bool MoveRel(Def.Motor motor, Def.MotorDir dir, double dis, double acc, double vel, double dec, Def.MotorProfile profile);
        #endregion
        #region MoveJog
        /// <summary>
        /// 모터 파라메터 속도에 입력 비율로 속도 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        public bool MoveJog(Def.Motor motor, Def.MotorDir dir, int speedRate = 100)
        {
            bool b = false;
            double acc = 0;
            double vel = 0;
            double dec = 0;

            //  전역 비율 속도
            b = GetMotorSpeedWithSpeedRate(motor, out acc, out vel, out dec);
            if (false == b)
            {
                return false;
            }

            //  입력 비율 속도
            acc = acc * speedRate / 100;
            vel = vel * speedRate / 100;
            dec = dec * speedRate / 100;

            b = MoveJog(motor, dir, acc, vel, dec);
            return b;
        }

        /// <summary>
        /// 입력 속도로 속도 이동
        /// </summary>
        /// <param name="motor"></param>
        /// <param name="dir"></param>
        /// <param name="acc"></param>
        /// <param name="vel"></param>
        /// <param name="dec"></param>
        /// <returns></returns>
        public abstract bool MoveJog(Def.Motor motor, Def.MotorDir dir, double acc, double vel, double dec);
        #endregion


        public Def.Code MoveAbsWait(Def.Motor motor, double destPos, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        {
            return MoveWait(motor, destPos, inputCheck1, inputCheck2);
        }
        public Def.Code MoveRelWait(Def.Motor motor, double relValue, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        {
            double curPos = GetEncPos(motor);
            double destPos = curPos + relValue;
            return MoveWait(motor, destPos, inputCheck1, inputCheck2);
        }

        protected Def.Code MoveWait(Def.Motor motor, double destPos, int inputCheck1 = Def.NotUsed, int inputCheck2 = Def.NotUsed)
        {          
            Stopwatch sw = new Stopwatch();
            sw.Start();

            double movetimeover = 10000;
            //double movetimeover = Program.teaching.motor.moveWaitTime;
            //double tolerance = Program.teaching.motor.moveTolerance;
            double tolerance = 0.05;

            bool b;
            bool bInputCheck;

            while (true)
            {
                Thread.Sleep(10);

                if (sw.ElapsedMilliseconds > movetimeover)
                {
                    Stop(motor);
                    Def.Code code = GetMotorTimeOver(motor);
                    return code;
                }

                if (SysOperator.GetUserStop())
                {
                    Stop(motor);
                    return Def.Code.USER_STOP;
                }

                if (IsStop(motor) == false)
                {
                    continue;
                }


                ////////////////////////////////
                /// 모터 정지 상태
                ////////////////////////////////
                if (IsAlarm(motor) == true)
                {
                    Stop(motor);
                    return Def.Code.MOTOR_ALARM;
                }

                if (inputCheck1 != Def.NotUsed)
                {
                    bInputCheck = GetInput(inputCheck1, Def.SensorType.B);
                    if (bInputCheck)
                    {
                        Stop(motor);
                        return Def.Code.MOTOR_EXT_SENSOR_1;
                    }
                }

                if (inputCheck2 != Def.NotUsed)
                {
                    bInputCheck = GetInput(inputCheck2);
                    if (bInputCheck)
                    {
                        Stop(motor);
                        return Def.Code.MOTOR_EXT_SENSOR_2;
                    }
                }

                if (SysOperator.Instance.motorPara.para[(int)motor].motorType == (int)Def.ServoType.SERVO)
                {
                    b = IsPosition(motor, destPos, tolerance);
                    if (b)
                    {
                        ////////////////////////////////
                        /// 모터 정상 정지
                        ////////////////////////////////
                        return Def.Code.OK;
                    }
                    else
                    {
                        Stop(motor);
                        return Def.Code.MOTION_MOVE_FAIL;
                    }
                }

                ////////////////////////////////
                /// 모터 정상 정지
                ////////////////////////////////
                return Def.Code.OK;
            }
        }

        protected Def.Code MoveWait_TorqueLimit(Def.Motor motor, double destPos, double torqueLimit)
        {          
            Stopwatch sw = new Stopwatch();
            sw.Start();

            double movetimeover = 10000;
            //double movetimeover = Program.teaching.motor.moveWaitTime;

            while (true)
            {
                if (GetInMotionStatus(motor) == false)
                {
                    if (IsAlarm(motor) == true)
                    {
                        Stop(motor);
                        return Def.Code.MOTOR_ALARM;
                    }

                    if(IsTorqueOver(motor, torqueLimit))
                    {
                        EmoStop(motor);
                    }

                    break;
                }

                if (sw.ElapsedMilliseconds > movetimeover)                   
                {
                    Stop(motor);
                    Def.Code code = GetMotorTimeOver(motor);
                    return code;
                }

                if (SysOperator.GetUserStop())
                {
                    Stop(motor);
                    return Def.Code.USER_STOP;
                }
                Thread.Sleep(1);
            }

            return Def.Code.OK;
        }

        protected Def.Code GetMotorTimeOver(Def.Motor motor)
        {
            return Def.GetMotorError(motor);
        }

        public virtual bool IsPosition(Def.Motor motor, double destPos, double tolerance)
        {
            if (SysOperator.Instance.motorPara.para[(int)motor].motorType == (int)Def.ServoType.STEP)
            {
                return true;
            }

            double curPos = GetEncPos(motor);

            bool b;
            b = Def.IsRange(curPos, destPos, tolerance);
            return b;
        }


        public abstract void SetTorqueLimit(Def.Motor motor, double plusTorqueLimit, double minusTorqueLimit);


        public abstract void Stop(Def.Motor motor);

        public abstract void EmoStop(Def.Motor motor);

        public bool IsStop(Def.Motor motor)
        {
            bool b = false;
            if (GetInPositionStatus(motor) == true && GetInMotionStatus(motor) == false)
            {
                b = true;
            }
            return b;
        }

        public abstract bool GetInPositionStatus(Def.Motor motor);

        public abstract bool GetInMotionStatus(Def.Motor motor);

        public abstract bool Home(Def.Motor motor);

        public virtual bool HomeWait(Def.Motor motor)
        {          
            Stopwatch sw = new Stopwatch();
            sw.Start();

            double movetimeover = 10000;
            //double movetimeover = Program.teaching.motor.homeWaitTime;

            int nStopCount = 5;

            while (true)
            {
                if (true == IsHomeCompleted(motor) && true == IsStop(motor))
                {
                    break;
                }

                if (sw.ElapsedMilliseconds > movetimeover)                  
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }

                if (SysOperator.GetUserStop() == true)
                {
                    for (int i = 0; i < nStopCount; i++)
                    {
                        Stop(motor);
                        Thread.Sleep(10);
                    }
                    return false;
                }
                Thread.Sleep(1);
            }

            return true;
        }

        public abstract bool IsHomeCompleted(Def.Motor motor); 

        public bool IsHomeAllCompleted(ref string motorName)
        {
            //if (Program.config.virtualMode)
            //{
            //    return true;
            //}

            foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
            {
                if (false == IsHomeCompleted(motor))
                {
                    motorName = motor.ToString();
                    return false;
                }
            }
            return true;
        }

        public abstract double GetCurrentTorque(Def.Motor motor);

        public bool IsTorqueOver(Def.Motor motor, double torqueLimit)
        {
            double curTorque = GetCurrentTorque(motor);
            if (torqueLimit <= curTorque)
            {
                return true;
            }
            return false;
        }

        public abstract bool IsAlarm(Def.Motor motor);

        public void StopAll(int stopCount = 3)
        {
            for (int i = 0; i < stopCount; i++)
            {
                foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
                {
                    Stop(motor);
                }
                Thread.Sleep(10);
            }
        }
        public void EmoStopAll(int stopCount = 1)
        {
            for (int i = 0; i < stopCount; i++)
            {
                foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
                {
                    EmoStop(motor);
                }
                Thread.Sleep(10);
            }
        }


        public abstract bool IsPlusSensor(Def.Motor motor);

        public abstract bool IsHomeSensor(Def.Motor motor);

        public abstract bool IsMinusSensor(Def.Motor motor);

        public bool IsServoOnAll()
        {
            foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
            {
                if (IsServoOn(motor) == false)
                {
                    return false;
                }
            }
            return true;
        }

        public abstract bool IsServoOn(Def.Motor motor);

        public abstract void ServoOn(Def.Motor motor);

        public abstract void ServoOff(Def.Motor motor);

        public void ServoOnAll()
        {
            foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
            {
                ServoOn(motor);
            }
        }

        public void ServoOffAll()
        {
            foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
            {
                ServoOff(motor);
            }
        }

        public void AlarmClearAll()
        {
            foreach (Def.Motor motor in Enum.GetValues(typeof(Def.Motor)))
            {
                ResetAlarm(motor);
            }
        }

        public abstract void SetPosZero(Def.Motor motor); 

        public abstract void ResetAlarm(Def.Motor motor); 

        //public abstract void SignalSetInpos(Def.Motor motor, Level level);

        public abstract void SetParameter(Def.Motor motor);

        public void SetParameterAll()
        {
            int count = Enum.GetNames(typeof(Def.Motor)).Length;
            string[] motors = Enum.GetNames(typeof(Def.Motor));
            foreach (string motorString in motors)
            {
                try
                {
                    Def.Motor motor = (Def.Motor)Enum.Parse(typeof(Def.Motor), motorString);
                    SetParameter(motor);
                }
                catch (ArgumentException)
                {
                    Thread.Sleep(1);
                }
            }
        }

        // 모터 인터락
        public bool CheckCrashMotor(Def.Motor m, ref string errMsg)
        {
            bool bNotCrash = true;
            Def.Motor mt;
            //switch (m)
            //{
            //    case Def.Motor.PPLoader_P1_R:
            //    case Def.Motor.PPLoader_P2_R:
            //    case Def.Motor.PPLoader_P3_R:
            //    case Def.Motor.PPLoader_P4_R:
            //        {
            //            int pi = (int)m - (int)Def.Motor.PPLoader_P1_R;
            //            mt = Def.Motor.PPLoader_P1_Z + pi;
            //            if (!Program.motion.IsServoOn(mt))
            //            {
            //                errMsg = "Is Not Servo On : " + mt.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(mt))
            //            {
            //                errMsg = "Is Not Home : " + mt.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsMinusSensor(mt);
            //                 bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + mt.ToString();
            //                return false;
            //            }
            //            bNotCrash &= bOrg;
            //        }
            //        break;

            //    case Def.Motor.PPUnloader_P1_R:
            //    case Def.Motor.PPUnloader_P2_R:
            //    case Def.Motor.PPUnloader_P3_R:
            //    case Def.Motor.PPUnloader_P4_R:
            //        {
            //            int pi = (int)m - (int)Def.Motor.PPUnloader_P1_R;
            //            mt = Def.Motor.PPUnloader_P1_Z + pi;
            //            if (!Program.motion.IsServoOn(mt))
            //            {
            //                errMsg = "Is Not Servo On : " + mt.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(mt))
            //            {
            //                errMsg = "Is Not Home : " + mt.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsMinusSensor(mt);
            //                 bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + mt.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.AutoCal_X:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + (Def.Motor.Dispenser_Left_Z).ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
            //            {
            //                errMsg = "Is Not Home : " + (Def.Motor.Dispenser_Left_Z).ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
            //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + (Def.Motor.Dispenser_Left_Z).ToString();
            //                return false;
            //            }

            //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + (Def.Motor.Dispenser_Right_Z).ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
            //            {
            //                errMsg = "Is Not Home : " + (Def.Motor.Dispenser_Left_Z).ToString();
            //                return false;
            //            }

            //                 bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
            //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + (Def.Motor.Dispenser_Right_Z).ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.Attach_1_Camera_Z:
            //    case Def.Motor.Attach_2_Camera_Z:
            //    case Def.Motor.Attach_3_Camera_Z:
            //        break;

            //    case Def.Motor.Attach_1_P1_Z:
            //    case Def.Motor.Attach_1_P2_Z:
            //    case Def.Motor.Attach_1_P3_Z:
            //    case Def.Motor.Attach_1_P4_Z:
            //        break;

            //    case Def.Motor.Attach_2_P1_Z:
            //    case Def.Motor.Attach_2_P2_Z:
            //    case Def.Motor.Attach_2_P3_Z:
            //    case Def.Motor.Attach_2_P4_Z:
            //        break;

            //    case Def.Motor.Attach_3_P1_Z:
            //    case Def.Motor.Attach_3_P2_Z:
            //    case Def.Motor.Attach_3_P3_Z:
            //    case Def.Motor.Attach_3_P4_Z:
            //        break;

            //    case Def.Motor.Attach_1_P1_R:
            //    case Def.Motor.Attach_1_P2_R:
            //    case Def.Motor.Attach_1_P3_R:
            //    case Def.Motor.Attach_1_P4_R:
            //        {
            //            double tolerance = Program.teaching.motor.moveTolerance;

            //            int pi = (int)m - (int)Def.Motor.Attach_1_P1_R;
            //            mt = Def.Motor.Attach_1_P1_Z + pi;
            //            if (!Program.motion.IsServoOn(mt))
            //            {
            //                errMsg = "Is Not Servo On : " + mt.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(mt))
            //            {
            //                errMsg = "Is Not Home : " + mt.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(mt);
            //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
            //                 bOrg |= Program.motion.IsMinusSensor(mt);
            //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + mt.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.Attach_2_P1_R:
            //    case Def.Motor.Attach_2_P2_R:
            //    case Def.Motor.Attach_2_P3_R:
            //    case Def.Motor.Attach_2_P4_R:
            //        {
            //            double tolerance = Program.teaching.motor.moveTolerance;

            //            int pi = (int)m - (int)Def.Motor.Attach_2_P1_R;
            //            mt = Def.Motor.Attach_2_P1_Z + pi;
            //            if (!Program.motion.IsServoOn(mt))
            //            {
            //                errMsg = "Is Not Servo On : " + mt.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(mt))
            //            {
            //                errMsg = "Is Not Home : " + mt.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(mt);
            //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
            //                 bOrg |= Program.motion.IsMinusSensor(mt);
            //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + mt.ToString();
            //                return false;
            //            }
            //        }
            //        break;
            //    case Def.Motor.Attach_3_P1_R:
            //    case Def.Motor.Attach_3_P2_R:
            //    case Def.Motor.Attach_3_P3_R:
            //    case Def.Motor.Attach_3_P4_R:
            //        {
            //            double tolerance = Program.teaching.motor.moveTolerance;

            //            int pi = (int)m - (int)Def.Motor.Attach_3_P1_R;
            //            mt = Def.Motor.Attach_3_P1_Z + pi;
            //            if (!Program.motion.IsServoOn(mt))
            //            {
            //                errMsg = "Is Not Servo On : " + mt.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(mt))
            //            {
            //                errMsg = "Is Not Home : " + mt.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(mt);
            //                 bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
            //                 bOrg |= Program.motion.IsMinusSensor(mt);
            //                 bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + mt.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPLoader_P1_Z:
            //    case Def.Motor.PPLoader_P2_Z:
            //    case Def.Motor.PPLoader_P3_Z:
            //    case Def.Motor.PPLoader_P4_Z:
            //    case Def.Motor.PPLoader_Camera_Z:
            //        break;

            //    case Def.Motor.PPUnloader_P1_Z:
            //    case Def.Motor.PPUnloader_P2_Z:
            //    case Def.Motor.PPUnloader_P3_Z:
            //    case Def.Motor.PPUnloader_P4_Z:
            //    case Def.Motor.PPUnloader_Camera_Z:
            //        break;

            //    case Def.Motor.Dispenser_Left_X:
            //    case Def.Motor.Dispenser_Left_Y:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Left_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Left_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
            //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Left_Z.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.Dispenser_Left_Z:
            //        break;

            //    case Def.Motor.Dispenser_Right_X:
            //    case Def.Motor.Dispenser_Right_Y:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Right_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Right_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
            //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Right_Z.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.Dispenser_Right_Z:
            //        break;

            //    case Def.Motor.Dispenser_Laser_Y:
            //        break;

            //    case Def.Motor.LensTrayElevator_1:
            //    case Def.Motor.LensTrayElevator_2:
            //    case Def.Motor.LensTrayElevator_3:
            //        break;

            //    case Def.Motor.LensTrayShuttle_1:
            //    case Def.Motor.LensTrayShuttle_2:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.LensTrayElevator_2))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.LensTrayElevator_2.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.LensTrayElevator_2))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.LensTrayElevator_2.ToString();
            //                return false;
            //            }


            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.LensTrayElevator_2);
            //                 bOrg |= Program.motion.lens.stacker.IsStandby_Z2();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.LensTrayElevator_2.ToString();
            //                return false;
            //            }

            //            double tolerance = Program.teaching.motor.moveTolerance;
      
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.Attach_1_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                     bOrg = Program.motion.IsHomeSensor(mt);
            //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
            //                     bOrg |= Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }

            //                mt = Def.Motor.Attach_2_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                     bOrg = Program.motion.IsHomeSensor(mt);
            //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
            //                     bOrg |= Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }

            //                mt = Def.Motor.Attach_3_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                     bOrg = Program.motion.IsHomeSensor(mt);
            //                     bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
            //                     bOrg |= Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.MagLoader_Z:
            //        {
            //            bNotCrash = !Program.motion.rail.loader.top.GetCarrierExist_Entry();

            //            bNotCrash &= Program.motion.magLoader.GetMagSlider(Def.MagPos.Top);
            //            bNotCrash &= Program.motion.magLoader.GetMagSliderLock(Def.MagPos.Top);

            //            bNotCrash &= Program.motion.magLoader.GetMagSlider(Def.MagPos.Btm);
            //            bNotCrash &= Program.motion.magLoader.GetMagSliderLock(Def.MagPos.Btm);
            //        }
            //        break;

            //    case Def.Motor.MagUnloader_Z:
            //        {
            //            bNotCrash = !Program.motion.rail.unloader.top.GetCarrierExist_Entry();

            //            bNotCrash &= Program.motion.magUnloader.GetMagSlider(Def.MagPos.Top);
            //            bNotCrash &= Program.motion.magUnloader.GetMagSliderLock(Def.MagPos.Top);

            //            bNotCrash &= Program.motion.magUnloader.GetMagSlider(Def.MagPos.Btm);
            //            bNotCrash &= Program.motion.magUnloader.GetMagSliderLock(Def.MagPos.Btm);
            //        }
            //        break;

            //    case Def.Motor.RailIndex_1_X:
            //    case Def.Motor.RailIndex_2_X:
            //    case Def.Motor.RailIndex_3_X:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.PPLoader_Camera_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.PPLoader_Camera_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPLoader_Camera_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.PPLoader_Camera_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPLoader_Camera_Z);
            //                 bOrg |= Program.motion.ppLoader.conv.IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.PPLoader_Camera_Z.ToString();
            //                return false;
            //            }


            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPLoader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                    bOrg = Program.motion.IsMinusSensor(mt);
            //                    bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.RailIndex_4_X:
            //    case Def.Motor.RailIndex_5_X:
            //    case Def.Motor.RailIndex_6_X:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.PPUnloader_Camera_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.PPUnloader_Camera_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPUnloader_Camera_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.PPUnloader_Camera_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPUnloader_Camera_Z);
            //            bOrg |= Program.motion.ppUnloader.camera.IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.PPUnloader_Camera_Z.ToString();
            //                return false;
            //            }

            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPUnloader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                    bOrg = Program.motion.IsMinusSensor(mt);
            //                    bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.Shuttle_1_X:
            //    case Def.Motor.Shuttle_2_X:
            //    case Def.Motor.Shuttle_3_X:
            //    case Def.Motor.Shuttle_4_X:
            //    case Def.Motor.Shuttle_5_X:
            //    case Def.Motor.Shuttle_6_X:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Left_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Left_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Left_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Left_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Left_Z);
            //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Left].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Left_Z.ToString();
            //                return false;
            //            }

            //            if (!Program.motion.IsServoOn(Def.Motor.Dispenser_Right_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.Dispenser_Right_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Dispenser_Right_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.Dispenser_Right_Z.ToString();
            //                return false;
            //            }

            //                 bOrg = Program.motion.IsHomeSensor(Def.Motor.Dispenser_Right_Z);
            //                 bOrg |= Program.motion.dispensing[(int)Def.DispenserIdx.Right].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.Dispenser_Right_Z.ToString();
            //                return false;
            //            }

            //            int si = (int)m - (int)Def.Motor.Shuttle_1_X;
            //            int ai = (int)(si / 2);
            //            bNotCrash &= Program.motion.mask[si].upDown.Get(Def.UpDown.Up);
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPLoader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }
                           
            //                    bOrg = Program.motion.IsMinusSensor(mt);
            //                    bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }

            //                mt = Def.Motor.PPUnloader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                bOrg = Program.motion.IsMinusSensor(mt);
            //                bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }

            //                double tolerance = Program.teaching.motor.moveTolerance;

            //                if (ai == (int)Def.AttachIdx.A1)
            //                {
            //                    mt = Def.Motor.Attach_1_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }
      
            //                        bOrg = Program.motion.IsHomeSensor(mt);
            //                        bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
            //                        bOrg |= Program.motion.IsMinusSensor(mt);
            //                        bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //                else if (ai == (int)Def.AttachIdx.A2)
            //                {
            //                    mt = Def.Motor.Attach_2_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                    bOrg = Program.motion.IsHomeSensor(mt);
            //                    bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
            //                    bOrg |= Program.motion.IsMinusSensor(mt);
            //                    bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //                else
            //                {
            //                    mt = Def.Motor.Attach_3_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                    bOrg = Program.motion.IsHomeSensor(mt);
            //                    bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
            //                    bOrg |= Program.motion.IsMinusSensor(mt);
            //                    bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPLoader_Camera_Y:
            //    case Def.Motor.PPLoader_Conv_W:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.PPLoader_Camera_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.PPLoader_Camera_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPLoader_Camera_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.PPLoader_Camera_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPLoader_Camera_Z);
            //                 bOrg |= Program.motion.ppLoader.conv.IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.PPLoader_Camera_Z.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPLoader_Y:
            //        {
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPLoader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                bool bOrg = Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPUnloader_Camera_Y:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.PPUnloader_Camera_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.PPUnloader_Camera_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.PPUnloader_Camera_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.PPUnloader_Camera_Z.ToString();
            //                return false;
            //            }
                        
            //            bool bOrg = Program.motion.IsHomeSensor(Def.Motor.PPUnloader_Camera_Z);
            //                 bOrg |= Program.motion.ppUnloader.camera.IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.PPUnloader_Camera_Z.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPUnloader_Y:
            //        {
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPUnloader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                bool bOrg = Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.Attach_1_X:
            //    case Def.Motor.Attach_2_X:
            //    case Def.Motor.Attach_3_X:
            //        {
            //            int ai = (int)m - (int)Def.Motor.Attach_1_X;
            //            mt = Def.Motor.Attach_1_Y + ai;
            //            if (!Program.motion.IsServoOn(mt))
            //            {
            //                errMsg = "Is Not Servo On : " + mt.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(mt))
            //            {
            //                errMsg = "Is Not Home : " + mt.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsHomeSensor(mt);
            //                 bOrg |= Program.motion.attach[ai].IsStandby_Y();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + mt.ToString();
            //                return false;
            //            }

            //            double tolerance = Program.teaching.motor.moveTolerance;

            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                if (ai == (int)Def.AttachIdx.A1)
            //                {
            //                    mt = Def.Motor.Attach_1_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                        bOrg = Program.motion.IsHomeSensor(mt);
            //                        bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
            //                        bOrg |= Program.motion.IsMinusSensor(mt);
            //                        bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //                else if (ai == (int)Def.AttachIdx.A2)
            //                {
            //                    mt = Def.Motor.Attach_2_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                         bOrg = Program.motion.IsHomeSensor(mt);
            //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
            //                         bOrg |= Program.motion.IsMinusSensor(mt);
            //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //                else
            //                {
            //                    mt = Def.Motor.Attach_3_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                         bOrg = Program.motion.IsHomeSensor(mt);
            //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
            //                         bOrg |= Program.motion.IsMinusSensor(mt);
            //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.Attach_1_Y:
            //    case Def.Motor.Attach_2_Y:
            //    case Def.Motor.Attach_3_Y:
            //        {
            //            int ai = (int)m - (int)Def.Motor.Attach_1_Y;
            //            bool bOrg = true;
            //            //if (Program.motion.IsHomeCompleted(Def.Motor.Attach_1_X + ai))
            //            //{
            //            //    bNotCrash &= Program.motion.attach[ai].IsStandby_X();
            //            //}
            //            //
            //            double tolerance = Program.teaching.motor.moveTolerance;

            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                if (ai == (int)Def.AttachIdx.A1)
            //                {
            //                    mt = Def.Motor.Attach_1_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                         bOrg = Program.motion.IsHomeSensor(mt);
            //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A1].picker[pi].IsStandby_Z();
            //                         bOrg |= Program.motion.IsMinusSensor(mt);
            //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //                else if (ai == (int)Def.AttachIdx.A2)
            //                {
            //                    mt = Def.Motor.Attach_2_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                         bOrg = Program.motion.IsHomeSensor(mt);
            //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A2].picker[pi].IsStandby_Z();
            //                         bOrg |= Program.motion.IsMinusSensor(mt);
            //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //                else
            //                {
            //                    mt = Def.Motor.Attach_3_P1_Z + pi;
            //                    if (!Program.motion.IsServoOn(mt))
            //                    {
            //                        errMsg = "Is Not Servo On : " + mt.ToString();
            //                        return false;
            //                    }
            //                    if (!Program.motion.IsHomeCompleted(mt))
            //                    {
            //                        errMsg = "Is Not Home : " + mt.ToString();
            //                        return false;
            //                    }

            //                         bOrg = Program.motion.IsHomeSensor(mt);
            //                         bOrg |= Program.motion.attach[(int)Def.AttachIdx.A3].picker[pi].IsStandby_Z();
            //                         bOrg |= Program.motion.IsMinusSensor(mt);
            //                         bOrg |= Program.motion.IsPosition(mt, 0, tolerance);
            //                    if (!bOrg)
            //                    {
            //                        errMsg = "Is Not Org Pos : " + mt.ToString();
            //                        return false;
            //                    }
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.Inspection_1_Y:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.Inspection_1_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.Inspection_1_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Inspection_1_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.Inspection_1_Z.ToString();
            //                return false;
            //            }
                        
            //            bool bOrg = Program.motion.IsMinusSensor(Def.Motor.Inspection_1_Z);
            //                 bOrg |= Program.motion.inspection[(int)Def.InspectionIdx.I1].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.Inspection_1_Z.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.Inspection_2_Y:
            //        {
            //            if (!Program.motion.IsServoOn(Def.Motor.Inspection_2_Z))
            //            {
            //                errMsg = "Is Not Servo On : " + Def.Motor.Inspection_2_Z.ToString();
            //                return false;
            //            }
            //            if (!Program.motion.IsHomeCompleted(Def.Motor.Inspection_2_Z))
            //            {
            //                errMsg = "Is Not Home : " + Def.Motor.Inspection_2_Z.ToString();
            //                return false;
            //            }

            //            bool bOrg = Program.motion.IsMinusSensor(Def.Motor.Inspection_2_Z);
            //                 bOrg |= Program.motion.inspection[(int)Def.InspectionIdx.I2].IsStandby_Z();
            //            if (!bOrg)
            //            {
            //                errMsg = "Is Not Org Pos : " + Def.Motor.Inspection_2_Z.ToString();
            //                return false;
            //            }
            //        }
            //        break;

            //    case Def.Motor.Inspection_1_Z:
            //    case Def.Motor.Inspection_2_Z:
            //        break;

            //    case Def.Motor.Reject_X:
            //        {
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPUnloader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                bool bOrg = Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPLoader_Bottom_Camera_X:
            //        {
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPLoader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                bool bOrg = Program.motion.IsMinusSensor(mt);
            //                bOrg |= Program.motion.ppLoader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //    case Def.Motor.PPUnloader_Bottom_Camera_X:
            //        {
            //            for (int pi = (int)Def.PickerIdx.P1; pi <= (int)Def.PickerIdx.P4; pi++)
            //            {
            //                mt = Def.Motor.PPUnloader_P1_Z + pi;
            //                if (!Program.motion.IsServoOn(mt))
            //                {
            //                    errMsg = "Is Not Servo On : " + mt.ToString();
            //                    return false;
            //                }
            //                if (!Program.motion.IsHomeCompleted(mt))
            //                {
            //                    errMsg = "Is Not Home : " + mt.ToString();
            //                    return false;
            //                }

            //                bool bOrg = Program.motion.IsMinusSensor(mt);
            //                     bOrg |= Program.motion.ppUnloader.picker[pi].IsStandby_Z();
            //                if (!bOrg)
            //                {
            //                    errMsg = "Is Not Org Pos : " + mt.ToString();
            //                    return false;
            //                }
            //            }
            //        }
            //        break;

            //}

            return bNotCrash;
        }


    }
}
