﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static KOSES_SEQ.HARDWARE.MC.AJIN.AJIN_PEM_DEFINE;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.CONFIG;

namespace KOSES_SEQ.HARDWARE.MC.AJIN
{
    public class AJIN_PEM
    {
        public AJIN_PEM()
        {

        }

        enum POS_MOVE_MODE
        {
            ABS = 0,
            REL = 1,
        }

        #region GetMotorNumber
        public int GetMotorNumber(int motor)
        {
            int motorNumber;
            motorNumber = motor;
            return motorNumber;
        }
        #endregion

        #region Open
        public bool Open()
        {
            try
            {
                uint retCode;
                retCode = CAXL.AxlOpenNoReset(7);
                //retCode = CAXL.AxlOpen(7);
                if ((uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS != retCode)
                {
                    return false;
                }
                
                uint uStatus = (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS;
                retCode = CAXM.AxmInfoIsMotionModule(ref uStatus);
                if ((uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS != retCode)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
        #region Close
        public void Close()
        {
            try
            {
                if (1 == CAXL.AxlIsOpened())
                {
                    CAXL.AxlClose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        public int GetMotorCount()
        {
            int motorCount = 0;
            try
            {
                if (1 == CAXM.AxmInfoGetAxisCount(ref motorCount))
                {
                    return motorCount;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }




        #region DIO - Input
        public bool GetInput(int num)
        {
            try
            {
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXD.AxdiReadInport(num, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if (uStatus == 1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion
        #region DIO - Output
        public bool GetOutput(int num)
        {
            try
            {
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXD.AxdoReadOutport(num, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if (uStatus == 1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        public bool SetOutput(int num, bool b)
        {
            try
            {
                uint retCode = 0;
                uint setValue = b ? (uint)1 : (uint)0;
                retCode = CAXD.AxdoWriteOutport(num, setValue);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion


        #region SMAC 모터 설정
        /// <summary>
        /// SMAC 모터는 긴급정지 신호에 의한 정지와 
        /// 리미트 신호에 의한 정지를 사용하지 않게 설정한다.
        /// </summary>
        /// <param name="motor"></param>
        void SetSmacProperty(int motor)
        {
            switch (motor)
            {
                case (int)Def.Motor.Attach_1_P1_Z:             
                case (int)Def.Motor.Attach_1_P1_R:
                            
                    CAXM.AxmSignalSetStop(motor, 0, 2);
                    CAXM.AxmSignalSetLimit(motor, 0, 2, 2);
                    break;
            }
        }
        #endregion

        #region Home
        public bool Home(int motor, Def.Rotate dir, uint homeSignal, uint zPhase, double homeClearTime, double homeOffset, double[] homeVel, double[] homeAcc)
        {

            uint retCode;
            int m = GetMotorNumber(motor);
            int homeDir;

            if (Def.Rotate.CCW == dir)
            {
                homeDir = 0;
            }
            else
            {
                homeDir = 1;
            }

            SetSmacProperty(motor);


            // 해당 축의 원점검색을 수행하기 위해서는 반드시 원점 검색관련 파라메타들이 설정되어 있어야 됩니다. 
            // 만약 MotionPara설정 파일을 이용해 초기화가 정상적으로 수행됐다면 별도의 설정은 필요하지 않다. 
            // 원점검색 방법 설정에는 검색 진행방향, 원점으로 사용할 신호, 원점센서 Active Level, 엔코더 Z상 검출 여부 등을 설정 한다.
            // 주의사항 : 레벨을 잘못 설정시 -방향으로 설정해도  +방향으로 동작할수 있으며, 홈을 찾는데 있어 문제가 될수있다.
            // (자세한 내용은 AxmMotSaveParaAll 설명 부분 참조)
            // 홈레벨은 AxmSignalSetHomeLevel 사용한다.
            // HClrTim : HomeClear Time : 원점 검색 Encoder 값 Set하기 위한 대기시간 
            // HmDir(홈 방향): DIR_CCW (0) -방향 , DIR_CW(1) +방향
            // HOffset - 원점검출후 이동거리.
            // uZphas: 1차 원점검색 완료 후 엔코더 Z상 검출 유무 설정  0: 사용안함 , 1: Hmdir과 반대 방향, 2: Hmdir과 같은 방향
            // HmSig : PosEndLimit(0) -> +Limit
            //         NegEndLimit(1) -> -Limit
            //         HomeSensor (4) -> 원점센서(범용 입력 0)
            retCode = CAXM.AxmHomeSetMethod(m, homeDir, homeSignal, zPhase, homeClearTime, homeOffset);
            if ((uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS != retCode)
            {
                return false;
            }

            retCode = CAXM.AxmHomeSetVel(m,
                homeVel[0],
                homeVel[1],
                homeVel[2],
                homeVel[3],
                homeAcc[0],
                homeAcc[1]);
            if ((uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS != retCode)
            {
                return false;
            }

            retCode = CAXM.AxmHomeSetResult(m, (uint)AXT_MOTION_HOME_RESULT.HOME_ERR_UNKNOWN);
            if ((uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS != retCode)
            {
                return false;
            }

            retCode = CAXM.AxmHomeSetStart((int)m);
            if ((uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS != retCode)
            {
                return false;
            }

            return true;
        }
        #endregion

        uint GetAxlProfileModeNumber(MotorProfile mp)
        {
            // 0: SYMMETRIC_TRAPEZOID  - Symmetric Trapezoid
            // 1: ASYMMETRIC_TRAPEZOID - Asymmetric Trapezoid
            // 3: SYMMETRIC_S_CURVE    - Symmetric S Curve
            // 4: ASYMMETRIC_S_CURVE   - Asymmetric S Curve
            uint mode = 0;
            switch (mp)
            {
                case MotorProfile.SYMMETRIC_TRAPEZOID:
                    mode = 0;
                    break;
                case MotorProfile.ASYMMETRIC_TRAPEZOID:
                    mode = 1;
                    break;
                case MotorProfile.SYMMETRIC_S_CURVE:
                    mode = 2;
                    break;
                case MotorProfile.ASYMMETRIC_S_CURVE:
                    mode = 3;
                    break;
            }
            return mode;
        }

        bool SetProfileMode(int motor, MotorProfile motorProfile)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint mp = GetAxlProfileModeNumber(motorProfile);
                uint retCode = 0;
                retCode = CAXM.AxmMotSetProfileMode(m, mp);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        bool SetMoveMode(int m, POS_MOVE_MODE pmm)
        {
            try
            {
                uint retCode = 0;
                //++ 지정 축의 구동 좌표계를 설정합니다. 
                // dwAbsRelMode : (0)POS_ABS_MODE - 현재 위치와 상관없이 지정한 위치로 절대좌표 이동합니다.
                //                (1)POS_REL_MODE - 현재 위치에서 지정한 양만큼 상대좌표 이동합니다.
                retCode = CAXM.AxmMotSetAbsRelMode((int)m, (uint)pmm);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void SetInposRange(int m, double inposRange)
        {
            CAXM.AxmSignalSetInposRange(m, inposRange);
        }

        public bool MoveAbs(int motor, double dPos, double dAcc, double dVel, double dDec, MotorProfile motorProfile = MotorProfile.SYMMETRIC_TRAPEZOID)
        {
            try
            {
                if (false == SetProfileMode(motor, motorProfile))
                {
                    return false;
                }
                if (false == SetMoveMode(motor, POS_MOVE_MODE.ABS))
                {
                    return false;
                }
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                retCode = CAXM.AxmMoveStartPos(m, dPos, dVel, dAcc, dDec);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    Console.WriteLine($"MoveAbs RetCode : {retCode}");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool MoveAbs_Block(int motor, double dPos, double dAcc, double dVel, double dDec, MotorProfile motorProfile = MotorProfile.SYMMETRIC_TRAPEZOID)
        {
            try
            {
                if (false == SetProfileMode(motor, motorProfile))
                {
                    return false;
                }
                if (false == SetMoveMode(motor, POS_MOVE_MODE.ABS))
                {
                    return false;
                }
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                retCode = CAXM.AxmMovePos(m, dPos, dVel, dAcc, dDec);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    Console.WriteLine($"MoveAbs_Block RetCode : {retCode}");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool MoveRel(int motor, MotorDir dir, double dDis, double dAcc, double dVel, double dDec, MotorProfile motorProfile = MotorProfile.SYMMETRIC_TRAPEZOID)
        {
            try
            {
                if (false == SetProfileMode(motor, motorProfile))
                {
                    return false;
                }
                if (false == SetMoveMode(motor, POS_MOVE_MODE.REL))
                {
                    return false;
                }
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                if (MotorDir.MINUS == dir)
                {
                    dDis = -dDis;
                }
                retCode = CAXM.AxmMoveStartPos(m, dDis, dVel, dAcc, dDec);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool MoveJog(int motor, MotorDir dir, double dAcc, double dVel, double dDec, MotorProfile mp = MotorProfile.SYMMETRIC_TRAPEZOID)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                double jogVel = dVel;
                if (MotorDir.MINUS == dir)
                {
                    jogVel = -dVel;
                }
                if (true == GetInPositionStatus(motor) && false == GetInMotionStatus(motor))
                {
                    retCode = CAXM.AxmMoveVel(m, jogVel, dAcc, dDec);
                }
                else
                {
                    return false;
                }
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void SetTorqueLimit(int motor, double plusTorqueLimit, double minusTorqueLimit)
        {
            CAXM.AxmMotSetTorqueLimit(motor, plusTorqueLimit, minusTorqueLimit);
        }

        public bool IsTorqueLimit(int motor)
        {
            double plusTorqueLimit = 0;
            double minusTorqueLimit = 0;

            CAXM.AxmMotGetTorqueLimit(motor, ref plusTorqueLimit, ref minusTorqueLimit);

            if (plusTorqueLimit != Def.FullTorque || minusTorqueLimit != Def.FullTorque)
            {
                return true;
            }

            return false;
            
        }

        public double GetCurrentTorque(int motor)
        {
            //if (Program.config.virtualMode)
            //{
            //    return 0;
            //}
            double curTorque = 0;
            CAXM.AxmStatusReadTorque(motor, ref curTorque);
            return curTorque;
        }

        public void Stop(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                CAXM.AxmMoveSStop(m);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void EmergencyStop(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                CAXM.AxmMoveEStop(m);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void ResetAlarm(int motor, int nDelay = 500)
        {
            try
            {
                int m = GetMotorNumber(motor);
                CAXM.AxmMoveSStop(m);
                if (true == GetAlarmStatus(motor))
                {
                    CAXM.AxmSignalServoAlarmReset(m, 1);
                    Thread.Sleep(nDelay);
                    CAXM.AxmSignalServoAlarmReset(m, 0);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void SignalSetInpos(int motor, Level level)
        {
            try
            {
                int m = GetMotorNumber(motor);
                CAXM.AxmSignalSetInpos(m, (uint)level);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void SetPosZero(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                CAXM.AxmStatusSetCmdPos(m, 0);
                CAXM.AxmStatusSetActPos(m, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool SetServo(int motor, bool b)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                Stop(motor);
                uint setValue = b ? (uint)1 : (uint)0;
                retCode = CAXM.AxmSignalServoOn(m, setValue);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public double GetCmdPos(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                double dPos = 0.0;
                retCode = CAXM.AxmStatusGetCmdPos(m, ref dPos);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return 0.0;
                }
                return dPos;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0.0;
            }
        }

        public double GetEncPos(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                double dPos = 0.0;
                retCode = CAXM.AxmStatusGetActPos(m, ref dPos);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return 0.0;
                }
                return dPos;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0.0;
            }
        }



        public bool GetAlarmStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXM.AxmSignalReadServoAlarm(m, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uStatus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool GetInPositionStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXM.AxmSignalReadInpos(m, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uStatus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool GetInMotionStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXM.AxmStatusReadInMotion((int)m, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uStatus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool IsMoving(int motor)
        {
            bool inMot = GetInMotionStatus(motor);
            bool inPos = GetInPositionStatus(motor);
            if (inMot == true && inPos == false)
            {
                return true;
            }
            return false;
        }

        public bool IsStop(int motor)
        {
            bool moving = GetInMotionStatus(motor);
            bool inpos = GetInPositionStatus(motor);
            if (moving == false && inpos == true)
            {
                return true;
            }
            return false;
        }

        public bool GetPlusLimitSensorStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uPlus = 0;
                uint uMinus = 0;
                retCode = CAXM.AxmSignalReadLimit(m, ref uPlus, ref uMinus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uPlus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool GetMinusLimitSensorStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uPlus = 0;
                uint uMinus = 0;
                retCode = CAXM.AxmSignalReadLimit(m, ref uPlus, ref uMinus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uMinus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool GetHomeSensorStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXM.AxmHomeReadSignal(m, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uStatus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool GetServoStatus(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXM.AxmSignalIsServoOn((int)m, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uStatus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool GetHomeComplete(int motor)
        {
            try
            {
                int m = GetMotorNumber(motor);
                uint retCode = 0;
                uint uStatus = 0;
                retCode = CAXM.AxmHomeGetResult(m, ref uStatus);
                if (retCode != (uint)AXT_FUNC_RESULT.AXT_RT_SUCCESS)
                {
                    return false;
                }
                if ((uint)AXT_MOTION_SIGNAL_LEVEL.ACTIVE == uStatus)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }


        public void SetMotorParameter(int motor, MotorParameterData para)
        {
            try
            {
                int m = GetMotorNumber(motor);

                // 지정 축의 펄스 당 움직이는 거리를 설정한다.
                CAXM.AxmMotSetMoveUnitPerPulse(m, para.unitPerPulse, 1);

                // 주의사항: 최소속도를 UNIT/PULSE 보다 작게할 경우 최소단위가 UNIT/PULSE로 맞추어지기때문에 최소 속도가 UNIT/PULSE 가 된다.
                // 지정 축에 초기 속도를 설정한다.
                CAXM.AxmMotSetMinVel(m, 1);

                // 지정 축에 등속도 구동 함수에서의 최고 속도를 설정한다.
                // 주의사항 : 입력 최대 속도 값이 PPS가 아니라 UNIT 이다.
                // ex) 최대 출력 주파수(PCI-N804/404 : 10 MPPS)
                // ex) 최대 출력 Unit/Sec(PCI-N804/404 : 10MPPS * Unit/Pulse)
                CAXM.AxmMotSetMaxVel(m, para.vel);

                // 지정 축의 펄스 출력 방식을 설정한다.
                // uMethod  0 :OneHighLowHigh, 1 :OneHighHighLow, 2 :OneLowLowHigh, 3 :OneLowHighLow, 4 :TwoCcwCwHigh
                //          5 :TwoCcwCwLow, 6 :TwoCwCcwHigh, 7 :TwoCwCcwLow, 8 :TwoPhase, 9 :TwoPhaseReverse
                //        OneHighLowHigh                = 0x0,                    // 1펄스 방식, PULSE(Active High), 정방향(DIR=Low)  / 역방향(DIR=High)
                //        OneHighHighLow                = 0x1,                    // 1펄스 방식, PULSE(Active High), 정방향(DIR=High) / 역방향(DIR=Low)
                //        OneLowLowHigh                 = 0x2,                    // 1펄스 방식, PULSE(Active Low),  정방향(DIR=Low)  / 역방향(DIR=High)
                //        OneLowHighLow                 = 0x3,                    // 1펄스 방식, PULSE(Active Low),  정방향(DIR=High) / 역방향(DIR=Low)
                //        TwoCcwCwHigh                  = 0x4,                    // 2펄스 방식, PULSE(CCW:역방향),  DIR(CW:정방향),  Active High
                //        TwoCcwCwLow                   = 0x5,                    // 2펄스 방식, PULSE(CCW:역방향),  DIR(CW:정방향),  Active Low
                //        TwoCwCcwHigh                  = 0x6,                    // 2펄스 방식, PULSE(CW:정방향),   DIR(CCW:역방향), Active High
                //        TwoCwCcwLow                   = 0x7,                    // 2펄스 방식, PULSE(CW:정방향),   DIR(CCW:역방향), Active Low
                //        TwoPhase                      = 0x8,                    // 2상(90' 위상차),  PULSE lead DIR(CW: 정방향), PULSE lag DIR(CCW:역방향)
                //        TwoPhaseReverse               = 0x9                     // 2상(90' 위상차),  PULSE lead DIR(CCW: 정방향), PULSE lag DIR(CW:역방향)
                CAXM.AxmMotSetPulseOutMethod(m, (uint)para.pulseMode);

                // 지정 축의 가속도 단위를 설정한다.
                // AccelUnit : UNIT_SEC2   '0' - 가감속 단위를 unit/sec2 사용
                //             SEC         '1' - 가감속 단위를 sec 사용
                CAXM.AxmMotSetAccelUnit(m, (uint)para.accUnit);

                // 지정 축의 end limit sensor의 사용 유무 및 신호의 입력 레벨을 설정한다. 
                // end limit sensor 신호 입력 시 감속정지 또는 급정지에 대한 설정도 가능하다.
                // uStopMode: EMERGENCY_STOP(0), SLOWDOWN_STOP(1)
                // uPositiveLevel, uNegativeLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)
                CAXM.AxmSignalSetLimit(m, 0, (uint)para.plusSignalLevel, (uint)para.minusSignalLevel);

                // 지정 축의 Inpositon 신호 사용 여부 및 신호 입력 레벨을 설정한다
                // uLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)
                CAXM.AxmSignalSetInpos(m, (uint)para.inposSignalLevel);

                // 지정 축의 Home 센서 Level 을 설정한다.
                // uLevel : LOW(0), HIGH(1)
                CAXM.AxmHomeSetSignalLevel(m, (uint)para.homeSignalLevel);

                // 지정 축의 알람 신호 입력 시 비상 정지의 사용 여부 및 신호 입력 레벨을 설정한다.
                // uLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)
                CAXM.AxmSignalSetServoAlarm(m, (uint)para.alarmSignalLevel);

                // 지정 축의 Servo-On신호의 출력 레벨을 설정한다.
                // uLevel : LOW(0), HIGH(1)
                CAXM.AxmSignalSetServoOnLevel(m, (uint)para.servoSignalLevel);

                // 지정 축의 외부(Actual) 카운트의 증가 방향 설정을 포함하여 지정 축의 Encoder 입력 방식을 설정한다.
                // uMethod : 0 - 7 설정.
                //        ObverseUpDownMode             = 0x0,                     // 정방향 Up/Down
                //        ObverseSqr1Mode               = 0x1,                     // 정방향 1체배
                //        ObverseSqr2Mode               = 0x2,                     // 정방향 2체배
                //        ObverseSqr4Mode               = 0x3,                     // 정방향 4체배
                //        ReverseUpDownMode             = 0x4,                     // 역방향 Up/Down
                //        ReverseSqr1Mode               = 0x5,                     // 역방향 1체배
                //        ReverseSqr2Mode               = 0x6,                     // 역방향 2체배
                //        ReverseSqr4Mode               = 0x7                      // 역방향 4체배
                CAXM.AxmMotSetEncInputMethod(m, (uint)para.encoderCount);


                //  급정지 신호의 사용우무 및 Active Level 설정
                //CAXM.AxmSignalSetStop(m, (uint)para.)

                //
                //CAXM.Axm
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #region 보간 제어

        #endregion 보간 제어

    }
}
