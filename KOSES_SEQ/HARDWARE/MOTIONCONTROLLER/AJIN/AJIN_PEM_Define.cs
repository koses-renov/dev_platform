﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.HARDWARE.MC.AJIN
{
    public class AJIN_PEM_DEFINE
    {
        public enum AjinCode
        {
            OK,
            MOTOR_TIME_OVER,
            USER_STOP,
        }

        public enum MotorProfile
        {
            SYMMETRIC_TRAPEZOID,
            ASYMMETRIC_TRAPEZOID,
            SYMMETRIC_S_CURVE,
            ASYMMETRIC_S_CURVE,
        }

        public static readonly int MaxHomeStepCount = 5;
        public static readonly int DefaultStopCount = MaxHomeStepCount;
        

        public enum AbsRel
        {
            ABS = 0,
            REL
        }

        public enum MotorDir
        {
            MINUS = -1,
            PLUS = 1
        }

        public static MotorDir GetDir(string temp)
        {
            MotorDir e;
            try
            {
                temp = temp.ToUpper();
                e = (MotorDir)Enum.Parse(typeof(MotorDir), temp);
                if (Enum.IsDefined(typeof(MotorDir), e) == false)
                {
                    e = MotorDir.PLUS;
                }
            }
            catch (ArgumentException)
            {
                e = MotorDir.PLUS;
            }
            return e;
        }

        public static MotorProfile TransMotorProfile(Def.MotorProfile p)
        {
            MotorProfile mp = MotorProfile.SYMMETRIC_TRAPEZOID;
            switch (p)
            {
                case Def.MotorProfile.SYMMETRIC_TRAPEZOID:
                    mp = MotorProfile.SYMMETRIC_TRAPEZOID;
                    break;
                case Def.MotorProfile.SYMMETRIC_S_CURVE:
                    mp = MotorProfile.SYMMETRIC_S_CURVE;
                    break;
                case Def.MotorProfile.ASYMMETRIC_TRAPEZOID:
                    mp = MotorProfile.ASYMMETRIC_TRAPEZOID;
                    break;
                case Def.MotorProfile.ASYMMETRIC_S_CURVE:
                    mp = MotorProfile.ASYMMETRIC_S_CURVE;
                    break;
            }
            return mp;
        }

        public static MotorDir TransMotorDir(Def.MotorDir d)
        {
            MotorDir dir = MotorDir.PLUS;
            switch (d)
            {
                case Def.MotorDir.PLUS:
                    dir = MotorDir.PLUS;
                    break;
                case Def.MotorDir.MINUS:
                    dir = MotorDir.MINUS;
                    break;
            }
            return dir;
        }

        // uLevel : LOW(0), HIGH(1), UNUSED(2), USED(3)        
        public enum Level
        {
            LOW = 0,
            HIGH,
            UNUSED,
            USED
        }
    }
}
