﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TEACHING
{
    /// <summary>
    /// 속도, 가속도 셋팅이 필요한 Teaching 위치
    /// </summary>
    [TypeConverter(typeof(ValueTypeConverter<TeachingPosition>))]
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TeachingPosition
    {
        public double Position { get; set; }
        public double Velocity { get; set; }
        public double Acceleration { get; set; }

        public bool UseCustomVelocity { get; set; }
        public override string ToString()
        {
            return " Teaching Position";
        }
    }
}
