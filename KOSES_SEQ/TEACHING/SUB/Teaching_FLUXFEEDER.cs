﻿using KOSES_SEQ.CONFIG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TEACHING.SUB
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class Teaching_FLUXFEEDER : TeachingBase
    {
        public int FeedingCount { get; set; }

        [MotorPosition(DEFINE.Def.Motor.MagLoader_Z)]
        public double ReadyY { get; set; }

        [MotorPosition(DEFINE.Def.Motor.MagLoader_Z)]
        public double FeedY { get; set; }

        [MotorPosition(DEFINE.Def.Motor.MagLoader_Z)]
        public TeachingPosition DownPosition { get; set; }

    }
}
