﻿using KOSES_SEQ.CONFIG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TEACHING.SUB
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class Teaching_Attach : TeachingBase
    {
        [MotorPosition(DEFINE.Def.Motor.Attach_1_P1_R)]
        public double ReadyY { get; set; }

        [MotorPosition(DEFINE.Def.Motor.PPLoader_P1_Z)]
        public double ReadyX { get; set; }

        public Teaching_Attach_Picker Picker = new Teaching_Attach_Picker();

        public Teaching_Attach()
        {
        }

        public override void ResizeRecipe()
        {
        }
    }
}
