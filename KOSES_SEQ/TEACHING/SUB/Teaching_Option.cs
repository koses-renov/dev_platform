﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TEACHING.SUB
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class Teaching_Option : TeachingBase
    {
        public bool IsUseFlux { get; set; }
    }
}
