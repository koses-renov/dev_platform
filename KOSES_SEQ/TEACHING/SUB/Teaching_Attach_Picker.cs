﻿using KOSES_SEQ.CONFIG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TEACHING.SUB
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class Teaching_Attach_Picker : TeachingBase
    {
        public int RetryPickup { get; set; }

        [Description("Unit : mm")]
        [MotorPosition(DEFINE.Def.Motor.Attach_1_P1_Z)]
        public double PickupZ { get; set; }

        [Description("Unit : mm")]
        [MotorPosition(DEFINE.Def.Motor.Attach_1_P1_Z)]
        public double ReadyZ { get; set; }

        [Description("Unit : deg")]
        [MotorPosition(DEFINE.Def.Motor.Attach_1_P1_R)]
        public double ReadyR { get; set; }
    }
}
