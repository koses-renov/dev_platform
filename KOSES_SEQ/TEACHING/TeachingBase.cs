﻿using KOSES_SEQ.MANAGER;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KOSES_SEQ.TEACHING
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class TeachingBase : ICloneable
    {
        //[XmlIgnore, NonSerialized()]
        [XmlIgnore]
        public string NodeName = string.Empty;

        /// <summary>
        /// Tree Node 표현 시 표시할 노드 이름 가져오기
        /// </summary>
        /// <returns></returns>
        public string GetNodeName()
        {
            if (string.IsNullOrEmpty(NodeName))
            {
                return this.GetType().Name;
            }

            return NodeName;
        }

        public object Clone()
        {
            using (MemoryStream m = new MemoryStream())
            {
                BinaryFormatter b = new BinaryFormatter();
                try
                {
                    b.Serialize(m, this);
                    m.Position = 0;
                }
                catch (Exception ex)
                {
                    LogManager.ExLog(ex);
                }
                return (TeachingBase)b.Deserialize(m);
            }
        }

        public virtual void ResizeRecipe()
        {

        }
    }
}
