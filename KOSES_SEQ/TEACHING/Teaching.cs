﻿using KOSES_SEQ.CONFIG;
using KOSES_SEQ.TEACHING.SUB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TEACHING
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class Teaching : TeachingBase
    {
        public Teaching_Common Common = new Teaching_Common();
        public Teaching_Loader Loader = new Teaching_Loader();
        public Teaching_Attach Attach = new Teaching_Attach();
        public Teaching_Option Option = new Teaching_Option();
        public Teaching_TEST Test = new Teaching_TEST();


        public Teaching_FLUXFEEDER Feeder = new Teaching_FLUXFEEDER();
        //public Teaching_Loader2 Loader2 = new Teaching_Loader2();

        public Teaching()
        {
        }

        public void SetTeachingNodeName()
        {
            Common.NodeName = "Common";
            Loader.NodeName = "Loader";
            Attach.NodeName = "Attach";
            Attach.Picker.NodeName = "Picker";
            Option.NodeName = "Option";
        }

        public override void ResizeRecipe()
        {
            Attach.ResizeRecipe();
        }
    }
}
