﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace KOSES_SEQ.ALARM
{
    public enum AlarmLevel
    {
        Info,   // 단순 정보
        Light,  // 경알람
        Heavy,  // 중알람
        Emegency,   // 비상알람
    }

    /// <summary>
    /// 알람
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class CAlarm : ICloneable
    {
        // 알람 고유 번호
        public int AlarmCode { get; set; }

        // 알람 내부 관리 번호
        public int AlarmNo { get; set; }

        // 알람 메시지
        public string Message { get; set; }

        // 알람설명
        public string Description { get; set; }

        // 알람 원인
        public string Cause { get; set; }

        // 조치 내용
        public string Action { get; set; }

        // 알람 수준
        public AlarmLevel AlarmLevel { get; set; }

        // 알람 발생 시간
        public DateTime OccurredTime { get; set; }

        // 알람 횟수로 카운팅 할지
        public bool IsCountAlarm { get; set; }

        // 알람 발생 시 뷰 이미지
        public string ImageFileName { get; set; }

        // 알람 발생 시 이미지 내 표시 위치
        public int ImagePivotX { get; set; }
        public int ImagePivotY { get; set; }

        public CAlarm()
        {
            OccurredTime = DateTime.Now;
        }

        public object Clone()
        {
            using (MemoryStream m = new MemoryStream())
            {
                BinaryFormatter b = new BinaryFormatter();
                try
                {
                    b.Serialize(m, this);
                    m.Position = 0;
                }
                catch (Exception ex)
                {
                }
                return (CAlarm)b.Deserialize(m);
            }
        }
    }
}
