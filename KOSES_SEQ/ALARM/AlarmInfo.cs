﻿using KOSES_SEQ.MANAGER;
using System;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace KOSES_SEQ.ALARM
{
    /// <summary>
    /// 알람 정보 관리 Table
    /// </summary>
    public class AlarmInfo : DBTable
    {
        public override string TableName
        {
            get { return "AlarmList"; }
        }

        /// <summary>
        /// Alarm List Table 생성 (추 후 컬럼 자동 생성으로 변경하자)
        /// </summary>
        /// <returns></returns>
        public override string MakeTable()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append($"CREATE TABLE IF NOT EXISTS {TableName} (");

                sb.Append("AlarmCode        INTEGER PRIMARY KEY NOT NULL,");
                sb.Append("AlarmNo          INTEGER,");
                sb.Append("AlarmLevel       INTEGER,");
                sb.Append("Message          TEXT,");
                sb.Append("Description      TEXT,");
                sb.Append("Cause            TEXT,");
                sb.Append("Action           TEXT,");
                sb.Append("IsCountAlarm     INTEGER,");
                sb.Append("ImageFileName    TEXT,");
                sb.Append("ImagePivotX      INTEGER,");
                sb.Append("ImagePivotY      INTEGER)");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }

            return sb.ToString();
        }


        /// <summary>
        /// Alarm List 에 nr개 빈 row 생성
        /// </summary>
        public void MakeEmptyAlarmList(int rowCount = 2000)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("WITH cte AS( ");
                sb.Append("SELECT 1 nr ");
                sb.Append("UNION ALL ");
                sb.Append("SELECT nr + 1 ");
                sb.Append("FROM cte ");
                sb.Append($"WHERE nr < {rowCount}) ");

                sb.Append($"INSERT INTO {TableName}(rowid) ");
                sb.Append("SELECT nr FROM cte ");
                sb.Append("WHERE NOT EXISTS(SELECT nr FROM AlarmList WHERE AlarmCode = nr)");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
        }

        public DataTable GetAlarms(int[] codes, bool isDetailed = false)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if (isDetailed)
                {
                    sb.Append($"SELECT * FROM {TableName}");
                }
                else
                {
                    sb.Append($"SELECT AlarmCode, Message FROM {TableName} ");
                }
                sb.Append("WHERE ");
                int count = 0;
                foreach (int i in codes)
                {
                    sb.Append($"AlarmCode == {i} ");

                    count++;

                    if (count < codes.Length)
                        sb.Append("OR ");
                }

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 모든 알람 리스트를 반환
        /// </summary>
        /// <param name="isDetailed">true : 모든 Column 정보, false : 간략한 정보(code, message)</param>
        /// <returns></returns>
        public DataTable GetAlarmLists(bool isDetailed = false)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if (isDetailed)
                {
                    sb.Append($"SELECT * FROM {TableName}");
                }
                else
                {
                    sb.Append($"SELECT AlarmCode, Message FROM {TableName}");
                }

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Alarm List 내 alarm code에 해당하는 Alarm 정보를 반환
        /// </summary>
        /// <param name="alarmCode"></param>
        /// <returns></returns>
        public CAlarm GetAlarm(int alarmCode)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"SELECT * FROM {TableName} ");
                sb.Append($"WHERE AlarmCode == {alarmCode}");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                if (dt.Rows.Count == 0)
                    return null;

                #region Data table to CAlarm class

                var item = dt.Rows[0].ItemArray;

                CAlarm alarm = new CAlarm();
                if (int.TryParse(item[0].ToString(), out int code))
                    alarm.AlarmCode = code;
                if (int.TryParse(item[1].ToString(), out int no))
                {
                    alarm.AlarmNo = no;
                }

                if (Enum.TryParse(item[2].ToString(), out AlarmLevel level))
                {
                    alarm.AlarmLevel = level;
                }

                alarm.Message = item[3].ToString();
                alarm.Description = item[4].ToString();
                alarm.Cause = item[5].ToString();
                alarm.Action = item[6].ToString();
                alarm.IsCountAlarm = item[7].ToString() == "1";
                alarm.ImageFileName = item[8].ToString();

                if (int.TryParse(item[9].ToString(), out int pivotX))
                {
                    alarm.ImagePivotX = pivotX;
                }

                if (int.TryParse(item[10].ToString(), out int pivotY))
                {
                    alarm.ImagePivotY = pivotY;
                }

                #endregion

                return alarm;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Alarm List 내 Alarm 정보 갱신
        /// </summary>
        /// <param name="alarm"></param>
        public void UpdateAlarmInfo(CAlarm alarm)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"UPDATE {TableName} SET ");

                sb.Append($"AlarmNo         = {alarm.AlarmNo},");
                sb.Append($"AlarmLevel      = {(int)alarm.AlarmLevel},");
                sb.Append($"Message         = '{alarm.Message}',");
                sb.Append($"Description     = '{alarm.Description}',");
                sb.Append($"Cause           = '{alarm.Cause}',");
                sb.Append($"Action          = '{alarm.Action}',");
                sb.Append($"IsCountAlarm    = {(alarm.IsCountAlarm ? 1 : 0)},");
                sb.Append($"ImageFileName          = '{alarm.ImageFileName}',");
                sb.Append($"ImagePivotX     = {alarm.ImagePivotX},");
                sb.Append($"ImagePivotY     = {alarm.ImagePivotY} ");

                sb.Append($"WHERE AlarmCode == {alarm.AlarmCode}");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// DataTable를 AlarmList Table로 Load 
        /// </summary>
        /// <param name="dt"></param>
        public void LoadAlarmList(DataTable dt)
        {
            try
            {
                #region Column Name 추출

                string sqlString = $"INSERT into {TableName} (";
                string valString = "";
                var sqlParams = new string[dt.Rows[0].ItemArray.Count()];
                int count = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    sqlString += dc.ColumnName + ", ";
                    valString += "@" + dc.ColumnName + ", ";
                    sqlParams[count] = "@" + dc.ColumnName;
                    count++;
                }
                valString = valString.Substring(0, valString.Length - 2);
                sqlString = sqlString.Substring(0, sqlString.Length - 2) + ") VALUES (" + valString + ")";

                #endregion

                // 행 갯수 만큼 Insert
                SQLiteCommand cmd = new SQLiteCommand(sqlString, DbConnection);
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < dr.ItemArray.Count(); i++)
                    {
                        cmd.Parameters.AddWithValue(sqlParams[i], dr.ItemArray[i] == null ? DBNull.Value : dr.ItemArray[i]);
                    }

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 주의) 모든 알람 리스트 삭제
        /// </summary>
        public void DeleteAlarmLists()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"DELETE FROM {TableName} ");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
        }

    }
}
