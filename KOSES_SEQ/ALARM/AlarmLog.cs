﻿using KOSES_SEQ.MANAGER;
using System;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace KOSES_SEQ.ALARM
{
    /// <summary>
    /// 알람 로그 Table
    /// </summary>
    public class AlarmLog : DBTable
    {
        public override string TableName
        {
            get { return "AlarmLog"; }
        }

        /// <summary>
        /// Alarm Log Table 및 Column 생성 (추 후 컬럼 자동 생성으로 변경하자)
        /// </summary>
        /// <returns></returns>
        public override string MakeTable()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.Append($"CREATE TABLE IF NOT EXISTS {TableName} (");

                sb.Append("No               INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
                sb.Append("OccurredTime     DATETIME,");
                sb.Append("AlarmCode        INTEGER,");
                sb.Append("Message          TEXT,");
                sb.Append("Description      TEXT,");
                sb.Append("Cause            TEXT,");
                sb.Append("Action           TEXT,");
                sb.Append("IsCountAlarm     INTEGER)");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }

            return sb.ToString();
        }

        /// <summary>
        /// Alarm이 발생하여 Log 추가
        /// </summary>
        /// <param name="alarm"></param>
        public void AddAlarmLog(CAlarm alarm)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"INSERT INTO {TableName} (OccurredTime, AlarmCode, Message) VALUES(");
                sb.Append($"'{alarm.OccurredTime:yyyy-MM-dd HH:mm:ss.fff}', ");
                sb.Append($"{alarm.AlarmCode}, ");
                sb.Append($"'{alarm.Message}')");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 모든 Alarm Log를 반환 함 (로그가 많을 시 주의)
        /// </summary>
        /// <returns></returns>
        public DataTable GetAlarmLogs()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"SELECT * FROM {TableName}");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        /// <summary>
        /// from ~ to 의 Alarm Log를 반환
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public DataTable GetAlarmLogs(DateTime from, DateTime to)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"SELECT OccurredTime, AlarmCode, Message FROM {TableName} ");
                sb.Append($"WHERE OccurredTime >= '{from:yyyy-MM-dd HH:mm}' AND OccurredTime <= '{to:yyyy-MM-dd HH:mm}'");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetAlarmRealMessage(int code, DateTime time)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"SELECT Message FROM {TableName} ");
                sb.Append($"WHERE AlarmCode == {code} AND OccurredTime == '{time:yyyy-MM-dd HH:mm}'");

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 알람 로그 삭제
        /// from이 null이면 'to' DateTime까지 알람 로그 삭제
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public void DeleteAlarmLogs(DateTime from, DateTime to)
        {
            try
            {
                if (to == null)
                    return;

                StringBuilder sb = new StringBuilder();
                sb.Append($"DELETE FROM {TableName} ");

                if (from != null)
                {
                    sb.Append($"WHERE OccurredTime >= '{from}' AND OccurredTime <= '{to}'");
                }
                else
                {
                    sb.Append($"WHERE OccurredTime <= '{to}'");
                }

                string query = sb.ToString();
                SQLiteCommand cmd = new SQLiteCommand(query, DbConnection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
        }


    }
}
