﻿namespace KOSES_SEQ.OP
{
    partial class frmBase
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBase));
            this.timer_SystemTime = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panBaseTop = new System.Windows.Forms.Panel();
            this.lbl_Login = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_MachineStatus = new System.Windows.Forms.Label();
            this.lbl_Version = new System.Windows.Forms.Label();
            this.lbl_SysTemDateTime = new System.Windows.Forms.Label();
            this.lbl_LoginUser = new System.Windows.Forms.Label();
            this.lbl_Message = new System.Windows.Forms.Label();
            this.lbl_JobName = new System.Windows.Forms.Label();
            this.lbl_ModelName = new System.Windows.Forms.Label();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.panLogo = new System.Windows.Forms.Panel();
            this.panBase = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.panBaseBottom = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.btnMotorStatus = new System.Windows.Forms.Button();
            this.btnAlarm = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnRecipe = new System.Windows.Forms.Button();
            this.btnManual = new System.Windows.Forms.Button();
            this.btnMain = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOption = new System.Windows.Forms.Button();
            this.panBaseRight = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panBaseTop.SuspendLayout();
            this.panBase.SuspendLayout();
            this.panBaseBottom.SuspendLayout();
            this.panBaseRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer_SystemTime
            // 
            this.timer_SystemTime.Interval = 500;
            this.timer_SystemTime.Tick += new System.EventHandler(this.timer_SystemTime_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "box-white.png");
            this.imageList1.Images.SetKeyName(1, "box-green.png");
            this.imageList1.Images.SetKeyName(2, "box-red.png");
            this.imageList1.Images.SetKeyName(3, "box-Yellow.bmp");
            // 
            // panBaseTop
            // 
            this.panBaseTop.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panBaseTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panBaseTop.Controls.Add(this.lbl_Login);
            this.panBaseTop.Controls.Add(this.label1);
            this.panBaseTop.Controls.Add(this.lbl_MachineStatus);
            this.panBaseTop.Controls.Add(this.lbl_Version);
            this.panBaseTop.Controls.Add(this.lbl_SysTemDateTime);
            this.panBaseTop.Controls.Add(this.lbl_LoginUser);
            this.panBaseTop.Controls.Add(this.lbl_Message);
            this.panBaseTop.Controls.Add(this.lbl_JobName);
            this.panBaseTop.Controls.Add(this.lbl_ModelName);
            this.panBaseTop.Controls.Add(this.btn_Close);
            this.panBaseTop.Controls.Add(this.btnMinimize);
            this.panBaseTop.Controls.Add(this.panLogo);
            this.panBaseTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panBaseTop.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panBaseTop.Location = new System.Drawing.Point(0, 0);
            this.panBaseTop.Name = "panBaseTop";
            this.panBaseTop.Padding = new System.Windows.Forms.Padding(1);
            this.panBaseTop.Size = new System.Drawing.Size(1280, 65);
            this.panBaseTop.TabIndex = 962;
            this.panBaseTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panBaseTop_Paint_1);
            this.panBaseTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmBase_MouseDown);
            this.panBaseTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmBase_MouseMove);
            // 
            // lbl_Login
            // 
            this.lbl_Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_Login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_Login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_Login.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Login.Image = ((System.Drawing.Image)(resources.GetObject("lbl_Login.Image")));
            this.lbl_Login.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_Login.Location = new System.Drawing.Point(865, 33);
            this.lbl_Login.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_Login.Name = "lbl_Login";
            this.lbl_Login.Size = new System.Drawing.Size(144, 29);
            this.lbl_Login.TabIndex = 988;
            this.lbl_Login.Text = "로그인";
            this.lbl_Login.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Login.Click += new System.EventHandler(this.lbl_Login_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1191, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 29);
            this.label1.TabIndex = 986;
            this.label1.Text = "???";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_MachineStatus
            // 
            this.lbl_MachineStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_MachineStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_MachineStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_MachineStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_MachineStatus.Location = new System.Drawing.Point(121, 33);
            this.lbl_MachineStatus.Name = "lbl_MachineStatus";
            this.lbl_MachineStatus.Size = new System.Drawing.Size(144, 29);
            this.lbl_MachineStatus.TabIndex = 985;
            this.lbl_MachineStatus.Text = "Machine Status";
            this.lbl_MachineStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Version
            // 
            this.lbl_Version.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_Version.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Version.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_Version.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Version.Location = new System.Drawing.Point(1011, 33);
            this.lbl_Version.Name = "lbl_Version";
            this.lbl_Version.Size = new System.Drawing.Size(179, 29);
            this.lbl_Version.TabIndex = 984;
            this.lbl_Version.Text = "Version Info";
            this.lbl_Version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_SysTemDateTime
            // 
            this.lbl_SysTemDateTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_SysTemDateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_SysTemDateTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_SysTemDateTime.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SysTemDateTime.Location = new System.Drawing.Point(1011, 0);
            this.lbl_SysTemDateTime.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_SysTemDateTime.Name = "lbl_SysTemDateTime";
            this.lbl_SysTemDateTime.Size = new System.Drawing.Size(179, 29);
            this.lbl_SysTemDateTime.TabIndex = 982;
            this.lbl_SysTemDateTime.Text = "System Date";
            this.lbl_SysTemDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_LoginUser
            // 
            this.lbl_LoginUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_LoginUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_LoginUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_LoginUser.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LoginUser.Location = new System.Drawing.Point(865, 0);
            this.lbl_LoginUser.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_LoginUser.Name = "lbl_LoginUser";
            this.lbl_LoginUser.Size = new System.Drawing.Size(144, 29);
            this.lbl_LoginUser.TabIndex = 981;
            this.lbl_LoginUser.Text = "Login User";
            this.lbl_LoginUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Message
            // 
            this.lbl_Message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_Message.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Message.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_Message.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Message.Location = new System.Drawing.Point(266, 33);
            this.lbl_Message.Name = "lbl_Message";
            this.lbl_Message.Size = new System.Drawing.Size(598, 29);
            this.lbl_Message.TabIndex = 980;
            this.lbl_Message.Text = "Message";
            this.lbl_Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_JobName
            // 
            this.lbl_JobName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_JobName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_JobName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_JobName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_JobName.Location = new System.Drawing.Point(266, 0);
            this.lbl_JobName.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_JobName.Name = "lbl_JobName";
            this.lbl_JobName.Size = new System.Drawing.Size(598, 29);
            this.lbl_JobName.TabIndex = 979;
            this.lbl_JobName.Text = "Job Name";
            this.lbl_JobName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_ModelName
            // 
            this.lbl_ModelName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.lbl_ModelName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_ModelName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_ModelName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ModelName.Location = new System.Drawing.Point(121, 0);
            this.lbl_ModelName.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_ModelName.Name = "lbl_ModelName";
            this.lbl_ModelName.Size = new System.Drawing.Size(144, 29);
            this.lbl_ModelName.TabIndex = 978;
            this.lbl_ModelName.Text = "Model Name";
            this.lbl_ModelName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.btn_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Close.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btn_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Close.Location = new System.Drawing.Point(1233, 0);
            this.btn_Close.Margin = new System.Windows.Forms.Padding(0);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(45, 30);
            this.btn_Close.TabIndex = 977;
            this.btn_Close.Text = "X";
            this.btn_Close.UseVisualStyleBackColor = false;
            this.btn_Close.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(120)))), ((int)(((byte)(170)))));
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnMinimize.ForeColor = System.Drawing.Color.White;
            this.btnMinimize.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMinimize.Location = new System.Drawing.Point(1190, 0);
            this.btnMinimize.Margin = new System.Windows.Forms.Padding(0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(45, 30);
            this.btnMinimize.TabIndex = 949;
            this.btnMinimize.Text = "-";
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // panLogo
            // 
            this.panLogo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panLogo.BackgroundImage")));
            this.panLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panLogo.Location = new System.Drawing.Point(1, 1);
            this.panLogo.Name = "panLogo";
            this.panLogo.Size = new System.Drawing.Size(118, 61);
            this.panLogo.TabIndex = 947;
            // 
            // panBase
            // 
            this.panBase.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panBase.Controls.Add(this.btnReset);
            this.panBase.Controls.Add(this.btnStop);
            this.panBase.Controls.Add(this.btnHome);
            this.panBase.Controls.Add(this.btnStart);
            this.panBase.Controls.Add(this.panBaseBottom);
            this.panBase.Controls.Add(this.panBaseRight);
            this.panBase.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panBase.Location = new System.Drawing.Point(2, 65);
            this.panBase.Margin = new System.Windows.Forms.Padding(0);
            this.panBase.Name = "panBase";
            this.panBase.Size = new System.Drawing.Size(1278, 959);
            this.panBase.TabIndex = 963;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnReset.Location = new System.Drawing.Point(1153, 351);
            this.btnReset.Name = "btnReset";
            this.btnReset.Padding = new System.Windows.Forms.Padding(5);
            this.btnReset.Size = new System.Drawing.Size(112, 104);
            this.btnReset.TabIndex = 983;
            this.btnReset.Text = "리셋";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            this.btnReset.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnReset.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Image = ((System.Drawing.Image)(resources.GetObject("btnStop.Image")));
            this.btnStop.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStop.Location = new System.Drawing.Point(1153, 238);
            this.btnStop.Name = "btnStop";
            this.btnStop.Padding = new System.Windows.Forms.Padding(5);
            this.btnStop.Size = new System.Drawing.Size(112, 104);
            this.btnStop.TabIndex = 982;
            this.btnStop.Text = "정지";
            this.btnStop.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            this.btnStop.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnStop.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnHome.Location = new System.Drawing.Point(1153, 12);
            this.btnHome.Name = "btnHome";
            this.btnHome.Padding = new System.Windows.Forms.Padding(5);
            this.btnHome.Size = new System.Drawing.Size(112, 104);
            this.btnHome.TabIndex = 981;
            this.btnHome.Text = "초기화";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            this.btnHome.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnHome.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStart.Location = new System.Drawing.Point(1153, 125);
            this.btnStart.Name = "btnStart";
            this.btnStart.Padding = new System.Windows.Forms.Padding(5);
            this.btnStart.Size = new System.Drawing.Size(112, 104);
            this.btnStart.TabIndex = 980;
            this.btnStart.Text = "시작";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            this.btnStart.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnStart.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // panBaseBottom
            // 
            this.panBaseBottom.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panBaseBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panBaseBottom.Controls.Add(this.button7);
            this.panBaseBottom.Controls.Add(this.btnMotorStatus);
            this.panBaseBottom.Controls.Add(this.btnAlarm);
            this.panBaseBottom.Controls.Add(this.btnSetting);
            this.panBaseBottom.Controls.Add(this.btnRecipe);
            this.panBaseBottom.Controls.Add(this.btnManual);
            this.panBaseBottom.Controls.Add(this.btnMain);
            this.panBaseBottom.Controls.Add(this.btnClose);
            this.panBaseBottom.Controls.Add(this.btnOption);
            this.panBaseBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panBaseBottom.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panBaseBottom.Location = new System.Drawing.Point(0, 874);
            this.panBaseBottom.Margin = new System.Windows.Forms.Padding(0);
            this.panBaseBottom.Name = "panBaseBottom";
            this.panBaseBottom.Size = new System.Drawing.Size(1276, 83);
            this.panBaseBottom.TabIndex = 960;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button7.Location = new System.Drawing.Point(989, 1);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(5);
            this.button7.Size = new System.Drawing.Size(140, 80);
            this.button7.TabIndex = 980;
            this.button7.Text = "MAINT MODE";
            this.button7.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.button7.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnMotorStatus
            // 
            this.btnMotorStatus.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnMotorStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMotorStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMotorStatus.Image = ((System.Drawing.Image)(resources.GetObject("btnMotorStatus.Image")));
            this.btnMotorStatus.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnMotorStatus.Location = new System.Drawing.Point(848, 1);
            this.btnMotorStatus.Margin = new System.Windows.Forms.Padding(0);
            this.btnMotorStatus.Name = "btnMotorStatus";
            this.btnMotorStatus.Padding = new System.Windows.Forms.Padding(5);
            this.btnMotorStatus.Size = new System.Drawing.Size(140, 80);
            this.btnMotorStatus.TabIndex = 979;
            this.btnMotorStatus.Text = "MOTORSTATUS";
            this.btnMotorStatus.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnMotorStatus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMotorStatus.UseVisualStyleBackColor = false;
            this.btnMotorStatus.Click += new System.EventHandler(this.btnMotorStatus_Click);
            this.btnMotorStatus.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnMotorStatus.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnAlarm
            // 
            this.btnAlarm.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnAlarm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlarm.Image = ((System.Drawing.Image)(resources.GetObject("btnAlarm.Image")));
            this.btnAlarm.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnAlarm.Location = new System.Drawing.Point(707, 1);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(0);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Padding = new System.Windows.Forms.Padding(5);
            this.btnAlarm.Size = new System.Drawing.Size(140, 80);
            this.btnAlarm.TabIndex = 978;
            this.btnAlarm.Text = "ALARM";
            this.btnAlarm.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnAlarm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAlarm.UseVisualStyleBackColor = false;
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            this.btnAlarm.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnAlarm.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSetting.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetting.Image = ((System.Drawing.Image)(resources.GetObject("btnSetting.Image")));
            this.btnSetting.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSetting.Location = new System.Drawing.Point(284, 1);
            this.btnSetting.Margin = new System.Windows.Forms.Padding(0);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Padding = new System.Windows.Forms.Padding(5);
            this.btnSetting.Size = new System.Drawing.Size(140, 80);
            this.btnSetting.TabIndex = 977;
            this.btnSetting.Text = "TEACHING";
            this.btnSetting.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnSetting.UseVisualStyleBackColor = false;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            this.btnSetting.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnSetting.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnRecipe
            // 
            this.btnRecipe.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnRecipe.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRecipe.Image = ((System.Drawing.Image)(resources.GetObject("btnRecipe.Image")));
            this.btnRecipe.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRecipe.Location = new System.Drawing.Point(566, 1);
            this.btnRecipe.Margin = new System.Windows.Forms.Padding(0);
            this.btnRecipe.Name = "btnRecipe";
            this.btnRecipe.Padding = new System.Windows.Forms.Padding(5);
            this.btnRecipe.Size = new System.Drawing.Size(140, 80);
            this.btnRecipe.TabIndex = 976;
            this.btnRecipe.Text = "RECIPE";
            this.btnRecipe.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnRecipe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRecipe.UseVisualStyleBackColor = false;
            this.btnRecipe.Click += new System.EventHandler(this.btnRecipe_Click);
            this.btnRecipe.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnRecipe.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnManual
            // 
            this.btnManual.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnManual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnManual.Image = ((System.Drawing.Image)(resources.GetObject("btnManual.Image")));
            this.btnManual.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnManual.Location = new System.Drawing.Point(143, 1);
            this.btnManual.Margin = new System.Windows.Forms.Padding(0);
            this.btnManual.Name = "btnManual";
            this.btnManual.Padding = new System.Windows.Forms.Padding(5);
            this.btnManual.Size = new System.Drawing.Size(140, 80);
            this.btnManual.TabIndex = 975;
            this.btnManual.Text = "MANUAL";
            this.btnManual.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnManual.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnManual.UseVisualStyleBackColor = false;
            this.btnManual.Click += new System.EventHandler(this.btnManual_Click);
            this.btnManual.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnManual.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnMain
            // 
            this.btnMain.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMain.Image = ((System.Drawing.Image)(resources.GetObject("btnMain.Image")));
            this.btnMain.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnMain.Location = new System.Drawing.Point(2, 1);
            this.btnMain.Margin = new System.Windows.Forms.Padding(5);
            this.btnMain.Name = "btnMain";
            this.btnMain.Padding = new System.Windows.Forms.Padding(5);
            this.btnMain.Size = new System.Drawing.Size(140, 80);
            this.btnMain.TabIndex = 974;
            this.btnMain.Text = "AUTO";
            this.btnMain.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnMain.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMain.UseVisualStyleBackColor = false;
            this.btnMain.Click += new System.EventHandler(this.btnMain_Click);
            this.btnMain.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnMain.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnClose.Location = new System.Drawing.Point(1130, 1);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Padding = new System.Windows.Forms.Padding(5);
            this.btnClose.Size = new System.Drawing.Size(140, 80);
            this.btnClose.TabIndex = 973;
            this.btnClose.Text = "CLOSE";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnClose.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // btnOption
            // 
            this.btnOption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnOption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOption.Image = ((System.Drawing.Image)(resources.GetObject("btnOption.Image")));
            this.btnOption.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnOption.Location = new System.Drawing.Point(425, 1);
            this.btnOption.Margin = new System.Windows.Forms.Padding(0);
            this.btnOption.Name = "btnOption";
            this.btnOption.Padding = new System.Windows.Forms.Padding(5);
            this.btnOption.Size = new System.Drawing.Size(140, 80);
            this.btnOption.TabIndex = 972;
            this.btnOption.Text = "SYSTEM";
            this.btnOption.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnOption.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOption.UseVisualStyleBackColor = false;
            this.btnOption.Click += new System.EventHandler(this.btnOption_Click);
            this.btnOption.MouseEnter += new System.EventHandler(this.btnHome_MouseEnter);
            this.btnOption.MouseLeave += new System.EventHandler(this.btnHome_MouseLeave);
            // 
            // panBaseRight
            // 
            this.panBaseRight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panBaseRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panBaseRight.Controls.Add(this.button1);
            this.panBaseRight.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panBaseRight.Location = new System.Drawing.Point(1140, -1);
            this.panBaseRight.Margin = new System.Windows.Forms.Padding(0);
            this.panBaseRight.Name = "panBaseRight";
            this.panBaseRight.Size = new System.Drawing.Size(135, 875);
            this.panBaseRight.TabIndex = 958;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(12, 792);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.button1.Size = new System.Drawing.Size(112, 75);
            this.button1.TabIndex = 952;
            this.button1.Text = "모터파라메터 Save";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmBase
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1280, 1024);
            this.Controls.Add(this.panBase);
            this.Controls.Add(this.panBaseTop);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Base";
            this.Load += new System.EventHandler(this.frmBase_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmBase_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmBase_MouseMove);
            this.Resize += new System.EventHandler(this.frmBase_Resize);
            this.panBaseTop.ResumeLayout(false);
            this.panBase.ResumeLayout(false);
            this.panBaseBottom.ResumeLayout(false);
            this.panBaseRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer_SystemTime;
        private System.Windows.Forms.ImageList imageList1;
        internal System.Windows.Forms.Panel panBaseTop;
        internal System.Windows.Forms.Panel panBase;
        internal System.Windows.Forms.Panel panBaseBottom;
        private System.Windows.Forms.Button btnManual;
        private System.Windows.Forms.Button btnMain;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOption;
        internal System.Windows.Forms.Panel panBaseRight;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel panLogo;
        private System.Windows.Forms.Button btnAlarm;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Button btnRecipe;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnMotorStatus;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Label lbl_Version;
        private System.Windows.Forms.Label lbl_SysTemDateTime;
        private System.Windows.Forms.Label lbl_LoginUser;
        private System.Windows.Forms.Label lbl_Message;
        private System.Windows.Forms.Label lbl_JobName;
        private System.Windows.Forms.Label lbl_ModelName;
        private System.Windows.Forms.Label lbl_MachineStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Login;
    }
}

