﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using KOSES_SEQ.MANAGER;
using System.IO.Compression;

namespace KOSES_SEQ.CONFIG
{
    internal class FileUtil
    {
        /// <summary>
        /// 문자열에서 특정문자 갯수 확인
        /// </summary>
        /// <param name="strTotal"></param>
        /// <param name="strFind"></param>
        /// <returns></returns>
        public int GetStringcount(string strTotal, string strFind)
        {
            System.Text.RegularExpressions.Regex cntStr = new System.Text.RegularExpressions.Regex(strFind);
            return int.Parse(cntStr.Matches(strTotal, 0).Count.ToString());
        }

        /// <summary>
        /// clipboard to image
        /// </summary>
        /// <param name="TargetFileName"></param>
        /// <returns></returns>
        public bool SaveClipboardImg(string TargetFileName)
        {
            try
            {
                System.Windows.Forms.IDataObject ob = null;
                PictureBox pic = new PictureBox();

                ob = Clipboard.GetDataObject();
                pic.Image = (Image)ob.GetData(DataFormats.Bitmap);
                Image Im = pic.Image;

                if (Im == null)
                    return false;

                FileStream fs = new FileStream(TargetFileName, FileMode.Create, FileAccess.ReadWrite);
                Im.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                fs.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return false;
            }
        }

        /// <summary>
        /// 문자열 안에서 숫자만 뽑기
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string parseInt(string str)
        {
            string strRet = "";
            strRet = Regex.Replace(str, @"\D", ""); // 닷넷 정규식에서 \d는 숫자. \D는 숫자가 아닌 문자를 의미
            return strRet;
        }

        /// <summary>
        /// 문자열 안에서 숫자를 제외한 문자만 뽑기
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string parseStr(string str)
        {
            string strRet = "";
            strRet = Regex.Replace(str, @"\d", ""); // 닷넷 정규식에서 \d는 숫자. \D는 숫자가 아닌 문자를 의미
            return strRet;
        }

        /// <summary>
        /// 파일경로 에서 파일명 얻기
        /// </summary>
        /// <param name="strFile"></param>
        /// <returns></returns>
        public string getFileName(string strFile)
        {
            string[] nameArr = strFile.Split('\\');
            //마지막으로 스플릿된 문자열이 파일이름
            string fileName = nameArr[nameArr.Length - 1];
            return fileName;
        }

        /// <summary>
        /// 파일경로 에서 확장자를 뺀 파일명 얻기
        /// </summary>
        /// <param name="strFile"></param>
        /// <returns></returns>
        public string getFileNameExceptExt(string strFile)
        {
            string strTemp = getFileName(strFile);

            string[] str = strTemp.Split('.');
            string fileName = str[0];
            return fileName;

        }
        /// <summary>
        /// 파일경로 에서 확장자 얻기
        /// </summary>
        /// <param name="strFile"></param>
        /// <returns></returns>
        public string getFileExtension(string strFile)
        {
            string[] extArr = strFile.Split('.');
            string fileExt = extArr[extArr.Length - 1];
            return fileExt;
        }

        /// <summary>
        /// 두 날짜의 차이 구하기 (날짜 포맷 : 2015-04-30)
        /// </summary>
        /// <param name="strSDate"></param>
        /// <param name="strEDate"></param>
        /// <returns></returns>
        public int getBetweenDaybyDay(string strSDate, string strEDate)
        {
            DateTime T1 = DateTime.Parse(strSDate);
            DateTime T2 = DateTime.Parse(strEDate);
            TimeSpan TS = T2 - T1;
            int nCnt = Convert.ToInt32(TS.TotalDays.ToString());
            return nCnt;
        }

        /// <summary>
        /// 파일내용 안에 같은 문자열의 존재 확인
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <param name="strCompareString"></param>
        /// <returns></returns>
        public bool isExistsStringInFile(string strFilePath, string strCompareString)
        {
            StreamReader streamReader = new StreamReader(strFilePath);
            string text = streamReader.ReadToEnd();
            streamReader.Close();

            return text.Contains(strCompareString);
        }

        /// <summary>
        /// 현재 exe실행 경로얻기
        /// </summary>
        /// <returns></returns>
        public string getExecutablePath()
        {
            return System.IO.Path.GetDirectoryName(Application.ExecutablePath);
        }

        /// <summary>
        /// File size 확인
        /// </summary>
        /// <param name="strPath"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public bool getFileSize(string strPath, ref int size)
        {
            size += 244;
            return true;
        }

        /// <summary>
        /// 로그 파일 삭제 - ??월 미만에 만들어진 파일 삭제
        /// </summary>
        /// <param name="keepDays">유지 일(그 이상은 삭제)</param>
        public void DeleteLogByMonth(string folderPath, int keepDays)
        {
            try
            {
                if (!System.IO.Directory.Exists(folderPath))
                    return;

                DirectoryInfo di = new DirectoryInfo(folderPath);
                // 하위 폴더의 로그도 삭제
                foreach (FileInfo fi in di.GetFiles("*.log", SearchOption.AllDirectories))
                {
                    if (fi.LastWriteTime <= DateTime.Now.AddDays(-keepDays))
                    {
                        fi.Delete();
                    }
                }
            }
            catch(Exception ex)
            {
                LogManager.ExLog(ex);
            }
        }

        /// <summary>
        /// 로그 파일 삭제 - ??월 미만에 만들어진 파일 삭제
        /// </summary>
        /// <param name="keepDays">유지 일(그 이상은 삭제)</param>
        public void DeleteFileByDays(string folderPath, int keepDays, string filenameExtension = "*")
        {
            if (!System.IO.Directory.Exists(folderPath))
                return;

            DirectoryInfo di = new DirectoryInfo(folderPath);
            // 하위 폴더의 로그도 삭제
            foreach (FileInfo fi in di.GetFiles("*." + filenameExtension, SearchOption.AllDirectories))
            {
                if (fi.LastWriteTime <= DateTime.Now.AddDays(-keepDays))
                {
                    fi.Delete();
                }
            }
        }

        /// <summary>
        /// 로그 파일 삭제 - ??월 미만에 만들어진 파일 삭제
        /// </summary>
        /// <param name="keepDays">유지 일(그 이상은 삭제)</param>
        public void DeleteProductDataByDays(string folderPath, int keepDays)
        {
            DirectoryInfo di = new DirectoryInfo(folderPath);
            // 하위 폴더의 로그도 삭제
            foreach (FileInfo fi in di.GetFiles("*.ini", SearchOption.AllDirectories))
            {
                if (fi.LastWriteTime <= DateTime.Now.AddDays(-keepDays))
                {
                    fi.Delete();
                }
            }
        }

        public void DeleteFile(string strPath)
        {
            FileInfo fi = new FileInfo(strPath);
            if (fi != null)
                fi.Delete();
        }

        public void DeleteFolder(string strPath)
        {
            DirectoryInfo di = new DirectoryInfo(strPath);

            if (di.Exists)
            {
                di.Delete(true);
            }
        }

        /// <summary>
        /// 해당 날짜의 파일 탐색
        /// </summary>
        /// <param name="IpClassName"></param>
        /// <param name="IpWindowName"></param>
        /// <returns></returns>
        public string SearchFiles(string folderPath, DateTime date)
        {
            string strFileNames = "";
            try
            {
                folderPath += date.ToString("yyyy_MM_dd");

                DirectoryInfo di = new DirectoryInfo(folderPath);
                // 마지막으로 수정된 날짜 순으로 소팅
                FileInfo[] files = di.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
                //
                foreach (FileInfo fi in files)
                {
                    if (fi.Extension == ".ini")
                    {
                        string str = fi.Name;
                        str = getFileNameExceptExt(str);
                        strFileNames += (str + ",");
                    }

                }
            }
            catch
            {
                return "";
            }

            return strFileNames;

        }


        //public void GetProductionData(string strLotName, DateTime date, ref _product product)
        //{
        //    try
        //    {
        //        if (strLotName == "")
        //            return;

        //        string strPath = DF_PATH.PATH_PRODUCTION + date.ToString("yyyy_MM_dd");

        //        string strFileName = strPath + "\\" + strLotName + ".ini";

        //        CIniUtil ini = new CIniUtil(strFileName);

        //        product.nRunTime = Convert.ToInt32(ini.GetIniValue("PRODUCTION", "RUNTIME"));
        //        product.nStopTime = Convert.ToInt32(ini.GetIniValue("PRODUCTION", "STOPTIME"));
        //        product.nErrTime = Convert.ToInt32(ini.GetIniValue("PRODUCTION", "ERRTIME"));
        //        product.nIdleTime = Convert.ToInt32(ini.GetIniValue("PRODUTION", "IDLETIME"));
        //        product.nGoodCnt = Convert.ToInt32(ini.GetIniValue("PRODUCTION", "GOOD"));
        //        product.nNgCnt = Convert.ToInt32(ini.GetIniValue("PRODUCTION", "NG"));

        //        for (int j = 0; j < DF_MAX.MAX_TESTER_INDEX; j++)
        //            for (int i = 0; i < 10; i++)
        //                product.nNgGroupCnt[j, i] = Convert.ToInt32(ini.GetIniValue("PRODUCTION", "NG_GROUP_" + j.ToString() + "_" + i.ToString()));

        //    }
        //    catch (System.Exception ex)
        //    {
        //        System.Diagnostics.Debug.Print(ex.Message);
        //    }

        //}

        public void SaveProductionData(string strPath, string strLotName, CProduct product)
        {
            StringBuilder text = new StringBuilder();

        }
        #region 윈도우 작업표시줄 숨기기 및 되살리기

        [DllImport("user32.dll")]
        public static extern int FindWindowA(string IpClassName, string IpWindowName);

        [DllImport("SHELL32", CallingConvention = CallingConvention.StdCall)]
        private static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);

        [StructLayout(LayoutKind.Sequential)]
        private struct APPBARDATA
        {
            public int cbSize;
            public IntPtr hWnd;
            public int uCallbackMessage;
            public int uEdge;
            public RECT rc;
            public IntPtr lParam;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }
        private enum ABMsg : int
        {
            ABM_NEW = 0,
            ABM_REMOVE,
            ABM_QUERYPOS,
            ABM_SETPOS,
            ABM_GETSTATE,
            ABM_GETTASKBARPOS,
            ABM_ACTIVATE,
            ABM_GETAUTOHIDEBAR,
            ABM_SETAUTOHIDEBAR,
            ABM_WINDOWPOSCHANGED,
            ABM_SETSTATE
        }
        /// <summary>
        /// 숨겨진 작업표시줄 되살리기
        /// </summary>
        public void ShowWinTaskBar()
        {
            APPBARDATA barData = new APPBARDATA();

            barData.hWnd = (IntPtr)FindWindowA("Shell_traywnd", "");
            barData.lParam = (IntPtr)0;
            SHAppBarMessage((int)ABMsg.ABM_SETSTATE, ref barData);

        }

        /// <summary>
        /// 작업표시줄 숨기기
        /// </summary>
        public void HideWinTaskBar()
        {
            APPBARDATA barData = new APPBARDATA();

            barData.hWnd = (IntPtr)FindWindowA("Shell_traywnd", "");
            barData.lParam = (IntPtr)1;
            SHAppBarMessage((int)ABMsg.ABM_SETSTATE, ref barData);
        }

        #endregion 윈도우 작업표시줄 숨기기 및 되살리기

        /// <summary>
        /// 리스트뷰에서 스크롤바를 선택적으로 보이게 하는 함수
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="wBar"></param>
        /// <param name="bShow"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern bool ShowScrollBar(IntPtr hWnd, int wBar, bool bShow); // wBar 0:HOR   1:VER

        public void ShowListViewScrollBar(IntPtr hWnd, int wBar, bool bShow)
        {
            ShowScrollBar(hWnd, wBar, bShow);
        }




        // 현재 커서가 위치하고 있는 컨트롤 찾기
        public Control FindControlAtCursor(Form form)
        {
            Point pos = Cursor.Position;
            if (form.Bounds.Contains(pos))
                return FindControlAtPoint(form, form.PointToClient(Cursor.Position));
            return null;
        }

        public Control FindControlAtPoint(Control container, Point pos)
        {
            Control child;
            foreach (Control c in container.Controls)
            {
                if (c.Visible && c.Bounds.Contains(pos))
                {
                    child = FindControlAtPoint(c, new Point(pos.X - c.Left, pos.Y - c.Top));
                    if (child == null)
                        return c;
                    else
                        return child;
                }
            }
            return null;
        }

        /// <SUMMARY>  
        /// 영문체크  
        /// </SUMMARY>  
        /// <PARAM name="letter">문자  
        ///   
        public bool CheckEnglish(string str)
        {
            bool IsCheck = true;

            Regex engRegex = new Regex(@"[a-zA-Z]");
            Boolean ismatch = engRegex.IsMatch(str);

            if (!ismatch)
            {
                IsCheck = false;
            }
            return IsCheck;

        }

        /// <SUMMARY>  
        /// 한글체크  
        /// </SUMMARY>  
        /// <PARAM name="letter">문자  
        ///   
        public bool CheckKorean(string str)
        {
            bool IsCheck = true;

            Regex engRegex = new Regex(@"[가-힣]");
            Boolean ismatch = engRegex.IsMatch(str);

            if (!ismatch)
            {
                IsCheck = false;
            }
            return IsCheck;

        }

        /// <SUMMARY>  
        /// 숫자체크  
        /// </SUMMARY>  
        /// <PARAM name="str">문자  
        ///   
        public bool CheckNumber(string str)
        {
            bool IsCheck = true;

            Regex engRegex = new Regex(@"[0-9]");
            Boolean ismatch = engRegex.IsMatch(str);

            if (!ismatch)
            {
                IsCheck = false;
            }

            return IsCheck;
        }

        /// <SUMMARY>  
        /// 특수문자체크  
        /// </SUMMARY>  
        /// <PARAM name="str">문자  
        ///   
        public bool CheckSpecialText(string str)
        {
            bool IsCheck = true;

            Regex engRegex = new Regex(@"^[a-zA-Z0-9가-힣]*$");
            Boolean ismatch = engRegex.IsMatch(str);

            if (ismatch)
            {
                IsCheck = false;
            }

            return IsCheck;
        }

        public bool isValidFileName(string str)
        {
            try
            {
                string fileName = Path.GetFullPath(str);
            }
            catch
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Delay 함수 MS
        /// </summary>
        /// <param name="MS">(단위 : MS)
        ///
        public DateTime Delay(int MS)
        {
            DateTime ThisMoment = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
            DateTime AfterWards = ThisMoment.Add(duration);
            while (AfterWards >= ThisMoment)
            {
                System.Windows.Forms.Application.DoEvents();
                ThisMoment = DateTime.Now;
                Thread.Sleep(1);
            }
            return DateTime.Now;
        }


        public object ConvertVarNameToValue(string VariabelName, object objClass)
        {
            object result = null;
            System.Reflection.FieldInfo[] f = objClass.GetType().GetFields(); //FrmMain.prg.ngMap가 해당하는 클래스의 필드정보를 가져옵니다. 
            for (int i = 0; i < f.Length; i++)
            {
                if (f[i].Name.Equals(VariabelName)) //해당 필드의 이름이 "변수명"과 같을 때 
                {
                    result = f[i].GetValue(objClass);
                    break;
                }
            }
            return result;
        }


        /// <summary>
        /// Folder Copy
        /// </summary>
        /// <param name="sourceFolder">Copy 폴더 경로</param>
        /// <param name="destFolder">Paste 폴더 경로 </param>
        /// <returns></returns>
        public static void CopyFolderFlie(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
            {
                Directory.CreateDirectory(destFolder);
            }

            string[] files = Directory.GetFiles(sourceFolder);
            string[] folders = Directory.GetDirectories(sourceFolder);

            foreach (string file in files)
            {
                string name = System.IO.Path.GetFileName(file);
                string dest = System.IO.Path.Combine(destFolder, name);
                File.Copy(file, dest);
            }

            foreach (string folder in folders)
            {
                string name = System.IO.Path.GetFileName(folder);
                string dest = System.IO.Path.Combine(destFolder, name);
                CopyFolderFlie(folder, dest);
            }
        }

        public void CopyFolderFlie(string sourceFolder, string destFolder, List<string> listDay)
        {
            if (!Directory.Exists(destFolder))
            {
                Directory.CreateDirectory(destFolder);
            }

            string[] files = Directory.GetFiles(sourceFolder);
            string[] folders = Directory.GetDirectories(sourceFolder);

            foreach (string file in files)
            {
                string name = System.IO.Path.GetFileName(file);
                string dest = System.IO.Path.Combine(destFolder, name);

                DateTime tDay = System.IO.Directory.GetCreationTime(file);
                string strFileDay = string.Format("{0}", tDay.Year.ToString("D4") + "-" + tDay.Month.ToString("D2") + "-" + tDay.Day.ToString("D2"));

                foreach (string day in listDay)
                {
                    if (day == strFileDay)
                    {
                        File.Copy(file, dest);
                    }
                }          
            }

            foreach (string folder in folders)
            {
                string name = System.IO.Path.GetFileName(folder);
                string dest = System.IO.Path.Combine(destFolder, name);
                CopyFolderFlie(folder, dest, listDay);
            }
        }
    }
}
