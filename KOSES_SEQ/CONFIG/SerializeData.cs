﻿//using EzEquipment.Material;
//using PrsInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using KOSES_SEQ.DEFINE;

namespace KOSES_SEQ.CONFIG
{
    #region Structure
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XyD
    {
        public double x;
        public double y;

        public XyD(double a, double b)
        {
            x = a;
            y = b;
        }

        public static XyD operator +(XyD data1, XyD data2) => new XyD(data1.x + data2.x, data1.y + data2.y);
        public static XyD operator -(XyD data1, XyD data2) => new XyD(data1.x - data2.x, data1.y - data2.y);

        public override string ToString()
        {
            return $"{x:F3}, {y:F3}";
        }
        
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XyzD
    {
        public double x;
        public double y;
        public double z;

        public XyzD(double a, double b, double c)
        {
            x = a;
            y = b;
            z = c;
        }

        public static XyzD operator +(XyzD data1, XyzD data2) => new XyzD(data1.x + data2.x, data1.y + data2.y, data1.z + data2.z);
        public static XyzD operator -(XyzD data1, XyzD data2) => new XyzD(data1.x - data2.x, data1.y - data2.y, data1.z - data2.z);

        public override string ToString()
        {
            return $"{x:F3}, {y:F3}, {z:F3}";
        }

        public XyD GetXy()
        {
            return new XyD(x, y);
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XyztD
    {
        public double x;
        public double y;
        public double z;
        public double t;

        public XyztD(double a, double b, double c, double d)
        {
            x = a;
            y = b;
            z = c;
            t = d;
        }
        public static XyztD operator +(XyztD data1, XytD data2) => new XyztD(data1.x + data2.x, data1.y + data2.y, data1.z, data1.t + data2.t);
        public static XyztD operator -(XyztD data1, XytD data2) => new XyztD(data1.x - data2.x, data1.y - data2.y, data1.z, data1.t - data2.t);

        public static XyztD operator +(XyztD data1, XyztD data2) => new XyztD(data1.x + data2.x, data1.y + data2.y, data1.z + data2.z, data1.t + data2.t);
        public static XyztD operator -(XyztD data1, XyztD data2) => new XyztD(data1.x - data2.x, data1.y - data2.y, data1.z - data2.z, data1.t - data2.t);

        public override string ToString()
        {
            return $"{x:F3}, {y:F3}, {z:F3}, {t:F3}";
        }

        public XyzD GetXyz()
        {
            return new XyzD(x, y, z);
        }

        public XyD GetXy()
        {
            return new XyD(x, y);
        }

        public XytD GetXyt()
        {
            return new XytD(x, y, t);
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XytD
    {
        public double x;
        public double y;
        public double t;

        public XytD(double a, double b, double c)
        {
            x = a;
            y = b;
            t = c;
        }

        public bool IsSet()
        {
            if (x == 0 && y == 0 && t == 0)
                return false;

            return true;
        }

        public static XytD operator +(XytD data1, XytD data2) => new XytD(data1.x + data2.x, data1.y + data2.y, data1.t + data2.t);
        public static XytD operator -(XytD data1, XytD data2) => new XytD(data1.x - data2.x, data1.y - data2.y, data1.t - data2.t);

        public static XytD operator +(XytD data1, XyD data2) => new XytD(data1.x + data2.x, data1.y + data2.y, data1.t);
        public static XytD operator -(XytD data1, XyD data2) => new XytD(data1.x - data2.x, data1.y - data2.y, data1.t);

        public override string ToString()
        {
            return $"{x:F3}, {y:F3}, {t:F3}";
        }

        public static XytD GetFromCoordXyt(CoordXyt xyt)
        {
            XytD convert = new XytD
            {
                x = xyt.X,
                y = xyt.Y,
                t = xyt.T
            };
            return convert;
        }

        public XyD GetXy()
        {
            return new XyD(x, y);
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct UvwD
    {
        public double u;
        public double v;
        public double w;

        public UvwD(double a, double b, double c)
        {
            u = a;
            v = b;
            w = c;
        }

        public static UvwD operator +(UvwD data1, UvwD data2) => new UvwD(data1.u + data2.u, data1.v + data2.v, data1.w + data2.w);
        public static UvwD operator -(UvwD data1, UvwD data2) => new UvwD(data1.u - data2.u, data1.v - data2.v, data1.w - data2.w);

        public override string ToString()
        {
            return $"{u:F3}, {v:F3}, {w:F3}";
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CircleD
    {
        public double x;
        public double y;
        public double r;    // 반지름 radius

        public override string ToString()
        {
            return $"{x:F3}, {y:F3}, {r:F3}";
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XyN
    {
        public int x;
        public int y;

        public XyN(int a, int b)
        {
            x = a;
            y = b;
        }

        public override string ToString()
        {
            return $"{x}, {y}";
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RoiN
    {
        public int x;
        public int y;
        public int width;
        public int height;

        public override string ToString()
        {
            return $"{x}, {y}, {width}, {height}";
        }
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XyzDPair
    {
        public XyzD Item1;
        public XyzD Item2;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct XyDPair
    {
        public XyD Item1;
        public XyD Item2;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MotionInfo
    {
        public string strAxisName;
        public int nAxis;
        public double dUnitPulse;
        public double dPos;
        public double dVel;
        public double dAcc;
        public double dDec;
        public int nAmpErrNo;
        public int nMoveErrNo;
        public bool isUseInPos; //엔코더 부착 모터일경우 사용
        public double dInposGap;
        public long lMoveTime;
        public int nHwLimitNErr;
        public int nHwLimitPErr;

        public double dVelHomeSearch;   // 원점 복귀 시 속도
        public double dVelLowHomeSearch;    // 원점 복귀 시 Low 속도
        public double dAccHomeSearch;   // 원점 복귀 시 가속도
        public double dDecHomeSearch;   // 원점 복귀 시 감속도
        public double dHomeOffset;

        public bool isReverseDirectionInCal;    // Vision 계산 시 좌표계 상 반대 방향으로 움직이는 모터
    }

    #endregion

    #region 데이터 복구 구조체
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class CRecoveryData
    {
        //private string strLotName;
        private string _strRecipeName;

        public string StrRecipeName
        {
            get
            {
                if (_strRecipeName == null || _strRecipeName == string.Empty)
                    _strRecipeName = "noname";
                return _strRecipeName;
            }
        }

        //public string LotName
        //{
        //    get { return strLotName; }
        //    set
        //    {
        //        strLotName = value;
        //    }
        //}

        public void SetRecipeName(string strPath)
        {
            if (strPath.Contains("\\"))
            {
                FileUtil fu = new FileUtil();
                _strRecipeName = fu.getFileNameExceptExt(strPath);
            }
            else
                _strRecipeName = strPath;
        }

        public string GetRecipePath()
        {
            return DefPath.Recipe + _strRecipeName + ".xml";
        }

        public string GetRecipeName()
        {
            return _strRecipeName;
        }

    };

    #endregion 데이터 복구 구조체

    #region Production Data 구조체

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class CProduct
    {
        public long nRunTime;
        public long nStopTime;
        public long nErrTime;
        public long nIdleTime;
        public int nGoodCnt;
        public int nNgCnt;
        //public int[] nPickFailCnt = new int[(int)enumRobotID.Max];

        public CProduct()
        {
            nRunTime = 0;
            nStopTime = 0;
            nErrTime = 0;
            nIdleTime = 0;
            nGoodCnt = 0;
            nNgCnt = 0;
            //nPickFailCnt = new int[(int)enumRobotID.Max];
        }
        public CProduct(long nRunTime, long nStopTime, long nErrTime, long nIdleTime, int nGoodCnt, int nNgCnt)
        {
            this.nRunTime = nRunTime;
            this.nStopTime = nStopTime;
            this.nErrTime = nErrTime;
            this.nIdleTime = nIdleTime;
            this.nGoodCnt = nGoodCnt;
            this.nNgCnt = nNgCnt;
        }

        public static CProduct operator +(CProduct data1, CProduct data2)
        {
            long nRunTime = data1.nRunTime + data2.nRunTime;
            long nStopTime = data1.nStopTime + data2.nStopTime;
            long nErrTime = data1.nErrTime + data2.nErrTime;
            long nIdleTime = data1.nIdleTime + data2.nIdleTime;
            int nGoodCnt = data1.nGoodCnt + data2.nGoodCnt;
            int nNgCnt = data1.nNgCnt + data2.nNgCnt;
            int[] nNgPickFailCnt = new int[(int)Def.Motor.Max];

            //for(int i=0; i< (int)enumRobotID.Max; i++)
            //{
            //    nNgPickFailCnt[i] = data1.nPickFailCnt[i] + data2.nPickFailCnt[i];
            //}

            //int[,] nNgGroupCnt = new int[DF_MAX.MAX_PARA, DF_MAX.MAX_NG_GROUP];
            //for (int j = 0; j < DF_MAX.MAX_PARA; j++)
            //    for (int i = 0; i < DF_MAX.MAX_NG_GROUP; i++)
            //        nNgGroupCnt[j, i] = data1.nNgGroupCnt[j, i] + data2.nNgGroupCnt[j, i];

            return new CProduct(nRunTime, nStopTime, nErrTime, nIdleTime, nGoodCnt, nNgCnt);
        }
    }


    #endregion Production Data 구조체

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class CLotInfo
    {
        public string LotID = string.Empty;
        public bool IsLotEnd = false;
    }

    #region Login 정보 구조체

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LoginInfo
    {
        public string strPassOp;
        public string strPassEng;
        public string strPassMak;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class MaterialMatrixInfo
    {
        //Material Matrix Backup정보
        //0=Empty, 1=Ready, 2=Vision확인 자재있음, 3=Vision확인 자재없음, 4=픽업됨, 5=플레이스됨
        //public enumSubMaterialMapStatus[,] TrayMap = new enumSubMaterialMapStatus[DF_MAX.MAX_MATERIAL_COL_COUNT, DF_MAX.MAX_MATERIAL_ROW_COUNT]; // X, Y
    };

    #endregion Login 정보 구조체

    /// <summary>
    /// X,Y,T 좌표계
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CoordXyt
    {
        /// <summary>
        /// X Axis
        /// </summary>
        public double X;
        /// <summary>
        /// Y Axis
        /// </summary>
        public double Y;
        /// <summary>
        /// Theta Axis
        /// </summary>
        public double T;
    }
}
