﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using KOSES_SEQ.DEFINE;
using static KOSES_SEQ.CONFIG.IniFile;

namespace KOSES_SEQ.CONFIG
{
    class MotorParameter
    {
        public MotorParameterHelp help;
        public MotorParameterData[] para;
        public string iniPath;

        public MotorParameter()
        {

        }

        public MotorParameter(List<MotorInfo> value, string motorini, string helpini)  //Motor List, Motor Ini Path,Motor Help txt Path
        {
            para = new MotorParameterData[value.Count];

            for (int i = 0; i < value.Count -1; i++)
            {
                iniPath = motorini + "\\" + value[(int)i].Name + ".ini";             
                para[i] = new MotorParameterData(iniPath);
            }
        }

        public void LoadAll()
        {
            List<MotorInfo> motorList = new List<MotorInfo>();
            int i = 0;
            foreach (string name in Enum.GetNames(typeof(Def.Motor)))
            {
                MotorInfo mi = new MotorInfo();
                mi.Name = name;
                mi.Num = i++;
                motorList.Add(mi);
                mi = null;
            }


            
            para = new MotorParameterData[motorList.Count];
            for (int j = 0; j < motorList.Count; j++)
            {
                //iniPath = motorini + "\\" + motorList[(int)j].Name + ".ini";
                para[j] = new MotorParameterData(iniPath);
            }
        }
        public void SaveAll()
        {

        }
    }


    public class MotorParameterHelp
    {
        public MotorParameterHelp(string value)
        {

        }

        public string GetData(string itemName)
        {

            return $"";
        }
    }

    public class MotorInfo
    {
        public string Name;
        public int Num;

        public MotorInfo()
        {
   
        }
    }

    public class MotorParameterFileUtility
    {
        IniFile ini;

        public MotorParameterFileUtility()
        {

        }

        public void DataProc(Act act, string sectionName, string keyName, ref int value)
        {
            if (act == 0)
            {
                value = ini.Read(sectionName, keyName, value);
                return;
            }
            else
            {
                ini.Write(sectionName, keyName, value);
            }           
        }
        public void DataProc(Act act, string sectionName, string keyName, ref double value)
        {
            if (act == 0)
            {
                value = ini.Read(sectionName, keyName, value);
                return;
            }
            else
            {
                ini.Write(sectionName, keyName, value);
            }
        }
        public void DataProc(Act act, string sectionName, string keyName, ref short value)
        {
            if (act == 0)
            {
                value = ini.Read(sectionName, keyName, value);
                return;
            }
            else
            {
                ini.Write(sectionName, keyName, value);
            }
        }
        public void DataProc(Act act, string sectionName, string keyName, ref bool value)
        {
            if (act == 0)
            {
                value = ini.Read(sectionName, keyName, value);
                return;
            }
            else
            {
                ini.Write(sectionName, keyName, value);
            }
        }
        public void DataProc(Act act, string sectionName, string keyName, ref string value)
        {
            if (act == 0)
            {
                value = ini.Read(sectionName, keyName, value);
                return;
            }
            else
            {
                ini.Write(sectionName, keyName, value);
            }
        }
        public bool IsExist(string path)
        {
            if (true == File.Exists(path))
            {
                ini = new IniFile(path);
                return true;
            }

            return false;               
        }
        public bool ReadBoolean(string section, string key, bool value)
        {           
            bool ret = ini.Read(section, key, value);
            return ret;
        }
        public double ReadDouble(string section, string key, double value)
        {
            double ret = ini.Read(section, key, value);
            return ret;
        }
        public int ReadInt(string section, string key, int value)
        {
            int ret = ini.Read(section, key, value);
            return ret;
        }      
        public short ReadShort(string section, string key, short value)
        {
            short ret = ini.Read(section, key, value);
            return ret;
        }
        public long Readlong(string section, string key, long value)
        {
            long ret = ini.Read(section, key, value);
            return ret;
        }
        public string ReadString(string filePath)
        {
            return $"{filePath}";
        }
        public string ReadString(string section, string key, string value)
        {
            string ret = ini.Read(section, key, value);
            return ret;
        }
        public void SetIniPath(string filePath)
        {
            ReadString(filePath);
        }
        public void WriteBoolean(string section, string key, bool value)
        {
            ini.Write(section, key, value);
        }
        public void WriteDouble(string section, string key, double value)
        {
            ini.Write(section, key, value);
        }
        public void WriteInt(string section, string key, int value)
        {
            ini.Write(section, key, value);
        }
        public void WriteShort(string section, string key, short value)
        {
            ini.Write(section, key, value);
        }
        public void Writelong(string section, string key, long value)
        {
            ini.Write(section, key, value);
        }
        public void WriteString(string section, string key, string value)
        {
            ini.Write(section, key, value);
        }
        public enum Act
        {
            Read = 0,
            Write = 1
        }
    }

    public class MotorParameterData : MotorParameterFileUtility
    {
        public double acc;
        public int accUnit;
        public int alarmSignalLevel;
        public double dec;
        public DataTable dt;
        public int emergencySignalLevel;
        public int encodeDir;
        public int encoderCount;
        public double homeClearTime;
        public double homeDec;
        public int homeDir;
        public int homeMethod;
        public double homeOffset;
        public int homeSignalLevel;
        public readonly int HOME_STEP_MAX;
        public int inposSignalLevel;
        public int minusSignalLevel;
        public int motorProfile;
        public int motorType;
        public double[] multiHomeAcc = new double[5];
        public byte[] multiHomeMethod = new byte[5];
        public double[] multiHomeVel = new double[5];
        public int plusSignalLevel;
        public int pulseMode;
        public bool sensorAvoid;
        public double sensorAvoidOffset;
        public int servoSignalLevel;
        public double unitPerPulse;
        public double vel;
        public int zPhase;

        public MotorParameterData(string value)   //Motor Ini File 명
        {
            SetIniPath(value);
            if (true == IsExist((value)))
            {
                Load();
            }
            else
            {
                Save();
            }
        }

        public void Load()
        {
            //Section [MOVE]
            DataProc(Act.Read, DefString.Move, DefString.Acc, ref acc);
            DataProc(Act.Read, DefString.Move, DefString.Dec, ref dec);
            DataProc(Act.Read, DefString.Move, DefString.Vel, ref vel);
            DataProc(Act.Read, DefString.Move, DefString.MotorProfile, ref motorProfile);
            DataProc(Act.Read, DefString.Move, DefString.MotorType, ref motorType);

            //Section [HOME]
            DataProc(Act.Read, DefString.Home, DefString.HomeMethod, ref homeMethod);
            DataProc(Act.Read, DefString.Home, DefString.Z_Phase, ref zPhase);
            DataProc(Act.Read, DefString.Home, DefString.HomeAcc_0, ref multiHomeAcc[0]);
            DataProc(Act.Read, DefString.Home, DefString.HomeAcc_1, ref multiHomeAcc[1]);
            DataProc(Act.Read, DefString.Home, DefString.HomeAcc_2, ref multiHomeAcc[2]);
            DataProc(Act.Read, DefString.Home, DefString.HomeAcc_3, ref multiHomeAcc[3]);
            DataProc(Act.Read, DefString.Home, DefString.HomeAcc_4, ref multiHomeAcc[4]);
            DataProc(Act.Read, DefString.Home, DefString.HomeVel_0, ref multiHomeVel[0]);
            DataProc(Act.Read, DefString.Home, DefString.HomeVel_1, ref multiHomeVel[1]);
            DataProc(Act.Read, DefString.Home, DefString.HomeVel_2, ref multiHomeVel[2]);
            DataProc(Act.Read, DefString.Home, DefString.HomeVel_3, ref multiHomeVel[3]);
            DataProc(Act.Read, DefString.Home, DefString.HomeVel_4, ref multiHomeVel[4]);
            DataProc(Act.Read, DefString.Home, DefString.HomeDec, ref homeDec);
            DataProc(Act.Read, DefString.Home, DefString.HomeOffset, ref homeOffset);
            DataProc(Act.Read, DefString.Home, DefString.HomeClearTime, ref homeClearTime);
            DataProc(Act.Read, DefString.Home, DefString.HomeDir, ref homeDir);

            //Section [SETUP]
            DataProc(Act.Read, DefString.SetUp, DefString.UnitPerPulse, ref unitPerPulse);
            DataProc(Act.Read, DefString.SetUp, DefString.PulseMode, ref pulseMode);
            DataProc(Act.Read, DefString.SetUp, DefString.EncodeDir, ref encodeDir);
            DataProc(Act.Read, DefString.SetUp, DefString.PlusSignalLevel, ref plusSignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.MinusSignalLevel, ref minusSignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.HomeSignalLevel, ref homeSignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.InposSignalLevel, ref inposSignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.AlarmSignalLevel, ref alarmSignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.ServoSignalLevel, ref servoSignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.EmergencySignalLevel, ref emergencySignalLevel);
            DataProc(Act.Read, DefString.SetUp, DefString.EncoderCount, ref encoderCount);
            DataProc(Act.Read, DefString.SetUp, DefString.AccUnit, ref accUnit);
    }

        public void Save()
        {           
            //Section [MOVE]
            DataProc(Act.Write, DefString.Move, DefString.Acc, ref acc);
            DataProc(Act.Write, DefString.Move, DefString.Dec, ref dec);
            DataProc(Act.Write, DefString.Move, DefString.Vel, ref vel);
            DataProc(Act.Write, DefString.Move, DefString.MotorProfile, ref motorProfile);
            DataProc(Act.Write, DefString.Move, DefString.MotorType, ref motorType);

            //Section [HOME]
            DataProc(Act.Write, DefString.Home, DefString.HomeMethod, ref homeMethod);
            DataProc(Act.Write, DefString.Home, DefString.Z_Phase, ref zPhase);
            DataProc(Act.Write, DefString.Home, DefString.HomeAcc_0, ref multiHomeAcc[0]);
            DataProc(Act.Write, DefString.Home, DefString.HomeAcc_1, ref multiHomeAcc[1]);
            DataProc(Act.Write, DefString.Home, DefString.HomeAcc_2, ref multiHomeAcc[2]);
            DataProc(Act.Write, DefString.Home, DefString.HomeAcc_3, ref multiHomeAcc[3]);
            DataProc(Act.Write, DefString.Home, DefString.HomeAcc_4, ref multiHomeAcc[4]);
            DataProc(Act.Write, DefString.Home, DefString.HomeVel_0, ref multiHomeVel[0]);
            DataProc(Act.Write, DefString.Home, DefString.HomeVel_1, ref multiHomeVel[1]);
            DataProc(Act.Write, DefString.Home, DefString.HomeVel_2, ref multiHomeVel[2]);
            DataProc(Act.Write, DefString.Home, DefString.HomeVel_3, ref multiHomeVel[3]);
            DataProc(Act.Write, DefString.Home, DefString.HomeVel_4, ref multiHomeVel[4]);
            DataProc(Act.Write, DefString.Home, DefString.HomeDec, ref homeDec);
            DataProc(Act.Write, DefString.Home, DefString.HomeOffset, ref homeOffset);
            DataProc(Act.Write, DefString.Home, DefString.HomeClearTime, ref homeClearTime);
            DataProc(Act.Write, DefString.Home, DefString.HomeDir, ref homeDir);

            //Section [SETUP]
            DataProc(Act.Write, DefString.SetUp, DefString.UnitPerPulse, ref unitPerPulse);
            DataProc(Act.Write, DefString.SetUp, DefString.PulseMode, ref pulseMode);
            DataProc(Act.Write, DefString.SetUp, DefString.EncodeDir, ref encodeDir);
            DataProc(Act.Write, DefString.SetUp, DefString.PlusSignalLevel, ref plusSignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.MinusSignalLevel, ref minusSignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.HomeSignalLevel, ref homeSignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.InposSignalLevel, ref inposSignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.AlarmSignalLevel, ref alarmSignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.ServoSignalLevel, ref servoSignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.EmergencySignalLevel, ref emergencySignalLevel);
            DataProc(Act.Write, DefString.SetUp, DefString.EncoderCount, ref encoderCount);
            DataProc(Act.Write, DefString.SetUp, DefString.AccUnit, ref accUnit);
        }
    }
}
