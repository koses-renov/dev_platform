﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.CONFIG
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class DefaultInfo
    {
        public string LotName { get; set; }

        string recipeGroupName = "Group";
        public string RecipeGroupName { 
            get
            {
                if (string.IsNullOrEmpty(recipeGroupName))
                    return "Group";
                return recipeGroupName;
            }
            set => recipeGroupName = value; 
        }

        string recipeName = "Recipe";
        public string RecipeName {
            get
            {
                if (string.IsNullOrEmpty(recipeName))
                    return "Recipe";
                return recipeName;
            }
            set => recipeName = value; 
        }
    }
}
