﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.CONFIG
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MotorPositionAttribute : Attribute
    {
        public Def.Motor Motor;

        public MotorPositionAttribute(Def.Motor motor)
        {
            Motor = motor;
        }
    }
}
