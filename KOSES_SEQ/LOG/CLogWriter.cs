﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.MANAGER;

namespace KOSES_SEQ.LOG
{
    public class CLogWriter
    {
        public string Filepath { get; set; }

        ConcurrentQueue<(string, StringBuilder)> LogQueue = new ConcurrentQueue<(string, StringBuilder)>();
        (string, StringBuilder) log = (string.Empty, new StringBuilder());

        Thread thrWriteLog;

        public CLogWriter()
        {
            StartWriteLog();
        }

        void StartWriteLog()
        {
            thrWriteLog = new Thread(WriteLogToFile);
            thrWriteLog.IsBackground = true;
            thrWriteLog.Start();
        }

        /// <summary>
        /// 큐에 넣음
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="text"></param>
        public void AddLog(string fileName, StringBuilder text)
        {
            lock (LogQueue)
            {
                try
                {
                    LogQueue.Enqueue((fileName, text));
                }
                catch(Exception ex)
                {
                    LogManager.ExLog(ex);
                }
            }
        }

        /// <summary>
        /// 로그를 파일에 쓰기
        /// </summary>
        private void WriteLogToFile()
        {
            int delay = 100;
            while (true)
            {
                try
                {
                    if (LogQueue.Count() > 0)
                    {
                        if (LogQueue.Count > 10)
                            delay = 1;
                        else if (LogQueue.Count > 2)
                            delay = 5;
                        else
                            delay = 10;

                        
                        lock (LogQueue)
                        {
                            if (!LogQueue.TryDequeue(out log))
                            {
                                Thread.Sleep(delay);
                                continue;
                            }
                        }

                        using (FileStream file = new FileStream(Filepath + log.Item1, FileMode.Append, FileAccess.Write, FileShare.Read))
                        {
                            using (StreamWriter writer = new StreamWriter(file, Encoding.Default))
                            {
                                writer.Write(log.Item2.ToString() + "\r\n");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ExLog(ex);
                }

                Thread.Sleep(delay);
            }
        }
    }
}
