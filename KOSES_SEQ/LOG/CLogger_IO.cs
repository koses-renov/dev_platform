﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KOSES_SEQ.LOG
{
    public class CLogger_IO : CLogger
    {
        int _iLogKeepDays = 7;
        string _baseLogPath = string.Empty;
        Dictionary<string, CLogWriter> _dcLogWriter = new Dictionary<string, CLogWriter>();

        public CLogger_IO(int iKeepDays, string strPath) : base(iKeepDays, strPath)
        {
            _iLogKeepDays = iKeepDays;

            _baseLogPath = strPath;

            CreateDirectory();

            StartDeleteLogThread();

        }

        object _oLogWrite = new object();
        public override void LogWrite(string strMsg, string strCategory)
        {
            CLogWriter oLogWriter = null;

            lock (_oLogWrite)
            {
                if (_dcLogWriter.TryGetValue(strCategory, out oLogWriter))
                {
                }
                else
                {
                    oLogWriter = new CLogWriter();
                    oLogWriter.Filepath = _baseLogPath + strCategory;
                    CreateSubDirectory(strCategory);
                    _dcLogWriter.Add(strCategory, oLogWriter);
                }
            }

            string fileName = @"\" + DateTime.Now.ToString("yyyy-MM-dd_HH") + ".log";
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("HH:mm:ss:fff") + ", " + strMsg);
            oLogWriter.AddLog(fileName, sb);
        }
    }
}
