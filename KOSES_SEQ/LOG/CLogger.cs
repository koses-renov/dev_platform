﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.CONFIG;

namespace KOSES_SEQ.LOG
{
    public class CLogger
    {
        int _iLogKeepDays = 30;
        string _baseLogPath = string.Empty;
        Dictionary<string, CLogWriter> _dcLogWriter = new Dictionary<string, CLogWriter>();

        protected Thread _thrDeleteOldLogs = null;
        //Timer _tmrDeleteOldLogs = null;

        bool _bRunDeleteThread = true;

        public CLogger(int iKeepDays, string strPath)
        {
            _iLogKeepDays = iKeepDays;

            _baseLogPath = strPath;

            CreateDirectory();

            StartDeleteLogThread();

        }

        ~CLogger()
        {
            _bRunDeleteThread = false;
        }

        protected void StartDeleteLogThread()
        {
            _thrDeleteOldLogs = new Thread(LoopDeleteOldLogs);
            _thrDeleteOldLogs.IsBackground = true;
            _thrDeleteOldLogs.Start();
        }

        protected void LoopDeleteOldLogs()
        {
            FileUtil fu = new FileUtil();
            while (_bRunDeleteThread)
            {
                try
                {
                    fu.DeleteLogByMonth(_baseLogPath, _iLogKeepDays);
                    foreach(string strSubPath in Directory.GetDirectories(_baseLogPath))
                    {
                        fu.DeleteLogByMonth(strSubPath, _iLogKeepDays);
                    }
                }
                catch(Exception ex)
                {
                    LogManager.ExLog(ex);
                }

                Thread.Sleep(60000);
            }
        }

        object _oLogWrite = new object();
        public virtual void LogWrite(string strMsg, string strCategory)
        {
            CLogWriter oLogWriter = null;

            lock (_oLogWrite)
            {
                if (_dcLogWriter.TryGetValue(strCategory, out oLogWriter))
                {
                }
                else
                {
                    oLogWriter = new CLogWriter();
                    oLogWriter.Filepath = _baseLogPath + strCategory;
                    CreateSubDirectory(strCategory);
                    _dcLogWriter.Add(strCategory, oLogWriter);
                }
            }

            string fileName = @"\" + strCategory + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("HH:mm:ss.fff") + ", " + strMsg);
            oLogWriter.AddLog(fileName, sb);
        }

        protected void CreateDirectory()
        {
            if (!Directory.Exists(_baseLogPath))
            {
                Directory.CreateDirectory(_baseLogPath);
            }
        }

        protected void CreateSubDirectory(string strCategory)
        {
            if (!Directory.Exists(_baseLogPath + strCategory))
            {
                Directory.CreateDirectory(_baseLogPath +strCategory);
            }
        }
    }
}
