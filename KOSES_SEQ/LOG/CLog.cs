﻿using KOSES_SEQ.MANAGER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.LOG
{
    public class CLog
    {
        public DateTime PostTime { get; set; }
        public string Message { get; set; }
        public string Category { get; set; }
        public enumLogLevel Level { get; set; }

        public CLog(enumLogLevel eLevel, string strMsg, string strCategory)
        {
            Message = strMsg;
            PostTime = DateTime.Now;
            Level = eLevel;
            Category = strCategory;
        }
    }
}
