 명칭 :
     UNIT PER PULSE

 설명 :
     1 PULSE 당 이동 거리(mm)

 파라미터 :
     사용자 지정

 비고 :
 FASTECH
  - 모터 1바퀴 당 펄스량