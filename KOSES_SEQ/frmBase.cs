﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KOSES_SEQ.CONFIG;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.MOTION;
using KOSES_SEQ.TASK_AUTO;
using KOSES_SEQ.TASK_HOME;
using KOSES_SEQ.TASK_MANUAL;

namespace KOSES_SEQ.OP
{
    public partial class frmBase : Form
    { 

        public frmBase()
        {
            InitializeComponent();

            //List<MotorInfo> motorList = new List<MotorInfo>();
            //int i = 0;
            //foreach (string name in Enum.GetNames(typeof(Def.Motor)))
            //{
            //    MotorInfo mi = new MotorInfo();
            //    mi.Name = name;
            //    mi.Num = i++;
            //    motorList.Add(mi);
            //}
            //SysOperator.Instance.motorPara = new MotorParameter(motorList, DefPath.Motor, DefPath.MotorHelp);
        }

        

        private Point mousePoint;


        private void ShowPagedForm(Form F)
        {
            if (F.Visible) return;

            panBase.HideChildForms();
            F.ShowInside(panBase);

            //lbltitle.Text = F.Text;
        }

        private void frmBase_Load(object sender, EventArgs e)
        {
            SysOperator.Instance.OnRecipeChanged += Instance_OnRecipeNameChanged;
            SysOperator.Instance.Init();

            this.Icon = new Icon(DefPath.ICON);

            timer_SystemTime.Start();
            //pic_CI.BackgroundImageLayout = ImageLayout.Stretch;
            //pic_CI.BackgroundImage = Image.FromFile(DefPath.CI);         

            ShowPagedForm(FRM.Auto);

            FRM.Login.OnLoginChanged += Login_OnLoginChanged;
            AlarmManager.Instance.OnAlarmOccured += Instance_OnAlarmOccured;

            SysOperator.Instance.Tenkey.OnDisplayChanged += Tenkey_OnDisplayChanged;
        }

        private void Tenkey_OnDisplayChanged(int num)
        {
            if (num == 0)
                return;
            this.Invoke((MethodInvoker)delegate ()
            {
                ShowPagedForm(FRM.Manual);
            });
            
        }

        /// <summary>
        /// Alarm Event 발생 시
        /// </summary>
        /// <param name="alarm"></param>
        /// <param name="isReset"></param>
        private void Instance_OnAlarmOccured(ALARM.CAlarm alarm, bool isReset = false)
        {

        }

        private void Login_OnLoginChanged(enumLogInLevel level, string userID)
        {
            if (level > enumLogInLevel.Unknown)
            {
                //lbl_LoginUser.Text = userID;
                switch (level)
                {
                    case enumLogInLevel.Level_OP:
                        lbl_LoginUser.Text = "OPERATOR";
                        break;
                    case enumLogInLevel.Level_ENG:
                        lbl_LoginUser.Text = "ENGINEER";
                        break;
                    case enumLogInLevel.Level_ADMIN:
                        lbl_LoginUser.Text = "ADMINISTRATOR";
                        break;
                }
            }
            else
            {
                lbl_LoginUser.Text = "Login User";
            }
        }

        private void Instance_OnRecipeNameChanged(string groupName, string recipeName)
        {
            lbl_JobName.Text = $"[{groupName}]  {recipeName}";
        }

        private void frmBase_MouseDown(object sender, MouseEventArgs e)
        {  
            //마우스 클릭시 mousePoint변수에 현재 마우스 위치값 저장
            mousePoint = new Point(e.X, e.Y);
        }

        private void frmBase_MouseMove(object sender, MouseEventArgs e)
        {
            //클릭상태로 마우스를 이동한 만큼 윈도우에서 위치값을 뺌
            if ((e.Button&MouseButtons.Left)==MouseButtons.Left)
            {
                Location = new Point(this.Left - (mousePoint.X - e.X),
                    this.Top - (mousePoint.Y - e.Y));
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (FRM.Msg.Confirm($"프로그램을 종료 하시겠습니까?"))
            {
                SysOperator.Instance.SaveDefaultInfo();
                timer_SystemTime.Stop();
                Close();
            }
            else
            {
                return;
            }            
        }

        private void btnHome_MouseEnter(object sender, EventArgs e)
        {
            //((Button)sender).BackColor = Color.DeepSkyBlue;
            //((Button)sender).BackColor = Color.Aqua;
            //((Button)sender).BackColor = Color.LightBlue;
            //((Button)sender).BackColor = Color.LightGreen;
            //((Button)sender).BackColor = Color.LightPink;
            //((Button)sender).BackColor = Color.Gray;
            //((Button)sender).BackColor = Color.LightGray;
            //((Button)sender).BackColor = Color.LightBlue;
            //((Button)sender).BackColor = DefColor.LightBlueGradientActiv;
            ((Button)sender).BackColor = DefColor.LightBlueActiv;
            
        }

        private void btnHome_MouseLeave(object sender, EventArgs e)
        {
           // ((Button)sender).BackColor = Color.LightGray;
            ((Button)sender).BackColor = DefColor.LightBlueGradientInActiv;

            //((Button)sender).BackColor = Color.Gray;
        }

        private void btnManual_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.Manual);
            LogManager.Log(enumLogLevel.UI, "Manual Button Click", this.GetType().Name);
            //FRM.Manual.ShowAndFront(this);
        }

        private void panBaseTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMain_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.Auto);
            LogManager.Log(enumLogLevel.UI, "Auto Button Click", this.GetType().Name);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.StartAuto();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            List<MotorInfo> motorList = new List<MotorInfo>();
            int i = 0;
            foreach (string name in Enum.GetNames(typeof(Def.Motor)))
            {
                MotorInfo mi = new MotorInfo();
                mi.Name = name;
                mi.Num = i++;
                motorList.Add(mi);
                mi = null;
            }
            SysOperator.Instance.motorPara = new MotorParameter(motorList, DefPath.Motor, DefPath.MotorHelp);

            //Def.Motor motor = Def.Motor.PPLoader_P1_R;

            //SysOperator.motion.SetParameter(motor);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("START");
        }

        private void frmBase_Resize(object sender, EventArgs e)
        {
            //
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panBaseTop_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void timer_SystemTime_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;

            lbl_SysTemDateTime.Text = $"{dt.Year:0000}-{dt.Month:00}-{dt.Day:00} {dt.Hour:00}:{dt.Minute:00}:{dt.Second:00}";
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (FRM.Msg.Confirm($"초기화 하시겠습니까?"))
            {
                SysOperator.Instance.Home();
            }
            else
            {
                return;
            }
        }

        #region ProgramInit
        public void ProgramInit_Task()
        {           
            SysOperator.homeTask = new HomeTask();
            SysOperator.homeData = new HomeData();
            SysOperator.manualTask = new ManualTask();
            SysOperator.autoTask = new AutoTask();
            SysOperator.autoData = new AutoData();
            SysOperator.autoUse = new AutoUse();

            //safetySensorTask = new SafetySensorTask();
            //doorSensorTask = new DoorSensorTask();
        }
        #endregion

        private void btnRecipe_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.Job);
            LogManager.Log(enumLogLevel.UI, "Recipe Button Click", this.GetType().Name);
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.Teach);
            LogManager.Log(enumLogLevel.UI, "Setting Button Click", this.GetType().Name);
        }
        private void btnMotorStatus_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.MotorStatus);
            LogManager.Log(enumLogLevel.UI, "MotorStatus Button Click", this.GetType().Name);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.StopAll();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            SysOperator.Instance.Reset();
        }

        private void btnOption_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.System);
            LogManager.Log(enumLogLevel.UI, "System Button Click", this.GetType().Name);
        }

        private void lbl_Login_Click(object sender, EventArgs e)
        {
            FRM.Login.TopMost = true;
            FRM.Login.Show();
        }

        private void btnAlarm_Click(object sender, EventArgs e)
        {
            ShowPagedForm(FRM.Alarm);
            LogManager.Log(enumLogLevel.UI, "Alarm Button Click", this.GetType().Name);
        }

    }
}
