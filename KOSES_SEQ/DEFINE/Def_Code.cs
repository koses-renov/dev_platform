﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.DEFINE
{
    public partial class Def
    {
        #region Code
        /// <summary>
        /// PROGRAM RETURN CODE
        /// </summary>
        public static string GetCodeString(Code code)
        {
            string codeString = code.ToString().ToUpper();
            codeString = codeString.Replace("_", " ");
            return codeString;
        }
        public enum Code
        {
            ////////////////////////////////
            //  GENERAL
            ////////////////////////////////
            OK,                 //  정상
            UNDEFINED,
            USER_ERROR,

            USER_STOP,              //  사용자 중지
            CODE_ERROR,             //  코드 에러
            MOTION_MOVE_FAIL,       //  이동 실패
            MOTION_TIME_OVER,       //  타임 오버

            DOOR_OPENED,

            MOTOR_SERVO_OFF,        //  모터 서보 OFF 상태
            MOTOR_NOT_INIT,         //  모터 초기화되지 않음
            MOTOR_ALARM,            //  모터 알람 발생
            MOTOR_INVALID_POS,      //  모터가 목적 위치에 도달하지 못한 경우
            MOTOR_EXT_SENSOR_1,     //  모터 이동중 추가 센서 감지시 발생 1
            MOTOR_EXT_SENSOR_2,     //  모터 이동중 추가 센서 감지시 발생 2

            INVALID_STEP,
            INVALID_STATUS,
            INVALID_DEST,

            MAIN_AIR_PRESSURE_LOW,

            SAFETY_SENSOR_DETECTED,

            EMERGENCY_BUTTON_PRESSED,


            ///////////////////////////////
            //  MOTOR ERROR
            ///////////////////////////////
            #region MOTOR ERROR
            MOTOR_ERROR_PPLOADER_P1_R,
            MOTOR_ERROR_PPLOADER_P2_R,        
            #endregion




        

            #region Mes
            MES_LOT_VALIDATION_NG,
            MES_RECIPE_VALIDATION_NG,
            MES_MATERIAL_LOT_VALIDATION_NG,
            MES_QUALITY_FAIL,
            MES_RECIVE_STOP,
            #endregion


        }
        #endregion


        static Def.Code GetErrorCode(string errorString)
        {
            Def.Code code;
            if (Enum.TryParse(errorString, out code))
            {
                return code;
            }
            else
            {             
                return Def.Code.UNDEFINED;
            }
        }


        public static Def.Code GetMotorError(Def.Motor motor)
        {
            string motorString = motor.ToString().ToUpper();
            string codeString = $"MOTOR_ERROR_{motorString}";
            return GetErrorCode(codeString);
        }

    }
}
