﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.DEFINE
{
    public class DefPath
    {
        #region 실행경로
        public readonly static string Root = string.Format($"{Application.StartupPath}");
        #endregion

        #region 메인 폴더
        public readonly static string Config = string.Format($"{Root}\\CONFIG");
        public readonly static string Image = string.Format($"{Root}\\IMAGE");
        //public readonly static string ImageMotionDraw = string.Format($"{Root}\\IMAGE\\MD");
        //public readonly static string Dispensing = string.Format($"{Root}\\DISPENSING");
        public readonly static string Log = string.Format($"{Root}\\LOG");
        public readonly static string Data = string.Format($"{Root}\\DATA");
        public readonly static string Message = string.Format($"{Root}\\MESSAGE");
        //public readonly static string Recipe = string.Format($"{Root}\\RECIPE");
        #endregion

        #region Log
        public readonly static string ActionLog = string.Format($"{Log}\\ACTION");
        #endregion

        #region User Log In Data
        public readonly static string UserLogInInfo = string.Format($"{Data}\\UserLogIn.bin");
        #endregion

        #region Config
        public readonly static string Recipe = string.Format($"{Config}\\RECIPE");
        public readonly static string RecipeBackup = string.Format($"{Config}\\RECIPE_BACKUP");
        public readonly static string Motor = string.Format($"{Config}\\MOTOR");
        public readonly static string MotorHelp = string.Format($"{Config}\\MOTOR_HELP");
        public readonly static string IO = string.Format($"{Config}\\IO");

        public readonly static string ProgramIni = string.Format($"{Config}\\PROGRAM.ini");
        public readonly static string ProductIni = string.Format($"{Config}\\Product");       
        #endregion

        #region MESSAGE
        public readonly static string ErrorMessage = string.Format($"{Message}\\ERROR");
        #endregion

        #region IMAGE
        public readonly static string CI = string.Format($"{Image}\\CI.png");
        //public readonly static string Maintenance = string.Format($"{Image}\\MAINTENANCE.png");
        public readonly static string ICON = string.Format($"{Image}\\PROGRAM_ICON.ico");
        public readonly static string AlarmImage = string.Format($"{Image}\\AlarmImage");
        #endregion IMAGE

        #region Dispensing
        //public readonly static string DispensingInfoPath = string.Format($"{Config}\\DispensingInfo.xml");
        //public readonly static string DispensingImagePath = string.Format($"{Dispensing}\\DispensingImage.png");
        //public readonly static string DispensingDefaultImagePath = string.Format($"{Dispensing}\\DispensingImage_Default.png");
        #endregion

        #region FileRemover
        //public readonly static string FileRemover = string.Format($"{Application.StartupPath}\\FileRemover.exe");
        #endregion

        /// <summary>
        /// Application.StartupPath 으로 조회되는 경로는 원격으로 디버깅시 \\192.168.100.200 과 같은 형식으로 나온다.
        /// SQLite 는 로컬에서만 접근이 가능하므로 로컬 경로를 참조하도록 해야 한다.
        /// </summary>
        #region LOG : SQLite
        //public readonly static string LocalLog = string.Format(".\\LOG");
        //public readonly static string ProgramLog = string.Format($"{LocalLog}\\Program.sqlite");
        //public readonly static string ZoneProductLog = string.Format($"{LocalLog}\\Product");
        //public readonly static string ZoneCountLog = string.Format($"{LocalLog}\\Count");
        //public readonly static string DB_EXT = string.Format("sqlite");
        #endregion

        #region BARCODE COMPARE
        //public readonly static string BarcodeCompareVcmIni = string.Format($"{Config}\\Barcode_Compare_Vcm.ini");
        // public readonly static string BarcodeCompareLensIni = string.Format($"{Config}\\Barcode_Compare_Lens.ini");
        #endregion

        #region LAS
        public readonly static string Las_EVMS = string.Format($"D:\\EVMS");
        public readonly static string Las_TP = string.Format($"{Las_EVMS}\\TP");
        public readonly static string Las_LOG = string.Format($"{Las_TP}\\LOG");
		public readonly static string Las_LOG2 = string.Format($"{Las_TP}\\LOG2");
        #endregion

        #region MES
        public readonly static string Mes_MES = string.Format($"D:\\MES");
        public readonly static string Mes_LotStatus_LOG = string.Format($"{Mes_MES}\\LOTSTATUS");
        public readonly static string Mes_Recipe_LOG = string.Format($"{Mes_MES}\\Recipe");
        public readonly static string Mes_Recipe_Down = string.Format($"{Mes_MES}\\Recipe_Down");

        public readonly static string Mes_APD_LOG = string.Format($"{Mes_MES}\\APD");      
        public readonly static string Mes_CONTROL_LOG = string.Format($"{Mes_MES}\\Controlstate");
        public readonly static string Mes_VALIDATION_LOG = string.Format($"{Mes_MES}\\ValidationResult");
        #endregion
    }
}
