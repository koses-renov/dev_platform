﻿using System.Drawing;

namespace KOSES_SEQ.DEFINE
{
    class DefColor
    {
        public readonly static Color Black = Color.FromArgb(0, 0, 0);
        public readonly static Color White = Color.FromArgb(255, 255, 255);
        public readonly static Color Yellow = Color.Yellow;
        public readonly static Color Orange = Color.Orange;
        public readonly static Color OrangeRed = Color.OrangeRed;

        public readonly static Color LightRed = Color.FromArgb(255, 0, 0);
        public readonly static Color LightGreen = Color.FromArgb(0, 255, 0);
        public readonly static Color LightBlue = Color.FromArgb(0, 0, 255);

        public readonly static Color HotTrack = Color.FromArgb(0, 102, 204);
        public readonly static Color SteelBlue = Color.FromArgb(46, 82, 180);

        public readonly static Color GoogleBlue = Color.FromArgb(66, 133, 244);
        public readonly static Color LG = Color.FromArgb(196, 3, 82);

        public readonly static Color Cream = Color.FromArgb(255, 255, 204);

        public readonly static Color Control = Color.FromKnownColor(KnownColor.Control);
        public readonly static Color Gray = Color.FromKnownColor(KnownColor.Gray);
        public readonly static Color DarkGray = Color.FromKnownColor(KnownColor.DarkGray);
        public readonly static Color Gold = Color.FromKnownColor(KnownColor.Gold);
        public readonly static Color Golddnrod = Color.FromKnownColor(KnownColor.Goldenrod);
        public readonly static Color DarkGolddnrod = Color.FromKnownColor(KnownColor.DarkGoldenrod);
        public readonly static Color LightGoldenrodYellow = Color.FromKnownColor(KnownColor.LightGoldenrodYellow);

        public readonly static Color LightBlueActiv = Color.FromKnownColor(KnownColor.ActiveCaption);
        public readonly static Color LightBlueGradientActiv= Color.FromKnownColor(KnownColor.GradientActiveCaption);
        public readonly static Color LightBlueGradientInActiv = Color.FromKnownColor(KnownColor.GradientInactiveCaption);

        public readonly static Color Unselect = Color.FromArgb(240, 240, 240);
        public readonly static Color Select = GoogleBlue;
        public readonly static Color MainTitle = GoogleBlue;
        public readonly static Color Title = Cream;

        public readonly static Color TopBack = LG;
        public readonly static Color TopText = White;

        public readonly static Color Off = Black;
        public readonly static Color On = LightGreen;

        public readonly static Color Signal = OrangeRed;
        public readonly static Color SignalAck = Cream;
        public readonly static Color Status = LightGreen;

        public readonly static Color Empty = White;
        public readonly static Color New = Yellow;
        public readonly static Color Used = Gray;
        public readonly static Color NotUse = Black;
        public readonly static Color NoSensing = SteelBlue;
        public readonly static Color Reverse = LightRed;
        public readonly static Color Aligned = Orange;
        public readonly static Color Pass = LightGreen;
        public readonly static Color Reject = LightRed;
        public readonly static Color PocketAligned = LightBlue;

        public readonly static Color SimpleManual = OrangeRed;


        public readonly static Color MsgNormal = LightGreen;
        public readonly static Color MsgWarning = Yellow;
        public readonly static Color MsgAlarm = LightRed;
    }
}
