﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.DEFINE
{
    public partial class Def
    {
        #region enum Motor
        public enum Motor
        {
            PPLoader_P1_R   = 0,
            MagLoader_Z     = 1,             
            Attach_1_P1_Z   = 2,                
            Attach_1_P1_R   = 3,                      
            PPUnloader_P1_Z = 4,
            PPLoader_P1_Z   = 5, 
            
            Max = 6,
        }
        public static string MotorNone = $"";

        public static Motor StringToMotor(string motorName)
        {
            Motor e;
            try
            {
                //motorName = motorName.ToUpper();
                e = (Motor)Enum.Parse(typeof(Motor), motorName);
                if (Enum.IsDefined(typeof(Motor), e) == false)
                {
                    e = Motor.MagLoader_Z;
                }
            }
            catch (ArgumentException)
            {
                e = Motor.MagLoader_Z;
            }
            return e;
        }
        public static string GetMotorString(Def.Motor m)
        {
            string motorName = m.ToString().ToUpper();
            motorName = motorName.Replace("_", " ");
            return motorName;
        }
        #endregion


        public enum MotorType
        {
            PCIB,
            ML3,
            ECAT
        }

        public static MotorType GetMotorType(Def.Motor motor)
        {
            MotorType type;

            switch (motor)
            {
                case Motor.PPLoader_P1_R:               
                    type = MotorType.PCIB;
                    break;

                case Motor.Attach_1_P1_Z:               
                case Motor.Attach_1_P1_R:                
                    type = MotorType.ECAT;
                    break;

                default:
                    type = MotorType.ML3;
                    break;
            }

            return type;
        }

    }
}
