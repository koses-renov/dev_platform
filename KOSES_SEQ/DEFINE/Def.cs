﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Serialization;


namespace KOSES_SEQ.DEFINE
{
    public partial class Def
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  일반 정의
        //
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public const string ProgramFontName = "Tahoma";
        public const int ProgramTitleFontSize = 12;
        public const int ProgramLargeFontSize = 12;
        public const int ProgramMediumFontSize = 10;
        public const int ProgramSmallFontSize = 9;
        public const int ProgramExtraSmallFontSize = 7;
        public const int ProgramUltraSmallFontSize = 5;

        public const int NoticeZoneLargeFontSize = 36;
        public const int NoticeZoneMediumFontSize = 28;
        public const int NoticeZoneSmallFontSize = 20;

        public const int NoticeLargeFontSize = 56;
        public const int NoticeMediumFontSize = 48;
        public const int NoticeSmallFontSize = 36;

        public const int FlashInterval = 1250;

        public const int Unknown = -1;

        public const int WS_EX_CPMPOSITED = 0x02000000;

        public const int TaskDelay = 32;

        public const int NotFound = -1;


        #region TaskType
        public enum TaskType
        {
            NONE = 0,

            AUTO,
            HOME,
            MANUAL,
        }
        #endregion

        #region TowerLamp
        public enum TowerLamp
        {
            Red,
            Yellow,
            Green,
            Blue,
            White,
        }
        #endregion

        /// <summary>
        /// Message scroll property
        /// </summary>
        #region Message scroll property
        public class MsgScrollProperty
        {
            public string msg;
            public string remark;
            public Color color;

            public MsgScrollProperty()
            {
                msg = "";
                remark = null;
                color = DefColor.LightGreen;
            }
        }
        #endregion

        #region Control SwapPos
        public static void SwapPos(Control c1, Control c2)
        {
            int left = c1.Left;
            int top = c1.Top;

            c1.Left = c2.Left;
            c1.Top = c2.Top;

            c2.Left = left;
            c2.Top = top;
        }
        #endregion

        public enum KeyStatus
        {
            Down,
            Up,
        }

        public enum MessageBoxShow
        {
            No,
            Yes,
        }

    }
}
