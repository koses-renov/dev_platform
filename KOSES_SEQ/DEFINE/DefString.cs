﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KOSES_SEQ.DEFINE
{
    public class DefString
    {
        #region 문자열 비교 함수
        public static bool IsEqual(string s1, string s2, bool IgnoreCase = true)
        {
            bool b;
            if (IgnoreCase)
            {
                b = String.Equals(s1, s2, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                b = s1.Equals(s2, StringComparison.Ordinal);
            }
            return b;
        }
        #endregion
        #region 문자열 변환 함수
        //public static bool GetBool(string stringValue, bool defaultValue = false)
        //{
        //    bool temp = defaultValue;
        //    if (Boolean.TryParse(stringValue, out temp))
        //    {
        //        return temp;
        //    }
        //    return defaultValue;
        //}
        public static bool GetBoolean(string temp)
        {
            if (temp.Length == 4 && Regex.IsMatch(temp, "true", RegexOptions.IgnoreCase))
            {
                return true;
            }
            if (temp.Length == 2 && Regex.IsMatch(temp, "on", RegexOptions.IgnoreCase))
            {
                return true;
            }
            return false;
        }

        public static int GetInt(string stringValue, int defaultValue = 0)
        {
            int temp = defaultValue;
            if (Int32.TryParse(stringValue, out temp))
            {
                return temp;
            }
            return defaultValue;
        }
        public static double GetDouble(string stringValue, double defaultValue = 0.0)
        {
            double temp = defaultValue;
            if (Double.TryParse(stringValue, out temp))
            {
                return temp;
            }
            return defaultValue;
        }
        #endregion


        public const string None = "";

        public const string OK = "OK";

        public const string StartOption = "START OPTION";
        
        public const string StartMode = "START_MODE";
        public const string ZeroPos = "ZERO_POS";

        public const string Left = "Left";
        public const string Top = "Top";
        public const string Right = "Right";
        public const string Bottom = "Bottom";

        public const string LEFT = "LEFT";
        public const string RIGHT = "RIGHT";

        public const string Width = "Width";
        public const string Height = "Height";

        public const string Pass = "PASS";
        public const string VisionFail = "Vision Fail";

        public const string Count = "COUNT";
        public const string RowCount = "ROW_COUNT";
        public const string ColCount = "COL_COUNT";
        public const string RowGap = "ROW_GAP";
        public const string ColGap = "COL_GAP";

        public const string LineCount = "LINE_COUNT";
        public const string LineGap = "LINE_GAP";

        public const string MaxGroupCount = "MAX_GROUP_COUNT";
        public const string GroupPerRow = "GROUP_PER_ROW";

        public const string StandardWeight = "STANDARD_WEIGHT";

        public const string IP = "IP";
        public const string Port = "PORT";
        public const string Baudrate = "BAUDRATE";

        public const string LeftPort = "LEFT_PORT";
        public const string LeftBaudrate = "LEFT_BAUDRATE";
        public const string LeftChannel = "LEFT_CHANNEL";

        public const string RightPort = "RIGHT_PORT";
        public const string RightBaudrate = "RIGHT_BAUDRATE";
        public const string RightChannel = "RIGHT_CHANNEL";

        public const string IP1 = "IP_1";
        public const string IP2 = "IP_2";
        public const string Port1 = "PORT_1";
        public const string Port2 = "PORT_2";
        public const string Port3 = "PORT_3";

        public const string IP_ppLoader = "IP_PP_LOADER";
        public const string IP_Attach_1 = "IP_ATTACH_1";
        public const string IP_Attach_2 = "IP_ATTACH_2";
        public const string IP_Attach_3 = "IP_ATTACH_3";

        public const string Port_ppLoader = "PORT_PP_LOADER";
        public const string Port_Attach_1 = "PORT_ATTACH_1";
        public const string Port_Attach_2 = "PORT_ATTACH_2";
        public const string Port_Attach_3 = "PORT_ATTACH_3";

        public const string LightPort = "LIGHT_PORT";
        public const string LightBaudrate = "LIGHT_BAUDRATE";

        public const string LightValue_ppLoader = "LIGHT_VALUE_PP_LOADER";
        public const string LightValue_ppAttach_1 = "LIGHT_VALUE_PP_ATTACH_1";
        public const string LightValue_ppAttach_2 = "LIGHT_VALUE_PP_ATTACH_2";
        public const string LightValue_ppAttach_3 = "LIGHT_VALUE_PP_ATTACH_3";

        public const string True = "TRUE";
        public const string False = "FALSE";

        public const string On = "ON";
        public const string Off = "OFF";

        public const string Red = "RED";
        public const string Yellow = "YELLOW";
        public const string Green = "GREEN";
        public const string Blue = "BLUE";
        public const string White = "WHITE";

        public const string Program = "PROGRAM";
        public const string Product = "PRODUCT";
        public const string User = "USER";
        public const string LastUser = "LAST USER";
        public const string Common = "COMMON";
        public const string General = "GENERAL";
        public const string Indivisual = "INDIVISUAL";
        public const string Setup = "SETUP";
        public const string System = "SYSTEM";
        public const string Alarm = "ALARM";
        public const string Error = "ERROR";
        public const string Type = "TYPE";
        public const string Data = "DATA";
        public const string Teaching = "TEACHING";
        public const string Code = "CODE";
        public const string Description = "DESCRIPTION";

        public const string CompanyName = "COMPANY_NAME";

        public const string ScreenLockKey = "SCREEN_LOCK_KEY";

        public const string VcmTestCount = "VCM_TEST_COUNT";

        public const string TerminateAgent = "TERMINATE_AGENT";

        public const string Serial = "SERIAL";
        public const string TimeStamp = "TIME_STAMP";
        public const string Number = "NUMBER";
        public const string Boolean = "BOOLEAN";
        public const string No = "NO";
        public const string Item = "ITEM";
        public const string Value = "VALUE";
        public const string ValueType = "VALUE_TYPE";
        public const string Cause = "CAUSE";
        public const string Solution = "SOLUTION";
        public const string Remark = "REMARK";
        public const string Name = "NAME";
        public const string Color = "COLOR";
        public const string Lot = "LOT";
        public const string Model = "MODEL";
        public const string EmpNum = "EMPLOYEE_NUMBER";
        public const string Status = "STATUS";
        public const string Pos = "POS";
        public const string Material1 = "MATERIAL1";
        public const string Material2 = "MATERIAL2";
        public const string Material3 = "MATERIAL3";

        public const string Login = "LOGIN";
        public const string Operator = "OPERATOR";
        public const string Engineer = "ENGINEER";
        public const string Vendor = "VENDOR";
        public const string Password = "PASSWORD";
        public const string Security = "SECURITY";

        public const string Icon = "ICON";

        public const string EquipmentName = "EQUIPMENT_NAME";
        public const string EquipmentType = "EQUIPMENT_TYPE";
        public const string EquipmentState = "EQUIPMENT_STATE";

        public const string Inline = "INLINE";
        public const string StandAlone = "STAND_ALONE";

        public const string Recipe = "RECIPE";
        public const string Current = "CURRENT";
        public const string Path = "PATH";
        public const string CommonRecipe = "COMMON_RECIPE";

        public const string TowerLamp = "TOWER_LAMP";

        public const string Language = "LANGUAGE";
        public const string MenuLanguage = "MENU_LANGUAGE";
        public const string ErrorLanguage = "ERROR_LANGUAGE";
        public const string Korean = "KOREAN";
        public const string English = "ENGLISH";
        public const string Chinese = "CHINESE";
        public const string Vietnamese = "VIETNAMESE";


        public const string Motor = "MOTOR";
        public const string BackupTable = "BACKUP_TABLE";
        public const string MotorCount = "MOTOR_COUNT";
        public const string Air = "AIR";
        public const string IO = "IO";
        public const string Speed = "SPEED";
        public const string Laser = "LASER";
        public const string Picker = "PICKER";
        public const string LoadingPicker = "LOADING_PICKER";
        public const string UnloadingPicker = "UNLOADING_PICKER";
        public const string ModuleSensor = "MODULE_SENSOR";
        public const string Test = "TEST";

        public const string EngineerReleaseTime = "ENGINEER_RELEASE_TIME";

        public const string ServoOnWhenStart = "SERVO_ON_WHEN_START";

        public const string TesterRestartWhenAckTimeover = "TESTER_RESEND_WHEN_ACK_TIMEOVER";

        public const string LogClearDay = "LOG_CLEAR_DAY";
        
        public const string Use = "USE";

        public const string UseSocket_1 = "USE_SOCKET_1";
        public const string UseSocket_2 = "USE_SOCKET_2";
        public const string UseSocket_3 = "USE_SOCKET_3";
        public const string UseSocket_4 = "USE_SOCKET_4";

        public const string Socket1_1_BaseUnitHeight = "SOCKET1_1_BASE_UNIT_HEIGHT";
        public const string Socket1_2_BaseUnitHeight = "SOCKET1_2_BASE_UNIT_HEIGHT";
        public const string Socket2_1_BaseUnitHeight = "SOCKET2_1_BASE_UNIT_HEIGHT";
        public const string Socket2_2_BaseUnitHeight = "SOCKET2_2_BASE_UNIT_HEIGHT";
        public const string Socket3_1_BaseUnitHeight = "SOCKET3_1_BASE_UNIT_HEIGHT";
        public const string Socket3_2_BaseUnitHeight = "SOCKET3_2_BASE_UNIT_HEIGHT";
        public const string Socket4_1_BaseUnitHeight = "SOCKET4_1_BASE_UNIT_HEIGHT";
        public const string Socket4_2_BaseUnitHeight = "SOCKET4_2_BASE_UNIT_HEIGHT";

        public const string TorqueLimit = "TORQUE_LIMIT";
        //public const string TorqueRange = "TORQUE_RANGE";
        public const string TorqueRange_min = "TORQUE_RANGE_MIN";
        public const string TorqueRange_max = "TORQUE_RANGE_MAX";

        public const string PP_Loader_Pos = "PP_LOADER_POS";
        public const string Mask_Pos = "MASK_POS";
        public const string Inspection_Before_Dispensing_Pos = "INS_BEFORE_DISPENSING_POS";
        public const string Line_Laser_1_Pos = "LINE_LASER_1_POS";
        public const string Dispensing_Pos = "DISPENSING_POS";
        public const string Inspection_After_Dispensing_Pos = "INS_AFTER_DISPENSING_POS";
        public const string Attach_Pos = "ATTACH_POS";
        public const string Inspection_Final_Pos = "INS_FINAL_POS";
        public const string Line_Laser_2_Pos = "LINE_LASER_2_POS";
        public const string PP_Unloader_Pos = "PP_UNLOADER_POS";

        public const string VcmCarrierCount = "VCM_CARRIER_COUNT";
        public const string VcmCarrierGap = "VCM_CARRIER_GAP";

        public const string VcmCarrierRow = "VCM_CARRIER_ROW";
        public const string VcmCarrierCol = "VCM_CARRIER_COL";
        public const string VcmCarrierRowGap = "VCM_CARRIER_ROW_GAP";
        public const string VcmCarrierColGap = "VCM_CARRIER_COL_GAP";

        public const string UseLoaderTiltSensor = "USE_LOADER_TILT_SENSOR";
        public const string UseUnloaderTiltSensor = "USE_UNLOADER_TILT_SENSOR";

        public const string RejectTrayRow = "REJECT_TRAY_ROW";
        public const string RejectTrayCol = "REJECT_TRAY_COL";
        public const string RejectTrayRowGap = "REJECT_TRAY_ROW_GAP";
        public const string RejectTrayColGap = "REJECT_TRAY_COL_GAP";

        public const string UvCureBeforeDelayTime = "UV_CURE_BEFORE_DELAY_TIME";
        public const string UvCureTime = "UV_CURE_TIME";

        public const string DispensingType = "DISPENSING_TYPE";

        public const string DispensingVisionType = "DISPENSING_VISION_TYPE";

        public const string UseLineLaserData = "USE_LINE_LASER_DATA";
        public const string UseTorqueOverStep = "USE_TORQUE_OVER_STEP";
        public const string TorqueOverStepCount = "TORQUE_OVER_STEP_COUNT";
        public const string TorqueOverStepDist = "TORQUE_OVER_STEP_DISTANCE";

        public const string Epoxy = "EPOXY";
        public const string EpoxyShotMax = "EPOXY_SHOT_MAX";
        public const string EpoxyTimeMax = "EPOXY_TIME_MAX";

        public const string EpoxyMeasure = "EPOXY_MEASURE";
        public const string LeftMeasureCount = "LEFT_MEASURE_COUNT";
        public const string RightMeasureCount = "RIGHT_MEASURE_COUNT";

        public const string CircleAngle = "CIRCLE_ANGLE";

        public const string CircleAcc = "CIRCLE_ACC";
        public const string CircleVel = "CIRCLE_VEL";
        public const string CircleDec = "CIRCLE_DEC";

        public const string TailAngle = "TAIL_ANGLE";

        public const string ShotStartDelay = "SHOT_START_DELAY";
        public const string ShotEndDelay = "SHOT_END_DELAY";

        public const string Clump_1_Vel = "CLUMP_1_VEL";
        public const string Clump_2_Vel = "CLUMP_2_VEL";

        public const string Clump_1_Use = "CLUMP_1_USE";
        public const string Clump_2_Use = "CLUMP_2_USE";
        public const string Clump_3_Use = "CLUMP_3_USE";
        public const string Clump_4_Use = "CLUMP_4_USE";

        public const string Clump_1_Z = "CLUMP_1_Z";
        public const string Clump_2_Z = "CLUMP_2_Z";
        public const string Clump_3_Z = "CLUMP_3_Z";
        public const string Clump_4_Z = "CLUMP_4_Z";

        public const string Clump_1_Angle = "CLUMP_1_ANGLE";
        public const string Clump_2_Angle = "CLUMP_2_ANGLE";
        public const string Clump_3_Angle = "CLUMP_3_ANGLE";
        public const string Clump_4_Angle = "CLUMP_4_ANGLE";

        public const string CirclePreDelay = "CIRCLE_PRE_DELAY";
        public const string CirclePostDelay = "CIRCLE_POST_DELAY";

        public const string LensTrayRow = "LENS_TRAY_ROW";
        public const string LensTrayCol = "LENS_TRAY_COL";
        public const string LensTrayRowGap = "LENS_TRAY_ROW_GAP";
        public const string LensTrayColGap = "LENS_TRAY_COL_GAP";

        public const string AssyCarrierCount = "ASSEMBLY_CARRIER_COUNT";
        public const string AssyCarrierGap = "ASSEMBLY_CARRIER_GAP";

        public const string AssyCarrierRow = "ASSEMBLY_CARRIER_ROW";
        public const string AssyCarrierCol = "ASSEMBLY_CARRIER_COL";
        public const string AssyCarrierRowGap = "ASSEMBLY_CARRIER_ROW_GAP";
        public const string AssyCarrierColGap = "ASSEMBLY_CARRIER_COL_GAP";


        public const string MagLoader = "MAG_LOADER";
        public const string MagUnloader = "MAG_UNLOADER";
        public const string Rail = "RAIL";
        public const string RailLoader = "RAIL_LOADER";
        public const string RailUnloader = "RAIL_UNLOADER";
        public const string RailIndex = "RAIL_INDEX";
        public const string RailIndex_1 = "RAIL_INDEX_1";
        public const string RailIndex_2 = "RAIL_INDEX_2";
        public const string RailIndex_3 = "RAIL_INDEX_3";
        public const string RailIndex_4 = "RAIL_INDEX_4";
        public const string RailIndex_5 = "RAIL_INDEX_5";
        public const string RailIndex_6 = "RAIL_INDEX_6";
        public const string PPLoader = "PP_LOADER";
        public const string PPLoader_Picker_1 = "PP_LOADER_PICKER_1";
        public const string PPLoader_Picker_2 = "PP_LOADER_PICKER_2";
        public const string PPLoader_Picker_3 = "PP_LOADER_PICKER_3";
        public const string PPLoader_Picker_4 = "PP_LOADER_PICKER_4";
        public const string PPLoaderCamera = "PP_LOADER_CAMERA";
        public const string PPUnloader = "PP_UNLOADER";
        public const string PPUnloader_Picker_1 = "PP_UNLOADER_PICKER_1";
        public const string PPUnloader_Picker_2 = "PP_UNLOADER_PICKER_2";
        public const string PPUnloader_Picker_3 = "PP_UNLOADER_PICKER_3";
        public const string PPUnloader_Picker_4 = "PP_UNLOADER_PICKER_4";
        public const string PPUnloaderCamera = "PP_UNLOADER_CAMERA";
        public const string Mask = "MASK";
        public const string Dispenser = "DISPENSER";
        public const string Dispensing = "DISPENSING";
        public const string Left_Dispensing = "LEFT DISPENSING";
        public const string Right_Dispensing = "RIGHT DISPENSING";
        public const string Left_Socket_1 = "LEFT_SOCKET_1";


        public const string Inspection = "INSPECTION";
        public const string Inspection_1 = "INSPECTION_1";
        public const string Inspection_2 = "INSPECTION_2";

        public const string Barcode_Read_Offset_R = "BARCODE_READ_OFFSET_R";
        public const string Attach_R = "ATTACH_R";

        public const string Lens = "LENS";
        public const string LensTrayStacker = "LENS_TRAY_STACKER";
        public const string LensTrayShuttle = "LENS_TRAY_SHUTTLE";

        public const string DebugMode = "DEBUG_MODE";
        public const string DebugModeKey = "DOTEC_ENGINEER";

        public const string VirtualMode = "VIRTUAL_MODE";
        public const string DevPanelShow = "DEV_PANEL_SHOW";
        public const string AllGoodMode = "ALL_GOOD_MODE";

        public const string VisionUse = "VISION_USE";
        public const string BarcodeUse = "BARCODE_USE";

        public const string MesUse = "MES_USE";
        public const string MesControlStateUse = "MES_CONTROL_STATE_USE";

        public const string DryRun = "DRYRUN";
        public const string AgingMode = "AGING_MODE";
        public const string SaveTaskData = "SAVE_TASK_DATA";

        public const string AgentRun = "AGENT_RUN";

        public const string DoorCheck = "DOOR_CHECK";
        public const string AttCameraFirst = "ATT_CAMERA_FIRST";

        public const string Move = "MOVE";
        public const string Acc = "ACC";
        public const string Dec = "DEC";
        public const string Vel = "VEL";
        public const string MotorProfile = "MOTOR_PROFILE";
        public const string MotorType = "MOTOR_TYPE";

        public const string Home = "HOME";
        public const string HomeMethod = "HOME_METHOD";
        public const string Z_Phase = "Z_PHASE";
        public const string HomeAcc_0 = "HOME_ACC_0";
        public const string HomeAcc_1 = "HOME_ACC_1";
        public const string HomeAcc_2 = "HOME_ACC_2";
        public const string HomeAcc_3 = "HOME_ACC_3";
        public const string HomeAcc_4 = "HOME_ACC_4";
        public const string HomeVel_0 = "HOME_VEL_0";
        public const string HomeVel_1 = "HOME_VEL_1";
        public const string HomeVel_2 = "HOME_VEL_2";
        public const string HomeVel_3 = "HOME_VEL_3";
        public const string HomeVel_4 = "HOME_VEL_4";
        public const string HomeDec = "HOME_DEC";
        public const string HomeDec_0 = "HOME_DEC_0";
        public const string HomeDec_1 = "HOME_DEC_1";
        public const string HomeDec_2 = "HOME_DEC_2";
        public const string HomeDec_3 = "HOME_DEC_3";
        public const string HomeDec_4 = "HOME_DEC_4";
        public const string HomeClearTime = "HOME_CLEAR_TIME";
        public const string HomeOffset = "HOME_OFFSET";
        public const string HomeDir = "HOME_DIR";

        public const string SetUp = "SETUP";
        public const string UnitPerPulse = "UNIT_PER_PULSE";
        public const string PulseMode = "PULSE_MODE";
        public const string EncodeDir = "ENCODE_DIR";
        public const string PlusSignalLevel = "PLUS_SIGNAL_LEVEL";
        public const string MinusSignalLevel = "MINUS_SIGNAL_LEVEL";
        public const string HomeSignalLevel = "HOME_SIGNAL_LEVEL";
        public const string InposSignalLevel = "INPOS_SIGNAL_LEVEL";
        public const string AlarmSignalLevel = "ALARM_SIGNAL_LEVEL";
        public const string ServoSignalLevel = "SERVO_SIGNAL_LEVEL";
        public const string EmergencySignalLevel = "EMERGENCY_SIGNAL_LEVEL";
        public const string EncoderCount = "ENCODER_COUNT";
        public const string AccUnit = "ACC_UNIT";

        public const string MoveTolerance = "MOVE_TOLERANCE";

        public const string WaitTime = "WAIT_TIME";
        public const string MoveWaitTime = "MOVE_WAIT_TIME";
        public const string HomeWaitTime = "HOME_WAIT_TIME";
        public const string DispensingWaitTime = "DISPENSING_WAIT_TIME";
        public const string PointShotTime = "POINT_SHOT_TIME";
        public const string VacuumWaitTime = "VACUUM_WAIT_TIME";
        public const string VacuumOnWaitTime = "VACUUM_ON_WAIT_TIME";

        //
        public const string VacOnWaitTmPrePP = "VACUUM_ON_WAIT_TIME_PRE_PP";
        public const string VacOnWaitTmLdPP = "VACUUM_ON_WAIT_TIME_LD_PP";
        public const string VacOnWaitTmShuttle = "VACUUM_ON_WAIT_TIME_SHUTTLE";
        public const string VacOnWaitTmShuttleMask = "VACUUM_ON_WAIT_TIME_MASK";
        public const string VacOnWaitTmAttach = "VACUUM_ON_WAIT_TIME_ATTACH";
        public const string VacOnWaitTmUldPP = "VACUUM_ON_WAIT_TIME_ULD_PP";

        //
        public const string BlowOnWaitTmPrePP = "BLOW_WAIT_TIME_PRE_PP";
        public const string BlowOnWaitTmLdPP = "BLOW_WAIT_TIME_LD_PP";
        public const string BlowOnWaitTmShuttle = "BLOW_WAIT_TIME_SHUTTLE";
        public const string BlowOnWaitTmShuttleMask = "BLOW_WAIT_TIME_MASK";
        public const string BlowOnWaitTmAttach = "BLOW_WAIT_TIME_ATTACH";
        public const string BlowOnWaitTmUldPP = "BLOW_WAIT_TIME_ULD_PP";

        public const string BlowWaitTime = "BLOW_WAIT_TIME";
        public const string VacuumCancelWaitTime = "VACUUM_CANCEL_WAIT_TIME";
        public const string VacuumOffTime = "VACUUM_OFF_TIME";
        public const string BackuptableVacuumWaitTime = "TABLE_VACUUM_WAIT_TIME";
        public const string VibrationControlTime = "VIBRATION_CONTROL_TIME";
        public const string AutoCalRepeatCount = "AUTO_CAL_REPEAT_COUNT";
        public const string AlignVisionRepeatCount = "ALIGN_VISION_REPEAT_COUNT";
        public const string RetryCount = "RETRY_COUNT";

        public const string RangeX = "RANGE_X";
        public const string RangeY = "RANGE_Y";
        public const string RangeR = "RANGE_R";

        public const string PickerDownTime = "PICKER_DOWN_TIME";
        public const string PickerDownWaitTime = "PICKER_DOWN_WAIT_TIME";

        public const string StopperDelayTime = "STOPPER_DELAY_TIME";

        public const string RailElevatorMoveWaitTime = "RAIL_ELEVAOTOR_MOVE_WAIT_TIME";
        public const string RailOpenCloseWaitTime = "RAIL_OPEN_CLOSE_WAIT_TIME";

        public const string AlignerFwdDelayTime = "ALIGNER_FWD_DELAY_TIME";
        public const string AlignerBwdDelayTime = "ALIGNER_BWD_DELAY_TIME";

        public const string UvFwdBwdDelayTime = "UV_FWD_BWD_DELAY_TIME";

        public const string Shuttle_1_Y = "SHUTTLE_1_Y";
        public const string Shuttle_1_Z = "SHUTTLE_1_Z";
        public const string Shuttle_1_R = "SHUTTLE_1_R";

        public const string Shuttle_2_Y = "SHUTTLE_2_Y";
        public const string Shuttle_2_Z = "SHUTTLE_2_Z";
        public const string Shuttle_2_R = "SHUTTLE_2_R";

        public const string Shuttle_3_Y = "SHUTTLE_3_Y";
        public const string Shuttle_3_Z = "SHUTTLE_3_Z";
        public const string Shuttle_3_R = "SHUTTLE_3_R";

        public const string Shuttle_4_Y = "SHUTTLE_4_Y";
        public const string Shuttle_4_Z = "SHUTTLE_4_Z";
        public const string Shuttle_4_R = "SHUTTLE_4_R";

        public const string Shuttle_5_Y = "SHUTTLE_5_Y";
        public const string Shuttle_5_Z = "SHUTTLE_5_Z";
        public const string Shuttle_5_R = "SHUTTLE_5_R";

        public const string Shuttle_6_Y = "SHUTTLE_6_Y";
        public const string Shuttle_6_Z = "SHUTTLE_6_Z";
        public const string Shuttle_6_R = "SHUTTLE_6_R";

        public const string VisionAlignUse = "VISION_ALGIN_USE";

        public const string AlignerUse = "ALIGNER_USE";

        public const string TwoStepHeight = "TWO_STEP_HEIGHT";
        public const string TwoStepSpeed = "TWO_STEP_SPEED";

        public const string TwoStepAttHeight = "TWO_STEP_ATT_HEIGHT";
        public const string TwoStepAttSpeed = "TWO_STEP_ATT_SPEED";
        public const string TwoStepAttAccDcc = "TWO_STEP_ATT_ACC_DCC";

        public const string TwoStepLensHeight = "TWO_STEP_LENS_HEIGHT";
        public const string TwoStepLensSpeed = "TWO_STEP_LENS_SPEED";
        public const string TwoStepLensAccDcc = "TWO_STEP_LENS_ACC_DCC";

        public const string LimitLensTilt = "LIMIT_LENS_TILT";

        public const string Camera_Socket_1_X = "CAMERA_SOCKET_1_X";
        public const string Camera_Socket_1_Y = "CAMERA_SOCKET_1_Y";
        public const string Camera_Socket_1_Z = "CAMERA_SOCKET_1_Z";

        public const string Line_Laser_2_Socket_1_X = "LINE_LASER_2_SOCKET_1_X";
        public const string Line_Laser_2_Socket_1_Y = "LINE_LASER_2_SOCKET_1_Y";
        public const string Line_Laser_2_Socket_1_Z = "LINE_LASER_2_SOCKET_1_Z";

        public const string CameraSocket_X = "CAMERA_SOCKET_X";
        public const string CameraSocket_Y = "CAMERA_SOCKET_Y";
        public const string CameraSocket_Z = "CAMERA_SOCKET_Z";
               
        public const string P2_Offset_X = "P2_OFFSET_X";
        public const string P2_Offset_Y = "P2_OFFSET_Y";
        public const string P3_Offset_X = "P3_OFFSET_X";
        public const string P3_Offset_Y = "P3_OFFSET_Y";
        public const string P4_Offset_X = "P4_OFFSET_X";
        public const string P4_Offset_Y = "P4_OFFSET_Y";

        public const string TrayRowCount = "TRAY_ROW_COUNT";
        public const string TrayColCount = "TRAY_COL_COUNT";

        public const string TrayRowGap = "TRAY_ROW_GAP";
        public const string TrayColGap = "TRAY_COL_GAP";

        public const string LoaderPickerGap = "LOADER_PICKER_GAP";
        
        public const string AttachPickerGap = "ATTACH_PICKER_GAP";

        public const string ShuttleSocketGap = "SHUTTLE_SOCKET_GAP";

        public const string LaserVcmHeight = "LASER_VCM_HEIGHT";
        public const string LaserDataLimit = "LASER_DATA_LIMIT";
        public const string Laser1_Vcm_Gap = "LASER1_VCM_GAP";
        public const string Laser1_Socket_Gap = "LASER1_SOCKET_GAP";
        public const string LaserMeasureGap_1_2 = "LASER_MEASURE_GAP_1_2";
        public const string LaserMeasureGap_1_3 = "LASER_MEASURE_GAP_1_3";
        public const string LaserMeasureGap_1_4 = "LASER_MEASURE_GAP_1_4";
        //public const string LaserMeasureGap_1_5 = "LASER_MEASURE_GAP_1_5";
        //public const string LaserMeasureGap_1_6 = "LASER_MEASURE_GAP_1_6";

        public const string BobinRadius = "BOBIN_RADIUS";

        public const string CalZMaxCount = "CAL_Z_MAX_COUNT";
        public const string CalZMoveUnit = "CAL_Z_MOVE_UNIT";

        public const string DispensingPointFilePath = "DISPENSING_POINT_FILE_PATH";

        public const string TopMagPos = "TOP_MAGZINE_POS";
        public const string BtmMagPos = "BOTTOM_MAGZINE_POS";

        public const string TopBtmGap = "TOP_BOTTOM_GAP";

        public const string GripReleaseDistance = "GRIP_RELEASE_DISTANCE";

        public const string GripSpeedRate = "GRIP_SPEED_RATE";

        public const string Ready_X = "READY_X";

        public const string Standby_X = "STANDBY_X";
        public const string Standby_Y = "STANDBY_Y";
        public const string Standby_Z = "STANDBY_Z";
        public const string Standby_W = "STANDBY_W";
        public const string Standby_R = "STANDBY_R";
        public const string Inspection2Use = "INSPECTION_2_USE";
        public const string Inspection1_Visual_Pass = "INSPECTION_1_VISUAL_PASS";

        public const string PickupStandby_Z = "PICKUP_STANDBY_Z";
        public const string PlaceStandby_Z = "PLACE_STANDBY_Z";

        public const string Btm_Standby_X = "BTM_STANDBY_X";

        public const string CameraStandby_Z = "CAMEARA_STANDBY_Z";

        public const string MagLeftGrip_X = "MAGAZINE_LEFT_GRIP_X";
        public const string ReverseCheck_X = "REVERSE_CHECK_X";
        public const string MagRightGrip_X = "MAGAZINE_RIGHT_GRIP_X";
        public const string BuffLeftGrip_X = "BUFFER_LEFT_GRIP_X";
        public const string BuffRightGrip_X = "BUFFER_RIGHT_GRIP_X";
        public const string HandOver_X = "HAND_OVER_X";
        public const string ElevatorGrip_X = "ELEVATOR_GRIP_X";
        public const string ElevatorLeftGrip_X = "ELEVATOR_LEFT_GRIP_X";
        public const string ElevatorRightGrip_X = "ELEVATOR_RIGHT_GRIP_X";

        public const string StopPos = "STOP_POS";

        public const string GripperToSensor = "GRIPPER_TO_SENSOR";

        public const string Attach_1_ValidLensCount = "ATTACH_1_VALID_LENS_COUNT";
        public const string Side_Pusher_Delay_Time = "SIDE_PUSHER_DELAY_TIME";

        public const string Shuttle_Stacker = "SHUTTLE_STACKER";
        public const string Shuttle_Work = "SHUTTLE_WORK";

        public const string Shuttle_Ionizer_1 = "SHUTTLE_IONIZER_1";
        public const string Shuttle_Ionizer_2 = "SHUTTLE_IONIZER_2";
        public const string Shuttle_Ionizer_3 = "SHUTTLE_IONIZER_3";
        public const string Shuttle_Ionizer_4 = "SHUTTLE_IONIZER_4";

        public const string Shuttle_Ionizer_1_Delay = "SHUTTLE_IONIZER_1_DELAY";
        public const string Shuttle_Ionizer_2_Delay = "SHUTTLE_IONIZER_2_DELAY";
        public const string Shuttle_Ionizer_3_Delay = "SHUTTLE_IONIZER_3_DELAY";
        public const string Shuttle_Ionizer_4_Delay = "SHUTTLE_IONIZER_4_DELAY";

        public const string SeperatorUpperPos = "SEPERATOR_UPPER_POS";
        public const string SeperatorLowerPos = "SEPERATOR_LOWER_POS";
        public const string MoverSeperatorUpperPos = "MOVER_SEPERATOR_UPPER_POS";
        public const string StandbyPos = "STANDBY_POS";

        public const string Carrier_1_X = "CARRIER_1_X";
        public const string Carrier_1_Y = "CARRIER_1_Y";
        public const string Carrier_1_Z = "CARRIER_1_Z";
        public const string Carrier_1_W = "CARRIER_1_W";

        public const string Buffer_X = "BUFFER_X";
        public const string Buffer_Y = "BUFFER_Y";
        public const string Buffer_Z = "BUFFER_Z";
        public const string Buffer_W = "BUFFER_W";

        public const string BtmAligner_X = "BTM_ALIGNER_X";
        public const string BtmAligner_Y = "BTM_ALIGNER_Y";
        public const string BtmAligner_Z = "BTM_ALIGNER_Z";
        public const string BtmAligner_R = "BTM_ALIGNER_R";

        public const string PickupCarrier_1_X = "PICK_UP_CARRIER_1_X";
        public const string PickupCarrier_1_Y = "PICK_UP_CARRIER_1_Y";
        public const string PickupCarrier_1_Z = "PICK_UP_CARRIER_1_Z";
        public const string PickupCarrier_1_R = "PICK_UP_CARRIER_1_R";

        public const string PickupCarrier_Z = "PICK_UP_CARRIER_Z";
        public const string PickupCarrier_R = "PICK_UP_CARRIER_R";

        public const string PickupTray_1_Z = "PICK_UP_TRAY_1_Z";
        public const string PickupTray_1_R = "PICK_UP_TRAY_1_R";

        public const string PickupTray_2_Z = "PICK_UP_TRAY_2_Z";
        public const string PickupTray_2_R = "PICK_UP_TRAY_2_R";

        public const string PlaceCarrier_1_X = "PLACE_CARRIER_1_X";
        public const string PlaceCarrier_1_Y = "PLACE_CARRIER_1_Y";

        public const string PlaceCarrier_Z = "PLACE_CARRIER_Z";
        public const string PlaceCarrier_R = "PLACE_CARRIER_R";

        public const string PlaceReject_1_X = "PLACE_REJECT_1_X";
        public const string PlaceReject_1_Y = "PLACE_REJECT_1_Y";

        public const string PlaceReject_Z = "PLACE_REJECT_Z";
        public const string PlaceReject_R = "PLACE_REJECT_R";

        public const string BtmCamera_X = "BTM_CAMERA_X";
        public const string BtmCamera_Y = "BTM_CAMERA_Y";
        public const string BtmCamera_Z = "BTM_CAMERA_Z";
        public const string BtmCamera_R = "BTM_CAMERA_R";

        public const string BtmCamera_1_X = "BTM_CAMERA_1_X";
        public const string BtmCamera_1_Y = "BTM_CAMERA_1_Y";

        public const string Loadcell_X = "LOADCELL_X";
        public const string Loadcell_Y = "LOADCELL_Y";
        public const string Loadcell_Z = "LOADCELL_Z";
        public const string Loadcell_Repeat_Count = "LOADCELL_REPEAT_COUNT";

        public const string AttachShuttle_1_Offset_X = "ATTACH_SHUTTLE_1_OFFSET_X";
        public const string AttachShuttle_1_Offset_Y = "ATTACH_SHUTTLE_1_OFFSET_Y";
        public const string AttachShuttle_1_Z = "ATTACH_SHUTTLE_1_Z";

        public const string AttachShuttle_2_Offset_X = "ATTACH_SHUTTLE_2_OFFSET_X";
        public const string AttachShuttle_2_Offset_Y = "ATTACH_SHUTTLE_2_OFFSET_Y";
        public const string AttachShuttle_2_Z = "ATTACH_SHUTTLE_2_Z";

        public const string CameraLensTray_1_X = "CAMERA_LENS_TRAY_1_X";
        public const string CameraLensTray_1_Y = "CAMERA_LENS_TRAY_1_Y";
        public const string CameraLensTray_1_Z = "CAMERA_LENS_TRAY_1_Z";

        public const string CameraLensTray_2_X = "CAMERA_LENS_TRAY_2_X";
        public const string CameraLensTray_2_Y = "CAMERA_LENS_TRAY_2_Y";
        public const string CameraLensTray_2_Z = "CAMERA_LENS_TRAY_2_Z";

        public const string CameraShuttle_1_X = "CAMERA_SHUTTLE_1_X";
        public const string CameraShuttle_1_Y = "CAMERA_SHUTTLE_1_Y";
        public const string CameraShuttle_1_Z = "CAMERA_SHUTTLE_1_Z";

        public const string CameraShuttle_2_X = "CAMERA_SHUTTLE_2_X";
        public const string CameraShuttle_2_Y = "CAMERA_SHUTTLE_2_Y";
        public const string CameraShuttle_2_Z = "CAMERA_SHUTTLE_2_Z";

        public const string CameraToPicker_1_X = "CAMERA_TO_PICKER_1_X";
        public const string CameraToPicker_1_Y = "CAMERA_TO_PICKER_1_Y";

        public const string CameraToPicker_2_X = "CAMERA_TO_PICKER_2_X";
        public const string CameraToPicker_2_Y = "CAMERA_TO_PICKER_2_Y";

        public const string CameraToPicker_3_X = "CAMERA_TO_PICKER_3_X";
        public const string CameraToPicker_3_Y = "CAMERA_TO_PICKER_3_Y";

        public const string CameraToPicker_4_X = "CAMERA_TO_PICKER_4_X";
        public const string CameraToPicker_4_Y = "CAMERA_TO_PICKER_4_Y";


        public const string TrayFixed_Y = "TRAY_FIXED_Y";

        public const string ShuttleMove = "셔틀 이동";
        public const string ShuttleMoveWarning = "셔틀 경로의 장애물을 제거 하세요";

        public const string SmacServoOffDelay = "SMAC_SERVO_OFF_DELAY";
        public const string SmacDownDistance = "SMAC_DOWN_DISTANCE";
        public const string SmacDownWaitTime = "SMAC_DOWN_WAIT_TIME";

        public const string TopBtmSync_X = "TOP_BTM_SYNC_X";
        public const string TopBtmSync_Y = "TOP_BTM_SYNC_Y";

        public const string NeedleCalPos_X = "NEEDLE_CAL_POS_X";
        public const string NeedleCalPos_Y = "NEEDLE_CAL_POS_Y";
        public const string NeedleCalPos_Z = "NEEDLE_CAL_POS_Z";

        public const string EpoxyInjectionPos_X = "EPOXY_INJECTION_POS_X";
        public const string EpoxyInjectionPos_Y = "EPOXY_INJECTION_POS_Y";
        public const string EpoxyInjectionPos_Z = "EPOXY_INJECTION_POS_Z";

        public const string Clean = "CLEAN";

        public const string Clean_X = "CLEAN_X";
        public const string Clean_Y = "CLEAN_Y";
        public const string Clean_Z = "CLEAN_Z";

        public const string CleanCount = "CLEAN_COUNT";
        public const string MoveCount = "MOVE_COUNT";

        public const string ShotCount = "SHOT_COUNT";
        public const string StartTime = "START_TIME";
        public const string EndTime = "END_TIME";
        public const string Year = "YEAR";
        public const string Month = "MONTH";
        public const string Day = "DAY";

        public const string CleanCountMax = "CLEAN_COUNT_MAX";
        public const string MoveDistance = "MOVE_DISTANCE";
        public const string MoveRowCountMax = "MOVE_ROW_COUNT_MAX";
        public const string MoveColCountMax = "MOVE_COL_COUNT_MAX";

        public const string LeftStart_X = "LEFT_START_X";
        public const string LeftStart_Y = "LEFT_START_Y";
        public const string LeftStart_Z = "LEFT_START_Z";

        public const string LeftStart_AutoCal_X = "LEFT_START_AUTO_CAL_X";

        public const string RightStart_X = "RIGHT_START_X";
        public const string RightStart_Y = "RIGHT_START_Y";
        public const string RightStart_Z = "RIGHT_START_Z";

        public const string RightStart_AutoCal_X = "RIGHT_START_AUTO_CAL_X";

        public const string ScaleStabliztionTime = "SCALE_STABLIZTION_TIME";

        public const string CalToSocket_Z = "CAL_TO_SOCKET_Z";
        public const string NeedleFloat_Z = "NEEDLE_FLOAT_Z";

        public const string CalLimit_XY = "CAL_LIMIT_XY";
        public const string CalLimit_Z = "CAL_LIMIT_Z";
        public const string CalRetry_Count = "CAL_RETRY_COUNT";

        public const string Socket_1_Offset_X = "SOCKET_1_OFFSET_X";
        public const string Socket_1_Offset_Y = "SOCKET_1_OFFSET_Y";
        public const string Socket_1_Offset_Z = "SOCKET_1_OFFSET_Z";

        public const string Socket_2_Offset_X = "SOCKET_2_OFFSET_X";
        public const string Socket_2_Offset_Y = "SOCKET_2_OFFSET_Y";
        public const string Socket_2_Offset_Z = "SOCKET_2_OFFSET_Z";

        public const string Socket_3_Offset_X = "SOCKET_3_OFFSET_X";
        public const string Socket_3_Offset_Y = "SOCKET_3_OFFSET_Y";
        public const string Socket_3_Offset_Z = "SOCKET_3_OFFSET_Z";

        public const string Socket_4_Offset_X = "SOCKET_4_OFFSET_X";
        public const string Socket_4_Offset_Y = "SOCKET_4_OFFSET_Y";
        public const string Socket_4_Offset_Z = "SOCKET_4_OFFSET_Z";

        public const string PickupSkipMaxCount = "PICK_UP_SKIP_MAX_COUNT";

        public const string Compare = "COMPARE";
        public const string Standard = "STANDARD";
        public const string Sorting = "SORTING";

        public const string C1 = "C1";
        public const string C2 = "C2";
        public const string C3 = "C3";
        public const string C4 = "C4";
        public const string C5 = "C5";
        public const string C6 = "C6";
        public const string C7 = "C7";
        public const string C8 = "C8";
        public const string C9 = "C9";
        public const string C10 = "C10";
        public const string C11 = "C11";
        public const string C12 = "C12";
        public const string C13 = "C13";
        public const string C14 = "C14";
        public const string C15 = "C15";
        public const string C16 = "C16";

        public const string JobReady = "JobReady";
        public const string JobStart = "JobStart";
        public const string JobEnd = "JobEnd";
        public const string CurrentRecipe = "Current_Recipe";
        public const string RecipeName = "Recipe_Name";
        public const string EQPStateChange = "EQPStateChange";
        public const string UnitId = "UnitID=1000";
        public const string AlarmSet = "Set";
        public const string AlarmReSet = "Reset";

        public const string Mes_Control_State = "MES CONTROL STATE";
        public const string Mes_Recive_Validation_Type = "VALIDATION_TYPE";
        public const string Mes_Recive_Result = "RESULT";
        public const string Mes_Recive_Reason_Code = "REASON_CODE";
        public const string Mes_Recive_Reason_Text = "REASON_TEXT";
        public const string Mes_Recive_Lot_Id = "LOT_ID";
        public const string Mes_Recive_Recipe_Id = "RECIPE_ID";
        public const string Mes_Recive_Total_Qty = "TOTAL_QTY";
        public const string Mes_Recive_Slot_No = "SLOT_NO";
        public const string Mes_Recive_Material_Id = "MATERIAL_ID";
        public const string Mes_Recive_Part_No = "PART_NO";

        public const string EqpName = "EQP_NAME";
        public const string ContiLot = "CONTI_LOT";

        public const string Mes_Machine_Status_Run = "RUN";
        public const string Mes_Machine_Status_Idle = "IDLE";
        public const string Mes_Machine_Status_Stop = "STOP";
        public const string Mes_Machine_Status_Error = "DOWN";
        public const string Mes_Machine_Status_Error_Reset = "IDLE";

        public const string LoaderMgz   = "LDMGZ";
        public const string UnLoaderMgz = "ULDMGZ";
        public const string RejectTray  = "RejectTray";
        public const string LensTray    = "LensTray";

        public const string LoaderRequest   = "LoaderRequest";
        public const string LoaderComplete  = "LoaderComplete";
        public const string UnLoaderRequest = "UnLoaderRequest";

        public const string Mes_Rcp_Section_General    = "GENERAL";
        public const string Mes_Rcp_Section_Dispensing = "DISPENSING";
        public const string Mes_Rcp_Section_Size       = "SIZE";
        public const string Mes_Rcp_Section_Attach     = "ATTACH";

        //Dispensing
        public const string Mes_Rcp_CircleRadious = "CIRCLE_RADIUS";
        public const string Mes_Rcp_CircleAngle = "CIRCLE_ANGLE";
        public const string Mes_Rcp_CircleAcc = "CIRCLE_ACC";
        public const string Mes_Rcp_CircleVel = "CIRCLE_VEL";
        public const string Mes_Rcp_CircleDec = "CIRCLE_DEC";
        public const string Mes_Rcp_DispenserType = "DISPENSING_TYPE";
        public const string Mes_Rcp_TailAngle = "TAIL_ANGLE";
        public const string Mes_Rcp_Clump1Use = "CLUMP_1_USE";
        public const string Mes_Rcp_Clump1Z = "CLUMP_1_Z";
        public const string Mes_Rcp_Clump2Use = "CLUMP_2_USE";
        public const string Mes_Rcp_Clump2Z = "CLUMP_2_Z";
        public const string Mes_Rcp_Clump3Use = "CLUMP_3_USE";
        public const string Mes_Rcp_Clump3Z = "CLUMP_3_Z";
        public const string Mes_Rcp_Clump4Use = "CLUMP_4_USE";
        public const string Mes_Rcp_Clump4Z = "CLUMP_4_Z";

        //Size
        public const string Mes_Rcp_BobinRadius = "BOBIN_RADIUS";

        //Attach
        public const string Mes_Rcp_UvCureTime         = "UV_CURE_TIME";


    }
}
