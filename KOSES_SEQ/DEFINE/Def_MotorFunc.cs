﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.DEFINE
{
    public partial class Def
    {
        public static readonly int MaxHomeStepCount = 5;
        public static readonly int DefaultStopCount = MaxHomeStepCount;

        #region Rotate
        public enum Rotate
        {
            CW = 0,
            CCW = 1
        }

        public static Rotate GetRotate(string temp)
        {
            Rotate e;
            try
            {
                temp = temp.ToUpper();
                e = (Rotate)Enum.Parse(typeof(Rotate), temp);
                if (Enum.IsDefined(typeof(Rotate), e) == false)
                {
                    e = Rotate.CW;
                }
            }
            catch (ArgumentException)
            {
                e = Rotate.CW;
            }
            return e;
        }
        #endregion
        #region MotorDir
        public enum MotorDir
        {
            MINUS = -1,
            PLUS = 1
        }
        public static MotorDir GetDir(string temp)
        {
            MotorDir e;
            try
            {
                temp = temp.ToUpper();
                e = (MotorDir)Enum.Parse(typeof(MotorDir), temp);
                if (Enum.IsDefined(typeof(MotorDir), e) == false)
                {
                    e = MotorDir.PLUS;
                }
            }
            catch (ArgumentException)
            {
                e = MotorDir.PLUS;
            }
            return e;
        }
        #endregion
        #region Servo
        public enum ServoType
        {
            STEP,
            SERVO,
        }
        public static ServoType GetServoType(string temp)
        {
            ServoType e;
            try
            {
                temp = temp.ToUpper();
                e = (ServoType)Enum.Parse(typeof(ServoType), temp);
                if (Enum.IsDefined(typeof(ServoType), e) == false)
                {
                    e = ServoType.SERVO;
                }
            }
            catch (ArgumentException)
            {
                e = ServoType.SERVO;
            }
            return e;
        }
        #endregion
        #region MotorProfile
        public enum MotorProfile
        {
            SYMMETRIC_TRAPEZOID,
            ASYMMETRIC_TRAPEZOID,
            SYMMETRIC_S_CURVE,
            ASYMMETRIC_S_CURVE,
        }
        public static MotorProfile GetMotorProfile(string temp)
        {
            MotorProfile e;
            try
            {
                temp = temp.ToUpper();
                e = (MotorProfile)Enum.Parse(typeof(MotorProfile), temp);
                if (Enum.IsDefined(typeof(MotorProfile), e) == false)
                {
                    e = MotorProfile.SYMMETRIC_TRAPEZOID;
                }
            }
            catch (ArgumentException)
            {
                e = MotorProfile.SYMMETRIC_TRAPEZOID;
            }
            return e;
        }
        #endregion
        #region SignalLevel
        public enum SignalLevel
        {
            LOW = 0,
            HIGH = 1
        }
        public static SignalLevel GetSignalLevel(string temp)
        {
            SignalLevel e;
            try
            {
                temp = temp.ToUpper();
                e = (SignalLevel)Enum.Parse(typeof(SignalLevel), temp);
                if (Enum.IsDefined(typeof(SignalLevel), e) == false)
                {
                    e = SignalLevel.HIGH;
                }
            }
            catch (ArgumentException)
            {
                e = SignalLevel.HIGH;
            }
            return e;
        }
        #endregion
        #region HomeMethod
        public enum HomeMethod
        {
            MINUS_LIMIT,
            PLUS_LIMIT,
            MINUS_HOME,
            PLUS_HOME,
        }
        public static HomeMethod GetHomeMethod(string temp)
        {
            HomeMethod e;
            try
            {
                temp = temp.ToUpper();
                e = (HomeMethod)Enum.Parse(typeof(HomeMethod), temp);
                if (Enum.IsDefined(typeof(HomeMethod), e) == false)
                {
                    e = HomeMethod.MINUS_LIMIT;
                }
            }
            catch (ArgumentException)
            {
                e = HomeMethod.MINUS_LIMIT;
            }
            return e;
        }
        #endregion
        #region PulseMode
        public enum PulseMode
        {
            //  PAIX 기준
            TWOPULSELOWCW2CCW,
            TWOPULSELOWCCW2CW,
            TWOPULSEHIGHCW2CCW,
            TWOPULSEHIGHCCW2CW,
            ONEPULSELOWCW2CCW,
            ONEPULSELOWCCW2CW,
            ONEPULSEHIGHCW2CCW,
            ONEPULSEHIGHCCW2CW,
        }
        public static PulseMode GetPulseMode(string temp)
        {
            PulseMode e;
            try
            {
                temp = temp.ToUpper();
                e = (PulseMode)Enum.Parse(typeof(PulseMode), temp);
                if (Enum.IsDefined(typeof(PulseMode), e) == false)
                {
                    e = PulseMode.TWOPULSELOWCW2CCW;
                }
            }
            catch (ArgumentException)
            {
                e = PulseMode.TWOPULSELOWCW2CCW;
            }
            return e;
        }
        #endregion
        #region EncoderCount
        public enum EncoderCount
        {
            //  PAIX 기준
            EC_4 = 0,
            EC_2 = 1,
            EC_1 = 2,
        }
        public static EncoderCount GetEncoderCount(string temp)
        {
            EncoderCount e;
            try
            {
                temp = temp.ToUpper();
                e = (EncoderCount)Enum.Parse(typeof(EncoderCount), temp);
                if (Enum.IsDefined(typeof(EncoderCount), e) == false)
                {
                    e = EncoderCount.EC_4;
                }
            }
            catch (ArgumentException)
            {
                e = EncoderCount.EC_4;
            }
            return e;
        }
        #endregion
        #region AccUnit
        public enum AccUnit
        {
            //  AJIN_AXL 기준
            UNIT_SEC2 = 0,
            SEC = 1,
        }
        public static AccUnit GetAccUnit(string temp)
        {
            AccUnit e;
            try
            {
                temp = temp.ToUpper();
                e = (AccUnit)Enum.Parse(typeof(AccUnit), temp);
                if (Enum.IsDefined(typeof(AccUnit), e) == false)
                {
                    e = AccUnit.UNIT_SEC2;
                }
            }
            catch (ArgumentException)
            {
                e = AccUnit.UNIT_SEC2;
            }
            return e;
        }
        #endregion

        public static bool IsRange(double curValue, double destValue, double tolerance)
        {
            double minPos = destValue - tolerance;
            double maxPos = destValue + tolerance;

            if (minPos > curValue)
            {
                return false;
            }
            if (maxPos < curValue)
            {
                return false;
            }

            return true;
        }

        public static int GetVirtaulDelay(int realSpeedRate)
        {
            int virtualDelay;
            if (realSpeedRate <= 0)
            {
                virtualDelay = 1;
            }
            else
            {
                virtualDelay = 1000 / realSpeedRate;
            }
            return virtualDelay;
        }

    }
}
