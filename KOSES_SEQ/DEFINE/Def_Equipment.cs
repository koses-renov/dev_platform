﻿//using DoCSP.TASK_DATA.SKELETON;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.DEFINE
{
    public partial class Def
    {
        public enum SensorType
        {
            A,
            B
        }
        public enum OutputType
        {
            Normal,
            Invert,
        }
        public enum Lock
        {
            Off = 0,
            On
        }
        public enum Grip
        {
            Off = 0,
            On
        }
        public enum Vac
        {
            Off = 0,
            On
        }
        public enum Blow
        {
            Off = 0,
            On
        }
        public enum Suction
        {
            Off = 0,
            On
        }
        public enum Align
        {
            Off = 0,
            On
        }
        public enum LeftRight
        {
            Left,
            Right
        }
        public enum UpDown
        {
            Up,
            Down,
        }
        public enum FwdBwd
        {
            Fwd,
            Bwd,
        }
        public enum OpenClose
        {
            Open,
            Close,
        }
        public enum InOut
        {
            In,
            Out,
        }
        public enum OnOff
        {
            Off = 0,
            On
        }
        public enum Data
        {
            Read,
            Write
        }
        public enum TopBtm
        {
            Top = 0,
            Btm
        }
        public enum EmptyExist
        {
            Empty,
            Exist
        }

        public enum Order
        {
            Desc,
            Asc
        }

        public enum Pickup
        {
            Pause,
            Resume,
        }

        public enum LasResult  // Las Result (Pass/Fail) 210222 shpark
        {
            Pass,
            Fail,
        }

        public enum ModuleType
        {
            Vcm,
            Lens,
            Assy
        }

        public enum RejectCode
        {
            None,

            //  도포 전 상부 비전 형상 검사 결과 불량
            Top_Vcm_Visual,

            //  디스펜싱 동작이 실패한 경우
            Dispensing,

            //  도포 후 디스펜싱 비전 결과 불량
            Epoxy,

            //  렌즈 어태치 동작이 실패한 경우
            Attach,
            
            //  상부 비전 최종 형상 검사 결과 불량
            Top_Final_Visual,

            //  하부 비전 최종 형상 검사 결과 불량
            Btm_Final_Visual,

            //  라인 레이저 2 검사 결과 불량
            LineLaser_2,

        }


        public enum SlotState
        {
            Empty,
            New,
            Used,
            NoSensing,
            Reverse,
        }

        /// <summary>
        /// Program.Stop 에 의해 각 태스크의 사이클 정지 또는 동작중 여부 표시
        /// </summary>
        public enum CycleState
        {
            Stop,
            Run,
        }

        public enum RejectChoiseStep
        {
            NONE,
            NG_FIND,
            POP_UP,
            GO_REJECT,
            GO_GOOD,
        }

        public enum LotEnd
        {
            No,
            Yes
        }

        public enum LotStatus
        {
            None,
            JobReady,
            JobStart,
            JobEnd
        }

        public enum PortId
        {
            None,
            Loader,
            UnLoader,
            RejectTray,
            LensTraySupply1,
            LensTraySupply2,
            LensTrayEmpty
        }

        public enum PortStatus
        {
            None,
            LoaderRequest,
            LoaderComplete,
            UnLoaderRequest
        }



        public const int NotUsed = -1;

        public const int ListBoxItemMax = 1000;

        public const int InvalidPort = -1;

        public const int DefaultCarrierCount = 10;
        public const int DefaultCarrierRowCount = 10;
        public const int DefaultCarrierColCount = 10;

        public const int DefaultTrayRowCount = 10;
        public const int DefaultTrayColCount = 10;

        public const int MagColCount = 1;
        public const int PickerRowCount = 1;

        public const int CirclePointMax = 16;

        public const int DefaultShuttleRow = 1;
        public const int DefaultShuttleRowGap = 0;

        public const int FullSpeed = 100;
        public const int FullTorque = 1000;

        public const int LotSize = 12;


        public enum Editor
        {
            MagLoader,
            VcmCarrier,
            PPLoader,
            Shuttle,
            LensTray,
            Attach,
            PPUnloader,
            AssyCarrier,
            MagUnloader,
            RejectTray
        }

        public enum TwoLayer
        {
            L1,
            L2
        }

        public enum EmoIdx
        {
            E1,
            E2,
            E3,
            E4,
            E5,
            E6,
            E7,
            E8,
        }

        public enum DoorIdx
        {
            Front_1,
            Front_2,
            Front_3,
            Front_4,
            Front_5,
            Right_1,
            Right_2,
            MagUnloader_Front,
            MagUnloader_Side,
            MagUnloader_Rear,
            Rear_1,
            Rear_2,
            Rear_3,
            Rear_4,
            MagLoader_Rear,
            MagLoader_Side,
            MagLoader_Front,
            Left_1,
            Left_2,
        }

        public enum MainAirIdx
        {
            A1,
            A2,
            A3,
            A4,
        }

        public enum RailPart
        {
            Loader,
            Unloader,
        }

        public enum IndexIdx
        {
            N1,
            N2,
            N3,
            N4,
            N5,
            N6
        }

        public enum ShuttleIdx
        {
            S1,
            S2,
        }

        public enum ShuttleSocketIdx
        {
            SS1,
            SS2,
            SS3,
            SS4,
        }

        public enum PickerIdx
        {
            P1,
            P2,
            P3,
            P4,
        }

        public enum IonizerPos
        {
            I1,
            I2,
            I3,
            I4,
        }

        public enum DispenserIdx
        {
            Left = 0,
            Right
        }

        public enum DispensingType
        {
            Circle,
            Free,
        }

        //////////////////////////////////////////////////////////
        // 라인레이저1
        //////////////////////////////////////////////////////////
        public enum Laser1MoveType
        {
            Socket,
            Vcm,
        }

        public enum DispensingVisionType
        {
            //  디스펜서에 붙어 있는 비전
            Dispenser,

            //  인스펙션 1 비전
            Inspection_1
        }

        //  Nomal : 일반 대기 위치
        //  Extra : 픽업과 안착 시 UPH 개선을 위한 대기 위치
        public enum StandbyPos
        {
            Normal,
            PnP
        }

        public enum StandbyZ
        {
            No,
            Yes
        }

        public enum MoveXY
        {
            Sequential,
            SameTime
        }

        public enum CheckSameHeight
        {
            No,
            Yes
        }





        public static DispensingType GetDispensingType(string rotateTypeString, DispensingType defaultValue = DispensingType.Circle)
        {
            DispensingType dt = defaultValue;
            if (Enum.TryParse<Def.DispensingType>(rotateTypeString, out dt))
            {
                return dt;
            }
            else
            {
                return defaultValue;
            }
        }


        public static DispensingVisionType GetDispensingVisionType(string rotateTypeString, DispensingVisionType defaultValue = DispensingVisionType.Dispenser)
        {
            DispensingVisionType dvt = defaultValue;
            if (Enum.TryParse<Def.DispensingVisionType>(rotateTypeString, out dvt))
            {
                return dvt;
            }
            else
            {
                return defaultValue;
            }
        }


        public enum DispensingDir
        {
            CCW = 0,
            CW,        
        }

        public enum LineLaserIdx
        {
            LL1,
            LL2
        }

        public enum LensTraySlot
        {
            Input_1,
            Input_2,
            Output,
        }

        public enum LensTrayShuttleIdx
        {
            S1,
            S2,
        }

        public enum AttachIdx
        {
            A1,
            A2,
            A3,
        }

        public enum WorkingAttachIdx
        {
            A1,
            A2,
            A3,
            NONE
        }

        public enum InspectionIdx
        {
            I1,
            I2
        }


        public enum PickupType
        {
            First,
            Retry,
        }




        public enum LaserPoint
        {
            LP_1,
            LP_2,
            LP_3,
            LP_4
        }



        #region Magazine Position
        public enum MgzPos
        {
            Top,
        }
        #endregion

        public enum LotendVcm  // Conti Lot == Last Vcm
        {
            Yes,
            No,
        }

        public enum SlotDest
        {
            Extract,
            Insert,
            Sensor
        }

        #region LoaderMgz
        public enum LoaderMgzState
        {
            STOP,
            IDLE,
            READY_GIVE,
            READY_RECV
        }

        public enum LoaderMgzStart
        {
            OFF,
            ON
        }
        public enum LoaderMgzStop
        {
            OFF,
            ON
        }
        public enum LoaderMgzGive
        {
            OFF,
            ON
        }
        public enum LoaderMgzRecv
        {
            OFF,
            ON
        }
        #endregion

        #region MagUnloader
        public enum MagUnloaderState
        {
            STOP,
            IDLE,
            READY_GIVE,
            READY_RECV
        }
        public enum MagUnloaderStart
        {
            OFF,
            ON
        }
        public enum MagUnloaderStop
        {
            OFF,
            ON
        }
        public enum MagUnloaderGive
        {
            OFF,
            ON
        }
        public enum MagUnloaderRecv
        {
            OFF,
            ON
        }
        #endregion



        #region ElevaterLoader
        public enum ElevatorLoaderState
        {
            CARRIER_EMPTY,
            WORKING,
            WORKING_COMPLETED
        }
        #endregion

        #region ElevaterUnloader
        public enum ElevatorUnloaderState
        {
            WORKING,
            WORKING_COMPLETED
        }
        #endregion

        #region ElevaterUnloaderTop
        public enum ElevatorUnloaderTopState
        {
            CARRIER_EMPTY,
            CARRIER_EXIST
        }
        #endregion


        #region PPLoaderCamera
        public enum PPLoaderCameraState
        {
            IDLE,
            BUFFER,
            WORKING
        }
        #endregion

        #region Pre PP Pos 
        public enum PPLoaderState
        {
            IDLE,
            BUFFER,
        }
        #endregion


        #region PPUnloaderCamera
        public enum PPUnloaderCameraState
        {
            IDLE,
            WORKING,
        }
        #endregion





        #region Index1
        public enum Index_1_State
        {
            CARRIER_EMPTY,
            CARRIER_EXIST_LEFTT,
            CARRIER_EXIST_RIGHTT
        }
        #endregion

        #region Index2
        public enum Index_2_State
        {
            CARRIER_EMPTY,
            WORKING_READY,
            WORKING_COMPLETED
        }
        #endregion

        #region Index3
        public enum Index_3_State
        {
            CARRIER_EMPTY,
            CARRIER_EXIST
        }
        #endregion

        #region Index4
        public enum Index_4_State
        {
            CARRIER_EMPTY,
            CARRIER_L_EXIST,
            CARRIER_R_EXIST
        }
        #endregion

        #region Index5
        public enum Index_5_State
        {
            CARRIER_EMPTY,
            WORKING,
            WORKING_READY,
            WORKING_COMPLETED
        }
        #endregion

        #region Index Speed
        public enum IndexSpeed
        { 
            Full,
            Slow,
        }
        #endregion



        #region LensTrayStacker
        public enum LensTrayStackerState
        {
            IDLE,
            WORKING,
        }
        #endregion

        #region LensTrayShuttle
        public enum LensTrayShuttleState
        {
            INIT,
            REQ_INPUT,
            REQ_DISCHARGE,
            NEW_TRAY,
            GO_WORK,
            WAITING_INPUT,
            WAITING_WORK,
            WAITING_USER_INPUT,
            WORKING_READY,
            WORKING_COMPLETED,
        }
        #endregion

        #region 셔틀 목적지
        public enum ShuttleDest
        {
            Standby,
            PPLoader,
            Mask,
            InspectionBeforeDispensing,
            LineLaser_1,
            Dispensing,
            InspectionAfterDispensing,
            Attach,
            InspectionFinal,
            LineLaser_2,
            PPUnloader
        }
        #endregion

        #region 셔틀 상태
        public enum ShuttleState
        {
            PP_LOADER_GO,
            PP_LOADER_READY,

            PUT_MASK_GO,

            INSPECTION_BEFORE_DISPENSING_GO,
            INSPECTION_BEFORE_DISPENSING_READY,

            LINE_LASER_1_GO,
            LINE_LASER_1_READY,

            DISPENSING_GO,
            DISPENSING_READY,

            INSPECTION_AFTER_DISPENSING_GO,
            INSPECTION_AFTER_DISPENSING_READY,

            ATTACH_GO,
            ATTACH_READY,

            INSPECTION_FINAL_GO,
            INSPECTION_FINAL_READY,

            REMOVE_MASK_GO,
            REMOVE_MASK_READY,

            PP_UNLOADER_GO,
            PP_UNLOADER_READY,
        }
        #endregion

        #region 레이저 측정 포인트
        public enum LaserMeasurePoint
        {
            First,
            Second,
            Third,
            Fourth,
            //Fifth,
            //Sixth,
        }
        #endregion



        #region GUI Color
        public static Color GetMagSlotColor(Def.SlotState ss)
        {
            switch (ss)
            {
                case Def.SlotState.New:
                    return DefColor.New;

                case Def.SlotState.Used:
                    return DefColor.Used;

                case Def.SlotState.NoSensing:
                    return DefColor.NoSensing;

                case Def.SlotState.Reverse:
                    return DefColor.Reverse;

                default:
                    return DefColor.Empty;
            }
        }
        //public static Color GetModuleColor(ModuleData module)
        //{
        //    if (module.Exist == false)
        //    {
        //        return DefColor.Empty;
        //    }

        //    switch (module.type)
        //    {
        //        case ModuleType.Vcm:
        //            if (module.Reject != RejectCode.None)
        //            {
        //                return DefColor.Reject;
        //            }
        //            else if (module.vcm.topAlign.valid)
        //            {
        //                return DefColor.Aligned;
        //            }
        //            else
        //            {
        //                return DefColor.New;
        //            }

        //        case ModuleType.Lens:
        //            if (module.lens.topAlign.valid)
        //            {
        //                return DefColor.Aligned;
        //            }
        //            else
        //            {
        //                return DefColor.New;
        //            }

        //        case ModuleType.Assy:
        //            if (module.Reject == RejectCode.None)
        //            {
        //                return DefColor.Pass;
        //            }
        //            else
        //            {
        //                return DefColor.Reject;
        //            }

        //        default:
        //            return DefColor.Black;
        //    }
        //}
        //public static Color GetModuleColor(ModuleData module, bool pocketAligned)
        //{
        //    if (module.Exist == false && pocketAligned == true)
        //    {
        //        return DefColor.PocketAligned;
        //    }
        //    return GetModuleColor(module);
        //}

        #endregion

        public enum DispensingGroup
        {
            Group_1_3,
            Group_2_4
        }

        public enum OperationMode
        {
            Manual,
            Auto
        }

        //  PP 로더가 vcm 캐리어에서 픽업할 때 건너뛰는 칸 수
        public const int PPLoaderPickupGap = 3;

        //  PP 언로더가 assy 캐리어에서 안착할 때 건너뛰는 칸 수
        public const int PPUnloaderPlaceGap = 2;


        public static Def.ShuttleSocketIdx PickerIdxToShuttleSockeIdx(Def.PickerIdx pi)
        {
            Def.ShuttleSocketIdx ssi = Def.ShuttleSocketIdx.SS1;
            switch (pi)
            {
                case Def.PickerIdx.P1:
                    ssi = Def.ShuttleSocketIdx.SS1;
                    break;
                case Def.PickerIdx.P2:
                    ssi = Def.ShuttleSocketIdx.SS2;
                    break;
                case Def.PickerIdx.P3:
                    ssi = Def.ShuttleSocketIdx.SS3;
                    break;
                case Def.PickerIdx.P4:
                    ssi = Def.ShuttleSocketIdx.SS4;
                    break;
            }
            return ssi;
        }
       
        /// <summary>
        /// 셔틀을 넣으면 해당 어태치 반환
        /// </summary>
        /// <param name="si"></param>
        /// <returns></returns>
        public static Def.AttachIdx GetAttachIdx(Def.ShuttleIdx si)
        {
            Def.AttachIdx ai = AttachIdx.A1;

            switch (si)
            {
                case ShuttleIdx.S1:
                case ShuttleIdx.S2:
                    ai = AttachIdx.A1;
                    break;

                //case ShuttleIdx.S3:
                //case ShuttleIdx.S4:
                //    ai = AttachIdx.A2;
                //    break;

                //case ShuttleIdx.S5:
                //case ShuttleIdx.S6:
                //    ai = AttachIdx.A3;
                //    break;
            }

            return ai;
        }

        public static bool GetOppoShuttle(Def.ShuttleIdx si, ref Def.ShuttleIdx osi)
        {

            switch (si)
            {
                case ShuttleIdx.S1:
                    osi = ShuttleIdx.S2;
                    break;
                case ShuttleIdx.S2:
                    osi = ShuttleIdx.S1;
                    break;
                //case ShuttleIdx.S3:
                //    osi = ShuttleIdx.S4;
                //    break;
                //case ShuttleIdx.S4:
                //    osi = ShuttleIdx.S3;
                //    break;
                //case ShuttleIdx.S5:
                //    osi = ShuttleIdx.S6;
                //    break;
                //case ShuttleIdx.S6:
                //    osi = ShuttleIdx.S5;
                //    break;

                default:
                    return false;
            }

            return true;
        }

        /// <summary>
        /// 원에 내각한 정사각형 한 변의 길이를 반환
        /// </summary>
        /// <param name="radius">반지름</param>
        /// <returns>정사각형 한 변의 길이</returns>
        public static double GetSquareLength_InCircle(double radius)
        {
            double length;

            //  반지름 * √2
            length = radius * Math.Sqrt(2);
            return length;
        }
    }
}
