﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.OP;
using KOSES_SEQ.TEACHING;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.MANAGER
{
    public class TeachingManager
    {
        static TeachingManager _Instance;
        // Folder
        //string GroupName;
        // File
        //string RecipeName;
        public Teaching TeachingData = new Teaching();

        static object singleton = new object();

        public static TeachingManager Instance
        {
            get
            {
                lock (singleton)
                {
                    if (_Instance == null)
                    {
                        _Instance = new TeachingManager();
                    }

                    return _Instance;
                }
            }
        }

        private TeachingManager()
        {

        }

        public bool Save()
        {
            try
            {
                string[] strPaths = { DefPath.Recipe, SysOperator.Instance.defaultInfo.RecipeGroupName, $"{SysOperator.Instance.defaultInfo.RecipeName}.xml" };
                string strPath = Path.Combine(strPaths);
                using (FileStream fileStream = File.Open(strPath, FileMode.Create))
                {
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(TeachingData.GetType());
                    x.Serialize(fileStream, TeachingData);
                    fileStream.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return false;
            }
        }

        public bool Load()
        {
            try
            {
                string[] strPaths = { DefPath.Recipe, SysOperator.Instance.defaultInfo.RecipeGroupName, $"{SysOperator.Instance.defaultInfo.RecipeName}.xml" };
                string strPath = Path.Combine(strPaths);

                #region Directory 없을 때 생성
                if (!Directory.Exists(DefPath.Recipe))
                {
                    Directory.CreateDirectory(DefPath.Recipe);
                }

                if (!Directory.Exists(DefPath.Recipe + @"\" + SysOperator.Instance.defaultInfo.RecipeGroupName))
                {
                    Directory.CreateDirectory(DefPath.Recipe + @"\" + SysOperator.Instance.defaultInfo.RecipeGroupName);
                }

                #endregion

                // 레시피 없을 때 파일 신규 생성
                if (!File.Exists(strPath))
                {
                    Save();
                }

                using (FileStream fileStream = new FileStream(strPath, FileMode.Open, FileAccess.Read))
                {
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(TeachingData.GetType());
                    TeachingData = (Teaching)x.Deserialize(fileStream);
                    fileStream.Close();
                }

                TeachingData.ResizeRecipe();

                return true;
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
                return false;
            }
        }
    }
}
