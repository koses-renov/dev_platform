﻿using KOSES_SEQ.LOG;
using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace KOSES_SEQ.MANAGER
{
    public enum enumLogLevel
    {
        Run,
        Error,
        Event,
        COMM,
        Info,
        Exception,
        UI,
        Transfer,
    }

    /// <summary>
    /// 로그 관리 객체
    /// </summary>
    public static class LogManager
    {
        public delegate void deleAddNewLog(CLog oLog);

        public static event deleAddNewLog OnAddNewLog;

        public static Dictionary<enumLogLevel, CLogger> _dcLogger = new Dictionary<enumLogLevel, CLogger>();

        public static CLogger_IO _LoggerVariable = null;

        public static int LogExpirationDays = 30;

        public static void CreateIOLogger(int iKeepDays)
        {
            _LoggerVariable = new CLogger_IO(iKeepDays, DefPath.Log + "VARIABLE" + @"\");
        }

        public static void CreateLogger(int iKeepDays)
        {
            CreateDirectory();

            _dcLogger.Add(enumLogLevel.Info, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.Info.ToString().ToUpper() + @"\"));
            _dcLogger.Add(enumLogLevel.Run, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.Run.ToString().ToUpper() + @"\"));
            _dcLogger.Add(enumLogLevel.Error, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.Error.ToString().ToUpper() + @"\"));
            _dcLogger.Add(enumLogLevel.Event, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.Event.ToString().ToUpper() + @"\"));
            _dcLogger.Add(enumLogLevel.COMM, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.COMM.ToString().ToUpper() + @"\"));
            _dcLogger.Add(enumLogLevel.Exception, new CLogger(iKeepDays, DefPath.Log));

            _dcLogger.Add(enumLogLevel.UI, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.UI.ToString().ToUpper() + @"\"));
            _dcLogger.Add(enumLogLevel.Transfer, new CLogger(iKeepDays, DefPath.Log + enumLogLevel.Transfer.ToString().ToUpper() + @"\"));

            LogExpirationDays = iKeepDays;
        }

        /// <summary>
        /// Input Output 신호 Log
        /// IO 관련 객체에서만 사용할 것
        /// </summary>
        /// <param name="strMsg">Log에 기록되는 Message</param>
        /// <param name="strCategory">IO Log 폴더</param>
        public static void InOutLog(string strMsg, string strCategory)
        {
            try
            {
                if (_LoggerVariable != null)
                {
                    _LoggerVariable.LogWrite(strMsg, strCategory);
                }
            }
            catch (Exception ex)
            {
                ExLog(ex);
            }
        }

        /// <summary>
        /// 일반적인 Log 
        /// </summary>
        /// <param name="eLevel">Log의 Level</param>
        /// <param name="strMsg">Log에 기록되는 Message</param>
        /// <param name="strCategory">Log 폴더 내의 폴더 자동 생성 및 파일이름에 쓰임</param>
        public static void Log(enumLogLevel eLevel, string strMsg, string strCategory)
        {
            try
            {
                CLog oLog = new CLog(eLevel, strMsg, strCategory);

                if (_dcLogger.TryGetValue(eLevel, out CLogger oLogger))
                {
                    oLogger.LogWrite(strMsg, strCategory);
                }

                if (oLog.Level < enumLogLevel.Exception)
                    RaiseOnAddNewLog(oLog);
                
            }
            catch (Exception ex)
            {
                ExLog(ex);
            }
        }


        /// <summary>
        /// Exception Log
        /// </summary>
        /// <param name="exLog"></param>
        public static void ExLog(Exception exLog)
        {
            try
            {
                CLogger oLogger;
                if (_dcLogger.TryGetValue(enumLogLevel.Exception, out oLogger))
                {
                    oLogger.LogWrite(exLog.ToString(), enumLogLevel.Exception.ToString().ToUpper());
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        /// <summary>
        /// Log 기본 Directory 생성
        /// </summary>
        private static void CreateDirectory()
        {
            if (!Directory.Exists(DefPath.Root)) { Directory.CreateDirectory(DefPath.Root); }

            if (!Directory.Exists(DefPath.Log)) { Directory.CreateDirectory(DefPath.Log); }
        }

        /// <summary>
        /// Log 추가됨을 Event로 띄움
        /// </summary>
        /// <param name="oLog"></param>
        private static void RaiseOnAddNewLog(CLog oLog)
        {
            Action action = new Action(delegate ()
            {
                if (OnAddNewLog != null)
                    OnAddNewLog(oLog);
            });
            action.BeginInvoke(null, null);
        }
    }

    public class CReturnCode
    {
        public const int Success = 0;    // 작업 정상 완료
        public const int AlreadyDone = 1;    // 이미 완료 됨
        public const int Busy = 11;   // 작업 진행 중

        public const int Fail = 101;  // 작업 실패
        public const int TimeOut = 102;  // 작업 시간 초과
        public const int Aborted = 103;  // 작업 거부 됨

        public const int Exception = 1001; // 예외 상황 발생
    }

    public class CTimeOut
    {
        /// <summary>
        /// Time Out Check 함수
        /// 일정 시간 경과 기다리는 용도로도 사용
        /// </summary>
        /// <param name="oVariable">Check할 객체</param>
        /// <param name="oTargetValue">목표 값</param>
        /// <param name="iInterval">어떤 주기로 체크할 건지(단위:msec)</param>
        /// <param name="iTimeoutSecond">Time Out Check 경과시간(단위:msec)</param>
        /// <returns></returns>
        public static bool IsTimeoutObj(object oVariable, object oTargetValue, int iInterval, int iTimeoutSecond)
        {
            bool bRet = true;
            try
            {
                if (oVariable.Equals(oTargetValue)) return false;

                DateTime? dateStartTime = DateTime.Now;


                int iTickCount = Environment.TickCount;
                while (true)
                {
                    if (oVariable.Equals(oTargetValue))
                    {
                        bRet = false;
                        break;
                    }

                    if (dateStartTime.Value.AddMilliseconds(iTimeoutSecond) < DateTime.Now)
                    {
                        bRet = true;
                        break;
                    }

                    Thread.Sleep(iInterval);
                }
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
            return bRet;
        }

        /// <summary>
        /// 기준 시간 대비하여 TimeOut인지 체크
        /// </summary>
        /// <param name="iTick">시작 Tick</param>
        /// <param name="iTimeoutMilSecond">TimeOut 설정 시간(단위 : mSec)</param>
        /// <returns></returns>
        public static bool IsTimeOut(int iTick, int iTimeoutMilSecond)
        {
            bool bRet = true;
            try
            {
                int iCurrTickCount = Environment.TickCount;

                if (iCurrTickCount > iTick + iTimeoutMilSecond)
                    bRet = true;
                else
                    bRet = false;
            }
            catch (Exception ex)
            {
                LogManager.ExLog(ex);
            }
            return bRet;
        }
    }
}
