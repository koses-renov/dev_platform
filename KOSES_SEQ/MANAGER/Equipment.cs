﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.MANAGER
{
    public class EquipmentStatus
    {
        
        public enum State
        {
            UNKNOWN = 0,
            MOTION_CONNECT_FAIL = 1,
            NEED_INITIALIZATION = 2,
            NOW_INITIALIZING = 3,
            READY = 4,
            EXECUTING = 5,
            ERROR = 6,
            ERROR_RESET = 7,
            STOP = 8,
            JOB_COMPLETED = 9
        }

        public EquipmentStatus()
        {
            //state = State.UNKNOWN;
        }

        State state = State.UNKNOWN;
        #region State
        object stateLock = new object();
        public State States
        {
            get
            {
                State ret;
                lock (stateLock)
                {
                    ret = state;
                }
                return state;
            }
            set
            {
                lock (stateLock)
                {
                    state = value;
                }
            }
        }
        #endregion
    }
}
