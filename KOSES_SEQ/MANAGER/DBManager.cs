﻿using KOSES_SEQ.ALARM;
using KOSES_SEQ.DEFINE;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace KOSES_SEQ.MANAGER
{
    public abstract class DBTable
    {
        protected SQLiteConnection DbConnection
        {
            get { return DBManager.Instance.DbConnection; }
        }

        public abstract string TableName { get; }

        public abstract string MakeTable();
    }

    /// <summary>
    /// DB 관리자
    /// </summary>
    public class DBManager
    {
        static DBManager _Instance;
        readonly string DbPathAlarm = DefPath.Data + @"\KosesEqpDB.db";

        // connection
        SQLiteConnection dbConnection;
        public SQLiteConnection DbConnection { get => dbConnection; set => dbConnection = value; }


        static readonly object singleTon = new object();
        public static DBManager Instance
        {
            get
            {
                lock (singleTon)
                {
                    if (_Instance == null)
                        _Instance = new DBManager();

                    return _Instance;
                }
            }
        }

        #region Table

        public AlarmInfo AlarmInfoDB { get; set; }
        public AlarmLog AlarmLogDB { get; set; }

        #endregion

        private DBManager()
        {
            dbConnection = new SQLiteConnection($"Data Source={DbPathAlarm};Version=3;");

            AlarmInfoDB = new AlarmInfo();
            AlarmLogDB = new AlarmLog();
        }

        ~DBManager()
        {
        }

        /// <summary>
        /// Open 상태인지
        /// </summary>
        /// <returns></returns>
        public bool IsOpen()
        {
            return DbConnection.State == ConnectionState.Open;
        }

        /// <summary>
        /// DB 연결 Open
        /// </summary>
        public void Open()
        {
            if (!File.Exists(DbPathAlarm))
            {
                SQLiteConnection.CreateFile(DbPathAlarm);
            }

            if (IsOpen())
                return;

            DbConnection.Open();
        }
    }
}
