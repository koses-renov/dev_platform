﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.MANAGER
{
    public enum enumLogInLevel
    {
        Unknown,
        Level_OP,   // Operator
        Level_ENG,  // Engineer
        Level_ADMIN,// Administrator
    }


    [Serializable]
    [StructLayout(LayoutKind.Auto, Pack = 1)]
    public class CUser
    {
        public string UserID;
        public string PassWord;
        public enumLogInLevel LogInLevel;
    }

    /// <summary>
    /// 사용자 관리 객체
    /// </summary>
    public class UserManager
    {
        Dictionary<string, CUser> _UserList = new Dictionary<string, CUser>();

        protected static UserManager _Instance;

        public Dictionary<string, CUser> UserList
        {
            get { return _UserList; }
        }

        protected UserManager()
        {

        }

        static object _LockInstance = new object();
        public static UserManager Instance
        {
            get
            {
                lock (_LockInstance)
                {
                    if (_Instance == null)
                        _Instance = new UserManager();
                }

                return _Instance;
            }
        }

        public CUser GetUserInfoByID(string userID)
        {
            if (_UserList.ContainsKey(userID))
            {
                return _UserList[userID];
            }

            return null;
        }

        public List<CUser> GetUserInfosByLevel(enumLogInLevel level)
        {
            var list = _UserList.Where(u => u.Value.LogInLevel == level).Select(u => u.Value).OrderBy(u => u.UserID);

            return list.ToList();
        }

        public int AddUser(CUser user)
        {
            lock (_UserList)
            {
                if (_UserList.ContainsKey(user.UserID))
                {
                    _UserList[user.UserID] = user;
                }
                else
                {
                    _UserList.Add(user.UserID, user);
                }
            }

            return CReturnCode.Success;
        }

        public int DeleteUser(string userID)
        {
            lock (_UserList)
            {
                if (_UserList.ContainsKey(userID))
                    _UserList.Remove(userID);
            }

            return CReturnCode.Success;
        }

        public void WriteLoginUserList()
        {
            lock (_UserList)
            {
                try
                {
                    using (var f_fileStream = new FileStream(DefPath.UserLogInInfo, FileMode.Create, FileAccess.Write))
                    {
                        var f_binaryFormatter = new BinaryFormatter();
                        f_binaryFormatter.Serialize(f_fileStream, _UserList);
                        f_fileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ExLog(ex);
                }
            }
        }

        public void ReadLoginUserList()
        {
            lock (_UserList)
            {
                try
                {
                    _UserList.Clear();
                    if (File.Exists(DefPath.UserLogInInfo))
                    {
                        using (var f_fileStream = File.OpenRead(DefPath.UserLogInInfo))
                        {
                            var f_binaryFormatter = new BinaryFormatter();
                            _UserList = (Dictionary<string, CUser>)f_binaryFormatter.Deserialize(f_fileStream);
                            f_fileStream.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ExLog(ex);
                }
            }
        }

        public enumLogInLevel ValidLogIn(enumLogInLevel level, string userID, string passWord, out string failString)
        {
            CUser user = UserManager.Instance.GetUserInfosByLevel(level).FirstOrDefault(u => u.UserID == userID);
            //if (UserManager.Instance.UserList.ContainsKey(userID))
            //{
            //    user = UserManager.Instance.UserList[userID];
            //}

            failString = string.Empty;

            switch (level)
            {
                case enumLogInLevel.Level_OP:
                case enumLogInLevel.Level_ENG:
                    if (user == null || string.IsNullOrEmpty(userID))
                    {
                        failString = "Please, Check User ID";
                        return enumLogInLevel.Unknown;
                    }
                    if (user.PassWord != passWord)
                    {
                        failString = "Password mismatch!";
                        return enumLogInLevel.Unknown;
                    }
                    break;
                case enumLogInLevel.Level_ADMIN:
#if DEBUG
                    passWord = "koses!";
#endif
                    if (!passWord.Equals("koses!") && !passWord.Equals("1"))
                    {
                        failString = "Password mismatch!";
                        return enumLogInLevel.Unknown;
                    }
                    break;
                case enumLogInLevel.Unknown:
                    break;
            }

            return level;
        }
    }
}
