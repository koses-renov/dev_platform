﻿using KOSES_SEQ.ALARM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.MANAGER
{
    public delegate void deleAlarmOccured(CAlarm alarm, bool isReset = false);
    public class AlarmManager
    {
        #region Instance
        private static AlarmManager _Instance;

        private static object lockObj = new object();
        public static AlarmManager Instance
        {
            get
            {
                lock (lockObj)
                {
                    if (_Instance == null)
                        _Instance = new AlarmManager();
                }

                return _Instance;
            }
        }
        #endregion

        /// <summary>
        /// 알람 발생 이벤트
        /// </summary>
        public event deleAlarmOccured OnAlarmOccured;

        /// <summary>
        /// 현재 발생되어 있는 알람들
        /// </summary>
        public Dictionary<int, CAlarm> CurrentAlarmList = new Dictionary<int, CAlarm>();

        protected AlarmManager()
        {
        }

        protected void RaiseOnAlarmOccured(CAlarm alarm, bool isReset = false)
        {
            Action action = new Action(delegate ()
            {
                if (OnAlarmOccured != null)
                    OnAlarmOccured(alarm, isReset);
            });

            action.BeginInvoke(null, null);
        }

        /// <summary>
        /// 알람 발생
        /// </summary>
        /// <param name="code"></param>
        public void SetError(int code)
        {
            CAlarm oAlarm = DBManager.Instance.AlarmInfoDB.GetAlarm(code);

            lock (CurrentAlarmList)
            {
                if (oAlarm != null && !CurrentAlarmList.ContainsKey(code))
                {
                    CurrentAlarmList.Add(code, oAlarm);
                }
            }
            DBManager.Instance.AlarmLogDB.AddAlarmLog(oAlarm);

            RaiseOnAlarmOccured(oAlarm);
        }

        /// <summary>
        /// 알람 발생 (알람 로그에 추가된 string을 message에 삽입)
        /// </summary>
        /// <param name="code"></param>
        /// <param name="attachMsg"></param>
        public void SetError(int code, string appendMsg)
        {
            CAlarm oAlarm = DBManager.Instance.AlarmInfoDB.GetAlarm(code).Clone() as CAlarm;

            oAlarm.Message += appendMsg;

            lock (CurrentAlarmList)
            {
                if (oAlarm != null && !CurrentAlarmList.ContainsKey(code))
                {
                    CurrentAlarmList.Add(code, oAlarm);
                }
            }
            DBManager.Instance.AlarmLogDB.AddAlarmLog(oAlarm);

            RaiseOnAlarmOccured(oAlarm);
        }

        /// <summary>
        /// 해당 알람 코드 클리어
        /// </summary>
        /// <param name="code"></param>
        public void ResetError(int code)
        {
            lock (CurrentAlarmList)
            {
                if (CurrentAlarmList.ContainsKey(code))
                {
                    RaiseOnAlarmOccured(CurrentAlarmList[code], true);
                    CurrentAlarmList.Remove(code);
                }
            }
        }

        /// <summary>
        /// 모든 알람 리스트 클리어
        /// </summary>
        public void ResetAllErrors()
        {
            lock (CurrentAlarmList)
            {
                foreach (CAlarm alarm in CurrentAlarmList.Values)
                {
                    RaiseOnAlarmOccured(alarm, true);
                }

                CurrentAlarmList.Clear();
            }
        }
    }
}
