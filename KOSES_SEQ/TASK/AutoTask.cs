﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.TASK;
//using DoCS.UTILITY;
using KOSES_SEQ.TASK_AUTO.LoaderMgz;
using KOSES_SEQ.TASK_DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static KOSES_SEQ.TASK.KoTask;
using static KOSES_SEQ.OP.SysOperator;
using KOSES_SEQ.MANAGER;

namespace KOSES_SEQ.TASK_AUTO
{
    public class AutoTask
    {
        public AutoTask()
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    AutoTask_Attach a = new AutoTask_Attach(ai);
            //    attach.Add(a);
            //    a = null;
            //}
         
            LoaderMgz = new AutoTask_LoaderMgz();
        }

        //public List<AutoTask_Attach> attach = new List<AutoTask_Attach>();
        //#region Attach
        //public AutoTask_Attach Attach1
        //{
        //    get
        //    {
        //        return attach[(int)Def.AttachIdx.A1];
        //    }
        //}
        //public AutoTask_Attach Attach2
        //{
        //    get
        //    {
        //        return attach[(int)Def.AttachIdx.A2];
        //    }
        //}
        //public AutoTask_Attach Attach3
        //{
        //    get
        //    {
        //        return attach[(int)Def.AttachIdx.A3];
        //    }
        //}
       // #endregion
        public AutoTask_LoaderMgz LoaderMgz;

        /// <summary>
        /// 안전 센서 감지는 별도의 테스크에서 수행(SafetySensorTask)
        /// 오토 모드 동작중 안전 센서가 감지되면 아래 플래그가 true 로 설정된다.
        /// </summary>
        #region 안전 센서 감지
        public bool SafetySensorDetected = false;
        #endregion


        /// <summary>
        /// 오토 모드 테스크 동작 여부 반환
        /// 오토 모드 테스크 중 한 개라도 동작중이면 true 반환
        /// </summary>
        public bool IsLive
        {
            get
            {
                //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
                //{
                //    if (attach[(int)ai].Task.IsLive)
                //    {
                //        return true;
                //    }
                //}

                if (LoaderMgz.Task.IsLive)
                {
                    return true;
                }
            
                return false;
            }
        }



        public void Init()
        {
            //Program.NoticeStop();
            equipmentstatus.States = EquipmentStatus.State.READY;
        }

        #region InitStep
        public bool InitStep()
        {
            if (IsLive)
            {
                return false;
            }

            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].InitStep();
            //}

           

            LoaderMgz.InitStep();
           

            return true;
        }
        #endregion
        #region InitStep_LotEnd
        public bool InitStep_LotEnd()
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].InitStep();
            //}

         
            //lens.stacker.InitStep();
            //lens.Shuttle_1.InitStep();
            //lens.Shuttle_2.InitStep();
           
            LoaderMgz.InitStep();
           

            //foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
            //{
            //    shuttle[(int)si].InitStep();
            //}

            return true;
        }
        #endregion

        public KoTask.Code Start()
        {
            if (IsLive)
            {
                return KoTask.Code.TASK_IS_WORKING;
            }


            // !! StartTask
            ResetUserStop();

            
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].StartTask();
            //}

            //attach[0].StartTask();
            //attach[1].StartTask();
            //attach[2].StartTask();

            
            LoaderMgz.StartTask();

            //foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
            //{
            //    shuttle[(int)si].StartTask();
            //}

            return KoTask.Code.OK;
        }

        public void Stop(int waitTime = 0)
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].Task.Stop();
            //}

           

            LoaderMgz.Task.Stop();
         
            Thread.Sleep(waitTime);
        }

        public void Finish(int waitTime = 0)
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].Task.Finish();
            //}

           
            LoaderMgz.Task.Finish();
           
            Thread.Sleep(waitTime);
        }

        public void ErrorMotion()
        {
        }
        
        public void Error(Def.Code errorCode, string errorTask, string errorStep)
        {
            //Program.SetEquipState(Equipment.State.ERROR);
            //Program.equipTime.ErrorTimeStart();
            //Program.motion.frame.buzzer.Set(true);

            ErrorMotion();

            //Program.baseForm.lastErrorForm.DoShow(errorCode.ToString(), errorTask, errorStep, DefPath.ErrorMessage);
            //Program.log.program.Add(errorCode.ToString(), string.Format($"{errorTask} : {errorStep}"), LibDef.LogType.Error);
            //Program.log.Error(string.Format($"{errorTask} {errorStep} {errorCode}"));
        }       

        public bool CheckEquipState()
        {
            //Equipment.State es;
            //es = Program.GetEquipState();

            //try
            //{
            //    switch (es)
            //    {
            //        case Equipment.State.EXECUTING:
            //            break;

            //        default:
            //            return false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    return false;
            //}

            return true;
        }

        public Def.Code PickerAllStanby_Z(Def.ShuttleIdx si)
        {
            Def.Code code;

            ////  pp 로더
            //code = ppLoader.Standby_All_Z();
            //if (code != Def.Code.OK)
            //{
            //    return code;
            //}

            ////  pp 언로더
            //code = ppUnloader.Standby_All_Z();
            //if (code != Def.Code.OK)
            //{
            //    return code;
            //}

            ////  어태치
            //switch (si)
            //{
            //    case Def.ShuttleIdx.S1:
            //    case Def.ShuttleIdx.S2:
            //        code = attach[(int)Def.AttachIdx.A1].Standby_All_Z();
            //        if (code != Def.Code.OK)
            //        {
            //            return code;
            //        }
            //        break;
             
            //    case Def.ShuttleIdx.S3:
            //    case Def.ShuttleIdx.S4:
            //        code = attach[(int)Def.AttachIdx.A2].Standby_All_Z();
            //        if (code != Def.Code.OK)
            //        {
            //            return code;
            //        }
            //        break;
                
            //    case Def.ShuttleIdx.S5:
            //    case Def.ShuttleIdx.S6:
            //        code = attach[(int)Def.AttachIdx.A3].Standby_All_Z();
            //        if (code != Def.Code.OK)
            //        {
            //            return code;
            //        }
            //        break;
                
            //    default:
            //        foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //        {
            //            code = attach[(int)ai].Standby_All_Z();
            //            if (code != Def.Code.OK)
            //            {
            //                return code;
            //            }
            //        }
            //        break;
            //}

            ////  디스펜서
            //foreach (Def.DispenserIdx di in Enum.GetValues(typeof(Def.DispenserIdx)))
            //{
            //    code = dispensing.dispensingAction[(int)di].Move_StandbyZ();
            //    if (code != Def.Code.OK)
            //    {
            //        return code;
            //    }
            //}

            ////  마스크
            //code = Program.motion.mask[(int)si].upDown.SetConfirm(Def.UpDown.Up);
            //if (code != Def.Code.OK)
            //{
            //    return code;
            //}

            return Def.Code.OK;
        }
    }
}
