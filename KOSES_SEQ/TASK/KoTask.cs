using System;
using System.Threading;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK
{
    public delegate void LoopFuncPointer();

    public class KoTask
    {
        public KoTask()
        {
            this.task = null;
            this.name = "KoTask";
            this.IsRunning = false;
            this.IsLive = false;
        }

        #region Task
        Task task;
        #endregion

        #region Name
        string name = "";
        public string Name
        {
            get 
            {
                return name; 
            }
            set 
            { 
                name = value; 
            }
        }
        #endregion

        public enum Code
        {
            UNKNOWN,
            OK,
            TASK_IS_WORKING,
            TASK_START_FAIL,
        }

        public Code Start(LoopFuncPointer loopFunc)
        {
            try
            {
                if (task != null)
                {
                    return Code.TASK_IS_WORKING;
                }

                task = new Task(() => loopFunc());
                if (task == null)
                {
                    return Code.TASK_START_FAIL;
                }

                IsRunning = true;
                IsLive = true;

                task.Start();
                
                return Code.OK;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"TaskStart : {ex.Message}");
                return Code.TASK_START_FAIL;
            }
        }

        /// <summary>
        /// 태스크를 일시 중시 시키기 위한 명령
        /// DoTask 를 사용하는 클래스의 루프문(while) 안에서 IsRunning 또는 IsStop 을 연동하여 사용한다.
        /// </summary>
        public void Stop()
        {
            IsRunning = false;
        }
        public bool IsRunning = false;
        public bool IsStop
        {
            get
            {
                return !IsRunning;
            }
        }


        /// <summary>
        /// 태스크를 완전 종료 시키는 명령
        /// DoTask 를 사용하는 클래스의 루프문(while) 안에서 IsLive 를 연동하여 사용한다.
        /// </summary>
        /// <param name="taskStopWaitTime"></param>
        public void Finish(int taskStopWaitTime = 0)
        {
            IsLive = false;
            Stop();
            Delay(taskStopWaitTime);
            task = null;
        }
        public bool IsLive = false;
        public bool IsFinished
        {
            get
            {
                return !IsLive;
            }
        }


        public void Delay(int delayTime)
        {
            Thread.Sleep(delayTime);
        }

    }
}
