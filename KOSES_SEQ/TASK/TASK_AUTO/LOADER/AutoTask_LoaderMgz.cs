﻿//using DoCS.FILE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TASK;
using KOSES_SEQ.DEFINE;
//using DoCSP.MOTION.MagLoader;
using KOSES_SEQ.TASK_DATA;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static KOSES_SEQ.TASK_AUTO.LoaderMgz.AutoStep_LoaderMgz;
using static KOSES_SEQ.OP.SysOperator;

namespace KOSES_SEQ.TASK_AUTO.LoaderMgz
{
    public class AutoTask_LoaderMgz
    {
        public AutoTask_LoaderMgz()
        {
            task = new KoTask();
            task.Name = $"{MethodBase.GetCurrentMethod().DeclaringType.Name}";

            step = new AutoStep_LoaderMgz();

        }

        /// <summary>
        /// Task
        /// </summary>
        #region Task
        KoTask task = null;
        public KoTask Task
        {
            get
            {
                return task;
            }
        }
        void TaskLoopDelay()
        {
            //if (Program.config.virtualMode)
            //{
            //    task.Delay(Def.GetVirtaulDelay(Program.config.motorSpeed));
            //}
            //else
            {
                task.Delay(Def.TaskDelay);
            }
        }
        public KoTask.Code StartTask()
        {
            KoTask.Code taskCode = task.Start(TaskLoopShell);
            return taskCode;
        }
        private void TaskError(Def.Code errorCode, Step step)
        {
            autoTask.Error(errorCode, task.Name, step.ToString());
        }
        #endregion


        /// <summary>
        /// Step
        /// </summary>
        #region Step
        AutoStep_LoaderMgz step = null;
        public void InitStep()
        {
            step.Init();
        }
        #endregion




        /// <summary>
        /// Z 축을 움직이기 전 센서 확인, I/O 동작
        /// </summary>
        /// <returns></returns>
        public Def.Code Prepare_Move_Z()
        {
            Def.Code code;

            //foreach (Def.MgzPos mp in Enum.GetValues(typeof(Def.MgzPos)))
            //{
            //    if (Program.motion.magLoader.GetMagSlider(mp) == false)
            //    {
            //        if (mp == Def.MagPos.Top)
            //        {
            //            return Def.Code.MAG_LOADER_TOP_SLIDER_WRONG_POSITION;
            //        }
            //        else
            //        {
            //            return Def.Code.MAG_LOADER_BTM_SLIDER_WRONG_POSITION;
            //        }
            //    }

            //    code = Program.motion.magLoader.SetConfirm(mp, Def.Lock.On);
            //    if (code != Def.Code.OK)
            //    {
            //        if (mp == Def.MagPos.Top)
            //        {
            //            return Def.Code.MAG_LOADER_TOP_SLIDER_LOCK;
            //        }
            //        else
            //        {
            //            return Def.Code.MAG_LOADER_BTM_SLIDER_LOCK;
            //        }
            //    }
            //}

            //if (Program.motion.rail.loader.top.GetCarrierExist_Entry())
            //{
            //    return Def.Code.MAG_LOADER_CARRIER_INTERLOCK;
            //}

            return Def.Code.OK;
        }

        public Def.Code Move_Z(double pos)
        {
            Def.Code code;

            code = Prepare_Move_Z();
            if (code != Def.Code.OK)
            {
                return code;
            }

            //code = Program.motion.magLoader.Move_Z_Confirm(pos);
            //if (code != Def.Code.OK)
            //{
            //    return code;
            //}

            return Def.Code.OK;
        }

        public Def.Code MoveToSlot(Def.MgzPos mgzPos, Def.SlotDest sd, int slot)
        {
            double orginPos = 0;
            double finalPos;
            //double gap = Program.teaching.size.vcmCarrierGap;

            //switch (mgzPos)
            //{
            //    case Def.MgzPos.Top:
            //        orginPos = Program.teaching.magLoader.topMagPos;
            //        break;

            //    //case Def.MgzPos.Btm:
            //    //    orginPos = Program.teaching.magLoader.btmMagPos;
            //    //    break;

            //    default:
            //        break;
            //}

            ////////////////////////////////////////////
            ////  투입 트레이는 아래에서 위로 진행
            ////////////////////////////////////////////
            //finalPos = orginPos - (gap * slot);
            //if (sd == Def.SlotDest.Sensor)
            //{
            //    finalPos = finalPos + Program.teaching.magLoader.gripperToSensor;
            //}
            //else if (sd == Def.SlotDest.Insert)
            //{
            //    finalPos = finalPos - Program.teaching.magLoader.topBtmGap;
            //}

            //Def.Code code = Move_Z(finalPos);
            //return code;
            return Def.Code.OK;
        }

        public Def.Code MoveStopPos()
        {
            Def.Code code;
            double teachZ;

            //teachZ = Program.teaching.magLoader.stopPos;
            teachZ = 0;
            code = Move_Z(teachZ);
            return code;
        }

        /// <summary>
        /// 지정 매거진내 비어 있는 슬롯 반환
        /// </summary>
        /// <param name="mgzPos"></param>
        /// <param name="slot"></param>
        /// <returns></returns>
        public int GetEmptySlot(Def.MgzPos mgzPos)
        {
            int slot = autoData.LoaderMgz.mgz[(int)mgzPos].GetEmptySlot();
            return slot;
        }

        /// <summary>
        /// 지정 매거진내 신규 캐리어가 있는 슬롯 반환
        /// </summary>
        /// <param name="mgzPos"></param>
        /// <param name="slot"></param>
        /// <returns></returns>
        public int GetNewSlot(Def.MgzPos mgzPos)
        {
            int slot = autoData.LoaderMgz.mgz[(int)mgzPos].GetNewSlot();
            return slot;
        }

        /// <summary>
        /// 해당 슬롯을 사용한 캐리어 정보로 변경
        /// </summary>
        /// <param name="mgzPos"></param>
        /// <param name="slot"></param>
        public void MakeUsedSlot(Def.MgzPos mgzPos, int slot)
        {
            autoData.LoaderMgz.mgz[(int)mgzPos].Slot[slot] = Def.SlotState.Used;
        }

        public void MakeNewSlot(Def.MgzPos mgzPos, int slot)
        {
            autoData.LoaderMgz.mgz[(int)mgzPos].Slot[slot] = Def.SlotState.New;
        }


        public bool IsNewSlot()
        {
            int slot;
            slot = autoData.LoaderMgz.TopMgz.GetNewSlot();
            if (slot != Def.NotFound)
            {
                return true;
            }

            return false;
        }

        public bool IsEmptySlot()
        {
            int slot;

            slot = autoData.LoaderMgz.TopMgz.GetEmptySlot();
            if (slot != Def.NotFound)
            {
                return true;
            }
            

            return false;
        }

        public bool IsUsedSlot(Def.MgzPos mp)
        {
            bool b;

            //if(Def.MgzPos.Top == mp)
           // {
                b = autoData.LoaderMgz.TopMgz.GetUsedSlot();
            //}
            
            return b;
        }

        public bool IsNoSensingSlot(Def.MgzPos mp)
        {
            bool b;

            //if (Def.MgzPos.Top == mp)
            {
                b = autoData.LoaderMgz.TopMgz.GetNoSensingSlot();
            }
           

            return b;
        }

        public bool IsReverseSlot(Def.MgzPos mp)
        {
            bool b;

            //if (Def.MgzPos.Top == mp)
            {
                b = autoData.LoaderMgz.TopMgz.GetNoReverseSlot();
            }
          

            return b;
        }


        /// <summary>
        /// 다른 매거진 가져오기
        /// </summary>
        /// <param name="mp"></param>
        /// <returns></returns>
        Def.MgzPos GetOtherMagPos(Def.MgzPos mp)
        {
            //if (mp == Def.MagPos.Top)
            //{
            //    return Def.MagPos.Btm;
            //}
            //else
            //{
            //    return Def.MagPos.Top;
            //}
            return Def.MgzPos.Top;
        }

        
        ///////////////////////////////////////////////////////////////////////////
        ///
        /// 테스크 루프
        /// 
        ///////////////////////////////////////////////////////////////////////////
        private void TaskLoopShell()
        {
            try
            {
                TaskLoop();
            }
            catch (Exception ex)
            {
                TaskLog($"======================================================", Step.TaskShell);
                TaskLog($"TaskLoop Exception : {ex.Message}", Step.TaskShell);
                TaskLog($"======================================================", Step.TaskShell);
            }
            finally
            {
                TaskLog("======================================================", Step.TaskEnd);
                TaskLog("Task : 종료", Step.TaskEnd);
                TaskLog("======================================================", Step.TaskEnd);
                task.Finish();
            }

            TaskLog($"Last Step : {step.Get()}", Step.TaskShell);
        }

        private void TaskLog(string msg, Step step)
        {
            string log = string.Format($"[{step}] : {msg}");
            //Program.baseForm.taskForm.LogMagLoader(log);
            //Program.log.Auto(task.Name, log);
            log = null;
        }


        private void TaskLoop()
        {
            Step curStep;
            Def.Code code;
            bool useMsg = true;
            bool b;
            int slotCount = 0;
            int slot = 0;
            bool bTopMagExist = false;
            bool bBtmMagExist = false;
            Def.MgzPos otherMagPos;

            Def.LoaderMgzGive giveRerserved = Def.LoaderMgzGive.OFF;
            
            AutoData_LoaderMgz dataOwn = autoData.LoaderMgz;
            //Motion_MagLoader motionOwn = Program.motion.magLoader;


            while (true)
            {
                ///////////////////////////////////////////////////////
                Entry:

                TaskLoopDelay();
                if (task.IsFinished)
                {
                    break;
                }
                if (task.IsStop)
                {
                    goto Entry;
                }

                if (autoUse.LoaderMgz)
                {
                    if (useMsg == false)
                    {
                        TaskLog($"태스크 재개", Step.None);
                    }
                    useMsg = true;
                }
                else
                {
                    if (useMsg)
                    {
                        useMsg = false;
                        TaskLog($"태스크 정지", Step.None);
                    }
                    goto Entry;
                }

                if (autoTask.CheckEquipState() == false)
                {
                    dataOwn.cycleState = Def.CycleState.Stop;
                    goto Entry;
                }
                dataOwn.cycleState = Def.CycleState.Run;

                curStep = step.Get();
                //if (Program.config.saveTaskData)
                //{
                //    //if (curStep != step.GetLast())
                //    //{
                //    //    TaskDataFile_Save(curStep);
                //    //}
                //}


                switch (curStep)
                {
                    ////////////////////////////////////////////////////////////////////////
                    //  초기화
                    ////////////////////////////////////////////////////////////////////////
                    case Step.Init:
                        TaskLog($"초기화", curStep);
                        
                        dataOwn.State = Def.LoaderMgzState.IDLE;
                        dataOwn.StartFlag = Def.LoaderMgzStart.OFF;
                        dataOwn.StopFlag = Def.LoaderMgzStop.OFF;
                        dataOwn.GiveFlag = Def.LoaderMgzGive.OFF;
                        dataOwn.RecvFlag = Def.LoaderMgzRecv.OFF;

                        //if (Program.motion.magLoader.GetTopMagExist())
                        //{
                        //    dataOwn.curGiveMagPos = Def.MgzPos.Top;
                        //    TaskLog($"현재 공급 매거진 : {dataOwn.curGiveMagPos}", curStep);
                        //    dataOwn.curRecvMagPos = Def.MagPos.Top;
                        //    TaskLog($"현재 배출 매거진 : {dataOwn.curRecvMagPos}", curStep);
                        //    TaskLog($"현재 선두 매거진 : {dataOwn.curRecvMagPos}", curStep);
                        //}
                        //else if (Program.motion.magLoader.GetBtmMagExist())
                        //{
                        //    dataOwn.curGiveMagPos = Def.MagPos.Btm;
                        //    TaskLog($"현재 공급 매거진 : {dataOwn.curGiveMagPos}", curStep);
                        //    dataOwn.curRecvMagPos = Def.MagPos.Btm;
                        //    TaskLog($"현재 배출 매거진 : {dataOwn.curRecvMagPos}", curStep);
                        //    TaskLog($"현재 선두 매거진 : {dataOwn.curRecvMagPos}", curStep);
                        //}
                        //else
                        //{
                        //    TaskLog($"매거진 없음", curStep);
                        //    TaskError(Def.Code.MAG_LOADER_NO_MAGZINE, curStep);
                        //    goto Entry;
                        //}

                        dataOwn.curGiveSlotPos = 0;
                        TaskLog($"현재 공급 슬롯 : {dataOwn.curGiveSlotPos}", curStep);
                        dataOwn.curRecvSlotPos = 0;
                        TaskLog($"현재 배출 슬롯 : {dataOwn.curRecvSlotPos}", curStep);

                        step.Set(Step.Starting);
                        break;


                    ////////////////////////////////////////////////////////////////////////
                    //  시작
                    ////////////////////////////////////////////////////////////////////////
                    case Step.Starting:
                        TaskLog($"시작", curStep);

                        //foreach (Def.MgzPos mp in Enum.GetValues(typeof(Def.MgzPos)))
                        //{
                        //    if (Program.motion.magLoader.GetMagSlider(mp) == false)
                        //    {
                        //        if (mp == Def.MagPos.Top)
                        //        {
                        //            TaskLog($"({mp}) 슬라이더 감지 실패", curStep);
                        //            TaskError(Def.Code.MAG_LOADER_TOP_SLIDER_WRONG_POSITION, curStep);
                        //            goto Entry;
                        //        }
                        //        else
                        //        {
                        //            TaskLog($"({mp}) 슬라이더 감지 실패", curStep);
                        //            TaskError(Def.Code.MAG_LOADER_BTM_SLIDER_WRONG_POSITION, curStep);
                        //            goto Entry;
                        //        }
                        //    }

                        //    code = Program.motion.magLoader.SetConfirm(mp, Def.Lock.On);
                        //    if (code != Def.Code.OK)
                        //    {
                        //        if (mp == Def.MagPos.Top)
                        //        {
                        //            TaskLog($"({mp}) 슬라이더 잠금 실패", curStep);
                        //            TaskError(Def.Code.MAG_LOADER_TOP_SLIDER_LOCK, curStep);
                        //            goto Entry;
                        //        }
                        //        else
                        //        {
                        //            TaskLog($"({mp}) 슬라이더 잠금 실패", curStep);
                        //            TaskError(Def.Code.MAG_LOADER_BTM_SLIDER_LOCK, curStep);
                        //            goto Entry;
                        //        }
                        //    }
                        //}

                        step.Set(Step.Scan_Carrier_Top);
                        break;


                    ////////////////////////////////////////////////////////////////////////
                    //  작업 완료 확인
                    ////////////////////////////////////////////////////////////////////////
                    case Step.Check_WorkCompleted:
                        TaskLog($"작업 완료 확인", curStep);
                        step.Set(Step.Check_State);
                        break;


                    ////////////////////////////////////////////////////////////////////////
                    //  상태 확인
                    ////////////////////////////////////////////////////////////////////////
                    case Step.Check_State:
                        //TaskLog($"STATE : {dataOwn.State}", curStep);
                        switch (dataOwn.State)
                        {
                            case Def.LoaderMgzState.STOP:
                                if (dataOwn.StartFlag == Def.LoaderMgzStart.ON)
                                {
                                    TaskLog($"START FLAG : {dataOwn.StartFlag}", curStep);
                                    dataOwn.StartFlag = Def.LoaderMgzStart.OFF;
                                    TaskLog($"START FLAG 변경 : {dataOwn.StartFlag}", curStep);

                                    step.Set(Step.Start_Action);
                                }
                                break;

                            /// 2021.02.27 수정 - INDEX RAIL 에서 "READY_RECV" 나 "READY_GIVE" 상태로 변경되면
                            ///                   자재가 없어도 IDLE 상태로 빠져나올 수 없음. HangUp 발생
                            case Def.LoaderMgzState.READY_RECV:
                            case Def.LoaderMgzState.READY_GIVE:
                                //if (dataOwn.State == Def.LoaderMgzState.READY_GIVE)
                                //{
                                //    if (Def.Index_1_State.CARRIER_EMPTY != autoData.rail.index1.State)
                                //        dataOwn.State = Def.LoaderMgzState.IDLE;
                                //}
                                //else if (dataOwn.State == Def.LoaderMgzState.READY_RECV)
                                //{
                                //    if (Def.Index_3_State.CARRIER_EXIST != autoData.rail.index3.State)
                                //        dataOwn.State = Def.LoaderMgzState.IDLE;
                                //}
                                goto Entry;

                            case Def.LoaderMgzState.IDLE:
                                step.Set(Step.Check_Flag);
                                break;

                            default:
                                TaskError(Def.Code.CODE_ERROR, curStep);
                                break;
                        }
                        break;


                    ////////////////////////////////////////////////////////////////////////
                    //  플래그 확인
                    ////////////////////////////////////////////////////////////////////////
                    case Step.Check_Flag:
                        if (dataOwn.StopFlag == Def.LoaderMgzStop.ON)
                        {
                            TaskLog($"STOP FLAG : {dataOwn.StopFlag}", curStep);
                            dataOwn.StopFlag = Def.LoaderMgzStop.OFF;
                            TaskLog($"STOP FLAG 변경 : {dataOwn.StopFlag}", curStep);

                            step.Set(Step.Stop_Action);
                        }
                        else if (dataOwn.GiveFlag == Def.LoaderMgzGive.ON)
                        {
                            // New Slot 확인시 로그 처리
                            if (IsNewSlot())
                                TaskLog($"GIVE FLAG : {dataOwn.GiveFlag}", curStep);

                            dataOwn.GiveFlag = Def.LoaderMgzGive.OFF;

                            // New Slot 확인시 로그 처리
                            if (IsNewSlot())
                                TaskLog($"GIVE FLAG 변경 : {dataOwn.GiveFlag}", curStep);

                            //TaskLog($"신규 슬롯 확인", curStep);
                            if (IsNewSlot())
                            {
                                TaskLog($"신규 슬롯 있음", curStep);
                                step.Set(Step.Give_Action);
                                goto Entry;
                            }

                            //if (false == Program.teaching.common.contiLot)
                            //{
                            //    giveRerserved = Def.LoaderMgzGive.ON;
                            //    TaskLog($"GIVE 예약 : {giveRerserved}", curStep);

                            //    TaskLog($"신규 슬롯 없음", curStep);
                            //    // 메인화면 디스플레이 예정
                            //    //Program.MessageBox($"매거진 로더", $"신규 슬롯 없음", DefColor.Yellow);
                            //    //Program.motion.frame.buzzer.Set(true);
                            //}

                            step.Set(Step.Check_State);
                            goto Entry;
                        }
                        else
                        {
                            step.Set(Step.Check_State);
                        }
                        break;
                  
                    ////////////////////////////////////////////////////////////////////////
                    //  요청 처리
                    ////////////////////////////////////////////////////////////////////////
                    case Step.Give_Action:
                        TaskLog($"공급 동작", curStep);

                        //////////////////////////////////////////////////////////////////////////////////////////////
                        //// No Sensing (or) Reverse Slot Error
                        //////////////////////////////////////////////////////////////////////////////////////////////
                        //if (IsNoSensingSlot(dataOwn.curGiveMagPos))
                        //{
                        //    TaskLog($"현재 매거진 : {dataOwn.curGiveMagPos} 노센싱 캐리어 확인", curStep);
                        //    TaskError(Def.Code.MAG_LOADER_CARRIER_EMPTY, curStep);
                        //    goto Entry;
                        //}

                        //if (IsReverseSlot(dataOwn.curGiveMagPos))
                        //{
                        //    TaskLog($"현재 매거진 : {dataOwn.curGiveMagPos} 리버스 캐리어 확인", curStep);
                        //    TaskError(Def.Code.MAG_LOADER_CARRIER_REVERSE, curStep);
                        //    goto Entry;
                        //}
                        //////////////////////////////////////////////////////////////////////////////////////////////

                        //TaskLog($"현재 공급 매거진 확인 : {dataOwn.curGiveMagPos}", curStep);
                        //TaskLog($"신규 슬롯 확인", curStep);
                        //slot = GetNewSlot(dataOwn.curGiveMagPos);
                        //if (slot == Def.NotFound)
                        //{
                        //    otherMagPos = GetOtherMagPos(dataOwn.curGiveMagPos);

                        //    TaskLog($"다른 매거진 확인 : {otherMagPos}", curStep);
                        //    TaskLog($"다른 매거진 신규 슬롯 확인", curStep);
                        //    slot = GetNewSlot(otherMagPos);
                        //    if (slot == Def.NotFound)
                        //    {
                        //        //giveRerserved = Def.MagLoaderGive.ON;
                        //        //TaskLog($"GIVE 예약 : {giveRerserved}", curStep);

                        //        //TaskLog($"신규 슬롯 없음", curStep);
                        //        // 메인화면 디스플레이 예정
                        //        //Program.MessageBox($"매거진 로더", $"신규 슬롯 없음", DefColor.Yellow);
                        //        //Program.motion.frame.buzzer.Set(true);

                        //        step.Set(Step.Check_State);
                        //        goto Entry;
                        //    }

                        //    TaskLog($"다른 매거진 신규 슬롯 : {slot}", curStep);
                        //    if (otherMagPos == Def.MgzPos.Top)
                        //    {
                        //        b = Program.motion.magLoader.GetTopMagExist();
                        //    }
                        //    else
                        //    {
                        //        b = Program.motion.magLoader.GetBtmMagExist();
                        //    }

                        //    if (b == false)
                        //    {
                        //        TaskLog($"다른 매거진 : {otherMagPos} IO 감지 실패", curStep);                                
                               
                        //        step.Set(Step.Check_State);
                        //        goto Entry;
                        //    }

                        //    dataOwn.curGiveMagPos = otherMagPos;
                        //    TaskLog($"현재 공급 매거진 변경 : {dataOwn.curGiveMagPos}", curStep);
                        //}

                        //TaskLog($"신규 슬롯 위치로 이동 : M({dataOwn.curGiveMagPos}) / S({slot})", curStep);
                        //code = MoveToSlot(dataOwn.curGiveMagPos, Def.SlotDest.Extract, slot);
                        //if (code != Def.Code.OK)
                        //{
                        //    TaskLog($"신규 슬롯 위치로 이동 실패", curStep);
                        //    TaskError(Def.Code.MAG_LOADER_MOVE_TO_NEW_SLOT_FAIL, curStep);
                        //    goto Entry;
                        //}

                        //////////////////////////////////////////////////////////////////////////////////
                        //// 210517 shpark
                        //// Lot Id Check
                        //////////////////////////////////////////////////////////////////////////////////
                        //if(Program.teaching.common.contiLot)
                        //{
                        //    Def.MagPos gmp = dataOwn.curGiveMagPos;
                        //    string lotId = Program.product[(int)gmp].lot;
                        //    if (lotId.Length != Def.LotSize)
                        //    {
                        //        TaskLog($"LOT ID FAIL. NEED LOT ID", curStep);
                        //        TaskError(Def.Code.NOT_FIND_LOT_ID, curStep);
                        //        //step.Set(Step.Check_State);
                        //        goto Entry;
                        //    }
                        //}
                        //////////////////////////////////////////////////////////////////////////////////

                        //dataOwn.curGiveSlotPos = slot;
                        //TaskLog($"현재 공급 슬롯 변경 : {dataOwn.curGiveSlotPos}", curStep);

                        //TaskLog($"공급 캐리어 정보 설정 : {dataOwn.curGiveMagPos} : {dataOwn.curGiveSlotPos}", curStep);
                        //dataOwn.caInfo.MagPos = dataOwn.curGiveMagPos;
                        //dataOwn.caInfo.Slot = dataOwn.curGiveSlotPos;

                        //////////////////////////////////////////////////////////////////////////////////
                        //// Ld Top -> Uld Top/ Ld Btm -> Uld Btm 인터락
                        //////////////////////////////////////////////////////////////////////////////////
                        //int uldMzNewSlot = Program.autoTask.magUnloader.GetNewSlot(Program.autoData.magUnloader.caInfo.MagPos);
                        //int uldMzEmptySlot = Program.autoTask.magUnloader.GetEmptySlot(Program.autoData.magUnloader.caInfo.MagPos);

                        //if ((Def.NotFound == uldMzNewSlot) && (Def.NotFound == uldMzEmptySlot))
                        //{
                        //    TaskLog($"현재 로더 Mz 위치: {dataOwn.caInfo.MagPos}, 현재 언로더 {Program.autoData.magUnloader.caInfo.MagPos} Mz 없음", curStep);
                        //    TaskError(Def.Code.MAG_UNLOADER_NO_EMPTY_SLOT, curStep);
                        //    goto Entry;
                        //}
                        //////////////////////////////////////////////////////////////////////////////////

                        //dataOwn.State = Def.MagLoaderState.READY_GIVE;
                        //TaskLog($"STATE 변경 : {dataOwn.State}", curStep);

                        step.Set(Step.Check_State);
                        break;
                    
                    ////////////////////////////////////////////////////////////////////////
                    //  예외 처리
                    ////////////////////////////////////////////////////////////////////////
                    default:
                        TaskError(Def.Code.INVALID_STEP, curStep);
                        task.Stop();
                        break;

                }
                step.SetLast(curStep);

            }
        }
    }
}
