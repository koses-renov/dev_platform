﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TASK_DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK_AUTO.LoaderMgz
{
    public class AutoData_LoaderMgz
    {
        public AutoData_LoaderMgz()
        {
            //int caCount = Program.teaching.size.vcmCarrierCount;
            int caCount = 10;
            foreach (Def.MgzPos mp in Enum.GetValues(typeof(Def.MgzPos)))
            {
                LoaderMagazineData vmd = new LoaderMagazineData(caCount);
                mgz.Add(vmd);
                vmd = null;
            }
        }

        #region Cycle State
        public Def.CycleState cycleState = Def.CycleState.Stop;
        #endregion


        public List<LoaderMagazineData> mgz = new List<LoaderMagazineData>();      
        public LoaderMagazineData TopMgz
        {
            get
            {
                return mgz[(int)Def.MgzPos.Top];
            }
        } 

        public Def.MgzPos curGiveMagPos;
        public Def.MgzPos curRecvMagPos;

        public int curGiveSlotPos;
        public int curRecvSlotPos;



        ///////////////////////////////////////////////////////////////
        /// 매거진 로더 상태
        ///////////////////////////////////////////////////////////////
        Def.LoaderMgzState state = Def.LoaderMgzState.STOP;
        #region State
        object stateLock = new object();
        public Def.LoaderMgzState State
        {
            get
            {
                Def.LoaderMgzState ret;
                lock (stateLock)
                {
                    ret = state;
                }
                return ret;
            }
            set
            {
                lock (stateLock)
                {
                    state = value;
                }
            }
        }
        #endregion



        ///////////////////////////////////////////////////////////////
        /// 매거진 로더 플래그
        ///////////////////////////////////////////////////////////////
        Def.LoaderMgzStart startFlag = Def.LoaderMgzStart.OFF;
        #region Start
        object startFlagLock = new object();
        public Def.LoaderMgzStart StartFlag
        {
            get
            {
                Def.LoaderMgzStart ret;
                lock (startFlagLock)
                {
                    ret = startFlag;
                }
                return ret;
            }
            set
            {
                lock (startFlagLock)
                {
                    startFlag = value;
                }
            }
        }
        #endregion

        Def.LoaderMgzStop stopFlag = Def.LoaderMgzStop.OFF;
        #region Stop
        object stopFlagLock = new object();
        public Def.LoaderMgzStop StopFlag
        {
            get
            {
                Def.LoaderMgzStop ret;
                lock (stopFlagLock)
                {
                    ret = stopFlag;
                }
                return ret;
            }
            set
            {
                lock (stopFlagLock)
                {
                    stopFlag = value;
                }
            }
        }
        #endregion

        Def.LoaderMgzGive giveFlag = Def.LoaderMgzGive.OFF;
        #region Give
        object gfLock = new object();
        public Def.LoaderMgzGive GiveFlag
        {
            get
            {
                Def.LoaderMgzGive ret;
                lock (gfLock)
                {
                    ret = giveFlag;
                }
                return ret;
            }
            set
            {
                lock (gfLock)
                {
                    giveFlag = value;
                }
            }
        }
        #endregion

        Def.LoaderMgzRecv recvFlag = Def.LoaderMgzRecv.OFF;
        #region Recv
        object rfLock = new object();
        public Def.LoaderMgzRecv RecvFlag
        {
            get
            {
                Def.LoaderMgzRecv ret;
                lock (rfLock)
                {
                    ret = recvFlag;
                }
                return ret;
            }
            set
            {
                lock (rfLock)
                {
                    recvFlag = value;
                }
            }
        }
        #endregion

    }
}
