﻿using KOSES_SEQ.TASK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK_AUTO.LoaderMgz
{
    public class AutoStep_LoaderMgz
    {
        public AutoStep_LoaderMgz()
        {
            Init();
        }

        Step step;
        Step lastStep;

        #region Get, Set
        public void Init()
        {
            Set(Step.Init);
            SetLast(Step.Init);
        }
        public void Set(Step step)
        {
            this.step = step;
        }
        public void SetLast(Step step)
        {
            this.lastStep = step;
        }
        public Step Get()
        {
            return this.step;
        }
        public Step GetLast()
        {
            return this.lastStep;
        }
        #endregion

        public enum Step
        {
            None,

            TaskShell,

            TaskCondition_Check,

            Init,

            Starting,
            Check_WorkCompleted,


            //  Start
            Start_Action,

            //  Stop
            Stop_Action,


            Scan_Carrier_Top,

            Scan_Carrier_Btm,


            //  상태 확인
            Check_State,

            //  플래그 확인
            Check_Flag,


            //  공급 동작
            Give_Action,

            //  배출 동작
            Recv_Action,


            TaskEnd,
        }

    }
}
