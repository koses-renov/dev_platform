﻿using KOSES_SEQ.TASK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK_MANUAL
{
    public partial class ManualTask : KoTask
    {
        public enum ManualList
        {
            None,

            #region MACRO TEST
            TEST_1,
            TEST_2,
            #endregion

            #region 일반
            MainSwitchLamp,
            TowerLamp,
            Buzzer,
            #endregion


            #region 매거진 로더
            MagLoaderSwitchLamp,
            MagLoaderMoveZ_GripperPos,
            MagLoaderMoveZ_SensorPos,
            MagLoaderMoveZ_StopPos,
            #endregion
           
        }
    }
}
