﻿using KOSES_SEQ.TASK;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK_MANUAL
{
    public partial class ManualTask : KoTask
    {
        #region ManualProperty
        [Serializable]
        public class ManualProperty
        {
            public ManualList list = ManualList.None;
            public string remark = string.Empty;

            public bool flag = true;
            public int stage = 0;
            public double angle = 0;
            public double destPos = 0;
            public int cleanTime = 0;
            public int uvCureTime = 0;

            public int trayModule;
            public Def.Lock magLock;
            public Def.Align align;

            public Def.Motor motorIndex;

            public Def.LeftRight leftRight = Def.LeftRight.Left;
            public Def.FwdBwd fwdBwd = Def.FwdBwd.Fwd;
            public Def.UpDown upDown = Def.UpDown.Up;
            public Def.OpenClose openClose = Def.OpenClose.Open;
            public Def.OnOff onOff = Def.OnOff.Off;
            public Def.Grip grip = Def.Grip.Off;
            public Def.MotorDir motorDir = Def.MotorDir.PLUS;

            public string destIp = "127.0.0.1";
            public int destPort = 7000;
            public int ownPort = 8000;

            //public LibDef.LampMotion startLamp = LibDef.LampMotion.Off;
            //public LibDef.LampMotion stopLamp = LibDef.LampMotion.Off;
            //public LibDef.LampMotion resetLamp = LibDef.LampMotion.Off;

            //public LibDef.LampMotion magUnloaderStartLamp = LibDef.LampMotion.Off;
            //public LibDef.LampMotion magUnloaderStopLamp = LibDef.LampMotion.Off;

            //public LibDef.LampMotion towerLampRed = LibDef.LampMotion.Off;
            //public LibDef.LampMotion towerLampYellow = LibDef.LampMotion.Off;
            //public LibDef.LampMotion towerLampGreen = LibDef.LampMotion.Off;

            public Def.Vac vac = Def.Vac.On;
            public Def.Blow blow = Def.Blow.On;
            public Def.Suction suction = Def.Suction.On;

            public Def.MgzPos mgzPos;
            public Def.SlotDest slotDest;
            public int carrierNum;
            //public Def.MainSwitch mainSwitch;
            //public Def.MagSwitch magSwitch;

            public Def.ShuttleIdx shuttleIdx;
            public Def.ShuttleDest shuttleDest;
            
            public Def.DispensingGroup dispensingGroup;

            public Def.LensTraySlot lensTraySlot;
            public Def.LensTrayShuttleIdx lensTrayShuttleIdx;
            public Def.IonizerPos ionizerPos;

            public Def.AttachIdx attachIdx;
            public Def.PickerIdx pickerIdx;
            public Def.ShuttleSocketIdx shuttleSocketIdx;
            public Def.DispenserIdx dispenserIdx;
            public Def.DispensingType dispensingType;

            public Def.LaserMeasurePoint laserMeasurePoint;
            public Def.Laser1MoveType laser1MoveType;

            public int vcmNum;
            public int lensNum;
            public int pocketNum;
            public int rejectNum;
        }
        #endregion
    }
}
