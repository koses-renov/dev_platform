﻿using KOSES_SEQ.TASK;
//using DoCS.UTILITY;
using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
//using DoCSP.TEACHING;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using static KOSES_SEQ.OP.SysOperator;
using KOSES_SEQ.OP;

namespace KOSES_SEQ.TASK_MANUAL
{
    public partial class ManualTask
    {
        public ManualTask()
        {
            SysOperator.Instance.Tenkey.OnPushedTenkeyHomeButton += Tenkey_OnPushedTenkeyHomeButton;
            SysOperator.Instance.Tenkey.OnPushedTenkeyStepButton += Tenkey_OnPushedTenkeyStepButton;
        }

        private void Tenkey_OnPushedTenkeyStepButton(int num, bool isPush)
        {

            if (num == 0)
                return;
            // Tenkey 조건 체크 추가 할것



            if (!isPush)
                return;

            switch (num)
            {
                case 11:
                    var mp = new ManualProperty();
                    mp.list = ManualList.MagLoaderMoveZ_GripperPos;
                    mp.shuttleIdx = Def.ShuttleIdx.S1;
                    this.StartSingle(mp);
                    break;

            }
        }

        private void Tenkey_OnPushedTenkeyHomeButton(int num, bool isPush)
        {
            if (num == 0)
                return;
            // Tenkey 조건 체크 추가 할것


            if (!isPush)
                return;

            switch (num)
            {
                case 11:
                    var mp = new ManualProperty();
                    mp.list = ManualList.MagLoaderMoveZ_GripperPos;
                    mp.shuttleIdx = Def.ShuttleIdx.S1;
                    this.StartSingle(mp);
                    break;
            }
        }

        KoTask task = new KoTask();

        /// <summary>
        /// 매뉴얼 Task를 사용하지 않는 간단한 매뉴얼 동작에 대해
        /// 매뉴얼 동작 가능 여부를 판단한다.
        /// </summary>
        /// <returns></returns>
        public bool IsEnable()
        {
            string msg = ($"수동 동작 제한");
            if (homeTask.Task.IsLive)
            {
                msg = ($"{msg} : HOME");
                OP.FRM.Msg.ShowMsg(msg);              
                return false;
            }

            if (autoTask.IsLive)
            {
                EquipmentStatus.State es = GetEquipState();
                switch (es)
                {
                    case EquipmentStatus.State.READY:
                    case EquipmentStatus.State.ERROR_RESET:
                    case EquipmentStatus.State.STOP:
                    case EquipmentStatus.State.JOB_COMPLETED:
                        break;

                    default:
                        msg = ($"{msg} : {es}");
                        OP.FRM.Msg.ShowMsg(msg);                       
                        return false;
                }
            }
            return true;
        }


        enum ActionType
        {
            single,
            macro
        }

        public List<ManualProperty> macroList = new List<ManualProperty>();
        public bool macroRecord = false;
        public int macroRepeatCount = 0;
        public int macroStepInterval = 0;
        private ManualProperty curAction = new ManualProperty();


        public bool StartSingle(ManualProperty mp)
        {
            //Program.keyboardTeaching.Stop();
            Def.Code code;

            code = CheckCondition_ManualStart();
            if (code != Def.Code.OK)
            {
                return false;
            }

            if (macroRecord == true)
            {
                AddMacro(mp);
            }
            curAction = mp;
            ResetUserStop();
            return TaskStart(ActionType.single);
        }

        public bool StartMacro()
        {
            //Program.ResetUserStop();
            return TaskStart(ActionType.macro);
        }

        private bool TaskStart(ActionType at)
        {
            string title = "MANUAL";
            Def.Code code;
            
            code = CheckCondition_Door();
            if (code != Def.Code.OK)
            {
                return false;
            }
            
            if (homeTask.Task.IsLive == true)
            {
                OP.FRM.Msg.ShowMsg("HOME OPERATION IS IN PROGRESS ");
                return false;
            }

            //if (manualTask.IsLive == true)
            //{
            //    Program.MessageBox(title, "MANUAL OPERATION IS IN PROGRESS", DefColor.LightRed);
            //    return false;
            //}

            EquipmentStatus.State es = GetEquipState();
            switch (es)
            {
                case EquipmentStatus.State.READY:
                case EquipmentStatus.State.ERROR_RESET:
                case EquipmentStatus.State.STOP:
                case EquipmentStatus.State.JOB_COMPLETED:
                    break;

                default:
                    //Program.MessageBox(title, $"MANUAL CAN NOT PROCESS WHEN {es}", DefColor.LightRed);
                    OP.FRM.Msg.ShowMsg($"MANUAL CAN NOT PROCESS WHEN {es}");
                    return false;
            }

            //Program.NoticeStart("MANUAL TASK IS RUNNING", Def.NoticeMediumFontSize, DefColor.GoogleBlue, 0);

            if (task.IsLive)
            {
                return false;
            }
            else
            { 
                ResetUserStop();
            
                SetActionType(at);
                KoTask.Code taskCode = task.Start(TaskLoop);
                if (taskCode != KoTask.Code.OK)
                {
                    return false;
                }
                return true;
            }
        }

        ActionType actionType;
        private void SetActionType(ActionType at)
        {
            actionType = at;
        }
        private ActionType GetActionType()
        {
            return actionType;
        }
        private void TaskLoop()
        {
            ActionType at = GetActionType();
            TaskLoop(at);
        }
        private void TaskLoop(ActionType at)
        {
            bool manualResult = false;
            bool macroUserStop = false;
            int r;
            Def.Code code = Def.Code.OK;

            while (task.IsLive)
            {
                ///////////////////////////////////////////////////////
                if (ActionType.single == at)
                {
                    manualResult = SingleAction(curAction, ref code);
                    break;
                }
                else
                {
                    for (r = 0; r < macroRepeatCount; r++)
                    {
                        for (int i = 0; i < macroList.Count; i++)
                        {
                            if (this.IsLive == false)
                            {
                                macroUserStop = true;
                                goto MANUAL_LOOP_EXIT;
                            }
                            curAction = macroList[i];
                            manualResult = SingleAction(curAction, ref code);
                            if (manualResult == false)
                            {
                                goto MANUAL_LOOP_EXIT;
                            }
                            if (GetUserStop() == true)
                            {
                                goto MANUAL_LOOP_EXIT;
                            }
                            Thread.Sleep(macroStepInterval);
                        }
                    }
                    if (r >= macroRepeatCount)
                    {
                        goto MANUAL_LOOP_EXIT;
                    }
                }

                task.Delay(1);
            }


            MANUAL_LOOP_EXIT:

            //Program.NoticeStop();

            if (GetUserStop() == true)
            {
                OP.FRM.Msg.ShowMsg($"MANUAL : USER STOP");
            }
            else
            {
                string errorString = string.Format($"{curAction.list.ToString()}({code}) ");
                if (manualResult == true)
                {
                    if (ActionType.single == at)
                    {
                        OP.FRM.Msg.ShowMsg($"MANUAL TASK SUCCEED ");
                    }
                    else
                    {
                        if (macroUserStop)
                        {
                            errorString = "MACRO STOP";                           
                            OP.FRM.Msg.ShowMsg($"MANUAL :   {errorString}");
                        }
                        else
                        {
                            errorString = "MACRO COMPLETED";
                            OP.FRM.Msg.ShowMsg($"MANUAL :   {errorString}");
                        }
                    }
                }
                else
                {
                    errorString += "FAILED";
                    OP.FRM.Msg.ShowMsg($"MANUAL :   {errorString}");
                }
            }

            task.Finish();
        }

        public void AddMacro(ManualProperty mp)
        {
            macroList.Add(mp);
        }


        ManualList lastManualList = ManualList.None;

        private bool SingleAction(ManualProperty mp, ref Def.Code code)
        {
            bool retFlag = false;

            int si = 0;
            int ai = 0;

            double x;
            //XYZ teach = new XYZ();
            //XYT visionAlign = new XYT();
            //XYZ xyz = new XYZ();
            double attachR;
            double offset;
            int shotCount;
            int waitTime;
            double totalWeight;
            double standardWeight;
            double weight_Range;
            double pressure_Value;
            double curPressure;
            double oneShotWeight;
            int curChannel;
            int newPressure;
            double circleFitRate;

            int leftCount;
            int rightCount;
            int totalCount;

            if (lastManualList != mp.list)
            {
                lastManualList = mp.list;
            }

            switch (mp.list)
            {
                //////////////////////////////////////////////////////////////////////////////////
                ///
                /// 일반
                /// 
                //////////////////////////////////////////////////////////////////////////////////
                #region 일반
                case ManualList.MainSwitchLamp:
                    //switch (mp.mainSwitch)
                    //{
                    //    case Def.MainSwitch.Start:
                    //        Program.motion.frame.button.SetSwitchLamp(mp.mainSwitch, mp.startLamp);
                    //        break;
                    //    case Def.MainSwitch.Stop:
                    //        Program.motion.frame.button.SetSwitchLamp(mp.mainSwitch, mp.stopLamp);
                    //        break;
                    //    case Def.MainSwitch.Reset:
                    //        Program.motion.frame.button.SetSwitchLamp(mp.mainSwitch, mp.resetLamp);
                    //        break;
                    //}
                    retFlag = true;
                    break;
                case ManualList.TowerLamp:
                    //Program.motion.frame.towerLamp.SetLamp(mp.towerLampRed, mp.towerLampYellow, mp.towerLampGreen);
                    retFlag = true;
                    break;
                case ManualList.Buzzer:
                    //Program.motion.frame.buzzer.Set(mp.flag);
                    retFlag = true;
                    break;
                #endregion

                //////////////////////////////////////////////////////////////////////////////////
                ///
                /// 매거진 로더
                /// 
                //////////////////////////////////////////////////////////////////////////////////
                #region 매거진 로더
                case ManualList.MagLoaderSwitchLamp:
                    //Program.motion.magLoader.button.SetSwitchLamp(mp.magSwitch, mp.startLamp);
                    retFlag = true;
                    break;
                case ManualList.MagLoaderMoveZ_GripperPos:
                case ManualList.MagLoaderMoveZ_SensorPos:
                    //code = Program.autoTask.magLoader.MoveToSlot(mp.magPos, mp.slotDest, mp.carrierNum);
                    //if (code == Def.Code.OK)
                    //{
                    //    retFlag = true;
                    //}
                    break;
                
                case ManualList.MagLoaderMoveZ_StopPos:
                    //code = Program.autoTask.magLoader.MoveStopPos();
                    //if (code == Def.Code.OK)
                    //{
                    //    retFlag = true;
                    //}
                    break;
                #endregion
              
            }
            return retFlag;
        }

    }

}
