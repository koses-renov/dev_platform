﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK_HOME
{
    public class HomeStep
    {
        public HomeStep()
        {
            Init();
        }

        Step step;
        Step lastStep;

        #region Get, Set
        public void Init()
        {
            Set(Step.Home_Start);
            SetLast(Step.Home_Start);
        }
        public void Set(Step step)
        {
            this.step = step;
        }
        public void SetLast(Step step)
        {
            this.lastStep = step;
        }
        public Step Get()
        {
            return this.step;
        }
        public Step GetLast()
        {
            return this.lastStep;
        }
        #endregion

        public enum Step
        {
            //////////////////////////////////////////
            /// 홈 시작
            //////////////////////////////////////////
            Home_Start = 0,
            Home_PreProcess,

            //////////////////////////////////////////
            /// 홈 시작 전 환경 체크
            //////////////////////////////////////////
            MagLoader_Z_Check,
            
            //////////////////////////////////////////
            /// 마스크 상승
            //////////////////////////////////////////
            Mask_Up,
            Mask_Up_Wait,

            //////////////////////////////////////////
            /// 실린더 초기화
            //////////////////////////////////////////
            Cylinder_Init,

            //////////////////////////////////////////
            /// 인덱스
            //////////////////////////////////////////
            Index_Down,
            Index_Down_Wait,

            //////////////////////////////////////////
            /// 수직 이동 시작
            //////////////////////////////////////////
            MagLoader_Z_Start,            
           
            //////////////////////////////////////////
            /// PP Loader 하부
            //////////////////////////////////////////
            PPLoader_Bottom_X,

            //////////////////////////////////////////
            /// 수직 이동 확인
            //////////////////////////////////////////
            MagLoader_Z_Wait,

            //////////////////////////////////////////
            /// 수평 이동 시작
            //////////////////////////////////////////
            Shuttle_X_Start,                       


            //////////////////////////////////////////
            /// 수평 이동 확인
            //////////////////////////////////////////
            Shuttle_X_Wait,



            //////////////////////////////////////////
            /// 홈 종료
            //////////////////////////////////////////
            Home_PostProcess,
            Home_End,

        }
    }
}
