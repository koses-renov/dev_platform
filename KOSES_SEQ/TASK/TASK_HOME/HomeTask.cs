﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.TASK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using KOSES_SEQ.OP;
using static KOSES_SEQ.TASK_HOME.HomeStep;
using static KOSES_SEQ.OP.SysOperator;
using KOSES_SEQ.MANAGER;

namespace KOSES_SEQ.TASK_HOME
{
    public partial class HomeTask
    {
        public HomeTask()
        {
            task.Name = $"{MethodBase.GetCurrentMethod().DeclaringType.Name}";

            step = new HomeStep();
        }

        KoTask task = new KoTask();
        readonly string taskTitle = "INITIALIZATION";

        /// <summary>
        /// Step
        /// </summary>
        #region Step
        HomeStep step = null;
        #endregion

        public KoTask Task
        {
            get { return task; }
        }

        public KoTask.Code Start()
        {
            //Program.baseForm.motorStatusForm.ShowModaless(true);
            ResetUserStop();
            step.Set(Step.Home_Start);

            KoTask.Code taskCode;
            taskCode = task.Start(TaskLoop);
            
            if (taskCode != KoTask.Code.OK)
            {
                return taskCode;
            }
            return KoTask.Code.OK;
        }

        public void Stop()
        {
            task.Stop();
            task.Finish();
        }
        

        private void TaskLog(string msg, Step step)
        {
            string log = string.Format($"[{step}] : {msg}");
           // Program.log.Home(log);
           // Program.baseForm.motorStatusForm.AddLog(log);
            log = null;
        }
        private void HomeError(string errMessage, Step curStep)
        {
            task.Finish();
            //Program.motion.StopAll();
            FRM.Msg.ShowMsg($"HOME :  {curStep}");
            SetEquipState(EquipmentStatus.State.NEED_INITIALIZATION);
            //Program.NoticeStop();
        }

        private void TaskLoop()
        {
            Def.Code code = Def.Code.OK;
            Step curStep = Step.Home_Start;
            int delay = 0;
            double downDistance = 0;
            Def.AttachIdx tempAI;

            while (true)
            {
                ///////////////////////////////////////////////////////
                task.Delay(1);
                if (task.IsFinished)
                {
                    break;
                }

                if (GetUserStop() == true)
                {
                    return;
                }

                curStep = step.Get();

                switch (curStep)
                {
                    //////////////////////////////////////////
                    /// 홈 시작
                    //////////////////////////////////////////
                    #region 홈 시작
                    case Step.Home_Start:
                        TaskLog($"홈 시작", curStep);
                        //Program.SetEquipState(Equipment.State.NOW_INITIALIZING);

                        TaskLog($"오토런 스텝 초기화", curStep);
                        //Program.autoTask.InitStep();

                        TaskLog($"오토런 데이터 초기화", curStep);
                        //Program.autoData.InitData();

                        TaskLog($"오토런 상태 초기화", curStep);
                        //Program.autoData.InitState();

                        step.Set(Step.Home_PreProcess);
                        break;

                    case Step.Home_PreProcess:
                        step.Set(Step.MagLoader_Z_Check);
                        break;
                    #endregion


                    //////////////////////////////////////////
                    /// 홈 시작 전 환경 체크
                    //////////////////////////////////////////
                    #region MagLoader
                    case Step.MagLoader_Z_Check:
                        //if (Program.homeData.MagLoader)
                        //{
                        //    TaskLog($"MagLoader Z 준비", curStep);
                        //    code = Program.autoTask.magLoader.Prepare_Move_Z();
                        //    if (code != Def.Code.OK)
                        //    {
                        //        TaskLog($"MagLoader Z 준비 실패", curStep);
                        //        HomeError(code.ToString(), curStep);
                        //        goto HomeExit;
                        //    }
                        //}
                        //step.Set(Step.MagUnloader_Z_Check);
                        break;
                    #endregion
                    




                    //////////////////////////////////////////
                    /// 마스크 상승
                    //////////////////////////////////////////
                    #region 마스크 상승
                    case Step.Mask_Up:
                        foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
                        {
                            TaskLog($"Mask {si} 상승", curStep);
                            //Program.motion.mask[(int)si].upDown.SetConfirm(Def.UpDown.Up);
                        }
                        step.Set(Step.Mask_Up_Wait);
                        break;
                    #endregion
                    #region 마스크 상승 대기
                    case Step.Mask_Up_Wait:
                        foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
                        {
                            TaskLog($"Mask {si} 상승 대기", curStep);
                            //code = Program.motion.mask[(int)si].upDown.SetConfirm(Def.UpDown.Up);
                            if (code != Def.Code.OK)
                            {
                                TaskLog($"Mask {si} 상승 실패", curStep);
                                HomeError(code.ToString(), curStep);
                                goto HomeExit;
                            }
                        }
                        step.Set(Step.Cylinder_Init);
                        break;
                    #endregion

                    #region 실린더 초기화
                    case Step.Cylinder_Init:
                        // Ld/Uld Rail Close
                        //Program.motion.rail.loader.top.rail.Set(Def.OpenClose.Close);
                        //Program.motion.rail.unloader.top.rail.Set(Def.OpenClose.Close);

                        //step.Set(Step.PPLoader_Z_Start);
                        break;
                    #endregion

                 

                    //////////////////////////////////////////
                    /// 인덱스 준비
                    //////////////////////////////////////////
                    #region 인덱스 하강
                    case Step.Index_Down:
                        //  레일 20mm 올림으로 인해 PP 로더와 간섭 발생. 인덱스 하강한다. 해당 인덱스 1 2 4 5 (배주환 과장님 요청)
                        TaskLog($"Index 1 하강", curStep);
                        //Program.motion.rail.index1.upDown.Set(Def.UpDown.Down);

                        step.Set(Step.Index_Down_Wait);
                        break;
                    #endregion
                    #region 인덱스 하강 대기
                    case Step.Index_Down_Wait:
                        //code = Program.motion.rail.index1.upDown.SetWait(Def.UpDown.Down);
                        if (code != Def.Code.OK)
                        {
                            TaskLog($"Index 1 하강 실패", curStep);
                            HomeError(code.ToString(), curStep);
                            goto HomeExit;
                        }

                        //step.Set(Step.Index_X_Start);
                        break;
                    #endregion



                    //////////////////////////////////////////
                    /// 수직 이동 시작
                    //////////////////////////////////////////
                   


                    //////////////////////////////////////////
                    /// 수직 이동 확인
                    //////////////////////////////////////////
                    



                    //////////////////////////////////////////
                    /// Dispenser Z 축 초기화 이후 오토캘 커버 닫기
                    //////////////////////////////////////////
                   

                    //////////////////////////////////////////
                    /// PP Loader Bottom X
                    /// (PP Loader Picker 충돌 방지를 위해 먼저 초기화)
                    //////////////////////////////////////////
                    


                    //////////////////////////////////////////
                    /// 인덱스 이동 시작 : Pre Pk 개조후 이동 위치 변경
                    //////////////////////////////////////////
                    

                    //////////////////////////////////////////
                    /// 어태치 Y
                    /// (바코드 조명 충돌 방지를 위해 먼저 초기화)
                    //////////////////////////////////////////
                    

                    //////////////////////////////////////////
                    /// 수평 이동 시작
                    //////////////////////////////////////////
                    


                    //////////////////////////////////////////
                    /// 수평 이동 확인
                    //////////////////////////////////////////
                    

                    //////////////////////////////////////////
                    /// 홈 종료
                    //////////////////////////////////////////
                    case Step.Home_PostProcess:
                       
                        step.Set(Step.Home_End);
                        break;

                    case Step.Home_End:
                        TaskLog($"홈 종료", curStep);
                        task.Stop();

                        //Program.autoData.lotEnd.ContiLotEndTop = false;
                        //Program.autoData.lotEnd.ContiLotEndBtm = false;
                        //Program.autoData.lotEnd.ContiLotWorkEnd = false;
                        //Program.homeData.HomeFisrtStart = true;
                        //Program.homeData.HomeCompForLdTopMz = true;
                        //Program.homeData.HomeCompForLdBtmMz = true;
                     
                        SetEquipState(EquipmentStatus.State.READY);                       
                        FRM.Msg.ShowMsg($"HOME FINISHED");
                        goto HomeExit;

                    default:
                        break;
                }

                switch (curStep)
                {
                    case Step.Home_End:
                        break;
                }

                task.Delay(1);
            }

            if (GetUserStop() == true)
            {
                FRM.Msg.ShowMsg($"USER STOP");
                TaskLog($"USER STOP", curStep);
            }

        HomeExit:

            TaskLog($"홈 태스크 종료", curStep);
            //Program.baseForm.GripperOverloadCheck_Stop();
            task.Finish();
            //Program.NoticeStop();
        }
    }
}
