﻿using KOSES_SEQ.DEFINE;
using KOSES_SEQ.MANAGER;
using KOSES_SEQ.TASK_AUTO.LoaderMgz;
using KOSES_SEQ.TASK_DATA;
using System;
using System.Collections.Generic;

namespace KOSES_SEQ.TASK_AUTO
{
    [Serializable]
    public class AutoData
    {
        public AutoData()
        {
            filePath = $"{DefPath.Data}\\AutoData.bin";

            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    AutoData_Attach a = new AutoData_Attach();
            //    attach.Add(a);
            //    a = null;
            //}
           
            LoaderMgz = new AutoData_LoaderMgz();
          

           
            //Create_VcmCarrier();
            //Create_RejectTray();
        }

        public string filePath = string.Empty;

        //public List<AutoData_Attach> attach = new List<AutoData_Attach>();
        //#region Attach
        //public AutoData_Attach Attach1
        //{
        //    get
        //    {
        //        return attach[(int)Def.AttachIdx.A1];
        //    }
        //}
        //public AutoData_Attach Attach2
        //{
        //    get
        //    {
        //        return attach[(int)Def.AttachIdx.A2];
        //    }
        //}
        //public AutoData_Attach Attach3
        //{
        //    get
        //    {
        //        return attach[(int)Def.AttachIdx.A3];
        //    }
        //}
        //#endregion
        

        public AutoData_LoaderMgz LoaderMgz;      

        // 언로더 PP에서 픽업 순서를 정하기 위한 리스트
        // 셔틀에서 마스크 벗길때 리스트에 순서를 기록
        // 언로더 픽업 이후 마지막 리스트를 삭제
        public List<Def.ShuttleIdx> unloadershuttle = new List<Def.ShuttleIdx>();


        #region SetShuttleUnloader
        private object dataLock = new object();
        public void SetShuttleUnloader(Def.ShuttleIdx si)
        {
            
            int i = unloadershuttle.Count;
            if (i > 0)
            {
                if ((unloadershuttle.Count > 5) || (unloadershuttle[i-1] == si)) return;  //6개만 가지고 있어야 함. 마지막 Data와 중복 된 Shuttle Data가 들어가면 안됨.              
            }

            lock (dataLock)
            {
                unloadershuttle.Add(si);
            }
        }
        public void RemoveShuttleUnloader()
        {
            if (unloadershuttle.Count > 0)
            {
                unloadershuttle.RemoveAt(0);
            }             
        }

        public void RemoveCurrShuttle(Def.ShuttleIdx si)
        {
            unloadershuttle.Remove(si);
        }
        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////




        //////////////////////////////////////////////////////////////////////////////
        ///
        /// 캐리어 자재 데이터 
        ///
        /////////////////////////////////////////////////////////////////////////////     

        //public ModuleTrayData vcmCarrier;
        //void Create_VcmCarrier()
        //{
        //    int row = Program.teaching.size.vcmCarrierRow;
        //    int col = Program.teaching.size.vcmCarrierCol;

        //    vcmCarrier = new ModuleTrayData(Def.ModuleType.Vcm, col, row);
        //}

        //////////////////////////////////////////////////////////////////////////////
        ///
        /// 자재 데이터 이동
        ///
        //////////////////////////////////////////////////////////////////////////////

        /*
        public void MoveData_VcmCarrierToPPLoader(int vcmNum, Def.PickerIdx pi)
        {
            ppLoader.module4set.Module[(int)pi].Copy(vcmCarrier.Module[(int)vcmNum]);
            //vcmCarrier.Module[(int)vcmNum].Exist = false;

            ModuleData md = new ModuleData(Def.ModuleType.Vcm, false);
            vcmCarrier.Module[(int)vcmNum].Copy(md);
        }
        */

        //// Ld Carrier -> Pre PP
        //public void MoveData_VcmCarrierToPrePP(int vcmNum, Def.PickerIdx pi)
        //{
        //    ppLoaderCamera.module4set.Module[(int)pi].Copy(vcmCarrier.Module[(int)vcmNum]);
        //    //vcmCarrier.Module[(int)vcmNum].Exist = false;

        //    ModuleData md = new ModuleData(Def.ModuleType.Vcm, false);
        //    vcmCarrier.Module[(int)vcmNum].Copy(md);
        //}

        //// Pre PP -> Buffer(Aligner)
        //public void MoveData_VcmPrePPToBuffer(Def.PickerIdx pi)
        //{
        //    ppLoader.module4setBuffer.Module[(int)pi].Copy(ppLoaderCamera.module4set.Module[(int)pi]);

        //    ModuleData md = new ModuleData(Def.ModuleType.Vcm, false);
        //    ppLoaderCamera.module4set.Module[(int)pi].Copy(md);
        //}




        //public void MoveData_PPLoaderToShuttle(Def.ShuttleIdx si)
        //{
        //    bool b;

        //    foreach (Def.ShuttleSocketIdx ssi in Enum.GetValues(typeof(Def.ShuttleSocketIdx)))
        //    {
        //        b = Program.autoData.GetUseSocket(si, ssi);
        //        if (b == false)
        //        {
        //            continue;
        //        }

        //        shuttle[(int)si].module4set.Module[(int)ssi].Copy(ppLoader.module4set.Module[(int)ssi]);
        //        //ppLoader.module4set.Module[(int)ssi].Exist = false;

        //        ModuleData md = new ModuleData(Def.ModuleType.Vcm, false);
        //        ppLoader.module4set.Module[(int)ssi].Copy(md);
        //    }
        //}

        

        ///// <summary>
        ///// VCM 데이터와 Lens 데이터를 입력받아 조립체 정보를 만든다.
        ///// </summary>
        ///// <param name="vcmModule"></param>
        ///// <param name="lensModule"></param>
        //public ModuleData Assemble(ModuleData vcmModule, ModuleData lensModule)
        //{
        //    ModuleData newData = new ModuleData(Def.ModuleType.Assy);

        //    newData.Exist = true;
        //    newData.vcm = vcmModule.vcm.Clone();
        //    newData.lens = lensModule.lens.Clone();
        //    newData.handlingInfo = vcmModule.handlingInfo.Clone();

        //    // Attach Data 추가
        //    newData.handlingInfo.AttachXCommand = lensModule.handlingInfo.AttachXCommand;
        //    newData.handlingInfo.AttachYCommand = lensModule.handlingInfo.AttachYCommand;
        //    newData.handlingInfo.AttachZCommand = lensModule.handlingInfo.AttachZCommand;
        //    newData.handlingInfo.AttachXEncoder = lensModule.handlingInfo.AttachXEncoder;
        //    newData.handlingInfo.AttachYEncoder = lensModule.handlingInfo.AttachYEncoder;
        //    newData.handlingInfo.AttachZEncoder = lensModule.handlingInfo.AttachZEncoder;
        //    newData.handlingInfo.AttachZTorque = lensModule.handlingInfo.AttachZTorque;

        //    return newData;
        //}

        #region InitData
        public void InitData()
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].module4set.Init();
            //}

            foreach (Def.MgzPos mp in Enum.GetValues(typeof(Def.MgzPos)))
            {
                LoaderMgz.mgz[(int)mp].MakeNew();
                //magUnloader.mag[(int)mp].MakeNew();
            }

            

           // vcmCarrier.MakeNew();
          

            //Program.autoData.curInputCount = 0;
            //Program.autoData.lotEnd.Reset();
        }
        #endregion
        #region InitData_LotEnd
        public void InitData_LotEnd()
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].module4set.Init();
            //}

            foreach (Def.MgzPos mp in Enum.GetValues(typeof(Def.MgzPos)))
            {
                LoaderMgz.mgz[(int)mp].MakeNew();
                //magUnloader.mag[(int)mp].MakeNew();
            }

            //ppLoader.module4set.Init();
           

            //foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
            //{
            //    shuttle[(int)si].module4set.Init();
            //}

          

           // vcmCarrier.MakeNew();
           
           

            //Program.autoData.curInputCount = 0;
            //Program.autoData.lotEnd.Reset();
        }
        #endregion

        #region InitState
        public void InitState()
        {
            //  투입 재개
            //Program.autoData.lens.stacker.NoInputTray = false;

            
            ////  어태치 셔틀 선점 초기화
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].WorkingShuttle = null;
            //}

           // Program.autoData.lens.WorkingAttach = null;

         
            //  매거진 로더 초기화
            LoaderMgz.State = Def.LoaderMgzState.IDLE;

            LoaderMgz.StartFlag = Def.LoaderMgzStart.OFF;
            LoaderMgz.StopFlag = Def.LoaderMgzStop.OFF;
            LoaderMgz.GiveFlag = Def.LoaderMgzGive.OFF;
            LoaderMgz.RecvFlag = Def.LoaderMgzRecv.OFF;
         
            //  레일 초기화
            //rail.loaderElevator.State = Def.ElevatorLoaderState.CARRIER_EMPTY;
            //rail.unloaderElevator.State = Def.ElevatorUnloaderState.WORKING_COMPLETED;
            //rail.unloaderElevator.TopState = Def.ElevatorUnloaderTopState.CARRIER_EMPTY;

            //rail.index1.State = Def.Index_1_State.CARRIER_EMPTY;
          

            //foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
            //{
            //    shuttle[(int)si].State = Def.ShuttleState.PP_LOADER_GO;
            //}
          
        }
        #endregion
        #region InitState_LotEnd
        public void InitState_LotEnd()
        {
            //  투입 재개
            //Program.autoData.lens.stacker.NoInputTray = false;

            //  어태치 셔틀 선점 초기화
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    attach[(int)ai].WorkingShuttle = null;
            //}

            //Program.autoData.lens.WorkingAttach = null;

            //  렌즈 트레이 셔틀 초기화
            //lens.Shuttle_1.State = Def.LensTrayShuttleState.INIT;
            //lens.Shuttle_2.State = Def.LensTrayShuttleState.INIT;

            //  렌즈 트레이 적재기 초기화
            //lens.stacker.State = Def.LensTrayStackerState.IDLE;

            //  매거진 로더 초기화
            LoaderMgz.State = Def.LoaderMgzState.IDLE;

            LoaderMgz.StartFlag = Def.LoaderMgzStart.OFF;
            LoaderMgz.StopFlag = Def.LoaderMgzStop.OFF;
            LoaderMgz.GiveFlag = Def.LoaderMgzGive.OFF;
            LoaderMgz.RecvFlag = Def.LoaderMgzRecv.OFF;

         
            //rail.index1.State = Def.Index_1_State.CARRIER_EMPTY;
           

            //foreach (Def.ShuttleIdx si in Enum.GetValues(typeof(Def.ShuttleIdx)))
            //{
            //    shuttle[(int)si].State = Def.ShuttleState.PP_LOADER_GO;
            //}
        }
        #endregion

        //#region reject
        //public bool IsReject(Def.ShuttleIdx si, Def.ShuttleSocketIdx ssi)
        //{
        //    bool b = Program.autoData.shuttle[(int)si].module4set.IsReject((int)ssi);
        //    return b;
        //}

        //public Def.RejectCode GetRejectCode(Def.ShuttleIdx si, Def.ShuttleSocketIdx ssi)
        //{
        //    Def.RejectCode rc = Program.autoData.shuttle[(int)si].module4set.Module[(int)ssi].Reject;
        //    return rc;
        //}

        //public void SetPPUnloaderRejectCode(Def.PickerIdx pi, Def.RejectCode rc)
        //{
        //    Program.autoData.ppUnloader.module4set.SetReject((int)pi, rc);
        //}
        //#endregion

        /// <summary>
        /// 해당 셔틀의 소켓 스킵 여부를 반환하고 스킵 이유를 참조 값으로 갖는 함수
        /// </summary>
        /// <param name="si">셔틀</param>
        /// <param name="ssi">소켓</param>
        /// <param name="skipReason">스킵 이유</param>
        /// <returns>스킵 여부</returns>
        #region 셔틀 소켓 스킵 여부와 스킵 이유
        public bool GetShuttleSocketSkip(Def.ShuttleIdx si, Def.ShuttleSocketIdx ssi, ref string skipReason)
        {
            bool b;
            Def.RejectCode rc;


            b = GetUseSocket(si, ssi);
            if (b == false)
            {
                skipReason = $"셔틀({si}) 소켓({ssi}) 사용 안함";
                return true;
            }

            ////  자재 유무
            //b = Program.autoData.shuttle[(int)si].module4set.Module[(int)ssi].Exist;
            //if (b == false)
            //{
            //    skipReason = $"셔틀({si}) 소켓({ssi}) 자재 없음";
            //    return true;
            //}

            //  리젝트 자재?
            //rc = GetRejectCode(si, ssi);           
            //if (rc != Def.RejectCode.None)
            //{                
            //    skipReason = $"셔틀({si}) 소켓({ssi}) 리젝트({rc})";
            //    return true;               
            //}

            return false;
        }
        #endregion

        #region 셔틀 소켓 사용 유무와 자재 여부
        public bool GetShuttleSocketExist(Def.ShuttleIdx si, Def.ShuttleSocketIdx ssi)
        {
            bool b;

            // 사용 유무
            b = GetUseSocket(si, ssi);

            //  자재 유무
            //b &= Program.autoData.shuttle[(int)si].module4set.Module[(int)ssi].Exist;

            return b;
        }
        #endregion

        public bool GetUseSocket(Def.ShuttleIdx si, Def.ShuttleSocketIdx ssi)
        {
            bool b;

            //b = Program.teaching.shuttle[(int)si].useSocket_1;
            //switch (ssi)
            //{
            //    case Def.ShuttleSocketIdx.SS1:
            //        b = Program.teaching.shuttle[(int)si].useSocket_1;
            //        break;
            //    case Def.ShuttleSocketIdx.SS2:
            //        b = Program.teaching.shuttle[(int)si].useSocket_2;
            //        break;
            //    case Def.ShuttleSocketIdx.SS3:
            //        b = Program.teaching.shuttle[(int)si].useSocket_3;
            //        break;
            //    case Def.ShuttleSocketIdx.SS4:
            //        b = Program.teaching.shuttle[(int)si].useSocket_4;
            //        break;
            //}

            //return b;
            return b=true;
        }



        public bool IsCycleStop()
        {
            //foreach (Def.AttachIdx ai in Enum.GetValues(typeof(Def.AttachIdx)))
            //{
            //    if (attach[(int)ai].cycleState == Def.CycleState.Run)
            //    {
            //        Program.log.Action($"IsCycleStop() : Def.CycleState.Run attach[{(int)ai}]");
            //        return false;
            //    }
            //}

            if (LoaderMgz.cycleState == Def.CycleState.Run)
            {
                //Program.log.Action($"IsCycleStop() : Def.CycleState.Run magLoader");
                return false;
            }

           

            return true;
        }




        ////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //  Lot
        //  투입 일시 정지
        //  투입 수량 제한
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Lot End
        /// </summary>
        //public LotEnd lotEnd = new LotEnd();

        bool pauseInput = false;
        #region 투입 일시 정지
        object lockPauseInput = new object();
        public bool PauseInput
        {
            get
            {
                bool temp;
                lock (lockPauseInput)
                {
                    temp = pauseInput;
                }
                return temp;
            }
            set
            {
                lock (lockPauseInput)
                {
                    pauseInput = value;
                }
            }
        }
        #endregion


        #region 투입 수량 제한 모드
        public int maxInputCount = 0;
        public int curInputCount = 0;
        public bool IsInputCountLimit()
        {
            if (maxInputCount > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

    }
}
