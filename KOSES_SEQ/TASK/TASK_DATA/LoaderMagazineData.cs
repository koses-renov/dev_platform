﻿using KOSES_SEQ.DEFINE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOSES_SEQ.TASK_DATA
{
    [Serializable]
    public class LoaderMagazineData
    {
        public LoaderMagazineData(int rowCount)
        {
            caCount = rowCount;
            MakeNew();
        }

        int caCount = 0;


        /////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////
        object dataLock = new object();

        bool exist = false;
        #region 매거진 유무
        public bool Exist
        {
            get
            {
                bool temp;
                lock (dataLock)
                {
                    temp = exist;
                }
                return temp;
            }
            set
            {
                lock (dataLock)
                {
                    exist = value;
                }
            }
        }
        #endregion

        List<Def.SlotState> slot = new List<Def.SlotState>();
        #region 매거진내 각 슬롯 상태
        public List<Def.SlotState> Slot
        {
            get
            {
                List<Def.SlotState> dataList = null;
                lock (dataLock)
                {
                    dataList = slot;
                }
                return dataList;
            }
            set
            {
                lock (dataLock)
                {
                    slot = value;
                }
            }
        }
        #endregion
        /////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////



        /// <summary>
        /// 신규 매거진을 만든다.
        /// </summary>
        #region MakeNew
        public void MakeNew()
        {
            slot.Clear();
            for (int i = 0; i < caCount; i++)
            {
                Def.SlotState newSlot = Def.SlotState.Empty;
                slot.Add(newSlot);
            }
        }
        #endregion
        
        /// <summary>
        /// 입력한 캐리어 정보를 현재 캐리어에 복사한다.
        /// </summary>
        /// <param name="srcMag"></param>
        #region Copy
        public void Copy(LoaderMagazineData srcMag)
        {
            for (int i = 0; i < caCount; i++)
            {
                this.slot[i] = srcMag.slot[i];
            }
        }
        #endregion

        #region Clone
        public LoaderMagazineData Clone()
        {
            LoaderMagazineData newData = new LoaderMagazineData(this.caCount);
            newData.Copy(this);
            return newData;
        }
        #endregion


        /// <summary>
        /// 매거진내 비어있는 슬롯 위치를 반환한다.
        /// </summary>
        /// <returns></returns>
        #region GetEmptySlot
        public int GetEmptySlot()
        {
            int pos = 0;
            foreach (Def.SlotState s in Slot)
            {
                if (s == Def.SlotState.Empty)
                {
                    return pos;
                }
                pos++;
            }
            return Def.NotFound;
        }
        #endregion


        /// <summary>
        /// 매거진내 신규 트레이 슬롯 위치를 반환한다.
        /// </summary>
        /// <returns></returns>
        #region GetNewSlot
        public int GetNewSlot()
        {
            int pos = 0;
            foreach (Def.SlotState s in Slot)
            {
                if (s == Def.SlotState.New)
                {
                    return pos;
                }
                pos++;
            }
            return Def.NotFound;
        }
        #endregion


        // 
        #region GetUsedSlot
        public bool GetUsedSlot()
        {
            foreach (Def.SlotState s in Slot)
            {
                if (s != Def.SlotState.Used)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region GetNoSensingSlot
        public bool GetNoSensingSlot()
        {
            foreach (Def.SlotState s in Slot)
            {
                if (s == Def.SlotState.NoSensing)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region GetReverseSlot
        public bool GetNoReverseSlot()
        {
            foreach (Def.SlotState s in Slot)
            {
                if (s == Def.SlotState.Reverse)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}
